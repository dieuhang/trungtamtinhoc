﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TTTH.Common;
using TrungTamTinHoc.Areas.ControlPanel.Models.SlideManagement;
using TrungTamTinHoc.Areas.ControlPanel.Models.SlideManagement.Schema;
using TrungTamTinHoc.Controllers;
using static TTTH.Common.Enums.ConstantsEnum;
using static TTTH.Common.Enums.MessageEnum;
using TTTH.Validate;

namespace TrungTamTinHoc.Areas.ControlPanel.Controllers
{
    /// <summary>
    /// Class chứa các điều hướng liên quan đến quản lý danh sách slide
    /// Author       :   QuyPN - 18/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class SlideManagementController : BaseAdminController
    {
        /// <summary>
        /// Điều hướng đến trang hiển thị danh sách slide nếu là request thông thường.
        /// Trả về table chứa danh sách slide nếu là Ajax.
        /// Điều hướng về trang lỗi nếu có lỗi sảy ra.
        /// Author       :   QuyPN - 18/07/2018 - create
        /// </summary>
        /// <param name="condition">Đối tượng chứa điều kiện tìm kiếm, tạo thành từ query string</param>
        /// <returns>
        /// Trang danh sách slide.
        /// Partial view chứa table danh sách slide.
        /// </returns>
        /// <remarks>
        /// Method: GET
        /// RouterName: ListOfSlide
        /// </remarks>
        public ActionResult ListOfSlide(SlideConditionSearch condition)
        {
            try
            {
                if (Request.IsAjaxRequest())
                {
                    return View("Partial/_tableOfSlide", new ListOfSlideModel().GetListOfSlide(condition));
                }
                return View(new ListOfSlideModel().GetListOfSlide(condition));
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }

        /// <summary>
        /// Xóa các slide theo danh sách id slide được gửi lên.
        /// Author       :   QuyPN - 18/07/2018 - create
        /// </summary>
        /// <param name="ids">Danh sách id các slide sẽ xóa</param>
        /// <returns>Đối tượng chứa thông tin về quá tringh xóa slide</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: DeleteSlides
        /// </remarks>
        public JsonResult DeleteSlides(List<int> ids)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                bool deleted = new ListOfSlideModel().DeleteSlides(ids);
                if (!deleted)
                {
                    response.Code = 201;
                    response.MsgNo = 45;
                }
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Update trạng thái hiển thị trên trang chủ của 1 slide.
        /// Author       :   QuyPN - 18/07/2018 - create
        /// </summary>
        /// <param name="id">id của slide sẽ update</param>
        /// <returns>Đối tượng chứa thông tin về quá trình update</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: UpdateShowSlide
        /// </remarks>
        public JsonResult UpdateShow(int id)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                bool updated = new ListOfSlideModel().UpdateShow(id);
                if (!updated)
                {
                    response.Code = 201;
                    response.MsgNo = 45;
                }
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewCreateSlide()
        {
            try
            {
                return View("SlideMaster");
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }

        public ActionResult ViewEditSlide(string id)
        {
            try
            {
                SlideMaster slideMaster = new SlideMasterModel().LoadSlide(id);
                if (slideMaster.Mode == (int)ModeMaster.Insert)
                {
                    return RedirectToAction("ViewCreateSlide");
                }
                return View("SlideMaster", slideMaster);
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }

        [HttpPost]
        public JsonResult SaveSlide(Slide data)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                if (ModelState.IsValid)
                {
                    response = new SlideMasterModel().SaveSlide(data);
                }
                else
                {
                    response.Code = (int)CodeResponse.NotValidate;
                    response.ListError = ModelState.GetModelErrors();
                }
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateSlideTranslate(Slide_Trans data)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                if (ModelState.IsValid)
                {
                    response = new SlideMasterModel().UpdateSlideTranslate(data);
                }
                else
                {
                    response.Code = (int)CodeResponse.NotValidate;
                    response.ListError = ModelState.GetModelErrors();
                }
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ReferSlideWithLang(int id, string lang)
        {
            try
            {
                return Json(new SlideMasterModel().ReferSlideWithLang(id, lang), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}