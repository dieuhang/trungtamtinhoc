﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TrungTamTinHoc.Areas.Home.Models.Schema;
using TTTH.Common;
using TTTH.DataBase;
using static TTTH.Common.Enums.ConstantsEnum;
using TblContact = TTTH.DataBase.Schema.Contact;

namespace TrungTamTinHoc.Areas.Home.Models
{
    /// <summary>
    /// Class chứa các phương thức liên quan đến việc gửi tin nhắn liên hệ
    /// Author       :   HaLTH - 03/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   Home.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ContactModel
    {
        private DataContext context;
        public ContactModel()
        {
            context = new DataContext();
        }
        /// <summary>
        /// Lưu thông tin của người gửi tin nhắn liên hệ.
        /// Author       :   HaLTH - 03/07/2018 - create
        /// </summary>
        /// <param name="newAccount">Nội dung liên hệ do người dùng cung cấp</param>
        /// <returns>Thông tin về việc gửi liên hệ thành công hay thất bại</returns>
        public ResponseInfo SendMessenger(Contact contain)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo result = new ResponseInfo();
                //Kiểm tra email - họ tên - số điện thoại - nội dung của cùng một người dùng gửi liên hệ đã tồn tại trong DB chưa
                //Nếu cùng một người dùng và cùng nội dung đã gửi rồi thì không lưu vào DB để tránh trùng lặp thông tin
                TblContact emailcontact = context.Contact.FirstOrDefault(x => x.Email == contain.Email && !x.DelFlag);
                TblContact namecontact = context.Contact.FirstOrDefault(x => x.HoTen == contain.HoTen && !x.DelFlag);
                TblContact sodienthoaicontact = context.Contact.FirstOrDefault(x => x.SoDienThoai == contain.SoDienThoai && !x.DelFlag);
                TblContact noidungcontact = context.Contact.FirstOrDefault(x => x.NoiDung == contain.NoiDung && !x.DelFlag);
                if (emailcontact == null || namecontact == null || sodienthoaicontact == null || noidungcontact == null)
                {
                    //Lưu thông tin người dùng gửi liên hệ vào database
                    TblContact contact = new TblContact
                    {
                        HoTen = contain.HoTen,
                        Email = contain.Email,
                        SoDienThoai = contain.SoDienThoai,
                        NoiDung = contain.NoiDung,
                        IdTrangThai = (int)TrangThaiLienHe.LienHeMoi
                    };
                    context.Contact.Add(contact);
                    // Lưu vào CSDL
                    context.SaveChanges();
                }

                transaction.Commit();
                result.Code = 200;
                result.MsgNo = 41;
                return result;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        /// <summary>
        /// Lấy văn bản hiển thị cho vùng "Số điện thoại" trên trang Contact.
        /// Author       :   HaLTH - 21/07/2018 - create
        /// </summary>
        /// <returns>Văn bản "Số điện thoại" có trong DB</returns>
        public string LoadSoDienThoai()
        {
            try
            {
                string lang = Common.GetLang();
                return context.CaiDatHeThong.FirstOrDefault(x => x.Lang == lang && x.Id == (int)OtherEnum.IdSetting
                    && !x.DelFlag).SoDienThoai;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Lấy văn bản hiển thị cho vùng "Email" trên trang Contact.
        /// Author       :   HaLTH - 21/07/2018 - create
        /// </summary>
        /// <returns>Văn bản "Email" có trong DB</returns>
        public string LoadEmail()
        {
            try
            {
                return context.CaiDatHeThong.FirstOrDefault(x => x.Id == (int)OtherEnum.IdSetting
                    && !x.DelFlag).Email;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Lấy văn bản hiển thị cho vùng "Địa chỉ" trên trang Contact.
        /// Author       :   HaLTH - 21/07/2018 - create
        /// </summary>
        /// <returns>Văn bản "Địa chỉ" có trong DB</returns>
        public string LoadDiaChi()
        {
            try
            {
                string lang = Common.GetLang();
                return context.CaiDatHeThong.FirstOrDefault(x => x.Lang == lang && x.Id == (int)OtherEnum.IdSetting
                    && !x.DelFlag).DiaChi;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}