﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrungTamTinHoc.Areas.Home.Models.Schema
{
    /// <summary>
    /// Class chứa các thuộc tính cần thiết lấy từ DB cho việc hiển thị các ảnh trên trang chủ
    /// Author       :   HaLTH - 12/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   Home.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class Photos
    {
        public int Id { set; get; }
        public string Link { set; get; }
        public string Note { set; get; }
    }
}