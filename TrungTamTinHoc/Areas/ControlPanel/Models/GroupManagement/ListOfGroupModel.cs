﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TrungTamTinHoc.Areas.ControlPanel.Models.GroupManagement.Schema;
using TTTH.Common;
using TTTH.DataBase;
using Z.EntityFramework.Plus;
using TblGroup = TTTH.DataBase.Schema.Group;
using TblGroupTrans = TTTH.DataBase.Schema.GroupTrans;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.GroupManagement
{
    /// <summary>
    /// Class dùng để xử lý các hoạt động liên quan đến Group.
    /// Author       :   HangNTD - 02/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ListOfGroupModel
    {
        DataContext context;
        public ListOfGroupModel()
        {
            context = new DataContext();
        }
        /// <summary>
        /// Tìm kiếm các Group theo điều kiện cho trước.
        /// Author       :   HangNTD - 02/08/2018 - create
        /// </summary>
        /// <param name="condition">Đối tượng chứa điều kiện tìm kiếm</param>
        /// <returns>Danh sách các Group đã tìm kiếm được. Exception nếu có lỗi</returns>
        public ListOfGroup GetListOfGroup(GroupConditionSearch condition)
        {
            try
            {
                // Nếu không tồn tại điều kiện tìm kiếm thì khởi tạo giá trị tìm kiếm ban đầu
                if (condition == null)
                {
                    condition = new GroupConditionSearch();
                }
                string lang = Common.GetLang();

                ListOfGroup listOfGroup = new ListOfGroup();
                // Lấy các thông tin dùng để phân trang
                listOfGroup.Paging = new Paging(context.GroupTrans.Count(x =>
                    (condition.KeySearch == null ||
                    (condition.KeySearch != null && (x.GroupName.Contains(condition.KeySearch) ||
                                                   x.MoTa.Contains(condition.KeySearch))))
                    && (condition.TrangThai == null || (condition.TrangThai != null &&
                                                       x.DelFlag == condition.TrangThai.Value))
                    && !x.DelFlag && x.Lang == lang), condition.CurentPage, condition.PageSize);

                // Tìm kiếm và lấy dữ liệu theo trang
                listOfGroup.GroupList = context.GroupTrans.Where(x =>
                    (condition.KeySearch == null ||
                    (condition.KeySearch != null && x.Id != 1 && (x.GroupName.Contains(condition.KeySearch) ||
                                                   x.MoTa.Contains(condition.KeySearch))))
                    && (condition.TrangThai == null || (condition.TrangThai != null &&
                                                       x.DelFlag == condition.TrangThai.Value))
                    && !x.DelFlag && x.Lang == lang).OrderBy(x => x.Id)
                    .Skip((listOfGroup.Paging.CurrentPage - 1) * listOfGroup.Paging.NumberOfRecord)
                    .Take(listOfGroup.Paging.NumberOfRecord).Select(x => new Group
                    {
                        Id = x.Id,
                        GroupName = x.GroupName,
                        MoTa = x.MoTa,
                        IsDefault = x.Group.IsDefault,
                        IsSystem = x.Group.IsSystem
                    }).ToList();
                listOfGroup.Condition = condition;
                return listOfGroup;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// Xóa các Group trong DB.
        /// Author       :   HangNTD - 02/08/2018 - create
        /// </summary>
        /// <param name="ids">Danh sách id của các Group sẽ xóa</param>
        /// <returns>True nếu xóa thành công, False nếu không còn Group được hiển thị trên trang chủ, Excetion nếu có lỗi</returns>
        public bool DeleteGroups(List<int> ids)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                bool result = true;
                if (context.Group.Count(x => !ids.Contains(x.Id) && !x.DelFlag) > 1)
                {
                    context.Group.Where(x => ids.Contains(x.Id) && !x.DelFlag).Update(x => new TblGroup
                    {
                        DelFlag = true
                    });

                    context.GroupTrans.Where(x => ids.Contains(x.Id) && !x.DelFlag).Update(x => new TblGroupTrans
                    {
                        DelFlag = true
                    });
                    context.SaveChanges();
                }
                else
                {
                    result = false;
                }
                transaction.Commit();
                return result;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }
        /// <summary>
        /// Cập nhật trạng thái hiển thị của Group trên trang chủ.
        /// Author       :   HangNTD - 02/08/2018 - create
        /// </summary>
        /// <param name="id">id của Group sẽ được cập nhật trạng thái</param>
        /// <returns>True nếu xóa thành công, False nếu không còn Group được hiển thị trên trang chủ, Excetion nếu có lỗi</returns>
        public bool UpdateShow(int id)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                bool result = true;
                if (context.Group.Count(x => x.Id != id && !x.DelFlag) > 1)
                {
                    context.Group.Where(x => x.Id == id && !x.DelFlag)
                        .Update(x => new TTTH.DataBase.Schema.Group
                        {
                            IsDefault = !x.IsDefault,
                            IsSystem = !x.IsSystem
                        });
                    context.SaveChanges();
                }
                else
                {
                    result = false;
                }
                transaction.Commit();
                return result;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }
    }
}