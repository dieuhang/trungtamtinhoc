﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TrungTamTinHoc.Areas.ControlPanel.Models.PermissionManagement.Schema;
using TrungTamTinHoc.Areas.ControlPanel.Models.SlideManagement.Schema;
using TTTH.Common;
using TTTH.DataBase;
using Z.EntityFramework.Plus;
using Permission = TrungTamTinHoc.Areas.ControlPanel.Models.PermissionManagement.Schema.Permission;
using TblPermission = TTTH.DataBase.Schema.Permission;
using TblManHinh = TTTH.DataBase.Schema.ManHinh;
using TblChucNang = TTTH.DataBase.Schema.ChucNang;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.PermissionManagement
{
    /// <summary>
    /// Class dùng để xử lý các hoạt động liên quan đến permission.
    /// Author       :   AnTM - 13/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ListOfPermissionModel
    {
        private DataContext context;

        public ListOfPermissionModel()
        {
            context = new DataContext();
        }

        /// <summary>
        /// Kiểm tra ID Group gởi lên từ client
        /// Author       :   AnTM - 18/08/2018 - create
        /// </summary>
        /// <param name="idGroup">ID Group từ client gửi lên</param>
        /// <returns>Id group đã tìm kiếm được, nếu không tìm được thì lấy id group đầu tiên trong bảng Group.
        /// Exception nếu có lỗi</returns>
        public int CheckAndGetGroupId(string idGroup, bool isSystem)
        {
            try
            {
                int idGroupLogin = XacThuc.GetAccount().GroupOfAccount.FirstOrDefault(x => !x.DelFlag).IdGroup;
                if (string.IsNullOrEmpty(idGroup))
                {
                    if (isSystem)
                    {
                        return context.Group.FirstOrDefault(x => !x.DelFlag && x.Id != idGroupLogin).Id;
                    }
                    return context.Group.FirstOrDefault(x => !x.DelFlag && !x.IsSystem && !x.IsDefault && x.Id != idGroupLogin).Id;
                }
                else
                {
                    int idGroupSelected = Convert.ToInt32(idGroup);
                    if (isSystem)
                    {
                        if (context.Group.FirstOrDefault(x => !x.DelFlag && x.Id == idGroupSelected && x.Id != idGroupLogin) != null)
                        {
                            return idGroupSelected;
                        }
                    }
                    else
                    {
                        if (context.Group.FirstOrDefault(x => !x.DelFlag && x.Id == idGroupSelected && !x.IsSystem && !x.IsDefault && x.Id != idGroupLogin) != null)
                        {
                            return idGroupSelected;
                        }
                    }
                }
                return -1;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Lấy danh sách các quyền trong từng màn hình theo ID group
        /// Author       :   AnTM - 18/08/2018 - create
        /// </summary>
        /// <param name="idGroup">ID của group cần lấy danh sách quyền</param>
        /// <returns>Danh sách các quyền đã tìm kiếm được. Exception nếu có lỗi</returns>

        public ListOfPermission GetListOfPermissionsInScreen(int idGroup, bool isSystem)
        {
            try
            {
                string lang = Common.GetLang();
                ListOfPermission listOfPermission = new ListOfPermission();

                listOfPermission.IdGroupSelected = idGroup;
                if (isSystem)
                {
                    listOfPermission.ListGroup = context.Group.Where(x => !x.DelFlag && !x.IsSystem).Select(x => new Group()
                    {
                        Id = x.Id,
                        TenGroup = x.GroupTrans.FirstOrDefault(y => !y.DelFlag && lang == y.Lang).GroupName
                    }).ToList();
                }
                else
                {
                    listOfPermission.ListGroup = context.Group.Where(x => !x.DelFlag && !x.IsDefault && !x.IsSystem).Select(x => new Group()
                    {
                        Id = x.Id,
                        TenGroup = x.GroupTrans.FirstOrDefault(y => !y.DelFlag && lang == y.Lang).GroupName
                    }).ToList();
                }

                listOfPermission.cacManHinh = context.ManHinh.Where(x => !x.DelFlag && x.ChucNang.Count > 0)
                    .Select(x => new PermissionsInScreen()
                    {
                        NameScreen = x.ManHinhTrans.FirstOrDefault(y => !y.DelFlag && y.Lang == lang).TenManHinh,
                        CacPermission = context.Permission.Where(y => !y.DelFlag && y.ChucNang.ManHinh.Id == x.Id && y.IdGroup == idGroup).Select(y => new Permission()
                        {
                            TenChucNang = y.ChucNang.ChucNangTrans.FirstOrDefault(z => z.Lang == lang && !z.DelFlag).TenChucMang,
                            Id = y.Id,
                            IsEnable = y.IsEnable,
                            isRequired = y.ChucNang.Required,
                            isToViewIndex = y.ChucNang.ToViewIndex
                        }).ToList()
                    }).ToList();

                return listOfPermission;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Cập nhật trạng thái kích hoạt của permission trên trang chủ.
        /// Author       :   AnTM - 14/08/2018 - create
        /// </summary>
        /// <param name="id">id của permission sẽ được cập nhật trạng thái</param>
        /// <returns>ResponseInfo, Excetion nếu có lỗi</returns>
        public ResponseInfo UpdateEnable(int id)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            ResponseInfo response = new ResponseInfo();
            try
            {
                bool result = true;
                TblPermission permission = context.Permission.FirstOrDefault(x => !x.DelFlag && x.Id == id);

                if (permission != null)
                {
                    TblManHinh manHinh = permission.ChucNang.ManHinh;
                    TblChucNang chucNangToViewIndex = context.ChucNang.FirstOrDefault(x => x.IdScreen == manHinh.Id && x.ToViewIndex);
                    if (permission.IdChucNang != chucNangToViewIndex.Id)
                    {
                        if (!(chucNangToViewIndex.Permission.FirstOrDefault(x => !x.DelFlag && x.IdGroup == permission.IdGroup).IsEnable))
                        {
                            response.Code = 201;
                            response.MsgNo = 59;
                            return response;
                        }
                    }
                    if (!permission.ChucNang.Required)
                    {
                        context.Permission.Where(x => x.Id == id && !x.DelFlag)

                            .Update(x => new TblPermission
                            {
                                IsEnable = !x.IsEnable
                            });
                        context.SaveChanges();
                    }
                    else
                    {
                        response.Code = 201;
                        response.MsgNo = 56;
                        return response;
                    }
                }
                else
                {
                    response.Code = 201;
                    response.MsgNo = 60;
                    return response;
                }
                transaction.Commit();
                return response;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }
    }
}