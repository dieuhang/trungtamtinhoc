﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrungTamTinHoc.Areas.ControlPanel.Models.EmailSubscriptionManagement;
using TrungTamTinHoc.Areas.ControlPanel.Models.EmailSubscriptionManagement.Schema;
using TTTH.Common;
using static TTTH.Common.Enums.ConstantsEnum;
using static TTTH.Common.Enums.MessageEnum;

namespace TrungTamTinHoc.Areas.ControlPanel.Controllers
{
    /// <summary>
    /// Class chứa các điều hướng liên quan đến quản lý danh sách đăng ký theo dõi qua email
    /// Author       :   HaLTH - 19/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class EmailSubscriptionManagementController : Controller
    {
        /// <summary>
        /// Điều hướng đến trang hiển thị danh sách email nếu là request thông thường.
        /// Trả về table chứa danh sách email nếu là Ajax.
        /// Điều hướng về trang lỗi nếu có lỗi sảy ra.
        /// Author       :   HaLTH - 19/07/2018 - create
        /// </summary>
        /// <param name="condition">Đối tượng chứa điều kiện tìm kiếm, tạo thành từ query string</param>
        /// <returns>
        /// Trang danh sách email.
        /// Partial view chứa table danh sách email.
        /// </returns>
        /// <remarks>
        /// Method: GET
        /// RouterName: ListOfEmailSubscription
        /// </remarks>
        public ActionResult ListOfEmailSubscription(EmailSubscriptionConditionSearch condition)
        {
            try
            {
                if (Request.IsAjaxRequest())
                {
                    return View("Partial/_tableOfEmailSubscription", new ListOfEmailSubscriptionModel().GetListOfEmailSubscription(condition));

                }
                return View(new ListOfEmailSubscriptionModel().GetListOfEmailSubscription(condition));
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }

        /// <summary>
        /// Xóa các email theo danh sách id email được gửi lên.
        /// Author       :   HaLTH - 19/07/2018 - create
        /// </summary>
        /// <param name="ids">Danh sách id các email sẽ xóa</param>
        /// <returns>Đối tượng chứa thông tin về quá trình xóa email</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: DeleteEmailSubscriptions
        /// </remarks>
        public JsonResult DeleteEmailSubscriptions(List<int> ids)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                new ListOfEmailSubscriptionModel().DeleteEmailSubscriptions(ids);
                
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}