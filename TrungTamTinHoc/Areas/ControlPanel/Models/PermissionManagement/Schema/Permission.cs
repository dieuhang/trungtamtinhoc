﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.PermissionManagement.Schema
{
    /// <summary>
    /// Class dùng để chứa thông tin của một quyền thực hiện chức năng
    /// Author       :   AnTM - 13/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class Permission
    {
        public int Id { get; set; }

        public string TenChucNang { get; set; }

        public bool isRequired { get; set; }

        public bool isToViewIndex { get; set; }

        public bool IsEnable { get; set; }
    }

    /// <summary>
    /// Class dùng để chứa thông tin của một group
    /// Author       :   AnTM - 13/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class Group
    {
        public int Id { get; set; }

        public string TenGroup { get; set; }
    }
}