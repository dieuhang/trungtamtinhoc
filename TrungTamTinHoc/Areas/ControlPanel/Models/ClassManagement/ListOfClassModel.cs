﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TrungTamTinHoc.Areas.ControlPanel.Models.ClassManagement.Schema;
using TTTH.Common;
using TTTH.DataBase;
using TTTH.DataBase.Schema;
using Z.EntityFramework.Plus;
using KhoaHoc = TrungTamTinHoc.Areas.ControlPanel.Models.ClassManagement.Schema.KhoaHoc;


namespace TrungTamTinHoc.Areas.ControlPanel.Models.ClassManagement
{
    /// <summary>
    /// Class dùng để xử lý các hoạt động liên quan đến lớp học.
    /// Author       :   HaLTH - 01/09/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ListOfClassModel
    {
        private DataContext context;

        public ListOfClassModel()
        {
            context = new DataContext();
        }

        /// <summary>
        /// Tìm kiếm các lớp học theo điều kiện cho trước.
        /// Author       :   HaLTH - 01/09/2018 - create
        /// </summary>
        /// <param name="condition">Đối tượng chứa điều kiện tìm kiếm</param>
        /// <returns>Danh sách các lớp học đã tìm kiếm được. Exception nếu có lỗi</returns>
        public ListOfClass GetListOfClass(ClassConditionSearch condition)
        {
            try
            {
                // Nếu không tồn tại điều kiện tìm kiếm thì khởi tạo giá trị tìm kiếm ban đầu
                if (condition == null)
                {
                    condition = new ClassConditionSearch();
                }
                string lang = Common.GetLang();
                ListOfClass listOfClass = new ListOfClass();

                // Lấy các thông tin dùng để phân trang
                listOfClass.Paging = new Paging(context.LopHocTrans.Count(x =>
                    (condition.KeySearch == null ||
                     (condition.KeySearch != null && (x.TenLop.Contains(condition.KeySearch))))
                    && (condition.SoLuongHocVien == 0 ||
                        (condition.SoLuongHocVien != 0 && x.LopHoc.SoLuongHocVien == condition.SoLuongHocVien))
                    && (condition.StartTime == null && condition.EndTime == null ||
                        (condition.StartTime == null && x.LopHoc.ThoiGianBatDau <= condition.EndTime) ||
                        (condition.EndTime == null && x.LopHoc.ThoiGianBatDau >= condition.StartTime) ||
                        (condition.StartTime != null && condition.EndTime != null &&
                         x.LopHoc.ThoiGianBatDau >= condition.StartTime && x.LopHoc.ThoiGianBatDau <= condition.EndTime))
                    && !x.DelFlag && x.Lang == lang), condition.CurrentPage, condition.PageSize);
                // Tìm kiếm và lấy dữ liệu theo trang
                listOfClass.ClassList = context.LopHocTrans.Where(x =>
                        (condition.KeySearch == null ||
                     (condition.KeySearch != null && (x.TenLop.Contains(condition.KeySearch))))
                    && (condition.SoLuongHocVien == 0 ||
                        (condition.SoLuongHocVien != 0 && x.LopHoc.SoLuongHocVien == condition.SoLuongHocVien))
                    && (condition.StartTime == null && condition.EndTime == null ||
                        (condition.StartTime == null && x.LopHoc.ThoiGianBatDau <= condition.EndTime) ||
                        (condition.EndTime == null && x.LopHoc.ThoiGianBatDau >= condition.StartTime) ||
                        (condition.StartTime != null && condition.EndTime != null &&
                         x.LopHoc.ThoiGianBatDau >= condition.StartTime && x.LopHoc.ThoiGianBatDau <= condition.EndTime))
                        && !x.DelFlag && x.Lang == lang).OrderBy(x => x.Id)
                    .Skip((listOfClass.Paging.CurrentPage - 1) * listOfClass.Paging.NumberOfRecord)
                    .Take(listOfClass.Paging.NumberOfRecord).Select(x => new Schema.Class
                    {
                        Id = x.Id,
                        TenLop = x.TenLop,
                        SoTiet = x.LopHoc.SoTiet,
                        ThoiGianMoiTiet = x.LopHoc.ThoiGianMoiTiet,
                        SoLuongHocVien = x.LopHoc.SoLuongHocVien,
                        ThoiGianBatDau = x.LopHoc.ThoiGianBatDau,
                        ThoiGianKetThuc = x.LopHoc.ThoiGianKetThuc
                    }).ToList();
                listOfClass.Condition = condition;
                listOfClass.KhoaHocList = context.KhoaHocTrans.Where(x => x.Lang == lang && !x.DelFlag)
                    .Select(x => new KhoaHoc
                    {
                        IdKhoaHoc = x.Id,
                        TenKhoaHoc = x.TenKhoaHoc
                    }).ToList();
                return listOfClass;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Xóa các lớp học trong DB.
        /// Author       :   HaLTH - 01/09/2018 - create
        /// </summary>
        /// <param name="ids">Danh sách id của các lớp học sẽ xóa</param>
        /// <returns>True nếu xóa thành công, False nếu không còn khóa học được hiển thị trên trang chủ, Excetion nếu có lỗi</returns>
        public bool DeleteClasses (List<int> ids)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                bool result = true;
                if (context.LopHoc.Count(x => !ids.Contains(x.Id) && !x.DelFlag) >= 3)
                {
                    context.LopHoc.Where(x => ids.Contains(x.Id) && !x.DelFlag).Update(x => new LopHoc
                    {
                        DelFlag = true
                    });

                    context.LopHocTrans.Where(x => ids.Contains(x.Id) && !x.DelFlag).Update(x => new LopHocTrans
                    {
                        DelFlag = true
                    });

                    context.SaveChanges();
                }
                else
                {
                    result = false;
                }
                transaction.Commit();
                return result;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }
    }
}