﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.NewsManagement.Schema
{
    /// <summary>
    /// Class dùng để chứa thông tin của một tin tức
    /// Author       :   HangNTD - 20/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class News
    {
        public int Id { set; get; }

        [Required]
        [StringLength(255)]
        public string BeautyId { get; set; }

        [Required]
        [StringLength(255)]
        public string AnhChinh { get; set; }
        public HttpPostedFileBase FileImg0 { set; get; }

        [Required]
        [StringLength(255)]
        public string AnhMinhHoa { get; set; }
        public HttpPostedFileBase FileImg1 { set; get; }

        [Required]
        [StringLength(100)]
        public string TieuDe { get; set; }

        [Required]
        [StringLength(1000)]
        public string TomTat { get; set; }

        [Required]
        public string NoiDung { get; set; }
        public DateTime? NgayDang { get; set; }
    }

    /// <summary>
    /// Class dùng để chứa thông tin của một News_Trans
    /// Author       :   HangNTD - 12/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class News_Trans
    {
        public int Id { set; get; }

        public string LanguageTrans { set; get; }

        [Required]
        [StringLength(100)]
        public string TieuDe { get; set; }

        [Required]
        [StringLength(1000)]
        public string TomTat { get; set; }

        [Required]
        [StringLength(2000)]
        public string NoiDung { get; set; }
    }

    /// <summary>
    /// Class dùng để chứa thông tin tìm kiếm của danh sách tin tức
    /// Author       :   HangNTD - 20/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class NewsConditionSearch
    {
        public string KeySearch { set; get; }
        public bool? TrangThai { set; get; }
        public int CurentPage { set; get; }
        public int PageSize { set; get; }
        public NewsConditionSearch()
        {
            this.KeySearch = "";
            this.TrangThai = null;
            this.PageSize = 10;
            this.CurentPage = 1;
        }
    }
}