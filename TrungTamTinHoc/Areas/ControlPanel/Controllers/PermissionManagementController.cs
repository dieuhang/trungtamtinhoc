﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrungTamTinHoc.Areas.ControlPanel.Models.PermissionManagement;
using TrungTamTinHoc.Areas.ControlPanel.Models.PermissionManagement.Schema;
using TTTH.Common;
using TTTH.Common.Enums;

namespace TrungTamTinHoc.Areas.ControlPanel.Controllers
{
    /// <summary>
    /// Class chứa các điều hướng liên quan đến quản lý phân quyền
    /// Author       :   AnTM - 13/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class PermissionManagementController : Controller
    {
        /// <summary>
        /// Điều hướng đến trang hiển thị danh sách các quyền nếu là request thông thường.
        /// Trả về table chứa danh sách quyền nếu là Ajax.
        /// Điều hướng về trang lỗi nếu có lỗi sảy ra.
        /// Author       :   AnTM - 13/08/2018 - create
        /// </summary>
        /// <param name="idGroupFromClient">ID Group gởi lên từ client</param>
        /// <returns>
        /// Trang danh sách các quyền trong màn hình tương ứng với group.
        /// Partial view chứa table danh sách quyền.
        /// </returns>
        /// <remarks>
        /// Method: GET
        /// RouterName: ListOfPermission
        /// </remarks>
        public ActionResult ListOfPermission(string idGroupFromClient)
        {
            try
            {
                bool isSystem = false;
                int idGroupAccount = XacThuc.GetAccount().GroupOfAccount.FirstOrDefault(x => !x.DelFlag).IdGroup;
                int idLTV = Common.GetIdGroupOfSystem();
                if (idGroupAccount == idLTV)
                {
                    isSystem = true;
                }
                int idGroup = new ListOfPermissionModel().CheckAndGetGroupId(idGroupFromClient, isSystem);
                if (idGroup == -1)
                {
                    return RedirectToAction("Error", "Error", new { area = "error", error = "Group not exist" });
                }
                if (Request.IsAjaxRequest())
                {
                    return View("Partial/_tableOfPermission", new ListOfPermissionModel().GetListOfPermissionsInScreen(idGroup, isSystem));
                }
                return View(new ListOfPermissionModel().GetListOfPermissionsInScreen(idGroup, isSystem));
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }

        /// <summary>
        /// Update trạng thái kích hoạt của một quyền
        /// Author       :   AnTM - 13/08/2018 - create
        /// </summary>
        /// <param name="id">id của permission sẽ update</param>
        /// <returns>Đối tượng chứa thông tin về quá trình update</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: UpdateEnablePermission
        /// </remarks>
        public JsonResult UpdateEnable(int id)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                response = new ListOfPermissionModel().UpdateEnable(id);
            }
            catch (Exception e)
            {
                response.Code = (int)ConstantsEnum.CodeResponse.ServerError;
                response.MsgNo = (int)MessageEnum.MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}