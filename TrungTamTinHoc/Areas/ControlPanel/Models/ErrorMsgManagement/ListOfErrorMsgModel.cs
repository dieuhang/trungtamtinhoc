﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Hosting;
using TrungTamTinHoc.Areas.ControlPanel.Models.ErrorMsgManagement.Schema;
using TTTH.Common;
using TTTH.DataBase;
using Z.EntityFramework.Plus;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.ErrorMsgMagement
{
    public class ListOfErrorMsgModel
    {
        DataContext context;
        public ListOfErrorMsgModel()
        {
            context = new DataContext();
        }

        public ListOfErrorMsg GetListOfErrorMsg(ContactConditionSearch condition)
        {
            try
            {
                if (condition == null)
                {
                    condition = new ContactConditionSearch();
                }
                string lang = Common.GetLang();

                ListOfErrorMsg listOfErrorMsg = new ListOfErrorMsg();
                listOfErrorMsg.Paging = new Paging(context.ErrorMsg.Count(x =>
                       (condition.SearchTitle == null || (condition.SearchTitle != null && x.Title.Contains(condition.SearchTitle)))
                    && (condition.Type == 0 || (condition.Type != 0 && x.Type == condition.Type))
                    && x.Lang == lang
                    && !x.DelFlag), condition.CurrentPage, condition.PageSize);

                listOfErrorMsg.ErrorMsgList = context.ErrorMsg.Where(x =>
                       (condition.SearchTitle == null || (condition.SearchTitle != null && x.Title.Contains(condition.SearchTitle)))
                    && (condition.Type == 0 || (condition.Type != 0 && x.Type == condition.Type))
                    && x.Lang == lang
                    && !x.DelFlag).OrderBy(x => x.Id)
                    .Skip((listOfErrorMsg.Paging.CurrentPage - 1) * listOfErrorMsg.Paging.NumberOfRecord)
                    .Take(listOfErrorMsg.Paging.NumberOfRecord).Select(x => new ErrorMsg
                    {
                        Id = x.Id,
                        Title = x.Title,
                        Msg = x.Msg,
                        Type = x.Type
                    }).ToList();
                listOfErrorMsg.Condition = condition;

                return listOfErrorMsg;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Cập nhật type của error trên db.
        /// Author       :   HoangNM - 03/08/2018 - create
        /// </summary>
        /// <param name="id">id của errorMgs sẽ được cập nhật type</param>
        /// <param name="Type">type của error vừa được cập nhật</param>
        public void UpdateType(int id, int Type)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                context.ErrorMsg.Where(x => x.Id == id && !x.DelFlag)
                .Update(x => new TTTH.DataBase.Schema.ErrorMsg
                {
                    Type = Type
                });
                context.SaveChanges();
                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        /// <summary>
        /// Xóa các error trong DB.
        /// Author       :   HoangNM - 03/08/2018 - create
        /// </summary>
        /// <param name="ids">Danh sách id của các error sẽ xóa</param>
        public void DeleteErrorMsg(List<int> ids)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                context.ErrorMsg.Where(x => ids.Contains(x.Id)).Delete();
                context.SaveChanges();
                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }
        /// <summary>
        /// dùng để lấy ra danh sách lỗi theo ngôn ngữ
        /// Author       :   HoangNM - 24/08/2018 - create
        /// </summary>
        /// <param name="lang">ngôn ngữ đang sử dụng</param>
        /// 
        private List<ErrorMsg> GetListErrorLang(string lang)
        {
            return context.ErrorMsg.Where(x => x.Lang == lang && !x.DelFlag).Select(x => new ErrorMsg
            {
                Id = x.Id,
                Title = x.Title,
                Msg = x.Msg,
                Type = x.Type
            }).ToList();
        }

        /// <summary>
        /// dùng để ghi các messenge vào file theo ngôn ngữ
        /// Author       :   HoangNM - 24/08/2018 - create
        /// </summary>
        /// <param name="listError">danh sách lỗi theo ngôn ngữ </param>
        /// 
        private void WriteFileMessage(StreamWriter sWriter , List<ErrorMsg> listError)
        {
            foreach (ErrorMsg erorrMsg in listError)
            {
                sWriter.WriteLine("        '" + erorrMsg.Id + "': '" + erorrMsg.Msg + "',");
            }
            sWriter.WriteLine("    },");
        }

        /// <summary>
        /// dùng để ghi các title vào file theo ngôn ngữ
        /// Author       :   HoangNM - 24/08/2018 - create
        /// </summary>
        /// <param name="listError">danh sách lỗi theo ngôn ngữ </param>
        /// 
        private void WriteFileTitle(StreamWriter sWriter, List<ErrorMsg> listError)
        {
            foreach (ErrorMsg erorrMsg in listError)
            {
                sWriter.WriteLine("        '" + erorrMsg.Id + "': '" + erorrMsg.Title + "',");
            }
            sWriter.WriteLine("    },");
        }

        /// <summary>
        /// update file js vào file.
        /// Author       :   HoangNM - 03/08/2018 - create
        /// </summary>
        public ResponseInfo UpdateFile()
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo response = new ResponseInfo();
                string filePath = HostingEnvironment.MapPath("~/public/js/common/message.js");
                List<ErrorMsg> listErrorVi = GetListErrorLang("vi");
                List<ErrorMsg> listErrorEn = GetListErrorLang("en");

                if (File.Exists(filePath))
                {
                    FileStream fs = new FileStream(filePath, FileMode.Create);
                    StreamWriter sWriter = new StreamWriter(fs, Encoding.UTF8);
                    List<TypeError> listTypeError = new Common().getTypeErrors();
                    sWriter.WriteLine("var _text = {");
                    sWriter.WriteLine("    'vi':{");
                    WriteFileMessage(sWriter, listErrorVi);
                    
                    sWriter.WriteLine("    'en':{");
                    WriteFileMessage(sWriter, listErrorEn);
                    
                    sWriter.WriteLine("};");
                    foreach(TypeError typeErrorMgs in listTypeError)
                    {
                        sWriter.WriteLine("//"+ typeErrorMgs.type+": "+ typeErrorMgs.typeMsg);
                    }
                    sWriter.WriteLine("var _typeMsg = {");
                    foreach (ErrorMsg erorrMsg in listErrorVi)
                    {
                        sWriter.WriteLine("    '" + erorrMsg.Id + "': '" + erorrMsg.Type + "',");
                    }
                    sWriter.WriteLine("};");
                    sWriter.WriteLine("var _title = {");
                    sWriter.WriteLine("'vi': {");

                    WriteFileTitle(sWriter, listErrorVi);
                    
                    sWriter.WriteLine("'en': {");

                    WriteFileTitle(sWriter, listErrorEn);

                    sWriter.WriteLine("};");
                    sWriter.WriteLine("var msg_required = _text[getLang()][1];");
                    sWriter.Flush();
                    fs.Close();
                }
                return response;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }
    }
}