﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace TrungTamTinHoc
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Home",
                url: "",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new string[] { "MvcNangCao.Areas.Home.Controllers" }
            ).DataTokens.Add("area", "home");

            routes.MapRoute(
                name: "About",
                url: "about",
                defaults: new { controller = "Home", action = "About", id = UrlParameter.Optional },
                namespaces: new string[] { "MvcNangCao.Areas.Home.Controllers" }
            ).DataTokens.Add("area", "home");

            routes.MapRoute(
                name: "Contact",
                url: "contact",
                defaults: new { controller = "Contact", action = "Contact", id = UrlParameter.Optional },
                namespaces: new string[] { "MvcNangCao.Areas.Home.Controllers" }
            ).DataTokens.Add("area", "home");

            routes.MapRoute(
                name: "UserLogin",
                url: "login",
                defaults: new { controller = "Login", action = "Index", id = UrlParameter.Optional },
                namespaces: new string[] { "MvcNangCao.Areas.Home.Controllers" }
            ).DataTokens.Add("area", "home");

            routes.MapRoute(
                name: "GuideLine",
                url: "guide-line",
                defaults: new { controller = "GuideLine", action = "GuideLine", id = UrlParameter.Optional },
                namespaces: new string[] { "MvcNangCao.Areas.Home.Controllers" }
            ).DataTokens.Add("area", "home");

            routes.MapRoute(
                name: "TermsConditions",
                url: "terms-conditions",
                defaults: new { controller = "GuideLine", action = "TermsConditions", id = UrlParameter.Optional },
                namespaces: new string[] { "MvcNangCao.Areas.Home.Controllers" }
            ).DataTokens.Add("area", "home");

            routes.MapRoute(
                name: "PrivacyPolicy",
                url: "privacy-policy",
                defaults: new { controller = "GuideLine", action = "PrivacyPolicy", id = UrlParameter.Optional },
                namespaces: new string[] { "MvcNangCao.Areas.Home.Controllers" }
            ).DataTokens.Add("area", "home");

            routes.MapRoute(
                name: "DangKyTaiKhoan",
                url: "dang-ky-tai-khoan",
                defaults: new { controller = "RegisterAccount", action = "Register", id = UrlParameter.Optional },
                namespaces: new string[] { "MvcNangCao.Areas.Home.Controllers" }
            ).DataTokens.Add("area", "home");

            routes.MapRoute(
                name: "TaoTaiKhoanThanhCong",
                url: "dang-ky-thanh-cong",
                defaults: new { controller = "RegisterAccount", action = "CreateAccountSuccess", id = UrlParameter.Optional },
                namespaces: new string[] { "MvcNangCao.Areas.Home.Controllers" }
            ).DataTokens.Add("area", "home");

            routes.MapRoute(
                name: "KichHoatTaiKhoan",
                url: "kich-hoat-tai-khoan",
                defaults: new { controller = "RegisterAccount", action = "AciveAccount", id = UrlParameter.Optional },
                namespaces: new string[] { "MvcNangCao.Areas.Home.Controllers" }
            ).DataTokens.Add("area", "home");

            routes.MapRoute(
                 name: "DanhSachKhoaHoc",
                 url: "danh-sach-khoa-hoc",
                 defaults: new { controller = "KhoaHoc", action = "DanhSachKhoaHoc", id = UrlParameter.Optional },
                 namespaces: new string[] { "MvcNangCao.Areas.Home.Controllers" }
             ).DataTokens.Add("area", "course");

            routes.MapRoute(
                name: "ChiTietKhoaHoc",
                url: "khoa-hoc/{id}",
                defaults: new { controller = "KhoaHoc", action = "ChiTietKhoaHoc", id = UrlParameter.Optional },
                namespaces: new string[] { "MvcNangCao.Areas.Home.Controllers" }
            ).DataTokens.Add("area", "course");

            routes.MapRoute(
               name: "DanhSachTinTuc",
               url: "bai-viet/danh-sach-bai-viet",
               defaults: new { controller = "TinTuc", action = "TinTuc", id = UrlParameter.Optional },
               namespaces: new string[] { "MvcNangCao.Areas.Home.Controllers" }
           ).DataTokens.Add("area", "home");

            routes.MapRoute(
                name: "ChiTietTinTuc",
                url: "bai-viet/{id}",
                defaults: new { controller = "TinTuc", action = "ChiTietTinTuc", id = UrlParameter.Optional },
                namespaces: new string[] { "MvcNangCao.Areas.Home.Controllers" }
            ).DataTokens.Add("area", "home");
            routes.MapRoute(
                name: "DangKyKhoaHoc",
                url: "dang-ky-khoa-hoc/{id}",
                defaults: new { controller = "DangKyKhoaHoc", action = "DangKyHoc", id = UrlParameter.Optional },
                namespaces: new string[] { "MvcNangCao.Areas.Home.Controllers" }
            ).DataTokens.Add("area", "course");
            routes.MapRoute(
                name: "DangKyKhoaHocNotID",
                url: "dang-ky-khoa-hoc",
                defaults: new { controller = "DangKyKhoaHoc", action = "DangKyHoc", id = UrlParameter.Optional },
                namespaces: new string[] { "MvcNangCao.Areas.Home.Controllers" }
            ).DataTokens.Add("area", "course");
            routes.MapRoute(
               name: "DangKyTheoDoi",
               url: "dang-ky-theo-doi",
               defaults: new { controller = "Home", action = "SubscribeEmail", id = UrlParameter.Optional },
               namespaces: new string[] { "MvcNangCao.Areas.Home.Controllers" }
           ).DataTokens.Add("area", "home");

            routes.MapRoute(
                name: "ThongTinCaNhan",
                url: "profile",
                defaults: new { controller = "Information", action = "Information", id = UrlParameter.Optional },
                namespaces: new string[] { "MvcNangCao.Areas.Home.Controllers" }
            ).DataTokens.Add("area", "Information");

            routes.MapRoute(
                name: "KichHoatEmail",
                url: "kich-hoat-email",
                defaults: new { controller = "Information", action = "AciveEmail", id = UrlParameter.Optional },
                namespaces: new string[] { "MvcNangCao.Areas.Home.Controllers" }
            ).DataTokens.Add("area", "Information");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
