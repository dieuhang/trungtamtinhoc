﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TTTH.Common;
using static TTTH.Common.Enums.ConstantsEnum;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.ErrorMsgManagement.Schema
{
    /// <summary>
    /// Class dùng để chứa thông tin của của ErrorMgs,dùng để thêm hoặc sửa
    /// Author       :   HoangNM - 13/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ErrorMsgMaster
    {
        public ErrorMsg ErrorMsgInfo { set; get; }
        public ErrorMsg ErrorMsgTrans { set; get; }
        public List<Lang> Language { set; get; }
        public int Mode { set; get; }
        public ErrorMsgMaster()
        {
            Mode = (int)ModeMaster.Insert;
            ErrorMsgInfo = null;
            ErrorMsgTrans = null;
            Language = new List<Lang>();
            
        }
    }
}