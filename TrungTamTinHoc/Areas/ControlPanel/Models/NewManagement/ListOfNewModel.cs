﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TrungTamTinHoc.Areas.ControlPanel.Models.NewsManagement.Schema;
using TTTH.Common;
using TTTH.DataBase;
using Z.EntityFramework.Plus;
using TblTinTuc = TTTH.DataBase.Schema.TinTuc;
using TblTinTucTrans = TTTH.DataBase.Schema.TinTucTrans;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.NewsManagement
{
    public class ListOfNewsModel
    {
        DataContext context;
        public ListOfNewsModel()
        {
            context = new DataContext();
        }
        /// <summary>
        /// Tìm kiếm các tin tức theo điều kiện cho trước.
        /// Author       :   HangNTD - 20/07/2018 - create
        /// </summary>
        /// <param name="condition">Đối tượng chứa điều kiện tìm kiếm</param>
        /// <returns>Danh sách các tin tức đã tìm kiếm được. Exception nếu có lỗi</returns>
        public ListOfNews GetListOfNews(NewsConditionSearch condition)
        {
            try
            {
                // Nếu không tồn tại điều kiện tìm kiếm thì khởi tạo giá trị tìm kiếm ban đầu
                if (condition == null)
                {
                    condition = new NewsConditionSearch();
                }
                string lang = Common.GetLang();

                ListOfNews listOfNews = new ListOfNews();
                // Lấy các thông tin dùng để phân trang
                listOfNews.Paging = new Paging(context.TinTucTrans.Count(x =>
                    (condition.KeySearch == null ||
                    (condition.KeySearch != null && (x.TieuDe.Contains(condition.KeySearch) ||
                                                   x.NoiDung .Contains(condition.KeySearch))))
                    && (condition.TrangThai == null || (condition.TrangThai != null &&
                                                       x.DelFlag == condition.TrangThai.Value))
                    && !x.DelFlag && x.Lang == lang), condition.CurentPage, condition.PageSize);

                // Tìm kiếm và lấy dữ liệu theo trang
                listOfNews.TinTucList = context.TinTucTrans.Where(x =>
                    (condition.KeySearch == null ||
                    (condition.KeySearch != null && (x.TieuDe.Contains(condition.KeySearch) ||
                                                   x.NoiDung.Contains(condition.KeySearch))))
                    && (condition.TrangThai == null || (condition.TrangThai != null &&
                                                       x.DelFlag == condition.TrangThai.Value))
                    && !x.DelFlag && x.Lang == lang).OrderBy(x => x.Id)
                    .Skip((listOfNews.Paging.CurrentPage - 1) * listOfNews.Paging.NumberOfRecord)
                    .Take(listOfNews.Paging.NumberOfRecord).Select(x => new News
                    {
                        Id = x.Id,
                        TieuDe = x.TieuDe,
                        BeautyId=x.TinTuc.BeautyId,
                        TomTat=x.TomTat,
                        NgayDang=x.TinTuc.Created_at,
                        AnhChinh= x.TinTuc.AnhChinh,
                        AnhMinhHoa=x.TinTuc.AnhMinhHoa
                    }).ToList();
                listOfNews.Condition = condition;
                return listOfNews;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Xóa các New trong DB.
        /// Author       :   HangNTD - 25/07/2018 - create
        /// </summary>
        /// <param name="ids">Danh sách id của các New sẽ xóa</param>
        /// <returns>True nếu xóa thành công, False nếu không còn New được hiển thị trên trang chủ, Excetion nếu có lỗi</returns>
        public bool DeleteNews(List<int> ids)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                bool result = true;
                if (context.TinTuc.Count(x => !ids.Contains(x.Id) && !x.DelFlag) > 1)
                {
                    List<string> fileWillDel1 = context.TinTuc.Where(x => ids.Contains(x.Id)).Select(x => x.AnhChinh).ToList();
                    List<string> fileWillDel2 = context.TinTuc.Where(x => ids.Contains(x.Id)).Select(x => x.AnhMinhHoa).ToList();
                    context.TinTuc.Where(x => ids.Contains(x.Id) && !x.DelFlag).Update(x => new TblTinTuc
                    {
                        DelFlag = true
                    });

                    context.TinTucTrans.Where(x => ids.Contains(x.Id) && !x.DelFlag).Update(x => new TblTinTucTrans
                    {
                        DelFlag = true
                    });
                    context.SaveChanges();
                }
                else
                {
                    result = false;
                }
                transaction.Commit();
                return result;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        /// <summary>
        /// Cập nhật trạng thái hiển thị của tin tức trên trang chủ.
        /// Author       :   HangNTD - 25/07/2018 - create
        /// </summary>
        /// <param name="id">id của tin tức sẽ được cập nhật trạng thái</param>
        /// <returns>True nếu xóa thành công, False nếu không còn tin tức được hiển thị trên trang chủ, Excetion nếu có lỗi</returns>
        public bool UpdateShow(int id)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                bool result = true;
                if (context.Slide.Count(x => x.HienThi && x.Id != id && !x.DelFlag) > 1)
                {
                    context.Slide.Where(x => x.Id == id && !x.DelFlag)
                        .Update(x => new TTTH.DataBase.Schema.Slide
                        {
                            HienThi = !x.HienThi
                        });
                    context.SaveChanges();
                }
                else
                {
                    result = false;
                }
                transaction.Commit();
                return result;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }
    }
}