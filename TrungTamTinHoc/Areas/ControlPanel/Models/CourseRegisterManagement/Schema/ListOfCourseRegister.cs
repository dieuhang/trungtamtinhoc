﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TTTH.Common;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.CourseRegisterManagement.Schema
{
    /// <summary>
    /// Class chứa tất cả dữ liệu cho việc hiển thị list các đăng ký học lên table quản lý ở trang admin.
    /// Author       :   AnTM - 07/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ListOfCourseRegister
    {
        public List<CourseRegister> CourseRegisterList { get; set; }
        public Paging Paging { get; set; }
        public CourseRegisterConditionSearch Condition { get; set; }
        public List<TrangThaiDangKy> cacTrangThaiDangKy { get; set; }
        public List<KhoaHocHienThi> cacKhoaHoc { get; set; }
        public List<LopHocHienThi> cacLopHoc { get; set; }

        public ListOfCourseRegister()
        {
            this.CourseRegisterList = new List<CourseRegister>();
            this.Condition = new CourseRegisterConditionSearch();
            this.Paging = new Paging();
        }
    }
    /// <summary>
    /// Class chứa các thuộc tính của 1 khóa học lấy từ DB cho việc hiển thị danh sách các khóa học để người dùng chọn để tìm kiếm ở control panel
    /// Author       :   AnTM - 05/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class KhoaHocHienThi
    {
        public int IDKhoaHoc { get; set; }
        public string TenKhoaHoc { set; get; }
    }

    /// <summary>
    /// Class chứa các thuộc tính của 1 lớp học lấy từ DB cho việc hiển thị danh sách các lớp học để người dùng chọn để tìm kiếm ở control panel
    /// Author       :   AnTM - 05/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class LopHocHienThi
    {
        public int IDLopHoc { get; set; }
        public string TenLopHoc { set; get; }
    }
}