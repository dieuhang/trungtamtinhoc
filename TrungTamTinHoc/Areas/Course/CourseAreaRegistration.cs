﻿using System.Web.Mvc;

namespace TrungTamTinHoc.Areas.Course
{
    public class CourseAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Course";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "getClassesByIdCourse",
                "course/get-classes-by-id-course",
                new { controller = "DangKyKhoaHoc", action = "LayDanhSachLopHocTheoIdKhoaHoc", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "getAddress",
                "course/get-address",
                new { controller = "DangKyKhoaHoc", action = "getAddress", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "registerCourse",
                "course/register-course",
                new { controller = "DangKyKhoaHoc", action = "XuLyDangKyHoc", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "courseCheckExistAccount",
                "course/check-exist-acc",
                new { controller = "DangKyKhoaHoc", action = "CheckExistAccount", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "commentReply",
                "Course/comment-reply",
                new { controller = "KhoaHoc", action = "CommentReply", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "seeMore",
                "Course/see-more",
                new { controller = "KhoaHoc", action = "SeeMore", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "Course_default",
                "Course/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}