﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrungTamTinHoc.Areas.Information.Models.Schema
{
    public class ThongTinCaNhan
    {
        public string username { get; set; }
        public string Ho { get; set; }
        public string Ten { get; set; }
        public bool GioiTinh { get; set; }
        public string SDT { get; set; }
        public string CMND { get; set; }
        public string email { get; set; }
        public DateTime? NgaySinh { get; set; }
        public string DiaChi { get; set; }
        public string IdXa { get; set; }
        public string IdHuyen { get; set; }
        public string Tinh { get; set; }
        public string HoNguoiGiamHo { get; set; }
        public string TenNguoiGiamHo { get; set; }
        public bool GioiTinhNguoiGiamHo { get; set; }
        public string SDTNguoiGiamHo { get; set; }
        public string CMNDNguoiGiamHo { get; set; }
        public DateTime? NgaySinhNguoiGiamHo { get; set; }
        public string DiaChiNguoiGiamHo { get; set; }
        public string IdXaNguoiGiamHo { get; set; }
        public string IdHuyenNguoiGiamHo { get; set; }
        public string TinhNguoiGiamHo { get; set; }
        public bool kichHoatemail { get; set; }
        public string avatar { get; set; }
        public string avatarNguoiGiamHo { get; set; }
    }

    /// <summary>
    /// Class chứa các thuộc tính của 1 Tĩnh lấy từ DB 
    /// Author       :   HoangNM - 14/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   Home.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    /// 
    public class TinhHocVien
    {
        public string IdTinh { get; set; }
        public string tenTinh { get; set; }
    }

    /// <summary>
    /// Class chứa các thuộc tính của 1 Huyện lấy từ DB 
    /// Author       :   HoangNM - 14/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   Home.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    /// 
    public class HuyenHocVien
    {
        public string IdHuyen { get; set; }
        public string tenHuyen { get; set; }
    }

    /// <summary>
    /// Class chứa các thuộc tính của 1 Xã lấy từ DB 
    /// Author       :   HoangNM - 14/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   Home.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    /// 
    public class XaHocVien
    {
        public string IdXa { get; set; }
        public string tenXa { get; set; }
    }

}