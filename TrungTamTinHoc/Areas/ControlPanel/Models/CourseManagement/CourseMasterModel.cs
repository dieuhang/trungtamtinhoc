﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Z.EntityFramework.Plus;
using TrungTamTinHoc.Areas.ControlPanel.Models.CourseManagement.Schema;
using TTTH.Common;
using TTTH.Common.Enums;
using TTTH.DataBase;
using TTTH.DataBase.Schema;
using ChuyenNganh = TrungTamTinHoc.Areas.ControlPanel.Models.CourseManagement.Schema.ChuyenNganh;
using TblCourseTrans = TTTH.DataBase.Schema.KhoaHocTrans;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.CourseManagement
{
    /// <summary>
    /// Class chứa các phương thức liên quan đến việc xử lý khóa học
    /// Author       :   TramHTD - 27/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class CourseMasterModel
    {
        DataContext context;
        List<string> typeFiles;
        public CourseMasterModel()
        {
            context = new DataContext();
            typeFiles = new List<string>();
            typeFiles.Add(".jpg");
            typeFiles.Add(".png");
        }
        /// <summary>
        /// Lấy danh sách chuyên ngành có trong db
        /// Author       :   TramHTD - 27/08/2018 - create
        /// </summary>
        /// <returns>danh sách chuyên ngành có trong db</returns>
        public List<ChuyenNganh> GetChuyenNganhs()
        {
            return context.ChuyenNganhTrans.Where(x => x.Lang == Common.defaultLang && !x.DelFlag)
                .Select(x => new ChuyenNganh
                {
                    IdChuyenNganh = x.Id,
                    TenChuyenNganh = x.TenChuyenNganh
                }).ToList();
        }

        /// <summary>
        /// Lấy thông tin của 1 khóa học
        /// Author       :   TramHTD - 27/08/2018 - create
        /// </summary>
        /// <param name="id">id của khóa học sẽ được hiển thị</param>
        /// <returns>Thông tin của khóa học</returns>
        public CourseMaster LoadCourse(string id)
        {
            try
            {
                CourseMaster courseMaster = new CourseMaster();
                int idCourse = 0;
                try
                {
                    idCourse = Convert.ToInt32(id);
                }
                catch { }
                TblCourseTrans course = context.KhoaHocTrans.FirstOrDefault(x => x.Id == idCourse && !x.DelFlag && x.Lang == Common.defaultLang);
                if (course != null)
                {
                    courseMaster.Mode = (int)ConstantsEnum.ModeMaster.Update;
                    courseMaster.CourseInfo = new Schema.Course();
                    courseMaster.CourseInfo.Id = course.Id;
                    courseMaster.CourseInfo.BeautyId = course.KhoaHoc.BeautyId;
                    courseMaster.CourseInfo.IdChuyenNganh = course.KhoaHoc.IdChuyenNganh;
                    courseMaster.CourseInfo.AnhMinhHoa = course.KhoaHoc.AnhMinhHoa;
                    courseMaster.CourseInfo.NgayKhaiGiang = course.KhoaHoc.NgayKhaiGiang;
                    courseMaster.CourseInfo.HocPhi = course.KhoaHoc.HocPhi;
                    courseMaster.CourseInfo.ChietKhau = course.KhoaHoc.ChietKhau;
                    courseMaster.CourseInfo.DoTuoi = course.KhoaHoc.DoTuoi;
                    courseMaster.CourseInfo.ThoiGian = course.KhoaHoc.ThoiGian;
                    courseMaster.CourseInfo.ThoiGianKetThuc = course.KhoaHoc.ThoiGianKetThuc;
                    courseMaster.CourseInfo.LichHoc = course.KhoaHoc.LichHoc;
                    courseMaster.CourseInfo.ChoPhepDangKy = course.KhoaHoc.ChoPhepDangKy;
                    courseMaster.CourseInfo.HienThi = course.KhoaHoc.HienThi;
                    courseMaster.CourseInfo.TenKhoaHoc = course.TenKhoaHoc;                    
                    courseMaster.CourseInfo.TomTat = course.TomTat;                    
                    courseMaster.CourseInfo.ChiTiet = course.ChiTiet;                    
                    courseMaster.Language = context.Language.Where(x => !x.DelFlag)
                        .Select(x => new Lang
                        {
                            Id = x.Id,
                            Name = x.Name
                        }).OrderBy(x => x.Id).ToList();
                    if (courseMaster.Language.Count > 0)
                    {
                        string lang = courseMaster.Language[0].Id;
                        course = context.KhoaHocTrans.FirstOrDefault(x => x.Id == idCourse && !x.DelFlag && x.Lang == lang);
                        courseMaster.CourseTrans = new Schema.Course();
                        courseMaster.CourseTrans.Id = course.Id;
                        courseMaster.CourseTrans.BeautyId = course.KhoaHoc.BeautyId;
                        courseMaster.CourseTrans.IdChuyenNganh = course.KhoaHoc.IdChuyenNganh;
                        courseMaster.CourseTrans.AnhMinhHoa = course.KhoaHoc.AnhMinhHoa;
                        courseMaster.CourseTrans.NgayKhaiGiang = course.KhoaHoc.NgayKhaiGiang;
                        courseMaster.CourseTrans.HocPhi = course.KhoaHoc.HocPhi;
                        courseMaster.CourseTrans.ChietKhau = course.KhoaHoc.ChietKhau;
                        courseMaster.CourseTrans.DoTuoi = course.KhoaHoc.DoTuoi;
                        courseMaster.CourseTrans.ThoiGian = course.KhoaHoc.ThoiGian;
                        courseMaster.CourseTrans.ThoiGianKetThuc = course.KhoaHoc.ThoiGianKetThuc;
                        courseMaster.CourseTrans.LichHoc = course.KhoaHoc.LichHoc;
                        courseMaster.CourseTrans.ChoPhepDangKy = course.KhoaHoc.ChoPhepDangKy;
                        courseMaster.CourseTrans.HienThi = course.KhoaHoc.HienThi;
                        courseMaster.CourseTrans.TenKhoaHoc = course.TenKhoaHoc;
                        courseMaster.CourseTrans.TomTat = course.TomTat;
                        courseMaster.CourseTrans.ChiTiet = course.ChiTiet;
                    }
                }
                return courseMaster;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Tạo hoặc cập nhật lại khóa học từ thông tin mà người dùng gửi lên
        /// Author       :   TramHTD - 27/08/2018 - create
        /// </summary>
        /// <param name="course">Các thông tin của khóa học muốn tạo hoặc cập nhật</param>
        /// <returns>Thông tin về việc tạo hoặc cập nhật khóa học thành công hay thất bại</returns>
        public ResponseInfo SaveCourse(Schema.Course course)
        {
            try
            {
                if (context.KhoaHoc.FirstOrDefault(x => x.Id == course.Id && !x.DelFlag) != null)
                {
                    return UpadateCourse(course);
                }
                return AddCourse(course);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Cập nhật lại  khóa học từ thông tin mà người dùng gửi lên
        /// Author       :   TramHTD - 27/08/2018 - create
        /// </summary>
        /// <param name="course">Các thông tin của khóa học muốn cập nhật</param>
        /// <returns>Thông tin về việc cập nhật khóa học thành công hay thất bại</returns>
        public ResponseInfo UpadateCourse(Schema.Course course)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo response = new ResponseInfo();
                bool update = true;
                List<string> imgsDelete = new List<string>();
                if (course.FileImg != null)
                {
                    string AnhMinhHoa = Common.SaveFileUpload(course.FileImg, "/public/img/course/", "", typeFiles);
                    if (AnhMinhHoa == "1")
                    {
                        response.Code = 202;
                        response.MsgNo = (int)MessageEnum.MsgNO.FileKhongDungDinhDang;
                        update = false;
                    }
                    else if (AnhMinhHoa == "2")
                    {
                        response.Code = 202;
                        response.MsgNo = (int)MessageEnum.MsgNO.DungLuongFileQuaLon;
                        update = false;
                    }
                    else if (AnhMinhHoa == "")
                    {
                        response.Code = 202;
                        response.MsgNo = (int)MessageEnum.MsgNO.TaiFileBiLoi;
                        update = false;
                    }
                    else
                    {
                        imgsDelete.Add(context.KhoaHoc.FirstOrDefault(x => x.Id == course.Id && !x.DelFlag).AnhMinhHoa);
                        course.AnhMinhHoa = AnhMinhHoa;
                    }
                }
                if (update)
                {
                    course.HienThi = course.CheckHienThi == "on" ? true : false;
                    course.ChoPhepDangKy = course.CheckChoPhepDangKy == "on" ? true : false;
                    context.KhoaHoc.Where(x => x.Id == course.Id && !x.DelFlag)
                        .Update(x => new TTTH.DataBase.Schema.KhoaHoc
                        {
                            HienThi = course.HienThi,
                            BeautyId= course.BeautyId,
                            AnhMinhHoa = course.AnhMinhHoa,
                            IdChuyenNganh = course.IdChuyenNganh,
                            NgayKhaiGiang = course.NgayKhaiGiang,
                            HocPhi = course.HocPhi,
                            ChietKhau = course.ChietKhau,
                            DoTuoi = course.DoTuoi,
                            ThoiGianKetThuc = course.ThoiGianKetThuc,
                            ThoiGian = course.ThoiGian,
                            ChoPhepDangKy = course.ChoPhepDangKy,
                            LichHoc = course.LichHoc
                        });
                    context.KhoaHocTrans.Where(x => x.Id == course.Id && x.Lang == Common.defaultLang && !x.DelFlag)
                        .Update(x => new TTTH.DataBase.Schema.KhoaHocTrans
                        {
                            TenKhoaHoc = course.TenKhoaHoc,
                            ChiTiet = course.ChiTiet,
                            TomTat = course.TomTat
                        });
                    context.SaveChanges();
                }
                transaction.Commit();
                Common.DeleteFile(imgsDelete);
                return response;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        /// <summary>
        /// Tạo  khóa học từ thông tin mà người dùng gửi lên
        /// Author       :   TramHTD - 27/08/2018 - create
        /// </summary>
        /// <param name="course">Các thông tin của khóa học muốn tạo</param>
        /// <returns>Thông tin về việc tạo  khóa học thành công hay thất bại</returns>
        public ResponseInfo AddCourse(Schema.Course course)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo response = new ResponseInfo();
                if (course.FileImg == null)
                {
                    response.Code = 202;
                    response.MsgNo = (int)MessageEnum.MsgNO.ChuaChonFile;
                }
                else
                {
                    course.AnhMinhHoa = Common.SaveFileUpload(course.FileImg, "/public/img/course/", "", typeFiles);
                    if (course.AnhMinhHoa == "1")
                    {
                        response.Code = 202;
                        response.MsgNo = (int)MessageEnum.MsgNO.FileKhongDungDinhDang;
                    }
                    else if (course.AnhMinhHoa == "2")
                    {
                        response.Code = 202;
                        response.MsgNo = (int)MessageEnum.MsgNO.DungLuongFileQuaLon;
                    }
                    else if (course.AnhMinhHoa == "")
                    {
                        response.Code = 202;
                        response.MsgNo = (int)MessageEnum.MsgNO.TaiFileBiLoi;
                    }
                    else
                    {
                        List<Lang> listOfLang = context.Language.Where(x => !x.DelFlag).Select(x => new Lang
                        {
                            Id = x.Id,
                            Name = x.Name
                        }).ToList();
                        course.HienThi = course.CheckHienThi == "on" ? true : false;
                        course.ChoPhepDangKy = course.CheckChoPhepDangKy == "on" ? true : false;
                        course.Id = context.KhoaHoc.Count() == 0 ? 1 : context.KhoaHoc.Max(x => x.Id) + 1;
                        context.KhoaHoc.Add(new KhoaHoc
                        {
                            Id = course.Id,
                            BeautyId = course.BeautyId,
                            AnhMinhHoa = course.AnhMinhHoa,
                            IdChuyenNganh = course.IdChuyenNganh,
                            NgayKhaiGiang = course.NgayKhaiGiang,
                            HocPhi = course.HocPhi,
                            ChietKhau = course.ChietKhau,
                            DoTuoi = course.DoTuoi,
                            ThoiGianKetThuc = course.ThoiGianKetThuc,
                            ThoiGian = course.ThoiGian,
                            LichHoc = course.LichHoc,
                            ChoPhepDangKy = course.ChoPhepDangKy,
                            HienThi = course.HienThi,
                        });
                        foreach (Lang lang in listOfLang)
                        {
                            context.KhoaHocTrans.Add(new KhoaHocTrans
                            {
                                Id = course.Id,
                                Lang = lang.Id,
                                TenKhoaHoc = course.TenKhoaHoc,
                                ChiTiet = course.ChiTiet,
                                TomTat = course.TomTat
                            });
                        }
                        context.SaveChanges();
                        response.ThongTinBoSung1 = course.Id + "";
                    }
                }
                transaction.Commit();
                return response;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }


        /// <summary>
        /// Cập nhật lại khóa học từ thông tin mà người dùng gửi lên bằng ngôn ngữ khác
        /// Author       :   TramHTD - 27/08/2018 - create
        /// </summary>
        /// <param name="course">Các thông tin ngôn ngữ khác của khóa học muốn cập nhật</param>
        /// <returns>Thông tin về việc cập nhật khóa học thành công hay thất bại</returns>
        public ResponseInfo UpdateCourseTranslate(Course_Trans course)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo response = new ResponseInfo();
                context.KhoaHocTrans.Where(x => x.Id == course.Id && x.Lang == course.LanguageTrans && !x.DelFlag)
                    .Update(x => new TTTH.DataBase.Schema.KhoaHocTrans
                    {
                        TenKhoaHoc = course.TenKhoaHoc,
                        TomTat = course.TomTat,
                        ChiTiet = course.ChiTiet
                    });
                context.SaveChanges();
                response.ThongTinBoSung1 = course.Id + "";
                transaction.Commit();
                return response;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        /// <summary>
        /// Lấy thông tin của khóa học bởi ngôn ngữ khác ngôn ngữ mặc định
        /// Author       :   TramHTD - 27/08/2018 - create
        /// </summary>
        /// <param name="id">Id của khóa học muốn lấy</param>
        /// <param name="lang">Ngôn ngữ muốn lấy thông tin liên quan</param>
        /// <returns>Thông tin của khóa học có nhiều ngôn ngữ</returns>
        public Course_Trans ReferCourseWithLang(int id, string lang)
        {
            try
            {
                return context.KhoaHocTrans.Where(x => x.Id == id && x.Lang == lang && !x.DelFlag)
                    .Select(x => new Course_Trans()
                    {
                        TenKhoaHoc = x.TenKhoaHoc,
                        TomTat = x.TomTat,
                        ChiTiet = x.ChiTiet,
                    }).FirstOrDefault();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Kiểm tra beautyID đã tồn tại hay chưa.
        /// Author       :   TramHTD - 27/08/2018 - create
        /// </summary>
        /// <param name="value">giá trị của beautyID cần kiểm tra</param>
        /// <returns>Nếu có tồn tại trả về true, ngược lại trả về false</returns>
        public bool CheckExistBeautyId(string value, int id)
        {
            try
            {
                KhoaHoc khoahoc = context.KhoaHoc.FirstOrDefault(x =>  x.BeautyId == value && x.Id !=id && !x.DelFlag);
                if (khoahoc != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}