﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.UserManagement.Schema
{
    /// <summary>
    /// Class thông tin của việc đăng ký tài khoản.
    /// Author       :   TramHTD - 10/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class Account
    {
        public int Id { set; get; }
        [Required(ErrorMessage = "1")]
        [MaxLength(50, ErrorMessage = "2")]
        [RegularExpression("^[a-zA-Z0-9_.-]{6,50}$", ErrorMessage = "34")]
        public string Username { set; get; }

        [Required(ErrorMessage = "1")]
        [MaxLength(50, ErrorMessage = "2")]
        public string Ho { set; get; }

        [Required(ErrorMessage = "1")]
        [MaxLength(50, ErrorMessage = "2")]
        public string Ten { set; get; }

        [Required(ErrorMessage = "1")]
        public bool GioiTinh { set; get; }

        [Required(ErrorMessage = "1")]
        public DateTime? NgaySinh { set; get; }

        [Required(ErrorMessage = "1")]
        [MaxLength(255, ErrorMessage = "2")]
        [EmailAddress(ErrorMessage = "5")]
        public string Email { set; get; }
        
        [MaxLength(50, ErrorMessage = "2")]
        public string Password { set; get; }
        
        [MaxLength(50, ErrorMessage = "2")]
        [Compare("Password", ErrorMessage = "20")]
        public string ConfirmPassword { set; get; }

        public List<int> IdGroup { set; get; }
    }
}