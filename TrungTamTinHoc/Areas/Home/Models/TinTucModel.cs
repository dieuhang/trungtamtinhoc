﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TrungTamTinHoc.Areas.Home.Models.Schema;
using TTTH.Common;
using TTTH.DataBase;
using TblUser = TTTH.DataBase.Schema.User;
using TblTinTuc = TTTH.DataBase.Schema.TinTuc;
using TblCommentTinTuc = TTTH.DataBase.Schema.CommentTinTuc;
using TblDanhGiaTinTuc = TTTH.DataBase.Schema.DanhGiaTinTuc;
namespace TrungTamTinHoc.Areas.Home.Models
{
    public class TinTucModel
    {
        DataContext context;

        public List<TinTuc> DanhSachTinTuc { get; set; }

        public TinTucModel()
        {
            context = new DataContext();
        }
        /// <summary>
        /// Lấy tintuc chi tiết và danh sách 4 tin tức có lượt xem cao nhất hiển thị bên cạnh
        /// Author:     HangNTD - 03/07/2018 - create
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <returns>CacTinTuc</returns>
        /// 
        public ChiTietTinTuc LoadChiTietTinTuc(string beautyid)
        {
            DbContextTransaction Transaction = context.Database.BeginTransaction();
            try
            {
                string lang = Common.GetLang();
                ChiTietTinTuc chiTietTinTuc = new ChiTietTinTuc();
                TblTinTuc news = context.TinTuc.FirstOrDefault(x => x.BeautyId == beautyid && !x.DelFlag);
                if (news != null)
                {
                    news.SoLuongView++;
                    context.SaveChanges();
                    chiTietTinTuc = context.TinTuc.Include("TinTucTrans").Where(p => p.BeautyId == beautyid && !p.DelFlag)
                        .Select(x => new ChiTietTinTuc
                        {
                            Id = x.Id,
                            BeautyId = x.BeautyId,
                            AnhMinhHoa = x.AnhMinhHoa,
                            AnhChinh = x.AnhChinh,
                            TieuDe = x.TinTucTrans.FirstOrDefault(y => y.Lang == lang).TieuDe,
                            NoiDung = x.TinTucTrans.FirstOrDefault(y => y.Lang == lang).NoiDung,
                            TomTat = x.TinTucTrans.FirstOrDefault(y => y.Lang == lang).TomTat,
                            NgayDang = x.Created_at,
                            SoLuongView = x.SoLuongView,
                            SoLuongComment = x.CommentTinTuc.Count,
                            SoLuongDanhGia = x.DanhGiaTinTuc.Count,
                            DiemDanhGia = x.DanhGiaTinTuc.Count != 0 ? x.DanhGiaTinTuc.Sum(y => y.DiemDanhGIa) : 0,
                            ListReccentNew = context.TinTuc.Where(m => m.Id != x.Id && !m.DelFlag)
                                .Select(n => new TinTuc()
                                {
                                    Id = n.Id,
                                    BeautyId = n.BeautyId,
                                    AnhMinhHoa = n.AnhMinhHoa,
                                    TieuDe = n.TinTucTrans.FirstOrDefault(t => t.Lang == lang).TieuDe,
                                    TomTat = n.TinTucTrans.FirstOrDefault(t => t.Lang == lang).TomTat,
                                    NgayDang = n.Created_at,
                                    SoLuongView = n.SoLuongView,
                                    SoLuongComment = n.CommentTinTuc.Count,
                                    SoLuongDanhGia = n.DanhGiaTinTuc.Count,
                                    DiemDanhGia = n.DanhGiaTinTuc.Count != 0 ? n.DanhGiaTinTuc.Sum(t => t.DiemDanhGIa) : 0
                                }).Take(4).OrderByDescending(q => q.NgayDang).ToList(),
                            CountComment = context.CommentTinTuc.Where(y => y.IdTinTuc == x.Id && y.IdComment == null && !y.DelFlag).Count()
                        }).FirstOrDefault();
                    if (chiTietTinTuc.CountComment < 1)
                    {
                        chiTietTinTuc.ListComment = null;
                    }
                    else
                    {
                        chiTietTinTuc.ListComment = GetListComment(chiTietTinTuc.Id, 0);
                    }
                    chiTietTinTuc.IdUser = XacThuc.GetIdUser();
                    if (chiTietTinTuc.IdUser != 0)
                    {
                        chiTietTinTuc.AvatarUser =
                            context.User.FirstOrDefault(x => x.Id == chiTietTinTuc.IdUser && !x.DelFlag)?.Avatar;
                        chiTietTinTuc.HotenUser = context.User.FirstOrDefault(x => x.Id == chiTietTinTuc.IdUser && !x.DelFlag)?.Ho +
                                                   context.User.FirstOrDefault(x => x.Id == chiTietTinTuc.IdUser)?.Ten;
                    }
                    else
                    {
                        chiTietTinTuc.AvatarUser = null;
                        chiTietTinTuc.HotenUser = "No Name";
                    }
                    Transaction.Commit();
                    return chiTietTinTuc;
                }
                else return null;

            }
            catch (Exception e)
            {
                Transaction.Rollback();
                throw e;
            }
        }

        /// <summary>
        /// Lấy tất cả các tin tức có lượt view giảm dần
        /// Author:     HangNTD - 03/07/2018 - create
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <returns>List<TinTuc></returns>
        public TinTucList GetAllListNews(NewsConditionSearch condition)
        {
            try
            {
                // Nếu không tồn tại điều kiện tìm kiếm thì khởi tạo giá trị tìm kiếm ban đầu
                if (condition == null)
                {
                    condition = new NewsConditionSearch();
                }
                string lang = Common.GetLang();
                TinTucList listOfNews = new TinTucList();
                int newSkip = (condition.CurrentPage - 1) * 8;
                // Lấy các thông tin dùng để phân trang
                listOfNews.Paging = new Paging(context.TinTucTrans.Count(x =>
                    (condition.KeySearch == null ||
                     (condition.KeySearch != null && (x.TieuDe.Contains(condition.KeySearch))))
                    && !x.DelFlag && x.Lang == lang), condition.CurrentPage, condition.PageSize);

                listOfNews.TinTucs = context.TinTucTrans.Where(x =>
                    (condition.KeySearch == null ||
                     (condition.KeySearch != null && (x.TieuDe.Contains(condition.KeySearch))))
                    && !x.DelFlag && x.Lang == lang).Select(x => new TinTuc
                    {
                        Id = x.Id,
                        BeautyId = x.TinTuc.BeautyId,
                        AnhMinhHoa = x.TinTuc.AnhMinhHoa,
                        TieuDe = x.TieuDe,
                        TomTat = x.TomTat,
                        NgayDang = x.Created_at,
                        SoLuongView = x.TinTuc.SoLuongView,
                        SoLuongComment = x.TinTuc.CommentTinTuc.Count,
                        SoLuongDanhGia = x.TinTuc.DanhGiaTinTuc.Count,
                        DiemDanhGia = x.TinTuc.DanhGiaTinTuc.Count != 0 ? x.TinTuc.DanhGiaTinTuc.Sum(y => y.DiemDanhGIa) : 0
                    }).OrderByDescending(x => x.NgayDang).Skip(newSkip).Take(8).ToList();
                listOfNews.Condition = condition;
                return listOfNews;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Lấy danh sách 5 comment được phép hiển thị từ DB.
        /// Author       :   HangNTD - 25/07/2018 - create
        /// </summary>
        /// <param name = "CountComment" ></ param >
        /// < returns > tin tuc có trong DB theo beautyId.</returns>
        public List<CommentTinTuc> GetListComment(int idTinTuc, int CountComment)
        {
            try
            {
                List<CommentTinTuc> listComment = new List<CommentTinTuc>();
                listComment = context.CommentTinTuc
                    .Where(x => x.IdTinTuc == idTinTuc && x.IdComment == null && !x.DelFlag)
                    .Select(y => new CommentTinTuc
                    {
                        Id = y.Id,
                        NoiDung = y.NoiDung,
                        ThoiGianCmt = y.Created_at,
                        IdUser = y.IdUser,
                        HoTen = y.IdUser == null ? "No Name" : context.User.FirstOrDefault(p => p.Id == y.IdUser).Ho + " " + context.User.FirstOrDefault(p => p.Id == y.IdUser).Ten,
                        Avatar = y.IdUser == null ? null : context.User.FirstOrDefault(p => p.Id == y.IdUser).Avatar,
                    }).OrderByDescending(t => t.ThoiGianCmt).Skip(CountComment).Take(5).ToList();
                foreach (var cmt in listComment)
                {

                    cmt.CountChildComment = context.CommentTinTuc.Count(x => x.IdTinTuc == idTinTuc && x.IdComment == cmt.Id && !x.DelFlag);
                    cmt.ListChildComment = cmt.CountChildComment < 1 ? null : GetListCommentReply(idTinTuc, 0, cmt.Id);
                }
                return listComment;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Lấy danh sách 2 comment con được phép hiển thị từ DB theo IdParentComment.
        /// Author       :   HangNTD - 25/07/2018 - create
        /// </summary>
        /// <param name="IdParentComment"></param>
        /// <returns>tin tức có trong DB theo beautyId.</returns>
        public List<CommentTinTuc> GetListCommentReply(int idTinTuc, int childComment, int idParentComment)
        {
            try
            {
                List<CommentTinTuc> listCommentReply = context.CommentTinTuc
                    .Where(x => x.IdTinTuc == idTinTuc && x.IdComment == idParentComment && !x.DelFlag)
                    .Select(y => new CommentTinTuc
                    {
                        Id = y.Id,
                        NoiDung = y.NoiDung,
                        ThoiGianCmt = y.Created_at,
                        IdUser = y.IdUser,
                        HoTen = y.IdUser == null ? "No Name" : context.User.FirstOrDefault(p => p.Id == y.IdUser).Ho + context.User.FirstOrDefault(p => p.Id == y.IdUser).Ten,
                        Avatar = y.IdUser == null ? null : context.User.FirstOrDefault(p => p.Id == y.IdUser).Avatar
                    }).OrderByDescending(y => y.ThoiGianCmt).Skip(childComment).Take(2).OrderBy(y => y.ThoiGianCmt).ToList();
                return listCommentReply;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        
       



        /// <summary>
        /// Lưu comment vào DB
        /// Author:     HangNTD - 011/07/2018 - create
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <returns>List<CommentTinTuc></returns>
        public CommentTinTuc SendComment(int idUser, int idTintuc, string content, int Rating)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                TblUser user = context.User.Where(x => x.Id == idUser).FirstOrDefault();
                CommentTinTuc comment = new CommentTinTuc {
                    Id = idTintuc,
                    Avatar = (user==null)?null: user.Avatar,
                    HoTen = (user == null) ? "NoName" : user.Ho+user.Ten,
                    NoiDung = content,
                    ThoiGianCmt = DateTime.Now,
                    Rating = Rating
                };
                TblCommentTinTuc cmt = new TblCommentTinTuc()
                {
                    IdTinTuc = idTintuc,
                    NoiDung = content,
                    IdComment = null,
                    Created_by = 0

                };
                TblDanhGiaTinTuc rating = new TblDanhGiaTinTuc()
                {
                    IdTinTuc = idTintuc,
                    DiemDanhGIa = Rating,
                    Created_by = 0
                };
                if (idUser == 0)
                {
                    cmt.IdUser = null;
                    rating.IdUser = null;
                }
                else
                {
                    cmt.IdUser = idUser;
                    rating.IdUser = idUser;
                }
                if(content != "")
                {
                    context.CommentTinTuc.Add(cmt);
                }
                if(Rating>0)
                {
                    context.DanhGiaTinTuc.Add(rating);
                }
                context.SaveChanges();
                comment.Id = context.CommentTinTuc.OrderByDescending(x => x.Id).First().Id;
                transaction.Commit();
                return comment;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }
        /// <summary>
        /// Lưu comment reply vào DB
        /// Author:     HangNTD - 14/07/2018 - create
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <returns>List<CommentTinTuc></returns>
        public CommentTinTuc SendCommentReply(int idUser, int idTintuc, string content, int idComment)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                TblUser user = context.User.Where(x => x.Id == idUser).FirstOrDefault();
                CommentTinTuc comment = new CommentTinTuc
                {
                    Id = idTintuc,
                    Avatar = (user == null) ? null : user.Avatar,
                    HoTen = (user == null) ? "NoName" : user.Ho + user.Ten,
                    NoiDung = content,
                    ThoiGianCmt = DateTime.Now,
                    IdComment=idComment
                };
                TblCommentTinTuc cmt = new TblCommentTinTuc()
                {
                    IdTinTuc = idTintuc,
                    NoiDung = content,
                    IdComment = idComment,
                    Created_by = 0

                };
                if (idUser == 0)
                {
                    cmt.IdUser = null;
                }
                else
                {
                    cmt.IdUser = idUser;
                }
                context.CommentTinTuc.Add(cmt);
                context.SaveChanges();
                transaction.Commit();
                return comment;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }
    }
}
