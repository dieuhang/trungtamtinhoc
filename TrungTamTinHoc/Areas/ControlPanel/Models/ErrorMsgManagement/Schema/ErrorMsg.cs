﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.ErrorMsgManagement.Schema
{
    public class ErrorMsg
    {
        public int Id { set; get; }
        [Required]
        [StringLength(255)]
        public string Msg { get; set; }
        [Required]
        public int Type { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
    }
    public class ErrorMsg_Trans
    {
        public int Id { set; get; }

        public string LanguageTrans { set; get; }

        [Required]
        [StringLength(1000)]
        public string Msg { get; set; }
    }

    public class ContactConditionSearch
    {
        public string SearchTitle { set; get; }
        public int Type { set; get; }
        public int CurrentPage { set; get; }
        public int PageSize { set; get; }
        public ContactConditionSearch()
        {
            this.SearchTitle = "";
            this.Type = 0;
            this.PageSize = 10;
            this.CurrentPage = 1;
        }
    }
    
}