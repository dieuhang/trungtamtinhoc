﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TTTH.Validate;

namespace TrungTamTinHoc.Areas.Information.Models.Schema
{
    /// <summary>
    /// Class thông tin của việc cập nhật thông tin của tài khoản.
    /// Author       :   HoangNM - 07/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   Information.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class UpDate
    {
        [MaxLength(255, ErrorMessage = "2")]
        public string Avatar { set; get; }

        [Required(ErrorMessage = "1")]
        [MaxLength(50, ErrorMessage = "2")]
        public string Ho { set; get; }

        [Required(ErrorMessage = "1")]
        [MaxLength(50, ErrorMessage = "2")]
        public string Ten { set; get; }

        [Required(ErrorMessage = "1")]
        public bool GioiTinh { set; get; }

        public DateTime? NgaySinh { set; get; }

        [MaxLength(12, ErrorMessage = "2")]
        public string SDT { set; get; }

        [MaxLength(255, ErrorMessage = "2")]
        public string DiaChi { set; get; }

        [MaxLength(9, ErrorMessage = "2")]
        public string CMND { set; get; }

        [Required(ErrorMessage = "1")]
        [MaxLength(50, ErrorMessage = "2")]
        public string Tinh { set; get; }

        [Required(ErrorMessage = "1")]
        [MaxLength(50, ErrorMessage = "2")]
        public string Huyen { set; get; }

        [Required(ErrorMessage = "1")]
        [MaxLength(50, ErrorMessage = "2")]
        public string Xa { set; get; }

        public bool nguoiGiamHo { set; get; }


        [MaxLength(50, ErrorMessage = "2")]

        public string AvatarNguoiGiamHo { set; get; }

        [MaxLength(50, ErrorMessage = "2")]
        public string HoNguoiGiamHo { set; get; }

        [Required(ErrorMessage = "1")]
        [MaxLength(50, ErrorMessage = "2")]
        public string TenNguoiGiamHo { set; get; }
        [Required(ErrorMessage = "1")]

        public bool GioiTinhNguoiGiamHo { set; get; }

        public DateTime? NgaySinhNguoiGiamHo { set; get; }

        [MaxLength(12, ErrorMessage = "2")]
        public string SDTNguoiGiamHo { set; get; }

        [MaxLength(255, ErrorMessage = "2")]
        public string DiaChiNguoiGiamHo { set; get; }

        [MaxLength(9, ErrorMessage = "2")]
        public string CMNDNguoiGiamHo { set; get; }

        [Required(ErrorMessage = "1")]
        [MaxLength(50, ErrorMessage = "2")]
        public string TinhNguoiGiamHo { set; get; }

        [Required(ErrorMessage = "1")]
        [MaxLength(50, ErrorMessage = "2")]
        public string HuyenNguoiGiamHo { set; get; }

        [Required(ErrorMessage = "1")]
        [MaxLength(50, ErrorMessage = "2")]
        public string XaNguoiGiamHo { set; get; }


    }
}