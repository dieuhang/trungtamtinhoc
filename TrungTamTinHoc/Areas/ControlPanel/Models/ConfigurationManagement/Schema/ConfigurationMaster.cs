﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.ConfigurationManagement.Schema
{
    public class ConfigurationMaster
    {
        public Configuration ConfigurationInfo { set; get; }
        public ConfigurationMaster()
        {
            ConfigurationInfo = null;
        }
    }
}