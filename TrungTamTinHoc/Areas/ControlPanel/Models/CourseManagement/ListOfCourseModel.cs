﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Web;
using TrungTamTinHoc.Areas.ControlPanel.Models.CourseManagement.Schema;
using TTTH.Common;
using TTTH.DataBase;
using TTTH.DataBase.Schema;
using Z.EntityFramework.Plus;
using ChuyenNganh = TrungTamTinHoc.Areas.ControlPanel.Models.CourseManagement.Schema.ChuyenNganh;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.CourseManagement
{
    /// <summary>
    /// Class dùng để xử lý các hoạt động liên quan đến khóa học.
    /// Author       :   TramHTD - 20/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ListOfCourseModel
    {
        DataContext context;

        public ListOfCourseModel()
        {
            context = new DataContext();
        }

        /// <summary>
        /// Tìm kiếm các khóa học theo điều kiện cho trước.
        /// Author       :   TramHTD - 20/07/2018 - create
        /// </summary>
        /// <param name="condition">Đối tượng chứa điều kiện tìm kiếm</param>
        /// <returns>Danh sách các khóa học đã tìm kiếm được. Exception nếu có lỗi</returns>
        public ListOfCourse GetListOfCourse(CourseConditionSearch condition)
        {
            try
            {
                // Nếu không tồn tại điều kiện tìm kiếm thì khởi tạo giá trị tìm kiếm ban đầu
                if (condition == null)
                {
                    condition = new CourseConditionSearch();
                }
                string lang = Common.GetLang();
                ListOfCourse listOfCourse = new ListOfCourse();

                // Lấy các thông tin dùng để phân trang
                listOfCourse.Paging = new Paging(context.KhoaHocTrans.Count(x =>
                    (condition.KeySearch == null ||
                     (condition.KeySearch != null && (x.TenKhoaHoc.Contains(condition.KeySearch))))
                    && (condition.TrangThai == null ||
                        (condition.TrangThai != null && x.KhoaHoc.HienThi == condition.TrangThai.Value))
                    && (condition.IdChuyenNganh == 0 ||
                        (condition.IdChuyenNganh != 0 && x.KhoaHoc.IdChuyenNganh == condition.IdChuyenNganh))
                    && (condition.StartDate == null && condition.EndDate == null ||
                        (condition.StartDate == null && x.KhoaHoc.NgayKhaiGiang <= condition.EndDate) ||
                        (condition.EndDate == null && x.KhoaHoc.NgayKhaiGiang >= condition.StartDate) ||
                        (condition.StartDate!=null && condition.EndDate !=null && 
                         x.KhoaHoc.NgayKhaiGiang >= condition.StartDate && x.KhoaHoc.NgayKhaiGiang <= condition.EndDate))
                    && !x.DelFlag && x.Lang == lang), condition.CurrentPage, condition.PageSize);
                // Tìm kiếm và lấy dữ liệu theo trang
                listOfCourse.CourseList = context.KhoaHocTrans.Where(x =>
                        (condition.KeySearch == null ||
                         (condition.KeySearch != null && (x.TenKhoaHoc.Contains(condition.KeySearch))))
                        && (condition.TrangThai == null ||
                            (condition.TrangThai != null && x.KhoaHoc.HienThi == condition.TrangThai.Value))
                        && (condition.IdChuyenNganh == 0 ||
                            (condition.IdChuyenNganh != 0 && x.KhoaHoc.IdChuyenNganh==condition.IdChuyenNganh))
                        && (condition.StartDate == null && condition.EndDate == null ||
                            (condition.StartDate == null && x.KhoaHoc.NgayKhaiGiang <= condition.EndDate) ||
                            (condition.EndDate == null && x.KhoaHoc.NgayKhaiGiang >= condition.StartDate) ||
                            (condition.StartDate != null && condition.EndDate != null &&
                             x.KhoaHoc.NgayKhaiGiang >= condition.StartDate && x.KhoaHoc.NgayKhaiGiang <= condition.EndDate))
                        && !x.DelFlag && x.Lang == lang).OrderBy(x => x.Id)
                    .Skip((listOfCourse.Paging.CurrentPage - 1) * listOfCourse.Paging.NumberOfRecord)
                    .Take(listOfCourse.Paging.NumberOfRecord).Select(x => new Schema.Course
                    {
                        Id = x.Id,
                        TenKhoaHoc = x.TenKhoaHoc,
                        AnhMinhHoa = x.KhoaHoc.AnhMinhHoa,
                        HocPhi = x.KhoaHoc.HocPhi,
                        DoTuoi = x.KhoaHoc.DoTuoi,
                        ThoiGian = x.KhoaHoc.ThoiGian,
                        ThoiGianKetThuc = x.KhoaHoc.ThoiGianKetThuc,
                        LichHoc = x.KhoaHoc.LichHoc,
                        NgayKhaiGiang = x.KhoaHoc.NgayKhaiGiang,
                        HienThi = x.KhoaHoc.HienThi
                    }).ToList();
                listOfCourse.Condition = condition;
                listOfCourse.ChuyenNganh = context.ChuyenNganhTrans.Where(x => x.Lang == lang && !x.DelFlag)
                    .Select(x => new ChuyenNganh
                    {
                        IdChuyenNganh = x.Id,
                        TenChuyenNganh = x.TenChuyenNganh
                    }).ToList();
                return listOfCourse;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// Xóa các khóa học trong DB.
        /// Author       :   TramHTD - 20/07/2018 - create
        /// </summary>
        /// <param name="ids">Danh sách id của các khóa học sẽ xóa</param>
        /// <returns>True nếu xóa thành công, False nếu không còn khóa học được hiển thị trên trang chủ, Excetion nếu có lỗi</returns>
        public bool DeleteCourses(List<int> ids)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                bool result = true;
                if (context.KhoaHoc.Count(x => x.HienThi && !ids.Contains(x.Id) && !x.DelFlag) >= 3)
                {
                    context.KhoaHoc.Where(x => x.HienThi && ids.Contains(x.Id) && !x.DelFlag).Update(x => new KhoaHoc
                    {
                        DelFlag = true
                    });

                    context.KhoaHocTrans.Where(x => ids.Contains(x.Id) && !x.DelFlag).Update(x => new KhoaHocTrans
                    {
                        DelFlag = true
                    });
                    
                    context.SaveChanges();
                }
                else
                {
                    result = false;
                }
                transaction.Commit();
                return result;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }
        /// <summary>
        /// Cập nhật trạng thái hiển thị của khóa học trên trang chủ.
        /// Author       :   TramHTD - 20/07/2018 - create
        /// </summary>
        /// <param name="id">id của khóa học sẽ được cập nhật trạng thái</param>
        /// <returns>True nếu xóa thành công, False nếu không còn khóa học được hiển thị trên trang chủ, Excetion nếu có lỗi</returns>
        public bool UpdateShow(int id)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                bool result = true;
                if (context.KhoaHoc.Count(x => x.HienThi && x.Id != id && !x.DelFlag) >= 3)
                {
                    context.KhoaHoc.Where(x => x.Id == id && !x.DelFlag)
                        .Update(x => new KhoaHoc
                        {
                            HienThi = !x.HienThi
                        });
                    context.SaveChanges();
                }
                else
                {
                    result = false;
                }
                transaction.Commit();
                return result;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }
    }
}