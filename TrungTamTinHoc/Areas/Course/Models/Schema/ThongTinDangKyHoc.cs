﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrungTamTinHoc.Areas.Course.Models.Schema
{
    /// <summary>
    /// Class chứa tất cả dữ liệu cho việc xử lý đăng ký lớp học ở server
    /// Author       :   AnTM - 08/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   Home.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ThongTinDangKyHoc
    {
        [Required(ErrorMessage = "1")]
        public int IDLopHoc { get; set; }

        [Required(ErrorMessage = "1")]
        [MaxLength(50, ErrorMessage = "2")]
        public string HoHocVien { get; set; }

        [Required(ErrorMessage = "1")]
        [MaxLength(50, ErrorMessage = "2")]
        public string TenHocVien { get; set; }

        [Required(ErrorMessage = "1")]
        public bool GioiTinhHocVien { get; set; }

        [Required(ErrorMessage = "1")]
        public DateTime NgaySinhHocVien { get; set; }

        [Required(ErrorMessage = "1")]
        [MaxLength(200, ErrorMessage = "2")]
        public string DiaChiHocVien { get; set; }

        [Required(ErrorMessage = "1")]
        [MaxLength(5, ErrorMessage = "2")]
        public string IdXaHocVien { get; set; }

        [MaxLength(12, ErrorMessage = "2")]
        public string SoCMNDHocVien { get; set; }

        [MaxLength(15, ErrorMessage = "2")]
        public string SoDienThoaiHocVien { get; set; }

        [MaxLength(255, ErrorMessage = "2")]
        [EmailAddress(ErrorMessage = "5")]
        public string EmailHocVien { get; set; }

        [MaxLength(50, ErrorMessage = "2")]
        public string HoGiamHo { get; set; }

        [MaxLength(50, ErrorMessage = "2")]
        public string TenGiamHo { get; set; }

        public bool GioiTinhGiamHo { get; set; }
        
        public DateTime? NgaySinhGiamHo { get; set; }

        [MaxLength(200, ErrorMessage = "2")]
        public string DiaChiGiamHo { get; set; }

        [MaxLength(5, ErrorMessage = "2")]
        public string IdXaGiamHo { get; set; }

        [MaxLength(12, ErrorMessage = "2")]
        public string SoCMNDGiamHo { get; set; }

        [MaxLength(15, ErrorMessage = "2")]
        public string SoDienThoaiGiamHo { get; set; }
        
        [MaxLength(255, ErrorMessage = "2")]
        [EmailAddress(ErrorMessage = "5")]
        public string EmailGiamHo { get; set; }

        [MaxLength(200, ErrorMessage = "2")]
        public string GhiChu { get; set; }

        [MaxLength(50, ErrorMessage = "2")]
        [RegularExpression("^[a-zA-Z0-9_.-]{6,50}$", ErrorMessage = "34")]
        public string Username { set; get; }

        [MaxLength(50, ErrorMessage = "2")]
        [RegularExpression("^(?=.*\\d)(?=.*[a-zA-Z])[a-zA-Z0-9]{8,50}$", ErrorMessage = "19")]
        public string Password { set; get; }

        [MaxLength(50, ErrorMessage = "2")]
        [Compare("Password", ErrorMessage = "20")]
        public string ConfirmPassword { set; get; }

        [Required(ErrorMessage = "1")]
        public bool CoGiamHo { get; set; }
    }
}