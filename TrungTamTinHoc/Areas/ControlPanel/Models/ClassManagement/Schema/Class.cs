﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.ClassManagement.Schema
{
    /// <summary>
    /// Class dùng để chứa thông tin của một lớp học
    /// Author       :   HaLTH - 01/09/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class Class
    {
        public int Id { set; get; }

        public int IdKhoaHoc { get; set; }

        public double SoTiet { get; set; }

        public int ThoiGianMoiTiet { get; set; }

        public int SoLuongHocVien { get; set; }

        public DateTime ThoiGianBatDau { get; set; }

        public DateTime ThoiGianKetThuc { get; set; }

        public bool ChoPhepDangKy { set; get; }
        public string CheckChoPhepDangKy { set; get; }

        [Required]
        [StringLength(50)]
        public string TenLop { get; set; }

        [Required]
        [StringLength(500)]
        public string TomTat { get; set; }
    }

    public class Class_Trans
    {
        public int Id { set; get; }

        public string LanguageTrans { set; get; }

        [Required]
        [StringLength(50)]
        public string TenLop { get; set; }

        [Required]
        [StringLength(500)]
        public string TomTat { get; set; }
    }

    /// <summary>
    /// Class dùng để chứa thông tin tìm kiếm của danh sách lớp học
    /// Author       :   HaLTH - 01/09/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ClassConditionSearch
    {
        public string KeySearch { set; get; }
        public DateTime? StartTime { set; get; }
        public DateTime? EndTime { set; get; }
        public int SoLuongHocVien { set; get; }
        public bool? TrangThai { set; get; }
        public int CurrentPage { set; get; }
        public int PageSize { set; get; }
        public ClassConditionSearch()
        {
            this.KeySearch = "";
            this.StartTime = null;
            this.EndTime = null;
            this.SoLuongHocVien = 0;
            this.TrangThai = null;
            this.PageSize = 10;
            this.CurrentPage = 1;
        }
    }

    /// <summary>
    /// Class dùng để chứa thông tin của khóa học
    /// Author       :   HaLTH - 01/09/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class KhoaHoc
    {
        public int IdKhoaHoc { set; get; }
        public string TenKhoaHoc { set; get; }
    }
}