﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TTTH.Common;
namespace TrungTamTinHoc.Areas.ControlPanel.Models.ScheduleManagement.Schema
{
    /// <summary>
    /// Class dùng để lấy danh sách lịch học trả về cho trang danh sách lịch học
    /// Author       :   HoangNM - 05/10/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ListOfSchedule
    {
        public List<Schedule> ScheduleList { set; get; }
        public Paging Paging { set; get; }
        public ScheduleConditionSearch Condition { set; get; }
        public List<KhoaHocHienThi> cacKhoaHoc { get; set; }
        public List<LopHocHienThi> cacLopHoc { get; set; }
        public List<PhongHocHienThi> cacPhongHoc { get; set; }

        public ListOfSchedule()
        {
            this.ScheduleList = new List<Schedule>();
            this.Condition = new ScheduleConditionSearch();
            this.Paging = new Paging();
           
        }
    }
    /// <summary>
    /// Class chứa các thuộc tính của 1 khóa học lấy từ DB cho việc hiển thị danh sách các khóa học để người dùng chọn để tìm kiếm ở control panel
    /// Author       :   HoangNM - 06/10/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class KhoaHocHienThi
    {
        public int IDKhoaHoc { get; set; }
        public string TenKhoaHoc { set; get; }
    }

    /// <summary>
    /// Class chứa các thuộc tính của 1 lớp học lấy từ DB cho việc hiển thị danh sách các lớp học để người dùng chọn để tìm kiếm ở control panel
    /// Author       :   HoangNM - 06/10/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class LopHocHienThi
    {
        public int IDLopHoc { get; set; }
        public string TenLopHoc { set; get; }
    }
    /// <summary>
    /// Class chứa các thuộc tính của 1 phòng học lấy từ DB cho việc hiển thị danh sách các phòng học để người dùng chọn để tìm kiếm ở control panel
    /// Author       :   HoangNM - 09/10/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    /// 
    public class PhongHocHienThi
    {
        public int IDPhongHoc { get; set; }
        public string TenPhongHoc { set; get; }
    }
}