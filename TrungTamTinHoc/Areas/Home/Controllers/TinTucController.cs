﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrungTamTinHoc.Areas.Home.Models;
using TrungTamTinHoc.Areas.Home.Models.Schema;
using TTTH.Common;
using TTTH.Validate;
using static TTTH.Common.Enums.ConstantsEnum;
using static TTTH.Common.Enums.MessageEnum;

namespace TrungTamTinHoc.Areas.Home.Controllers
{
    public class TinTucController : Controller
    {
        // GET: Home/TinTuc
        /// <summary>
        /// Các điều hướng dành cho các trang liên quan đến Tin Tức
        /// Author       :   HangNTD - 03/07/2018 - create
        /// </summary>
        /// <remarks>
        /// Package      :   Home
        /// Copyright    :   Team Noname
        /// Version      :   1.0.0
        /// </remarks>
        public ActionResult TinTuc(NewsConditionSearch condition)
        {
            TinTucList listAllNews = new TinTucModel().GetAllListNews(condition);
            if (Request.IsAjaxRequest())
            {
                return View("Partial/_listTinTuc", listAllNews);
            }
            return View("TinTuc", listAllNews);
        }

        /// <summary>
        /// Lấy danh sách Tin tức trả về view chitiettintuc
        /// Author:     HangNTD - 03/07/2018 - create
        /// </summary>
        /// <param name="id">id ở url lấy xuống</param>
        /// <returns></returns>
        public ActionResult ChiTietTinTuc(string id)
        {
            ChiTietTinTuc chiTietTinTuc = new TinTucModel().LoadChiTietTinTuc(id);
            if (chiTietTinTuc != null)
            {
                return View("ChiTietTinTuc", chiTietTinTuc);
            }
            else
            {
                return RedirectToRoute("TinTuc");
            }
        }
        

        /// <summary>
        /// Lấy thêm comment.
        /// Author: HangNTD - 13/07/2018 - create
        /// </summary>
        /// <param name=""></param>
        /// <returns>Chỗi Json chứa comment ???</returns>
        /// </remarks>
        /// 
        public ActionResult SeeMoreComment(int idTintuc, int countComment, int idCommentParrent,int countCommentChild)
        {
            try
            {
                if(idCommentParrent !=0 )
                {
                    return View("Partial/_listChildCommentTinTuc", new TinTucModel().GetListCommentReply(idTintuc,countCommentChild,idCommentParrent));
                }
                return View("Partial/_listCommentTinTuc", new TinTucModel().GetListComment(idTintuc,countComment));
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }
        
        /// <summary>
        /// Lấy comment-rating người dùng gửi lên.
        /// Author: HangNTD - 13/07/2018 - create
        /// </summary>
        /// <param name=""></param>
        /// <returns>Chỗi Json chứa comment</returns>
        /// </remarks>
        
        public ActionResult ReplyComment()
        {
            try
            {
                return View("Partial/_replyCommentTinTuc", XacThuc.GetUser());
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }

        public ActionResult SendComment(int idUser, int idTintuc, string content, int Rating)
        {
            try
            {
                return View("Partial/_comment", new TinTucModel().SendComment(idUser, idTintuc, content, Rating));
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }

        /// <summary>
        /// Lấy comment-reply người dùng gửi lên.
        /// Author: HangNTD - 13/07/2018 - create
        /// </summary>
        /// <param name=""></param>
        /// <returns>Chỗi Json chứa comment</returns>
        /// </remarks>
        public ActionResult SendCommentReply(int idUser, int idTintuc, string content, int idComment)
        {
            try
            {
                return View("Partial/_commentReply", new TinTucModel().SendCommentReply(idUser, idTintuc, content, idComment));
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }
    }
}