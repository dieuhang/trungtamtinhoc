﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TrungTamTinHoc.Areas.Course.Models.Schema;
using TTTH.Common;
using TTTH.DataBase;
using TblKhoaHoc = TTTH.DataBase.Schema.KhoaHoc;
using TblLopHoc = TTTH.DataBase.Schema.LopHoc;
using TblUser = TTTH.DataBase.Schema.User;
using TblDangKyLopHoc = TTTH.DataBase.Schema.DangKyLopHoc;
using static TTTH.Common.Enums.ConstantsEnum;
using TblGroupOfAccount = TTTH.DataBase.Schema.GroupOfAccount;
using TblAccount = TTTH.DataBase.Schema.Account;
using TblCauHinh = TTTH.DataBase.Schema.CauHinh;
using TrungTamTinHoc.Areas.Home.Models;

namespace TrungTamTinHoc.Areas.Course.Models
{
    /// <summary>
    /// Class chứa các phương thức liên quan đến việc đăng ký học
    /// Author       :   AnTM - 07/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   Home.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class DangKyHocModel
    {
        private DataContext context;

        public DangKyHocModel()
        {
            context = new DataContext();
        }

        /// <summary>
        /// Lấy danh sách các khóa học được phép hiển thị từ DB theo ngôn ngữ.
        /// Author       :   AnTM - 07/07/2018 - create
        /// </summary>
        /// <returns>Danh sách khóa học có trong DB</returns>
        public ThongTinUser LayThongTinUserHienThi(int? idUser)
        {
            try
            {
                return context.User.Where(x => x.Id == idUser && !x.DelFlag).Select(x => new ThongTinUser()
                {
                    Ho = x.Ho,
                    Ten = x.Ten,
                    NgaySinh = x.NgaySinh,
                    GioiTinh = x.GioiTinh,
                    DiaChi = x.DiaChi,
                    IdXa = x.IdXa != null ? x.IdXa : "00001",
                    IdHuyen = x.Xa.IdHuyen,
                    IdTinh = x.Xa.Huyen.IdTinh,
                    IdNguoiGiamHo = x.IdNguoiGiamHo,
                    SoDienThoai = x.SoDienThoai,
                    SoCMND = x.CMND,
                    Email = x.Account.FirstOrDefault(y => !y.DelFlag).Email
                }).FirstOrDefault();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Lấy danh sách các khóa học được phép hiển thị từ DB theo ngôn ngữ.
        /// Author       :   AnTM - 07/07/2018 - create
        /// </summary>
        /// <returns>Danh sách khóa học có trong DB</returns>
        public List<KhoaHocHienThi> LayKhoaHocHienThi()
        {
            try
            {
                string lang = Common.GetLang();
                return context.KhoaHoc.Include("KhoaHocTrans").Where(x => !x.DelFlag && x.LopHoc.Count > 0 && x.KhoaHocTrans.Count > 0)
                    .Select(x => new KhoaHocHienThi
                    {
                        TenKhoaHoc = x.KhoaHocTrans.FirstOrDefault(y => y.Lang == lang && y.TenKhoaHoc != null).TenKhoaHoc,
                        IDKhoaHoc = x.Id
                    }).ToList();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Lấy danh sách các khóa học được phép hiển thị từ DB theo id khóa học và theo ngôn ngữ
        /// Author       :   AnTM - 07/07/2018 - create
        /// </summary>
        /// <param name="idKhoaHoc">ID khóa học</param>
        /// <returns>Danh sách lớp học có trong DB</returns>
        public List<LopHocHienThi> LayLopHocHienThiTheoIDKhoaHoc(int idKhoaHoc)
        {
            try
            {
                string lang = Common.GetLang();
                if (idKhoaHoc != -1)
                {
                    return context.KhoaHoc.FirstOrDefault(x => x.Id == idKhoaHoc && !x.DelFlag)?.LopHoc
                        .Where(x => x.ChoPhepDangKy && !x.DelFlag && x.LopHocTrans.Count > 0).Select(x => new LopHocHienThi()
                        {
                            IDLopHoc = x.Id,
                            TenLopHoc = x.LopHocTrans.FirstOrDefault(q => q.Lang == lang && q.TenLop != null)?.TenLop
                        }).ToList();
                }
                return context.KhoaHoc.FirstOrDefault(x => !x.DelFlag)?.LopHoc
                    .Where(x => x.ChoPhepDangKy && !x.DelFlag && x.LopHocTrans.Count > 0).Select(x => new LopHocHienThi()
                    {
                        IDLopHoc = x.Id,
                        TenLopHoc = x.LopHocTrans.FirstOrDefault(q => q.Lang == lang && q.TenLop != null)?.TenLop
                    }).ToList();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Lấy danh sách tất cả các tỉnh được phép hiển thị từ DB
        /// Author       :   AnTM - 08/07/2018 - create
        /// </summary>
        /// <returns>Danh sách tỉnh có trong DB</returns>
        public List<TinhHienThi> LayTatcaTinh()
        {
            try
            {
                return context.Tinh.Where(x => !x.DelFlag).Select(x => new TinhHienThi()
                {
                    IDTinh = x.Id,
                    TenTinh = x.TenTinh
                }).OrderBy(x => x.TenTinh).ToList();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Lấy danh sách các huyện theo id tỉnh
        /// Author       :   AnTM - 07/07/2018 - create
        /// </summary>
        /// <param name="idTinh">ID khóa học</param>
        /// <returns>Danh sách lớp học có trong DB</returns>
        public List<HuyenHienThi> LayCacHuyenTheoIdTinh(string idTinh = "01")
        {
            try
            {
                return context.Huyen.Where(x => !x.DelFlag && x.IdTinh == idTinh).Select(x => new HuyenHienThi()
                {
                    IDHuyen = x.Id,
                    TenHuyen = x.TenHuyen
                }).OrderBy(x => x.TenHuyen).ToList();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Lấy danh sách các xã theo id huyện
        /// Author       :   AnTM - 07/07/2018 - create
        /// </summary>
        /// <param name="idHuyen">ID khóa học</param>
        /// <returns>Danh sách lớp học có trong DB</returns>
        public List<XaHienThi> LayCacXaHienThiTheoIdHuyen(string idHuyen = "001")
        {
            try
            {
                return context.Xa.Where(x => !x.DelFlag && x.IdHuyen == idHuyen).Select(x => new XaHienThi()
                {
                    IDXa = x.Id,
                    TenXa = x.TenXa
                }).OrderBy(x => x.TenXa).ToList();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Lấy ID khóa học được selected trên view đăng ký khóa học từ DB.
        /// Author       :   AnTM - 07/07/2018 - create
        /// </summary>
        /// <returns>ID khóa học đc selected</returns>
        public int GetIdKhoaHocIsSelected(string beautyId)
        {
            try
            {
                TblKhoaHoc kh = context.KhoaHoc.FirstOrDefault(x => x.BeautyId == beautyId && !x.DelFlag);
                if (kh != null)
                {
                    return kh.Id;
                }
                return -1;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Đăng ký lớp học đã chọn theo thông tin từ client gửi lên, nếu user chưa tồn tại thì tạo mới user
        /// Author       :   AnTM - 08/07/2018 - create
        /// </summary>
        /// <param name="infomation">Thông tin đăng ký lớp học</param>
        /// <returns>Thông tin về việc tạo tài khoản thành công hay thất bại</returns>
        public ResponseInfo DangKyKhoaHoc(ThongTinDangKyHoc infomation)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                TblCauHinh cauHinh = Common.LayCauHinh();
                ResponseInfo result = new ResponseInfo();
                TblLopHoc lopHoc = context.LopHoc.FirstOrDefault(x => x.Id == infomation.IDLopHoc && !x.DelFlag);
                if (lopHoc == null)
                {
                    result.Code = 202;
                    result.MsgNo = 43;
                    return result;
                }
                TblDangKyLopHoc dangKyLopHoc = new TblDangKyLopHoc()
                {
                    TenHocVien = infomation.TenHocVien,
                    HoHocVien = infomation.HoHocVien,
                    GioiTinhHocVien = infomation.GioiTinhHocVien,
                    NgaySinhHocVien = infomation.NgaySinhHocVien,
                    DiaChiHocVien = infomation.DiaChiHocVien,
                    IdXaHocVien = infomation.IdXaHocVien,
                    SoDienThoaiHocVien = infomation.SoDienThoaiHocVien,
                    EmailHocVien = infomation.EmailHocVien,
                    CMNDHocVien = infomation.SoCMNDHocVien,
                    TenGiamHo = infomation.TenGiamHo,
                    HoGiamHo = infomation.HoGiamHo,
                    GioiTinhGiamHo = infomation.GioiTinhGiamHo,
                    NgaySinhGiamHo = infomation.NgaySinhGiamHo,
                    DiaChiGiamHo = infomation.DiaChiGiamHo,
                    IdXaGiamHo = infomation.IdXaGiamHo,
                    CMNDGiamHo = infomation.SoCMNDGiamHo,
                    EmailGiamHo = infomation.EmailGiamHo,
                    SoDienThoaiGiamHo = infomation.SoDienThoaiGiamHo,
                    ThoiGianDangKy = DateTime.Now,
                    GhiChu = infomation.GhiChu,
                    TrangThaiDangKy = (int)TrangThaiDangKy.DangKyMoi
                };
                TblUser hocVien = context.Account.FirstOrDefault(x => x.Email.Equals(infomation.EmailHocVien) && !x.DelFlag)?.User;
                if (hocVien == null)
                {
                    hocVien = new TblUser()
                    {
                        Ho = infomation.HoHocVien,
                        Ten = infomation.TenHocVien,
                        Avatar = Common.defaultAvata,
                        GioiTinh = infomation.GioiTinhHocVien,
                        NgaySinh = infomation.NgaySinhHocVien,
                        CMND = infomation.SoCMNDHocVien,
                        DiaChi = infomation.DiaChiHocVien,
                        SoDienThoai = infomation.SoCMNDHocVien,
                        IdXa = infomation.IdXaHocVien
                    };
                    context.User.Add(hocVien);
                    if (infomation.CoGiamHo)
                    {
                        TblUser giamHo = new TblUser()
                        {
                            Ho = infomation.HoGiamHo,
                            Ten = infomation.TenGiamHo,
                            Avatar = Common.defaultAvata,
                            GioiTinh = infomation.GioiTinhGiamHo,
                            NgaySinh = infomation.NgaySinhGiamHo,
                            SoDienThoai = infomation.SoDienThoaiGiamHo,
                            CMND = infomation.SoCMNDGiamHo,
                            DiaChi = infomation.DiaChiGiamHo,
                            IdXa = infomation.IdXaGiamHo
                        };
                        giamHo.NguoiSeGiamHo.Add(hocVien);
                        context.User.Add(giamHo);
                    }
                }
                else
                {
                    if (XacThuc.GetIdUser() != hocVien.Id)
                    {
                        result.Code = 202;
                        result.MsgNo = 46;
                        return result;
                    }
                }
                hocVien.DangKyLopHoc.Add(dangKyLopHoc);
                lopHoc.DangKyLopHoc.Add(dangKyLopHoc);
                context.DangKyLopHoc.Add(dangKyLopHoc);
                context.SaveChanges();

                //Đăng ký tài khoản
                if (infomation.EmailHocVien != null && infomation.Username != null && infomation.Password != null && infomation.Password == infomation.ConfirmPassword)
                {
                    TblAccount account = context.Account.FirstOrDefault(x => x.Username == infomation.Username && !x.DelFlag);
                    if (account == null)
                    {
                        // Kiểm tra xem email đã tồn tại hay chưa
                        account = context.Account.FirstOrDefault(x => x.Email == infomation.EmailHocVien && !x.DelFlag);
                        if (account == null)
                        {
                            // Tạo tài khoản đăng nhập cho user
                            account = new TblAccount
                            {
                                Username = infomation.Username,
                                Password = BaoMat.GetMD5(infomation.Password),
                                Email = infomation.EmailHocVien,
                                TokenActive = Common.GetToken(infomation.Username),
                                IsActived = false,
                                IsActiveEmail = false,
                                TimeOfToken = DateTime.Now.AddHours(cauHinh.ThoiGianTonTaiToken),
                                SoLanDangNhapSai = 0,
                                KhoaTaiKhoanDen = DateTime.Now
                            };
                            // Cho tài khoản thuộc vào 1 group
                            account.GroupOfAccount.Add(new TblGroupOfAccount
                            {
                                IdGroup = (int)GroupAccount.User
                            });
                            hocVien.Account.Add(account);
                            context.SaveChanges();
                            // Tiến hành gửi mail
                            new RegisterModel().SendEmail(account);
                            result.ThongTinBoSung1 = BaoMat.Base64Encode(account.TokenActive);
                            result.MsgNo = 47;
                        }
                        else
                        {
                            result.Code = 202;
                            result.MsgNo = 37;
                        }
                    }
                    else
                    {
                        result.Code = 202;
                        result.MsgNo = 36;
                    }
                }

                transaction.Commit();
                return result;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }
    }
}