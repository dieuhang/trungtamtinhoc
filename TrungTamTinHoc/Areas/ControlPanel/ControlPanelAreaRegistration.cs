﻿using System.Web.Mvc;

namespace TrungTamTinHoc.Areas.ControlPanel
{
    public class ControlPanelAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ControlPanel";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ListOfSlide",
                "control-panel/list-of-slide",
                new { controller = "SlideManagement", action = "ListOfSlide", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "DeleteSlides",
                "control-panel/list-of-slide/delete-slide",
                new { controller = "SlideManagement", action = "DeleteSlides", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "UpdateShowSlide",
                "control-panel/list-of-slide/update-show-slide",
                new { controller = "SlideManagement", action = "UpdateShow", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "CreateSlide",
                "control-panel/create-slide",
                new { controller = "SlideManagement", action = "ViewCreateSlide", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "EditSlide",
                "control-panel/edit-slide/{id}",
                new { controller = "SlideManagement", action = "ViewEditSlide", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "SaveSlide",
                "control-panel/slide-master/save-slide",
                new { controller = "SlideManagement", action = "SaveSlide", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "ReferSlideWithLang",
                "control-panel/slide-master/refer-slide-with-lang",
                new { controller = "SlideManagement", action = "ReferSlideWithLang", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "UpdateSlideTranslate",
                "control-panel/slide-master/update-slide-translate",
                new { controller = "SlideManagement", action = "UpdateSlideTranslate", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "ListOfCourseRegister",
                "control-panel/list-of-course-register",
                new { controller = "CourseRegisterManagement", action = "ListOfCourseRegister", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "DetailCourseRegister",
                "control-panel/detail-course-register",
                new { controller = "CourseRegisterManagement", action = "LayChiTietThongTinDangKyHoc", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "getClassesByIdCourseInManagerment",
                "control-panel/get-classes-by-id-khoa-hoc",
                new { controller = "CourseRegisterManagement", action = "LayDanhSachLopHocTheoIdKhoaHoc", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "ListOfNew",
                "control-panel/list-of-new",
                new { controller = "NewManagement", action = "ListOfNew", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "UpdateShowNew",
                "control-panel/list-of-new/update-show-new",
                new { controller = "NewManagement", action = "UpdateShow", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "DeleteNews",
                "control-panel/list-of-new/delete-new",
                new { controller = "NewManagement", action = "DeleteNews", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "CreateNews",
                "control-panel/create-news",
                new { controller = "NewManagement", action = "ViewCreateNews", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "EditNews",
                "control-panel/edit-news/{id}",
                new { controller = "NewManagement", action = "ViewEditNews", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "SaveNews",
                "control-panel/news-master/save-news",
                new { controller = "NewManagement", action = "SaveNews", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "ReferNewsWithLang",
                "control-panel/news-master/refer-news-with-lang",
                new { controller = "NewManagement", action = "ReferNewsWithLang", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "UpdateNewsTranslate",
                "control-panel/news-master/update-news-translate",
                new { controller = "NewManagement", action = "UpdateNewsTranslate", id = UrlParameter.Optional }
            );
            context.MapRoute(
            "CheckExistUrlNews",
            "control-panel/news-master/check-exist",
            new { controller = "NewManagement", action = "CheckExistUrlNews", id = UrlParameter.Optional }
        );
            //context.MapRoute(
            //    "DeleteSlides",
            //    "control-panel/list-of-slide/delete-slide",
            //    new { controller = "SlideManagement", action = "DeleteSlides", id = UrlParameter.Optional }
            //);
            //context.MapRoute(
            //    "UpdateShowSlide",
            //    "control-panel/list-of-slide/update-show-slide",
            //    new { controller = "SlideManagement", action = "UpdateShow", id = UrlParameter.Optional }
            //);


            context.MapRoute(
                "ListOfCourse",
                "control-panel/list-of-courses",
                new { controller = "CourseManagement", action = "ListOfCourse", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "DeleteCourse",
                "control-panel/list-of-courses/delete-course",
                new { controller = "CourseManagement", action = "DeleteCourses", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "UpdateShowCourse",
                "control-panel/list-of-courses/update-show-course",
                new { controller = "CourseManagement", action = "UpdateShow", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "CreateCourse",
                "control-panel/create-course",
                new { controller = "CourseManagement", action = "ViewCreateCourse", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "EditCourse",
                "control-panel/edit-course/{id}",
                new { controller = "CourseManagement", action = "ViewEditCourse", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "SaveCourse",
                "control-panel/course-master/save-course",
                new { controller = "CourseManagement", action = "SaveCourse", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "CheckExistBeautyId",
                "control-panel/course-master/checkExistBeautyId",
                new { controller = "CourseManagement", action = "CheckExistBeautyId", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "ReferCourseWithLang",
                "control-panel/course-master/refer-course-with-lang",
                new { controller = "CourseManagement", action = "ReferCourseWithLang", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "UpdateCourseTranslate",
                "control-panel/slide-master/update-course-translate",
                new { controller = "CourseManagement", action = "UpdateCourseTranslate", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "ListOfEmailSubscription",
                "control-panel/list-of-email",
                new { controller = "EmailSubscriptionManagement", action = "ListOfEmailSubscription", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "DeleteEmailSubscriptions",
                "control-panel/list-of-email/delete-email",
                new { controller = "EmailSubscriptionManagement", action = "DeleteEmailSubscriptions", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "ListOfThingsAchieved",
                "control-panel/list-of-things-achieved",
                new { controller = "ThingsAchievedManagement", action = "ListOfThingsAchieved", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "DeleteThingsAchieved",
                "control-panel/list-of-things-achieved/delete-things-achieved",
                new { controller = "ThingsAchievedManagement", action = "DeleteThingsAchieved", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "UpdateShowThingsAchieved",
                "control-panel/list-of-things-achieved/update-show-things-achieved",
                new { controller = "ThingsAchievedManagement", action = "UpdateShow", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "CreateThingAchieved",
                "control-panel/create-thing-achieved",
                new { controller = "ThingsAchievedManagement", action = "ViewCreateThingAchieved", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "EditThingsAchieved",
                "control-panel/edit-things-achieved/{id}",
                new { controller = "ThingsAchievedManagement", action = "ViewEditThingsAchieved", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "SaveThingAchieved",
                "control-panel/things-achieved-master/save-things-achieved",
                new { controller = "ThingsAchievedManagement", action = "SaveThingAchieved", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "ReferThingsAchievedWithLang",
                "control-panel/things-achieved-master/refer-things-achieved-with-lang",
                new { controller = "ThingsAchievedManagement", action = "ReferThingsAchievedWithLang", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "UpdateThingsAchievedTranslate",
                "control-panel/things-achieved-master/update-things-achieved-translate",
                new { controller = "ThingsAchievedManagement", action = "UpdateThingsAchievedTranslate", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "UpdateShowContact",
                "control-panel/list-of-contact/update-show",
                new { controller = "ContactManagement", action = "UpdateShow", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "ListOfContact",
                "control-panel/list-of-contact",
                new { controller = "ContactManagement", action = "ListOfContact", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "DeleteContacts",
                "control-panel/list-of-contact/delete-contact",
                new { controller = "ContactManagement", action = "DeleteContacts", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "UpdateShowCourseRegister",
                "control-panel/list-of-courseRegister/update-show-courseRegister",
                new { controller = "CourseRegisterManagement", action = "UpdateShow", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "DeleteCourseRegister",
                "control-panel/list-of-courseRegister/delete-courseRegister",
                new { controller = "CourseRegisterManagement", action = "DeleteCourseRegister", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "ListOfUser",
                "control-panel/list-of-users",
                new { controller = "UserManagement", action = "ListOfUser", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "ResetPasswordAccount",
                "control-panel/list-of-users/reset-password-user",
                new { controller = "UserManagement", action = "ResetPasswordAccount", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "adminCheckExistAccount",
                "control-panel/list-of-user/check-exist-account",
                new { controller = "UserManagement", action = "CheckExistAccount", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "CreateUser",
                "control-panel/create-user",
                new { controller = "UserManagement", action = "ViewCreateUser", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "EditUser",
                "control-panel/edit-user/{id}",
                new { controller = "UserManagement", action = "ViewEditUser", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "SaveUser",
                "control-panel/user-master/save-user",
                new { controller = "UserManagement", action = "SaveUser", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "ListOfErrorMsg",
                "control-panel/list-of-errorMgs",
                new { controller = "ErrorMsgManagement", action = "ListOfErrorMsg", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "UpdateType",
                "control-panel/list-of-errorMsg/update-Type-errorMsg",
                new { controller = "ErrorMsgManagement", action = "UpdateType", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "DeleteErrorMsg",
                "control-panel/list-of-errorMsg/delete-errorMsg",
                new { controller = "ErrorMsgManagement", action = "DeleteErrorMsg", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "ListOfErrorMsgUpdateFile",
                "control-panel/list-of-errorMsg/updateFile",
                new { controller = "ErrorMsgManagement", action = "UpdateFile", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "CreateErrorMsg",
                "control-panel/create-errorMsg",
                new { controller = "ErrorMsgManagement", action = "ViewCreateErrorMsg", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "EditErrorMsg",
                "control-panel/edit-errorMsg/{id}",
                new { controller = "ErrorMsgManagement", action = "ViewEditErrorMsg", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "SaveErrorMsg",
                "control-panel/errorMsg-master/save-errorMsg",
                new { controller = "ErrorMsgManagement", action = "SaveErrorMsg", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "UpdateErrorMsgTranslate",
                "control-panel/errorMsg-master/update-errorMsg-translate",
                new { controller = "ErrorMsgManagement", action = "UpdateErrorMsgTranslate", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "ReferErrorMsgWithLang",
                "control-panel/errorMsg-master/refer-errorMsg-with-lang",
                new { controller = "ErrorMsgManagement", action = "ReferErrorMsgWithLang", id = UrlParameter.Optional }
            );

            context.MapRoute(
               "ListOfGroup",
               "control-panel/list-of-group",
               new { controller = "GroupManagement", action = "ListOfGroup", id = UrlParameter.Optional }
           );

            context.MapRoute(
                "UpdateShowGroup",
                "control-panel/list-of-group/update-show-group",
                new { controller = "GroupManagement", action = "UpdateShow", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "DeleteGroups",
                "control-panel/list-of-group/delete-group",
                new { controller = "GroupManagement", action = "DeleteGroups", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "CreateGroup",
                "control-panel/create-group",
                new { controller = "GroupManagement", action = "ViewCreateGroup", id = UrlParameter.Optional }
            );

            context.MapRoute(
               "EditGroup",
               "control-panel/edit-group/{id}",
               new { controller = "GroupManagement", action = "ViewEditGroup", id = UrlParameter.Optional }
           );

            context.MapRoute(
                "SaveGroup",
                "control-panel/group-master/save-group",
                new { controller = "GroupManagement", action = "SaveGroup", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "UpdateGroupTranslate",
                "control-panel/group-master/update-group-translate",
                new { controller = "GroupManagement", action = "UpdateGroupTranslate", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "ReferGroupWithLang",
                "control-panel/group-master/refer-group-with-lang",
                new { controller = "GroupManagement", action = "ReferGroupWithLang", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "ListOfPhoto",
                "control-panel/list-of-photo",
                new { controller = "PhotoManagement", action = "ListOfPhoto", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "DeletePhotos",
                "control-panel/list-of-photo/delete-photo",
                new { controller = "PhotoManagement", action = "DeletePhotos", id = UrlParameter.Optional }
            );


            context.MapRoute(
                "CreatePhotos",
                "control-panel/create-photo",
                new { controller = "PhotoManagement", action = "ViewCreatePhotos", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "EditPhotos",
                "control-panel/edit-photos/{id}",
                new { controller = "PhotoManagement", action = "ViewEditPhotos", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "SavePhoto",
                "control-panel/photos-master/save-photos",
                new { controller = "PhotoManagement", action = "SavePhotos", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "UpdatePhotoTranslate",
                "control-panel/photo-master/update-photo-translate",
                new { controller = "PhotoManagement", action = "UpdatePhotoTranslate", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "ReferPhotoWithLang",
                "control-panel/photo-master/refer-photo-with-lang",
                new { controller = "PhotoManagement", action = "ReferPhotoWithLang", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "ListOfPermission",
                "control-panel/list-of-permission",
                new { controller = "PermissionManagement", action = "ListOfPermission", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "UpdateEnablePermission",
                "control-panel/update-enable-permission",
                new { controller = "PermissionManagement", action = "UpdateEnable", id = UrlParameter.Optional }
            );


            context.MapRoute(
                "updateSetting",
                "control-panel/update-setting",
                new { controller = "SettingManagement", action = "UpdateSetting", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "updateOtherSettings",
                "control-panel/update-Other-settings/{id}",
                new { controller = "SettingManagement", action = "UpdateOtherSettings", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "saveSetting",
                "control-panel/save-update-setting",
                new { controller = "SettingManagement", action = "SaveSetting", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "saveOtherSettings",
                "control-panel/save-update-Other-settings",
                new { controller = "SettingManagement", action = "SaveOtherSettings", id = UrlParameter.Optional }
            );
            
            context.MapRoute(
                "updateOtherSettingsTranslate",
                "control-panel/update-Other-settings-translate",
                new { controller = "SettingManagement", action = "UpdateOtherSettingsTranslate", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "updateSettingTranslate",
                "control-panel/update-setting-translate",
                new { controller = "SettingManagement", action = "UpdateSettingTranslate", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "ReferSettingWithLang",
                "control-panel/setting-translate",
                new { controller = "SettingManagement", action = "ReferSettingWithLang", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "ReferOtherSettingsWithLang",
                "control-panel/OtherSettings-translate",
                new { controller = "SettingManagement", action = "ReferOtherSettingsWithLang", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "EditConfiguration",
                "control-panel/edit-configuration",
                new { controller = "ConfigurationManagement", action = "EditConfiguration", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "SaveConfiguration",
                "control-panel/save-configuration",
                new { controller = "ConfigurationManagement", action = "SaveConfiguration", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "ListOfClass",
                "control-panel/list-of-class",
                new { controller = "ClassManagement", action = "ListOfClass", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "DeleteClasses",
                "control-panel/list-of-class/delete-classes",
                new { controller = "ClassManagement", action = "DeleteClasses", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "CreateClass",
                "control-panel/create-class",
                new { controller = "ClassManagement", action = "ViewCreateClass", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "EditClass",
                "control-panel/edit-class/{id}",
                new { controller = "ClassManagement", action = "ViewEditClass", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "SaveClass",
                "control-panel/class-master/save-class",
                new { controller = "ClassManagement", action = "SaveClass", id = UrlParameter.Optional }
            );


            context.MapRoute(
                "ReferClassWithLang",
                "control-panel/class-master/refer-class-with-lang",
                new { controller = "ClassManagement", action = "ReferClassWithLang", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "UpdateClassTranslate",
                "control-panel/slide-master/update-class-translate",
                new { controller = "ClassManagement", action = "UpdateClassTranslate", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "ListOfSchedule",
                "control-panel/list-of-schedule",
                new { controller = "ScheduleManagement", action = "ListOfSchedule", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "DeleteSchedule",
                "control-panel/list-of-schedule/delete-schedule",
                new { controller = "ScheduleManagement", action = "DeleteSchedule", id = UrlParameter.Optional }
            );


            context.MapRoute(
                "CreateSchedule",
                "control-panel/create-schedule",
                new { controller = "ScheduleManagement", action = "ViewCreateSchedule", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "EditSchedule",
                "control-panel/edit-schedule/{id}",
                new { controller = "ScheduleManagement", action = "ViewEditSchedule", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "SaveSchedule",
                "control-panel/schedule-master/save-schedule",
                new { controller = "ScheduleManagement", action = "SaveSchedule", id = UrlParameter.Optional }
            );


            context.MapRoute(
                "ControlPanel_default",
                "control-panel/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );


        }
    }
}