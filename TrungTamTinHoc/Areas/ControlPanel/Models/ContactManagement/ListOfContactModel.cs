﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TrungTamTinHoc.Areas.ControlPanel.Models.ContactManagement.Schema;
using TTTH.Common;
using TTTH.DataBase;
using Z.EntityFramework.Plus;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.ContactManagement
{
    public class ListOfContactModel
    {
        DataContext context;
        public ListOfContactModel()
        {
            context = new DataContext();
        }
        public ListOfContact GetListOfContact(ContactConditionSearch condition)
        {
            try
            {
                if (condition == null)
                {
                    condition = new ContactConditionSearch();
                }
                string lang = Common.GetLang();

                ListOfContact listOfContact = new ListOfContact();
                listOfContact.Paging = new Paging(context.Contact.Count(x =>
                      (condition.SearchName == null || (condition.SearchName != null && x.HoTen.Contains(condition.SearchName)))
                    && (condition.SearchSdt == null || (condition.SearchSdt != null && x.SoDienThoai.Contains(condition.SearchSdt)))
                    && (condition.SearchEmail == null || (condition.SearchEmail != null && x.Email.Contains(condition.SearchEmail)))
                    && (condition.tuNgay == null || (condition.tuNgay != null && x.Created_at >= condition.tuNgay.Value))
                    && (condition.toiNgay == null || (condition.toiNgay != null && x.Created_at <= condition.toiNgay.Value))
                    && (condition.TrangThai == 0 || (condition.TrangThai != 0 && x.IdTrangThai == condition.TrangThai))
                    && !x.DelFlag), condition.CurrentPage, condition.PageSize);

                listOfContact.ContactList = context.Contact.Where(x =>
                       (condition.SearchName == null || (condition.SearchName != null && x.HoTen.Contains(condition.SearchName)))
                    && (condition.SearchSdt == null || (condition.SearchSdt != null && x.SoDienThoai.Contains(condition.SearchSdt)))
                    && (condition.SearchEmail == null || (condition.SearchEmail != null && x.Email.Contains(condition.SearchEmail)))
                    && (condition.tuNgay == null || (condition.tuNgay != null && x.Created_at >= condition.tuNgay.Value))
                    && (condition.toiNgay == null || (condition.toiNgay != null && x.Created_at <= condition.toiNgay.Value))
                    && (condition.TrangThai == 0 || (condition.TrangThai != 0 && x.IdTrangThai == condition.TrangThai))
                    && !x.DelFlag).OrderBy(x => x.IdTrangThai).ThenByDescending(x => x.Id)
                    .Skip((listOfContact.Paging.CurrentPage - 1) * listOfContact.Paging.NumberOfRecord)
                    .Take(listOfContact.Paging.NumberOfRecord).Select(x => new Contact
                    {
                        Id = x.Id,
                        HoTen = x.HoTen,
                        Sdt = x.SoDienThoai,
                        NoiDung = x.NoiDung,
                        idTrangThai = x.IdTrangThai,
                        Email = x.Email
                    }).ToList();
                listOfContact.Condition = condition;
                listOfContact.tenTrangThai = context.TrangThaiTrans.Where(x => x.Lang == lang && (x.Id == 1 || x.Id == 2 || x.Id == 3) && !x.DelFlag).Select(x => new TrangThai
                {
                    Id = x.Id,
                    tenTrangThai = x.TenTrangThai
                }).ToList();
                return listOfContact;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Cập nhật trạng thái hiển thị của contact trên trang chủ.
        /// Author       :   HoangNM - 23/07/2018 - create
        /// </summary>
        /// <param name="id">id của contact sẽ được cập nhật trạng thái</param>
        /// <param name="newIdTrangThai">idTrangThai của contact vừa được cập nhật</param>
        public void UpdateShow(int id, int newIdTrangThai)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                context.Contact.Where(x => x.Id == id && !x.DelFlag)
                .Update(x => new TTTH.DataBase.Schema.Contact
                {
                    IdTrangThai = newIdTrangThai
                });
                context.SaveChanges();
                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        /// <summary>
        /// Xóa các contact trong DB.
        /// Author       :   HoangNM - 22/07/2018 - create
        /// </summary>
        /// <param name="ids">Danh sách id của các contact sẽ xóa</param>
        public void DeleteContacts(List<int> ids)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                context.Contact.Where(x => ids.Contains(x.Id)).Delete();
                context.SaveChanges();
                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        /// <summary>
        /// get thông tin liên hệ mới
        /// Author       :   HoangNM - 12/08/2018 - create
        /// </summary>
        public int getLienHeMoi()
        {
            return context.Contact.Where(x => x.IdTrangThai == 1).Count();
        }
    }
}