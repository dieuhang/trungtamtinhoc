﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.CourseRegisterManagement.Schema
{
    /// <summary>
    /// Class chứa tất cả dữ liệu cho việc hiển thị các trạng thái ở trang quản lý đăng ký học của admin
    /// Author       :   AnTM -20/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   Home.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class TrangThaiDangKy
    {
        public int IDTrangThai { get; set; }
        public string TenTrangThai { get; set; }
    }
}