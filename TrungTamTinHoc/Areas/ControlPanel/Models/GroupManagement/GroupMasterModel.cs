﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TrungTamTinHoc.Areas.ControlPanel.Models.GroupManagement.Schema;
using TTTH.Common;
using TTTH.DataBase;
using Z.EntityFramework.Plus;
using static TTTH.Common.Enums.ConstantsEnum;
using static TTTH.Common.Enums.MessageEnum;
using TblGroup = TTTH.DataBase.Schema.Group;
using TblGroupTrans = TTTH.DataBase.Schema.GroupTrans;
using TblChucNang = TTTH.DataBase.Schema.ChucNang;
using TblPermission = TTTH.DataBase.Schema.Permission;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.GroupManagement
{
    /// <summary>
    /// Class chứa các phương thức liên quan đến việc xử lý group
    /// Author       :   HangNTD - 17/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class GroupMasterModel
    {
        DataContext context;
        List<string> typeFiles;
        public GroupMasterModel()
        {
            context = new DataContext();
            typeFiles = new List<string>();
            typeFiles.Add(".jpg");
            typeFiles.Add(".png");
        }
        /// <summary>
        /// Lấy thông tin của 1 group
        /// Author       :   HangNTD - 17/08/2018 - create
        /// </summary>
        /// <param name="id">id của group sẽ được hiển thị</param>
        /// <returns>Thông tin của group</returns>
        public GroupMaster LoadGroup(string id)
        {
            try
            {
                GroupMaster groupMaster = new GroupMaster();
                int idGroup = 0;
                try
                {
                    idGroup = Convert.ToInt32(id);
                }
                catch { }
                TblGroupTrans group = context.GroupTrans.FirstOrDefault(x => x.Id == idGroup && !x.DelFlag && x.Lang == Common.defaultLang);
                if (group != null)
                {
                    groupMaster.Mode = (int)ModeMaster.Update;
                    groupMaster.GroupInfo = new Group();
                    groupMaster.GroupInfo.Id = group.Id;
                    groupMaster.GroupInfo.GroupName = group.GroupName;
                    groupMaster.GroupInfo.MoTa = group.MoTa;
                    groupMaster.GroupInfo.IsDefault = group.Group.IsDefault;
                    groupMaster.GroupInfo.IsSystem = group.Group.IsSystem;
                    groupMaster.Language = context.Language.Where(x => !x.DelFlag && x.Id != Common.defaultLang)
                        .Select(x => new Lang
                        {
                            Id = x.Id,
                            Name = x.Name
                        }).OrderBy(x => x.Id).ToList();
                    if (groupMaster.Language.Count > 0)
                    {
                        string lang = groupMaster.Language[0].Id;
                        group = context.GroupTrans.FirstOrDefault(x => x.Id == idGroup && !x.DelFlag && x.Lang == lang);
                        groupMaster.GroupTrans = new Group();
                        groupMaster.GroupTrans.Id = group.Id;
                        groupMaster.GroupTrans.GroupName = group.GroupName;
                        groupMaster.GroupTrans.MoTa = group.MoTa;
                        groupMaster.GroupTrans.IsDefault = group.Group.IsDefault;
                        groupMaster.GroupTrans.IsSystem = group.Group.IsSystem;
                    }
                }
                return groupMaster;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// Tạo hoặc cập nhật lại group từ thông tin mà người dùng gửi lên
        /// Author       :   HangNTD - 17/08/2018 - create
        /// </summary>
        /// <param name="course">Các thông tin của group muốn tạo hoặc cập nhật</param>
        /// <returns>Thông tin về việc tạo hoặc cập nhật group thành công hay thất bại</returns>
        public ResponseInfo SaveGroup(Group Group)
        {
            try
            {
                if (context.Group.FirstOrDefault(x => x.Id == Group.Id && !x.DelFlag) != null)
                {
                    return UpadateGroup(Group);
                }
                return AddGroup(Group);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// Cập nhật lại  group từ thông tin mà người dùng gửi lên
        /// Author       :   HangNTD - 17/08/2018 - create
        /// </summary>
        /// <param name="course">Các thông tin của group muốn cập nhật</param>
        /// <returns>Thông tin về việc cập nhật group thành công hay thất bại</returns>
        public ResponseInfo UpadateGroup(Group group)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo response = new ResponseInfo();
                bool update = true;
                List<string> imgsDelete = new List<string>();
                if (update)
                {
                    group.IsDefault = group.CheckIsDefault == "on" ? true : false;
                    group.IsSystem = group.CheckIsSystem == "on" ? true : false;
                    context.Group.Where(x => x.Id == group.Id && !x.DelFlag)
                        .Update(x => new TTTH.DataBase.Schema.Group
                        {
                            IsDefault = group.IsDefault,
                            IsSystem = group.IsSystem
                        });
                    context.GroupTrans.Where(x => x.Id == group.Id && x.Lang == Common.defaultLang && !x.DelFlag)
                        .Update(x => new TTTH.DataBase.Schema.GroupTrans
                        {
                            GroupName = group.GroupName,
                            MoTa = group.MoTa
                        });
                    context.SaveChanges();
                }
                transaction.Commit();
                return response;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }
        /// <summary>
        /// Tạo  group từ thông tin mà người dùng gửi lên
        /// Author       :   HangNTD - 17/08/2018 - create
        /// </summary>
        /// <param name="course">Các thông tin của group muốn tạo</param>
        /// <returns>Thông tin về việc tạo  group thành công hay thất bại</returns>
        public ResponseInfo AddGroup(Group group)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo response = new ResponseInfo();

                List<Lang> listOfLang = context.Language.Where(x => !x.DelFlag).Select(x => new Lang
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList();
                group.IsDefault = group.CheckIsDefault == "on" ? true : false;
                group.IsSystem = group.CheckIsSystem == "on" ? true : false;
                group.Id = context.Group.Count() == 0 ? 1 : context.Group.Max(x => x.Id) + 1;
                context.Group.Add(new TblGroup
                {
                    Id = group.Id,
                    IsDefault = group.IsDefault,
                    IsSystem = group.IsSystem
                });
                foreach (Lang lang in listOfLang)
                {
                    context.GroupTrans.Add(new TblGroupTrans
                    {
                        Id = group.Id,
                        Lang = lang.Id,
                        GroupName = group.GroupName,
                        MoTa = group.MoTa
                    });
                }
                List<TblChucNang> listChucNang = context.ChucNang.ToList();
                foreach(TblChucNang chucNang in listChucNang)
                {
                    context.Permission.Add(new TblPermission
                    {
                        IdGroup = group.Id,
                        IdChucNang=chucNang.Id,
                        IsEnable=chucNang.Required
                    });
                }
                context.SaveChanges();
                response.ThongTinBoSung1 = group.Id + "";

                transaction.Commit();
                return response;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }
        /// <summary>
        /// Cập nhật lại group từ thông tin mà người dùng gửi lên bằng ngôn ngữ khác
        /// Author       :   HangNTD - 17/08/2018 - create
        /// </summary>
        /// <param name="course">Các thông tin ngôn ngữ khác của group muốn cập nhật</param>
        /// <returns>Thông tin về việc cập nhật group thành công hay thất bại</returns>
        public ResponseInfo UpdateGroupTranslate(Group_Trans group)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo response = new ResponseInfo();
                context.GroupTrans.Where(x => x.Id == group.Id && x.Lang == group.LanguageTrans && !x.DelFlag)
                    .Update(x => new TTTH.DataBase.Schema.GroupTrans
                    {
                        GroupName=group.GroupName,
                        MoTa=group.MoTa
                    });
                context.SaveChanges();
                response.ThongTinBoSung1 = group.Id + "";
                transaction.Commit();
                return response;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }
        /// <summary>
        /// Refer lại ngôn ngữ
        /// Author       :   HangNTD - 17/08/2018 - create
        /// </summary>
        /// <param name="id">id của news</param>
        /// <param name="lang">ngôn ngữ được chọn</param>
        /// <returns>news đã chuyển ngôn ngữ</returns>
        /// <remarks>
        /// </remarks>
        public Group_Trans ReferGroupWithLang(int id, string lang)
        {
            try
            {
                return context.GroupTrans.Where(x => x.Id == id && x.Lang == lang && !x.DelFlag)
                    .Select(x => new Group_Trans
                    {
                        GroupName = x.GroupName,
                        MoTa = x.MoTa
                    }).FirstOrDefault();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}