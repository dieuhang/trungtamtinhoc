﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace TTTH.Common.Filters
{
    /// <summary>
    /// Kiểm tra request của tài khoản hệ thống trước khi đưa đến action của controller
    /// Author       :   AnTM - 31/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   TTTH.Common
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class SystemAuthentication : ActionFilterAttribute
    {
        /// <summary>
        /// Ghi đè phương thức dùng để lọc request.
        /// Author       :   AnTM - 31/08/2018 - create
        /// </summary>
        /// <param name="filterContext">
        /// Data của 1 request.
        /// </param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //Giá trị check có phải là ajax không
            bool isAjax = filterContext.HttpContext.Request.IsAjaxRequest();
            string token = Common.GetCookie("token");
            if (token != "")
            {
                if (isAjax)
                {
                    //Nếu là ajax và không có quyền truy cập action này
                    if (XacThuc.GetAccount().GroupOfAccount.FirstOrDefault(x => !x.DelFlag).IdGroup != Common.GetIdGroupOfSystem())
                    {
                        //Chuyển qua action của controller xử lý lỗi không có quyền truy cập cho ajax
                        filterContext.Result = new RedirectToRouteResult(
                            new RouteValueDictionary(new { controller = "Error", action = "NotAccessAjax", area = "error" })
                        );
                    }
                }
                else
                {
                    //Nếu không phải là ajax và không có quyền truy cập action này
                    if (XacThuc.GetAccount().GroupOfAccount.FirstOrDefault(x => !x.DelFlag).IdGroup != Common.GetIdGroupOfSystem())
                    {
                        //Trả về trang lỗi không có quyền truy cập
                        filterContext.Result = new RedirectToRouteResult(
                            new RouteValueDictionary(new { controller = "Error", action = "NotAccess", area = "error" })
                        );
                    }
                }
            }
            base.OnActionExecuting(filterContext);
        }
    }
}