﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TTTH.Common;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.SettingManagement.Schema
{
    /// <summary>
    /// Class dùng để chứa thông tin của của phần cài đặt hệ thống
    /// Author       :   HoangNM - 18/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class SettingMaster
    {
        public Setting SettingInfo { set; get; }
        public Setting SettingTrans { set; get; }
        public List<Lang> Language { set; get; }
        public SettingMaster()
        {
            SettingInfo = null;
            SettingTrans = null;
            Language = new List<Lang>();
        }
    }

    
}