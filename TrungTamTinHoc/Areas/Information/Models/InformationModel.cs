﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TrungTamTinHoc.Areas.Information.Models.Schema;
using TTTH.DataBase;
using TblAccount = TTTH.DataBase.Schema.Account;
using TblUser = TTTH.DataBase.Schema.User;
using TblXa = TTTH.DataBase.Schema.Xa;
using TblHuyen = TTTH.DataBase.Schema.Huyen;
using TblTinh = TTTH.DataBase.Schema.Tinh;
using TTTH.Common;
using static TTTH.Common.Enums.MessageEnum;
using System.Data.Entity;
using TblTokenLogin = TTTH.DataBase.Schema.TokenLogin;
using TblBieuMau = TTTH.DataBase.Schema.BieuMau;
using static TTTH.Common.Enums.ConstantsEnum;
using EntityFramework.Extensions;

namespace TrungTamTinHoc.Areas.Information.Models
{
    /// <summary>
    /// Class chứa các phương thức liên quan đến Information
    /// Author       :   HoangNM - 04/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   Information.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class InformationModel
    {
        private DataContext context;
        List<string> typeFiles;
        public InformationModel()
        {
            context = new DataContext();
            typeFiles = new List<string>();
            typeFiles.Add(".jpg");
            typeFiles.Add(".png");
        }

        /// <summary>
        /// lấy các thông tin của account
        /// Author       :   HoangNM - 04/07/2018 - create
        /// Author       :   HoangNM - 09/07/2018 - update
        /// </summary>
        /// <param name="token">token của tài khoản</param>
        /// <returns>Trả về Account</returns>

        public ThongTinCaNhan GetThongTin(int IdAccount)
        {
            try
            {
                TblAccount account = context.Account.FirstOrDefault(x => x.Id == IdAccount && !x.DelFlag);
                ThongTinCaNhan thongtin = new ThongTinCaNhan
                {
                    Ho = account.User.Ho,
                    Ten = account.User.Ten,
                    GioiTinh = account.User.GioiTinh,
                    SDT = account.User.SoDienThoai,
                    CMND = account.User.CMND,
                    NgaySinh = account.User.NgaySinh,
                    email = account.Email,
                    DiaChi = account.User.DiaChi,
                    kichHoatemail = account.IsActiveEmail,
                    username = account.Username,
                    avatar = account.User.Avatar
                };
                if (account.User.IdXa != null)
                {
                    thongtin.IdXa = account.User.Xa.Id;
                    thongtin.IdHuyen = account.User.Xa.Huyen.Id;
                    thongtin.Tinh = account.User.Xa.Huyen.Tinh.TenTinh;
                }
                if (account.User.IdNguoiGiamHo != null)
                {
                    TblUser nguoiGiamHo = account.User.NguoiGiamHo;
                    thongtin.HoNguoiGiamHo = nguoiGiamHo.Ho;
                    thongtin.TenNguoiGiamHo = nguoiGiamHo.Ten;
                    thongtin.GioiTinhNguoiGiamHo = nguoiGiamHo.GioiTinh;
                    thongtin.SDTNguoiGiamHo = nguoiGiamHo.SoDienThoai;
                    thongtin.CMNDNguoiGiamHo = nguoiGiamHo.CMND;
                    thongtin.NgaySinhNguoiGiamHo = nguoiGiamHo.NgaySinh;
                    thongtin.DiaChiNguoiGiamHo = nguoiGiamHo.DiaChi;
                    thongtin.avatarNguoiGiamHo = nguoiGiamHo.Avatar;
                    if (nguoiGiamHo.IdXa != null)
                    {
                        thongtin.IdXaNguoiGiamHo = nguoiGiamHo.Xa.Id;
                        thongtin.IdHuyenNguoiGiamHo = nguoiGiamHo.Xa.Huyen.Id;
                        thongtin.TinhNguoiGiamHo = nguoiGiamHo.Xa.Huyen.Tinh.TenTinh;
                    }
                }
                return thongtin;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// thay đổi password cho tài khoản
        /// Author       :   HoangNM - 05/07/2018 - create
        /// </summary>
        /// <param name="pass">Đối tượng chứa password cũ và mới</param>
        /// <returns>Đối tượng ResponseInfo chứa thông tin của việc kiểm tra</returns>
        public ResponseInfo CheckAccount(changePass pass)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo result = new ResponseInfo();
                int idAccount = XacThuc.GetIdAccount();
                TblAccount taiKhoan = context.Account.FirstOrDefault(x => x.Id == idAccount && !x.DelFlag);
                if (taiKhoan == null)
                {
                    result.MsgNo = (int)MsgNO.MatKhauSai;
                    result.Code = 202;
                }
                else
                {
                    taiKhoan.Password = BaoMat.GetMD5(pass.newPassword);
                    context.SaveChanges();
                }
                transaction.Commit();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// kiểm tra mật khẩu có nhập chính xác không
        /// Author       :   HoangNM - 22/07/2018 - create
        /// </summary>
        /// <param name="pass">Đối tượng chứa password cũ </param>
        /// <returns>Đối tượng ResponseInfo chứa thông tin của việc kiểm tra</returns>
        public bool CheckPass(string pass)
        {
            try
            {
                ResponseInfo result = new ResponseInfo();
                int idAccount = XacThuc.GetIdAccount();
                string password = BaoMat.GetMD5(pass);
                TblAccount taiKhoan = context.Account.FirstOrDefault(x => x.Password == password && x.Id == idAccount && !x.DelFlag);
                if (taiKhoan == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }

            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// lưu username cho account
        /// Author       :   HoangNM - 06/07/2018 - create
        /// Author       :   HoangNM - 10/07/2018 - update
        /// </summary>
        /// <param name="account">đối tượng chứa username và password</param>
        /// <returns>Đối tượng ResponseInfo chứa thông tin của việc kiểm tra</returns>
        /// 
        public ResponseInfo Save(SaveUsername account)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo result = new ResponseInfo();
                int idAccount = XacThuc.GetIdAccount();
                string password = BaoMat.GetMD5(account.Password);
                TblAccount taiKhoan = context.Account.FirstOrDefault(x => x.Id == idAccount && !x.DelFlag);
                if (taiKhoan != null)
                {
                    taiKhoan.Username = account.Username;
                    taiKhoan.Password = BaoMat.GetMD5(account.Password);
                    context.SaveChanges();
                }
                transaction.Commit();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// lấy danh sach cac Tĩnh
        /// Author       :   HoangNM - 07/07/2018 - create
        /// </summary>
        /// <param ></param>
        /// <returns>trả về danh sách tỉnh </string></returns>
        /// 
        public List<TinhHocVien> getTinh()
        {
            try
            {
                return context.Tinh.Select(x => new TinhHocVien
                {
                    IdTinh = x.Id,
                    tenTinh = x.TenTinh
                }).ToList();

            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// lấy danh sách các huyện dựa vào tỉnh đã chọn
        /// Author       :   HoangNM - 07/07/2018 - create
        /// Author       :   HoangNM - 14/07/2018 - update
        /// </summary>
        /// <param ></param>
        /// <returns>trả về danh sách Huyện </string></returns>
        /// 
        public List<HuyenHocVien> getHuyen(string id)
        {
            try
            {
                return context.Huyen.Where(x => x.IdTinh == id && !x.DelFlag).Select(x => new HuyenHocVien
                {
                    IdHuyen = x.Id,
                    tenHuyen = x.TenHuyen
                }).ToList();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// lấy danh sách các Xã dựa vào Huyện đã chọn
        /// Author       :   HoangNM - 07/07/2018 - create
        /// Author       :   HoangNM - 14/07/2018 - update
        /// </summary>
        /// <param ></param>
        /// <returns>trả về danh sách Xã </string></returns>
        /// 
        public List<XaHocVien> getXa(string id)
        {
            try
            {
                return context.Xa.Where(x => x.IdHuyen == id && !x.DelFlag).Select(x => new XaHocVien
                {
                    IdXa = x.Id,
                    tenXa = x.TenXa
                }).ToList();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// kiểm tra file ảnh
        /// Author       :   HoangNM - 22/08/2018 - create
        /// </summary>
        /// <param name="account">chứa thông tin của người dùng muốn thay đổi </param>
        /// <returns>Đối tượng ResponseInfo chứa thông tin của việc kiểm tra</returns>

        public string CheckFile(HttpPostedFileBase file, ResponseInfo result)
        {
            if (file != null)
            {
                string linkAnh = Common.SaveFileUpload(file, "/public/img/avatar/", "", typeFiles);
                return linkAnh;
            }
            return null;   
        }

        /// <summary>
        /// cập nhật thông tin cho người dùng
        /// Author       :   HoangNM - 07/07/2018 - create
        /// </summary>
        /// <param name="account">chứa thông tin của người dùng muốn thay đổi </param>
        /// <returns>Đối tượng ResponseInfo chứa thông tin của việc kiểm tra</returns>
        public ResponseInfo UpDate(UpDate account, HttpPostedFileBase FileImg, HttpPostedFileBase FileImgNGH)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo result = new ResponseInfo();
                int idUser = XacThuc.GetIdUser();
                TblUser user = context.User.FirstOrDefault(x => x.Id == idUser && !x.DelFlag);
                List<string> imgsDelete = new List<string>();
                string LinkAvatar = CheckFile(FileImg, result);
                string LinkAvatarNGH = CheckFile(FileImgNGH, result);
                if(LinkAvatar != null)
                {
                    imgsDelete.Add(user.Avatar);
                    account.Avatar = LinkAvatar;
                }
                else
                {
                    account.Avatar = user.Avatar;
                }
                
              
                if (user != null)
                {
                    user.Ho = account.Ho;
                    user.Ten = account.Ten;
                    user.NgaySinh = account.NgaySinh;
                    user.GioiTinh = account.GioiTinh;
                    user.SoDienThoai = account.SDT;
                    user.DiaChi = account.DiaChi;
                    user.CMND = account.CMND;
                    user.Avatar = account.Avatar;
                    TblXa xa = context.Xa.FirstOrDefault(x => x.Id == account.Xa && x.IdHuyen == account.Huyen && x.Huyen.IdTinh == account.Tinh && !x.DelFlag);
                    user.IdXa = xa.Id;
                    if (!account.nguoiGiamHo)
                    {
                        if (user.IdNguoiGiamHo != null)
                        {
                            TblUser nguoiGiamHo = context.User.FirstOrDefault(x => x.Id == user.IdNguoiGiamHo && !x.DelFlag);
                            if(nguoiGiamHo.Avatar != Common.defaultAvata)
                            {
                                imgsDelete.Add(nguoiGiamHo.Avatar);
                            }
                            context.User.Remove(nguoiGiamHo);
                            user.IdNguoiGiamHo = null;
                        }
                    }
                    else
                    {
                        TblXa xaNguoiGiamHo = context.Xa.FirstOrDefault(x => x.Id == account.XaNguoiGiamHo && !x.DelFlag);
                        if (user.IdNguoiGiamHo != null)
                        {
                            TblUser nguoiGiamHo = context.User.FirstOrDefault(x => x.Id == user.IdNguoiGiamHo && !x.DelFlag);
                            if (LinkAvatarNGH != null)
                            {
                                if(user.NguoiGiamHo.Avatar != Common.defaultAvata)
                                {
                                    imgsDelete.Add(user.NguoiGiamHo.Avatar);
                                }
                                account.AvatarNguoiGiamHo = LinkAvatarNGH;
                            }
                            else
                            {
                                account.AvatarNguoiGiamHo = nguoiGiamHo.Avatar;
                            }
                            nguoiGiamHo.Ho = account.HoNguoiGiamHo;
                            nguoiGiamHo.Ten = account.TenNguoiGiamHo;
                            nguoiGiamHo.NgaySinh = account.NgaySinhNguoiGiamHo;
                            nguoiGiamHo.GioiTinh = account.GioiTinhNguoiGiamHo;
                            nguoiGiamHo.SoDienThoai = account.SDTNguoiGiamHo;
                            nguoiGiamHo.DiaChi = account.DiaChiNguoiGiamHo;
                            nguoiGiamHo.CMND = account.CMNDNguoiGiamHo;
                            nguoiGiamHo.Avatar = account.AvatarNguoiGiamHo;
                            nguoiGiamHo.IdXa = xaNguoiGiamHo.Id;

                        }
                        else
                        {
                            if (LinkAvatarNGH != null)
                            {
                                imgsDelete.Add(user.NguoiGiamHo.Avatar);
                                account.AvatarNguoiGiamHo = LinkAvatarNGH;
                            }
                            else
                            {
                                account.AvatarNguoiGiamHo = Common.defaultAvata;
                            }
                            TblUser nguoiGiamHo = new TblUser
                            {
                                Ho = account.HoNguoiGiamHo,
                                Ten = account.TenNguoiGiamHo,
                                NgaySinh = account.NgaySinhNguoiGiamHo,
                                GioiTinh = account.GioiTinhNguoiGiamHo,
                                SoDienThoai = account.SDTNguoiGiamHo,
                                DiaChi = account.DiaChiNguoiGiamHo,
                                CMND = account.CMNDNguoiGiamHo,
                                Avatar = account.AvatarNguoiGiamHo,
                                IdXa = xaNguoiGiamHo.Id
                            };
                            nguoiGiamHo.NguoiSeGiamHo.Add(user);
                            context.User.Add(nguoiGiamHo);
                        }
                    }
                    context.SaveChanges();
                }
                transaction.Commit();
                Common.DeleteFile(imgsDelete);
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// thay đôi email
        /// Author       :   HoangNM - 07/07/2018 - create
        /// Author       :   HoangNM - 10/07/2018 - Update
        /// </summary>
        /// <param name="account">đối tượng chứa email và thông tin account</param>
        /// <returns>Đối tượng ResponseInfo chứa thông tin của việc kiểm tra</returns>
        /// 
        public ResponseInfo ChangeEmail(ChangeEmail account)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo result = new ResponseInfo();
                int idAccount = XacThuc.GetIdAccount();
                TblAccount taiKhoan = context.Account.FirstOrDefault(x => x.Id == idAccount && !x.DelFlag);
                if (taiKhoan == null)
                {
                    result.MsgNo = (int)MsgNO.KhongCoTaiKhoan;
                    result.Code = 202;
                }
                else
                {
                    taiKhoan.Email = account.Email;
                    taiKhoan.IsActiveEmail = false;
                    context.SaveChanges();
                    // Tiến hành gửi mail
                    SendEmail(taiKhoan);
                }
                transaction.Commit();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// Hàm gửi mail cập nhật email khi thay đổi
        /// Author       :   HoangNM - 07/07/2018 - create
        /// </summary>
        /// <param name="account">Tài khoản được lưu trong DB</param>
        public void SendEmail(TblAccount account)
        {
            try
            {
                string lang = Common.GetLang();
                TblBieuMau bieuMau = context.BieuMau.FirstOrDefault(x => x.Id == (int)TemplateEnum.ActiveEmail && x.Lang == lang && !x.DelFlag);
                if (bieuMau != null)
                {
                    //nhớ vào thay đổi db và thay đổi ở đây
                    bieuMau.NoiDung = bieuMau.NoiDung.Replace("#linkActive", Common.domain + @"kich-hoat-email?email="
                        + account.Email);
                    //EmailService.Send(account.Email, bieuMau.TenBieuMau, bieuMau.NoiDung);
                    EmailService.Send(account.Email, bieuMau.TenBieuMau, bieuMau.NoiDung);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Hàm gửi mail tiếp email khi thay đổi
        /// Author       :   HoangNM - 07/07/2018 - create
        /// </summary>
        /// <param name="account">Tài khoản được lưu trong DB</param>
        public void SendEmail(string email)
        {
            try
            {
                TblAccount account = context.Account.FirstOrDefault(x => x.Email == email && !x.DelFlag);
                string lang = Common.GetLang();
                TblBieuMau bieuMau = context.BieuMau.FirstOrDefault(x => x.Id == (int)TemplateEnum.ActiveEmail && x.Lang == lang && !x.DelFlag);
                if (bieuMau != null)
                {
                    //nhớ vào thay đổi db và thay đổi ở đây
                    bieuMau.NoiDung = bieuMau.NoiDung.Replace("#linkActive", Common.domain + @"kich-hoat-email?email="
                        + account.Email);
                    //EmailService.Send(account.Email, bieuMau.TenBieuMau, bieuMau.NoiDung);
                    EmailService.Send(account.Email, bieuMau.TenBieuMau, bieuMau.NoiDung);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Kích hoạt tài khoản theo link đã gửi trong mail.
        /// Author       :   HoangNM - 08/07/2018 - create
        /// </summary>
        /// <param name="username">Tên đăng nhập mà user đăng ký được gửi kèm trong mail</param>
        /// <returns>Thông tin của việc thay đổi email</returns>
        public void ActiveEmail(string email)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                TblAccount account = context.Account.FirstOrDefault(x => x.Email == email && !x.DelFlag);
                if (account != null)
                    if (!account.IsActiveEmail)
                    {
                        account.IsActiveEmail = true;
                        context.SaveChanges();
                        transaction.Commit();
                    }
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        /// <summary>
        /// Kiểm tra username đã tồn tại hay chưa.
        /// Author       :   HoangNM - 08/07/2018 - create
        /// </summary>
        /// <param name="value">giá trị  username cần kiểm tra</param>
        /// <returns>Nếu có tồn tại trả về true, ngược lại trả về false</returns>
        public bool CheckUser(string value)
        {
            try
            {
                TblAccount acount = context.Account.FirstOrDefault(x => x.Username == value && !x.DelFlag);
                if (acount != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// lấy email của tài khoản
        /// Author       :   HoangNM - 10/07/2018 - create
        /// </summary>
        /// <returns>trả về chuổi string là email của tài khoản</returns>
        public string getEmail()
        {

            try
            {
                int idAccount = XacThuc.GetIdAccount();
                TblAccount taiKhoan = context.Account.FirstOrDefault(x => x.Id == idAccount && !x.DelFlag);
                return taiKhoan.Email;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// Xóa token login của user khi xác nhận email
        /// Author       :   HoangNM - 20/07/2018 - create
        /// </summary>
        /// <returns>true nếu xóa thành công</returns>
        public bool RemoveToken()
        {
            try
            {
                string token = Common.GetCookie("token");
                context.TokenLogin.Where(x => x.Token == token).Delete();
                context.TokenLogin.Where(x => x.ThoiGianTonTai < DateTime.Now).Delete();
                return true;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Kiểm tra email đã tồn tại hay chưa.
        /// Author       :   HoangNM - 04/09/2018 - create
        /// </summary>
        /// <param name="value">giá trị của email cần kiểm tra</param>
        /// <returns>Nếu có tồn tại trả về true, ngược lại trả về false</returns>
        public bool CheckExistEmail(string value)
        {
            try
            {
                TblAccount acount = context.Account.FirstOrDefault(x => x.Email == value && !x.DelFlag);
                if (acount != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}