﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace TrungTamTinHoc.App_Start.Bundle
{
    public class ControlPanelBundle
    {
        public static BundleCollection RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/public/js/admin/listOfSlide").Include(
                "~/public/assets/pagination/pagination.js",
                "~/public/assets/iCheck/icheck.js",
                "~/public/assets/switchery/dist/switchery.js",
                "~/public/js/control-panel/slide-management/listOfSlide.js"
            ));

            bundles.Add(new StyleBundle("~/public/css/admin/listOfSlide").Include(
               "~/public/assets/switchery/dist/switchery.css"
            ));

            bundles.Add(new ScriptBundle("~/public/js/admin/slideMaster").Include(
                "~/public/assets/switchery/dist/switchery.js",
                "~/public/js/control-panel/slide-management/slideMaster.js"
            ));

            bundles.Add(new StyleBundle("~/public/css/admin/slideMaster").Include(
               "~/public/assets/switchery/dist/switchery.css"
           ));

            bundles.Add(new ScriptBundle("~/public/js/admin/listOfCourseRegister").Include(
                "~/public/assets/pagination/pagination.js",
                "~/public/assets/iCheck/icheck.js",
                "~/public/assets/switchery/dist/switchery.js",
                "~/public/assets/moment/moment.js",
                "~/public/assets/bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js",
                "~/public/js/control-panel/course-register-management/listOfCourseRegister.js"
            ));

            bundles.Add(new StyleBundle("~/public/css/admin/listOfCourseRegister").Include(
                "~/public/assets/switchery/dist/switchery.css",
                "~/public/css/control-panel/course-register-management/listOfCourseRegister.css"
            ));

            bundles.Add(new ScriptBundle("~/public/js/admin/listOfNew").Include(
                "~/public/assets/pagination/pagination.js",
                "~/public/assets/iCheck/icheck.js",
                "~/public/assets/switchery/dist/switchery.js",
                "~/public/js/control-panel/new-management/listOfNew.js"
            ));

            bundles.Add(new StyleBundle("~/public/css/admin/listOfNew").Include(
               "~/public/assets/switchery/dist/switchery.css"
               ));

            bundles.Add(new ScriptBundle("~/public/js/admin/newsMaster").Include(
                "~/public/js/control-panel/new-management/newsMaster.js"
            ));

            bundles.Add(new StyleBundle("~/public/css/admin/newsMaster").Include(
               "~/public/assets/switchery/dist/switchery.css"
           ));

            bundles.Add(new ScriptBundle("~/public/js/admin/listOfEmailSubscription").Include(
                "~/public/assets/pagination/pagination.js",
                "~/public/assets/iCheck/icheck.js",
                "~/public/assets/switchery/dist/switchery.js",
                "~/public/js/control-panel/email-subscription-management/listOfEmailSubscription.js"
            ));
            bundles.Add(new StyleBundle("~/public/css/admin/listOfEmailSubscription").Include(
                "~/public/assets/switchery/dist/switchery.css"
            ));

            bundles.Add(new ScriptBundle("~/public/js/admin/listOfThingsAchieved").Include(
                "~/public/assets/pagination/pagination.js",
                "~/public/assets/iCheck/icheck.js",
                "~/public/assets/switchery/dist/switchery.js",
                "~/public/js/control-panel/things-achieved-management/listOfThingsAchieved.js"
            ));
            bundles.Add(new StyleBundle("~/public/css/admin/listOfThingsAchieved").Include(
                "~/public/assets/switchery/dist/switchery.css",
                "~/public/css/control-panel/things-achieved-management/listOfThingsAchieved.css"
            ));

            bundles.Add(new StyleBundle("~/public/css/admin/thingsAchievedMaster").Include(
               "~/public/assets/switchery/dist/switchery.css"
            ));
            bundles.Add(new ScriptBundle("~/public/js/admin/thingsAchievedMaster").Include(
                "~/public/assets/switchery/dist/switchery.js",
                "~/public/js/control-panel/things-achieved-management/thingsAchievedMaster.js"
            ));

            bundles.Add(new ScriptBundle("~/public/js/admin/listOfContact").Include(
                "~/public/assets/pagination/pagination.js",
                "~/public/assets/iCheck/icheck.js",
                "~/public/assets/switchery/dist/switchery.js",
                "~/public/assets/moment/moment.js",
                "~/public/assets/bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js",
                "~/public/js/control-panel/contact-management/listOfContact.js"
            ));
            bundles.Add(new StyleBundle("~/public/css/admin/listOfContact").Include(
                "~/public/assets/switchery/dist/switchery.css",
                "~/public/assets/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css",
                "~/public/css/control-panel/contact-management/ListOfContact.css"
            ));

            bundles.Add(new ScriptBundle("~/public/js/admin/listOfCourse").Include(
                "~/public/assets/pagination/pagination.js",
                "~/public/assets/iCheck/icheck.js",
                "~/public/assets/switchery/dist/switchery.js",
                "~/public/assets/moment/moment.js",
                "~/public/assets/bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js",
                "~/public/js/control-panel/course-management/listOfCourse.js"
            ));

            bundles.Add(new StyleBundle("~/public/css/admin/listOfCourse").Include(
                "~/public/assets/switchery/dist/switchery.css",
                "~/public/assets/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css"
            ));

            bundles.Add(new ScriptBundle("~/public/js/admin/courseMaster").Include(
                "~/public/assets/moment/moment.js",
                "~/public/assets/switchery/dist/switchery.js",
                "~/public/assets/bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js",
                "~/public/js/control-panel/course-management/courseMaster.js"
            ));

            bundles.Add(new StyleBundle("~/public/css/admin/courseMaster").Include(
                "~/public/assets/switchery/dist/switchery.css",
                "~/public/assets/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css"
            ));

            bundles.Add(new ScriptBundle("~/public/js/admin/listOfErrorMsg").Include(
                "~/public/assets/iCheck/icheck.js",
                "~/public/assets/switchery/dist/switchery.js",
                "~/public/assets/pagination/pagination.js",
                "~/public/js/control-panel/errorMng-management/ListOfErrorMsg.js"
            ));
            bundles.Add(new StyleBundle("~/public/css/admin/listOfErrorMsg").Include(
                "~/public/assets/switchery/dist/switchery.css",
                "~/public/css/control-panel/errorMgs-management/ListOfErrorMsg.css"
            ));

            bundles.Add(new ScriptBundle("~/public/js/admin/listOfGroup").Include(
               "~/public/assets/pagination/pagination.js",
               "~/public/assets/iCheck/icheck.js",
               "~/public/assets/switchery/dist/switchery.js",
               "~/public/js/control-panel/group-management/listOfGroup.js",
               "~/public/js/common/nicEdit-latest.js"
           ));
            bundles.Add(new StyleBundle("~/public/css/admin/listOfPermission").Include(
                "~/public/assets/switchery/dist/switchery.css",
                "~/public/css/control-panel/permission-management/listOfPermission.css"
            ));

            bundles.Add(new ScriptBundle("~/public/js/admin/listOfPermission").Include(
               "~/public/assets/iCheck/icheck.js",
               "~/public/assets/switchery/dist/switchery.js",
               "~/public/js/control-panel/permission-management/listOfPermission.js"
           ));

            bundles.Add(new StyleBundle("~/public/css/admin/listOfGroup").Include(
               "~/public/assets/switchery/dist/switchery.css"
            ));
            bundles.Add(new ScriptBundle("~/public/js/admin/listOfUser").Include(
                "~/public/assets/pagination/pagination.js",
                "~/public/assets/iCheck/icheck.js",
                "~/public/assets/switchery/dist/switchery.js",
                "~/public/assets/moment/moment.js",
                "~/public/assets/bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js",
                "~/public/js/control-panel/user-management/listOfUser.js"
            ));

            bundles.Add(new StyleBundle("~/public/css/admin/listOfUser").Include(
                "~/public/assets/switchery/dist/switchery.css",
                "~/public/css/control-panel/user-management/listOfUser.css",
                "~/public/assets/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css"
            ));

            bundles.Add(new ScriptBundle("~/public/js/admin/userMaster").Include(
                "~/public/js/common/md5.js",
                "~/public/assets/moment/moment.js",
                "~/public/assets/bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js",
                "~/public/assets/switchery/dist/switchery.js",
                "~/public/js/control-panel/user-management/fSelect.js",
                "~/public/js/control-panel/user-management/userMaster.js"
            ));

            bundles.Add(new StyleBundle("~/public/css/admin/userMaster").Include(
                "~/public/assets/switchery/dist/switchery.css",
                "~/public/assets/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css",
                "~/public/css/control-panel/user-management/fSelect.css"
            ));

            bundles.Add(new ScriptBundle("~/public/js/admin/listOfPhoto").Include(
                "~/public/assets/pagination/pagination.js",
                "~/public/assets/iCheck/icheck.js",
                "~/public/assets/switchery/dist/switchery.js",
                "~/public/js/control-panel/photo-management/listOfPhoto.js"
            ));

            bundles.Add(new StyleBundle("~/public/css/admin/listOfPhoto").Include(
               "~/public/assets/switchery/dist/switchery.css"
            ));

            bundles.Add(new ScriptBundle("~/public/js/admin/photoMaster").Include(
               "~/public/assets/switchery/dist/switchery.js",
               "~/public/js/control-panel/photo-management/photoMaster.js"
           ));

            bundles.Add(new StyleBundle("~/public/css/admin/photoMaster").Include(
               "~/public/assets/switchery/dist/switchery.css"
           ));

            bundles.Add(new ScriptBundle("~/public/js/admin/errorMsgMaster").Include(
               "~/public/assets/switchery/dist/switchery.js",
               "~/public/js/control-panel/errorMng-management/errorMsgMaster.js"
           ));

            bundles.Add(new StyleBundle("~/public/css/admin/errorMsgMaster").Include(
               "~/public/assets/switchery/dist/switchery.css"
           ));
            bundles.Add(new ScriptBundle("~/public/js/admin/groupMaster").Include(
               "~/public/assets/switchery/dist/switchery.js",
               "~/public/js/control-panel/group-management/groupMaster.js"
           ));

            bundles.Add(new StyleBundle("~/public/css/admin/groupMaster").Include(
               "~/public/assets/switchery/dist/switchery.css"
           ));

            bundles.Add(new ScriptBundle("~/public/js/admin/setting").Include(
                "~/public/assets/bootstrap-tagsinput-master/dist/bootstrap-tagsinput.js",
                "~/public/assets/pagination/pagination.js",
                "~/public/js/common/md5.js",
                "~/public/js/control-panel/setting-management/setting.js"
            ));

            bundles.Add(new StyleBundle("~/public/css/admin/setting").Include(
               "~/public/assets/bootstrap-tagsinput-master/dist/bootstrap-tagsinput.css",
               "~/public/css/control-panel/setting-management/setting.css"
            ));

            bundles.Add(new ScriptBundle("~/public/js/admin/Configuration").Include(
                "~/public/js/control-panel/configuration-management/configuration.js"
            ));

            bundles.Add(new StyleBundle("~/public/css/admin/Configuration").Include(
               "~/public/css/control-panel/configuration-management/configuration.css"
            ));

            bundles.Add(new ScriptBundle("~/public/js/admin/listOfClass").Include(
                "~/public/assets/pagination/pagination.js",
                "~/public/assets/iCheck/icheck.js",
                "~/public/assets/switchery/dist/switchery.js",
                "~/public/assets/moment/moment.js",
                "~/public/assets/bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js",
                "~/public/js/control-panel/class-management/listOfClass.js"
            ));

            bundles.Add(new StyleBundle("~/public/css/admin/listOfClass").Include(
                "~/public/assets/switchery/dist/switchery.css",
                "~/public/assets/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css"
            ));

            bundles.Add(new ScriptBundle("~/public/js/admin/classMaster").Include(
                "~/public/assets/moment/moment.js",
                "~/public/assets/switchery/dist/switchery.js",
                "~/public/assets/bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js",
                "~/public/js/control-panel/class-management/classMaster.js"
            ));

            bundles.Add(new StyleBundle("~/public/css/admin/classMaster").Include(
                "~/public/assets/switchery/dist/switchery.css",
                "~/public/assets/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css"
            ));

            bundles.Add(new ScriptBundle("~/public/js/admin/listOfSchedule").Include(
                "~/public/assets/pagination/pagination.js",
                "~/public/assets/iCheck/icheck.js",
                "~/public/assets/switchery/dist/switchery.js",
                "~/public/js/control-panel/schedule-management/listOfSchedule.js"
            ));

            bundles.Add(new StyleBundle("~/public/css/admin/listOfSchedule").Include(
               "~/public/assets/switchery/dist/switchery.css"
            ));

            bundles.Add(new ScriptBundle("~/public/js/admin/ScheduleMaster").Include(
               "~/public/assets/switchery/dist/switchery.js",
               "~/public/assets/clockpicker-gh-pages/dist/bootstrap-clockpicker.js",
               "~/public/assets/clockpicker-gh-pages/dist/jquery-clockpicker.js",
               "~/public/js/control-panel/schedule-management/ScheduleMaster.js"
           ));

            bundles.Add(new StyleBundle("~/public/css/admin/ScheduleMaster").Include(
               "~/public/assets/switchery/dist/switchery.css",
               "~/public/assets/clockpicker-gh-pages/dist/bootstrap-clockpicker.css",
               "~/public/assets/clockpicker-gh-pages/dist/jquery-clockpicker.css"
           ));

            return bundles;
        }
    }
}