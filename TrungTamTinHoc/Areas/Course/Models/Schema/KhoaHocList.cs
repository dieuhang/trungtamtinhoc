﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TTTH.Common;

namespace TrungTamTinHoc.Areas.Course.Models.Schema
{
    /// <summary>
    /// Class dùng để lấy danh sách khóa học trả về cho trang danh sách khóa học
    /// Author       :   TramHDT - 06/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class KhoaHocList
    {
        public List<KhoaHoc> KhoaHocs { set; get; }
        public List<ChuyenNganh> ChuyenNganh { set; get; }
        public Paging Paging { set; get; }
        public CourseConditionSearch Condition { set; get; }

        public KhoaHocList()
        {
            this.KhoaHocs = new List<KhoaHoc>();
            this.ChuyenNganh = new List<ChuyenNganh>();
            this.Condition = new CourseConditionSearch();
            this.Paging = new Paging();
        }
    }
}