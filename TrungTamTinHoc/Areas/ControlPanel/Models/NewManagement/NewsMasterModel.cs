﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TrungTamTinHoc.Areas.ControlPanel.Models.NewsManagement.Schema;
using TTTH.Common;
using TTTH.DataBase;
using Z.EntityFramework.Plus;
using static TTTH.Common.Enums.ConstantsEnum;
using static TTTH.Common.Enums.MessageEnum;
using TblNews = TTTH.DataBase.Schema.TinTuc;
using TblNewsTrans = TTTH.DataBase.Schema.TinTucTrans;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.NewsManagement
{
    /// <summary>
    /// Class chứa các phương thức liên quan đến việc xử lý news
    /// Author       :   HangNTD - 27/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class NewsMasterModel
    {
        DataContext context;
        List<string> typeFiles;
        public NewsMasterModel()
        {
            context = new DataContext();
            typeFiles = new List<string>();
            typeFiles.Add(".jpg");
            typeFiles.Add(".png");
        }
        /// <summary>
        /// Lấy thông tin của 1 news
        /// Author       :   HangNTD - 27/08/2018 - create
        /// </summary>
        /// <param name="id">id của news sẽ được hiển thị</param>
        /// <returns>Thông tin của news</returns>
        public NewsMaster LoadNews(string id)
        {
            try
            {
                NewsMaster newsMaster = new NewsMaster();
                int idNews = 0;
                try
                {
                    idNews = Convert.ToInt32(id);
                }
                catch { }
                TblNewsTrans news = context.TinTucTrans.FirstOrDefault(x => x.Id == idNews && !x.DelFlag && x.Lang == Common.defaultLang);
                if (news != null)
                {
                    newsMaster.Mode = (int)ModeMaster.Update;
                    newsMaster.NewsInfo = new News();
                    newsMaster.NewsInfo.Id = news.Id;
                    newsMaster.NewsInfo.TieuDe = news.TieuDe;
                    newsMaster.NewsInfo.BeautyId = news.TinTuc.BeautyId;
                    newsMaster.NewsInfo.NoiDung = news.NoiDung;
                    newsMaster.NewsInfo.TomTat = news.TomTat;
                    newsMaster.NewsInfo.AnhChinh = news.TinTuc.AnhChinh;
                    newsMaster.NewsInfo.AnhMinhHoa = news.TinTuc.AnhMinhHoa;
                    newsMaster.Language = context.Language.Where(x => !x.DelFlag && x.Id != Common.defaultLang)
                        .Select(x => new Lang
                        {
                            Id = x.Id,
                            Name = x.Name
                        }).OrderBy(x => x.Id).ToList();
                    if (newsMaster.Language.Count > 0)
                    {
                        string lang = newsMaster.Language[0].Id;
                        news = context.TinTucTrans.FirstOrDefault(x => x.Id == idNews && !x.DelFlag && x.Lang == lang);
                        newsMaster.NewsTrans = new News();
                        newsMaster.NewsTrans.Id = news.Id;
                        newsMaster.NewsTrans.TieuDe = news.TieuDe;
                        newsMaster.NewsTrans.BeautyId = news.TinTuc.BeautyId;
                        newsMaster.NewsTrans.NoiDung = news.NoiDung;
                        newsMaster.NewsTrans.TomTat = news.TomTat;
                        newsMaster.NewsTrans.AnhChinh = news.TinTuc.AnhChinh;
                        newsMaster.NewsTrans.AnhMinhHoa = news.TinTuc.AnhMinhHoa;
                    }
                }
                return newsMaster;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// Tạo hoặc cập nhật lại news từ thông tin mà người dùng gửi lên
        /// Author       :   HangNTD - 27/08/2018 - create
        /// </summary>
        /// <param name="news">Các thông tin của news muốn tạo hoặc cập nhật</param>
        /// <returns>Thông tin về việc tạo hoặc cập nhật news thành công hay thất bại</returns>
        public ResponseInfo SaveNews(News news)
        {
            try
            {
                if (context.TinTuc.FirstOrDefault(x => x.Id == news.Id && !x.DelFlag) != null)
                {
                    return UpadateNews(news);
                }
                return AddNews(news);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// Cập nhật lại  news từ thông tin mà người dùng gửi lên
        /// Author       :   HangNTD - 27/08/2018 - create
        /// </summary>
        /// <param name="course">Các thông tin của news muốn cập nhật</param>
        /// <returns>Thông tin về việc cập nhật news thành công hay thất bại</returns>
        public ResponseInfo UpadateNews(News news)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo response = new ResponseInfo();
                bool update = true;
                List<string> imgsDelete = new List<string>();
                if (news.FileImg0 != null)
                {
                    string anhChinh = Common.SaveFileUpload(news.FileImg0, "/public/img/new/", "", typeFiles);
                    if (anhChinh == "1")
                    {
                        response.Code = 202;
                        response.MsgNo = (int)MsgNO.FileKhongDungDinhDang;
                        update = false;
                    }
                    else if (anhChinh == "2")
                    {
                        response.Code = 202;
                        response.MsgNo = (int)MsgNO.DungLuongFileQuaLon;
                        update = false;
                    }
                    else if (anhChinh == "")
                    {
                        response.Code = 202;
                        response.MsgNo = (int)MsgNO.TaiFileBiLoi;
                        update = false;
                    }
                    else
                    {
                        imgsDelete.Add(context.TinTuc.FirstOrDefault(x => x.Id == news.Id && !x.DelFlag).AnhChinh);
                        news.AnhChinh = anhChinh;
                    }
                }
                else if (news.FileImg1 != null)
                {
                    string anhMinhHoa = Common.SaveFileUpload(news.FileImg1, "/public/img/new/", "", typeFiles);
                    if (anhMinhHoa == "1")
                    {
                        response.Code = 202;
                        response.MsgNo = (int)MsgNO.FileKhongDungDinhDang;
                        update = false;
                    }
                    else if (anhMinhHoa == "2")
                    {
                        response.Code = 202;
                        response.MsgNo = (int)MsgNO.DungLuongFileQuaLon;
                        update = false;
                    }
                    else if (anhMinhHoa == "")
                    {
                        response.Code = 202;
                        response.MsgNo = (int)MsgNO.TaiFileBiLoi;
                        update = false;
                    }
                    else
                    {
                        imgsDelete.Add(context.TinTuc.FirstOrDefault(x => x.Id == news.Id && !x.DelFlag).AnhMinhHoa);
                        news.AnhMinhHoa = anhMinhHoa;
                    }
                }

                if (update)
                {
                    context.TinTuc.Where(x => x.Id == news.Id && !x.DelFlag)
                        .Update(x => new TTTH.DataBase.Schema.TinTuc
                        {
                            BeautyId = news.BeautyId,
                            AnhChinh = news.AnhChinh,
                            AnhMinhHoa = news.AnhMinhHoa
                        });
                    context.TinTucTrans.Where(x => x.Id == news.Id && x.Lang == Common.defaultLang && !x.DelFlag)
                        .Update(x => new TTTH.DataBase.Schema.TinTucTrans
                        {
                            TieuDe = news.TieuDe,
                            NoiDung = news.NoiDung,
                            TomTat = news.TomTat
                        });
                    context.SaveChanges();
                }
                transaction.Commit();
                Common.DeleteFile(imgsDelete);
                return response;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }
        /// <summary>
        /// Tạo  news từ thông tin mà người dùng gửi lên
        /// Author       :   HangNTD - 27/08/2018 - create
        /// </summary>
        /// <param name="course">Các thông tin của news muốn tạo</param>
        /// <returns>Thông tin về việc tạo  news thành công hay thất bại</returns>
        public ResponseInfo AddNews(News news)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo response = new ResponseInfo();
                if (news.FileImg0 == null)
                {
                    response.Code = 202;
                    response.MsgNo = (int)MsgNO.ChuaChonFile;
                }
                else
                {
                    news.AnhChinh = Common.SaveFileUpload(news.FileImg0, "/public/img/new/", "", typeFiles);
                    if (news.AnhChinh == "1")
                    {
                        response.Code = 202;
                        response.MsgNo = (int)MsgNO.FileKhongDungDinhDang;
                    }
                    else if (news.AnhChinh == "2")
                    {
                        response.Code = 202;
                        response.MsgNo = (int)MsgNO.DungLuongFileQuaLon;
                    }
                    else if (news.AnhChinh == "")
                    {
                        response.Code = 202;
                        response.MsgNo = (int)MsgNO.TaiFileBiLoi;
                    }
                    else if (news.FileImg1 == null)
                    {
                        response.Code = 202;
                        response.MsgNo = (int)MsgNO.ChuaChonFile;
                    }
                    else
                    {
                        news.AnhMinhHoa = Common.SaveFileUpload(news.FileImg1, "/public/img/new/", "", typeFiles);
                        if (news.AnhMinhHoa == "1")
                        {
                            response.Code = 202;
                            response.MsgNo = (int)MsgNO.FileKhongDungDinhDang;
                        }
                        else if (news.AnhMinhHoa == "2")
                        {
                            response.Code = 202;
                            response.MsgNo = (int)MsgNO.DungLuongFileQuaLon;
                        }
                        else if (news.AnhMinhHoa == "")
                        {
                            response.Code = 202;
                            response.MsgNo = (int)MsgNO.TaiFileBiLoi;
                        }
                        else
                        {
                            List<Lang> listOfLang = context.Language.Where(x => !x.DelFlag).Select(x => new Lang
                            {
                                Id = x.Id,
                                Name = x.Name
                            }).ToList();
                            news.Id = context.TinTuc.Count() == 0 ? 1 : context.TinTuc.Max(x => x.Id) + 1;
                            context.TinTuc.Add(new TblNews
                            {
                                Id = news.Id,
                                BeautyId = news.BeautyId,
                                SoLuongView = 0,
                                AnhChinh = news.AnhChinh,
                                AnhMinhHoa = news.AnhMinhHoa,

                            });
                            foreach (Lang lang in listOfLang)
                            {
                                context.TinTucTrans.Add(new TblNewsTrans
                                {
                                    Id = news.Id,
                                    Lang = lang.Id,
                                    TieuDe = news.TieuDe,
                                    NoiDung = news.NoiDung,
                                    TomTat = news.TomTat
                                });
                            }
                            context.SaveChanges();
                            response.ThongTinBoSung1 = news.Id + "";
                        }
                    }
                }

                transaction.Commit();
                return response;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        /// <summary>
        /// Update lại news
        /// Author       :   HangNTD - 25/07/2018 - create
        /// </summary>
        /// <param name="news">news cần update</param>
        /// <returns>Thông tin về việc cập nhật news thành công hay thất bại</returns>
        /// <remarks>
        /// </remarks>
        public ResponseInfo UpdateNewsTranslate(News_Trans news)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo response = new ResponseInfo();
                context.TinTucTrans.Where(x => x.Id == news.Id && x.Lang == news.LanguageTrans && !x.DelFlag)
                    .Update(x => new TTTH.DataBase.Schema.TinTucTrans
                    {
                        TieuDe = news.TieuDe,
                        NoiDung = news.NoiDung,
                        TomTat = news.TomTat
                    });
                context.SaveChanges();
                response.ThongTinBoSung1 = news.Id + "";
                transaction.Commit();
                return response;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }
        /// <summary>
        /// Refer lại ngôn ngữ
        /// Author       :   HangNTD - 25/07/2018 - create
        /// </summary>
        /// <param name="id">id của news</param>
        /// <param name="lang">ngôn ngữ được chọn</param>
        /// <returns>news đã chuyển ngôn ngữ</returns>
        /// <remarks>
        /// </remarks>
        public News_Trans ReferNewsWithLang(int id, string lang)
        {
            try
            {
                return context.TinTucTrans.Where(x => x.Id == id && x.Lang == lang && !x.DelFlag)
                    .Select(x => new News_Trans
                    {
                        TieuDe = x.TieuDe
                        //ChiTiet = x.ChiTiet
                    }).FirstOrDefault();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// Kiểm tra url của news đã tồn tại chưa
        /// Author       :   HangNTD - 25/07/2018 - create
        /// </summary>
        /// <param name="value">url của news</param>
        /// <returns>true: nếu đã tồn tại, false nếu chưa tồn tại</returns>
        /// <remarks>
        /// </remarks>
        public bool CheckExistUrlNews(string value)
        {
            try
            {
                TblNews news = context.TinTuc.FirstOrDefault(x => x.BeautyId == value && !x.DelFlag);
                if (news != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}