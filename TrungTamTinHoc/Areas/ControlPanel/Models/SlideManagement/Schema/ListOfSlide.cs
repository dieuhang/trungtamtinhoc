﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TTTH.Common;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.SlideManagement.Schema
{
    /// <summary>
    /// Class dùng để lấy danh sách slide trả về cho trang danh sách slide
    /// Author       :   QuyPN - 18/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ListOfSlide
    {
        public List<Slide> SlideList { set; get; }
        public Paging Paging { set; get; }
        public SlideConditionSearch Condition { set; get; }
        public ListOfSlide()
        {
            this.SlideList = new List<Slide>();
            this.Condition = new SlideConditionSearch();
            this.Paging = new Paging();
        }
    }
}