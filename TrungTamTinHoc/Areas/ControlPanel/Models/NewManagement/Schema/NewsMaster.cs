﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TrungTamTinHoc.Areas.ControlPanel.Models.NewsManagement.Schema;
using TTTH.Common;
using static TTTH.Common.Enums.ConstantsEnum;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.NewsManagement.Schema
{
    public class NewsMaster
    {
        public News NewsInfo { get; set; }
        public News NewsTrans { get; set; }
        public List<Lang> Language { set; get; }
        public int Mode { set; get; }
        public NewsMaster()
        {
            Mode = (int)ModeMaster.Insert;
            NewsTrans = null;
            NewsInfo = null;
            Language = new List<Lang>();
        }
    }
}