﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TrungTamTinHoc.Areas.ControlPanel.Models.PhotoManagement.Schema;
using TTTH.Common;
using TTTH.DataBase;
using Z.EntityFramework.Plus;
using static TTTH.Common.Enums.ConstantsEnum;
using static TTTH.Common.Enums.MessageEnum;
using tblPhotos = TTTH.DataBase.Schema.Photos;
namespace TrungTamTinHoc.Areas.ControlPanel.Models.PhotoManagement
{
    /// <summary>
    /// Class dùng để xử lý các hoạt động liên quan đến thêm và sửa danh sách hình ảnh.
    /// Author       :   HoangNM - 11/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class PhotoMasterModel
    {

        DataContext context;
        List<string> typeFiles;
        public PhotoMasterModel()
        {
            context = new DataContext();
            typeFiles = new List<string>();
            typeFiles.Add(".jpg");
            typeFiles.Add(".png");
        }

        /// <summary>
        /// Load thông tin hình ảnh
        /// Author       :   HoàngNM - 11/08/2018 - create
        /// </summary>
        /// <param name="id">id của hình ảnh</param>
        /// <returns>Thông tin của hình ảnh theo id. Exception nếu có lỗi</returns>
        public PhotoMaster LoadPhoto(string id)
        {
            try
            {
                PhotoMaster photoMaster = new PhotoMaster();
                int idPhoto = 0;
                try
                {
                    idPhoto = Convert.ToInt32(id);
                }
                catch { }
                tblPhotos photo = context.Photos.FirstOrDefault(x => x.Id == idPhoto && !x.DelFlag && x.Lang == Common.defaultLang);
                if (photo != null)
                {
                    photoMaster.Mode = (int)ModeMaster.Update;
                    photoMaster.PhotoInfo = new Photo();
                    photoMaster.PhotoInfo.Id = photo.Id;
                    photoMaster.PhotoInfo.Note = photo.Note;
                    photoMaster.PhotoInfo.LinkAnh = photo.Link;
                    photoMaster.Language = context.Language.Where(x => !x.DelFlag)
                        .Select(x => new Lang
                        {
                            Id = x.Id,
                            Name = x.Name
                        }).OrderBy(x => x.Id).ToList();
                    if (photoMaster.Language.Count > 0)
                    {
                        string lang = photoMaster.Language[0].Id;
                        photo = context.Photos.FirstOrDefault(x => x.Id == idPhoto && !x.DelFlag && x.Lang == lang);
                        photoMaster.PhotoTrans = new Photo();
                        photoMaster.PhotoTrans.Id = photo.Id;
                        photoMaster.PhotoTrans.Note = photo.Note;
                        photoMaster.PhotoTrans.LinkAnh = photo.Link;
                    }
                }
                return photoMaster;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// dùng để lưu thông tin hình anh ( dùng cho update hoặc add)
        /// Author       :   HoàngNM - 11/08/2018 - create
        /// </summary>
        /// <param name="photo">một đối tượng của hình ảnh</param>
        public ResponseInfo SavePhotos(Photo photo)
        {
            try
            {
                if (context.Photos.FirstOrDefault(x => x.Id == photo.Id && !x.DelFlag) != null)
                {
                    return UpadatePhotos(photo);
                }
                return AddPhotos(photo);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// dùng để cập nhật thông tin hình ảnh
        /// Author       :   HoàngNM - 11/08/2018 - create
        /// </summary>
        /// <param name="photo">một đối tượng của hình ảnh</param>
        public ResponseInfo UpadatePhotos(Photo photo)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo response = new ResponseInfo();
                bool update = true;
                List<string> imgsDelete = new List<string>();
                if (photo.FileImg != null)
                {
                    string linkAnh = Common.SaveFileUpload(photo.FileImg, "/public/img/slider/", "", typeFiles);
                    if (linkAnh == "1")
                    {
                        response.Code = 202;
                        response.MsgNo = (int)MsgNO.FileKhongDungDinhDang;
                        update = false;
                    }
                    else if (linkAnh == "2")
                    {
                        response.Code = 202;
                        response.MsgNo = (int)MsgNO.DungLuongFileQuaLon;
                        update = false;
                    }
                    else if (linkAnh == "")
                    {
                        response.Code = 202;
                        response.MsgNo = (int)MsgNO.TaiFileBiLoi;
                        update = false;
                    }
                    else
                    {
                        imgsDelete.Add(context.Photos.FirstOrDefault(x => x.Id == photo.Id && !x.DelFlag).Link);
                        photo.LinkAnh = linkAnh;
                    }
                }
                if (update)
                {
                    
                    context.Photos.Where(x => x.Id == photo.Id && !x.DelFlag)
                        .Update(x => new TTTH.DataBase.Schema.Photos
                        {
                            Link = photo.LinkAnh
                        });
                    context.Photos.Where(x => x.Id == photo.Id && x.Lang == Common.defaultLang && !x.DelFlag)
                        .Update(x => new TTTH.DataBase.Schema.Photos
                        {
                            Note = photo.Note
                        });
                    context.SaveChanges();
                }
                transaction.Commit();
                Common.DeleteFile(imgsDelete);
                return response;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }
        /// <summary>
        /// dùng để thêm một hình ảnh
        /// Author       :   HoàngNM - 11/08/2018 - create
        /// </summary>
        /// <param name="photo">một đối tượng của hình ảnh</param>
        public ResponseInfo AddPhotos(Photo photo)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo response = new ResponseInfo();
                if (photo.FileImg == null)
                {
                    response.Code = 202;
                    response.MsgNo = (int)MsgNO.ChuaChonFile;
                }
                else
                {
                    photo.LinkAnh = Common.SaveFileUpload(photo.FileImg, "/public/img/album/", "", typeFiles);
                    if (photo.LinkAnh == "1")
                    {
                        response.Code = 202;
                        response.MsgNo = (int)MsgNO.FileKhongDungDinhDang;
                    }
                    else if (photo.LinkAnh == "2")
                    {
                        response.Code = 202;
                        response.MsgNo = (int)MsgNO.DungLuongFileQuaLon;
                    }
                    else if (photo.LinkAnh == "")
                    {
                        response.Code = 202;
                        response.MsgNo = (int)MsgNO.TaiFileBiLoi;
                    }
                    else
                    {
                        List<Lang> listOfLang = context.Language.Where(x => !x.DelFlag).Select(x => new Lang
                        {
                            Id = x.Id,
                            Name = x.Name
                        }).ToList();
                        photo.Id = context.Photos.Count() == 0 ? 1 : context.Photos.Max(x => x.Id) + 1;
                        foreach (Lang lang in listOfLang)
                        {
                            context.Photos.Add(new tblPhotos
                            {
                                Id = photo.Id,
                                Lang = lang.Id,
                                Link = photo.LinkAnh,
                                Note = photo.Note
                            });
                        }
                        context.SaveChanges();
                        response.ThongTinBoSung1 = photo.Id + "";
                    }
                }
                transaction.Commit();
                return response;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }
        /// <summary>
        /// dùng để lưu thông tin hình ảnh bằng một ngôn ngữ (khác với ngôn ngữ chính)
        /// Author       :   HoàngNM - 11/08/2018 - create
        /// </summary>
        /// <param name="photo">một đối tượng của hình ảnh</param>
        public ResponseInfo UpdatePhotosTranslate(Photos_Trans photo)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo response = new ResponseInfo();
                context.Photos.Where(x => x.Id == photo.Id && x.Lang == photo.LanguageTrans && !x.DelFlag)
                    .Update(x => new TTTH.DataBase.Schema.Photos
                    {
                        Note = photo.Note
                    });
                context.SaveChanges();
                response.ThongTinBoSung1 = photo.Id + "";
                transaction.Commit();
                return response;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }
        /// <summary>
        /// dùng để lấy thông tin ngôn ngữ theo ngôn ngữ đã chọn
        /// Author       :   HoàngNM - 11/08/2018 - create
        /// </summary>
        /// <param name="id">id của hình ảnh</param>
        /// <param name="lang">ngôn ngữ </param>
        public Photos_Trans ReferPhotoWithLang(int id, string lang)
        {
            try
            {
                return context.Photos.Where(x => x.Id == id && x.Lang == lang && !x.DelFlag)
                    .Select(x => new Photos_Trans
                    {
                        Note = x.Note
                    }).FirstOrDefault();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}