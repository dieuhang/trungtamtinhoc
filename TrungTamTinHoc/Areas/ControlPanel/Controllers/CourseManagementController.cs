﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrungTamTinHoc.Areas.ControlPanel.Models.CourseManagement;
using TrungTamTinHoc.Areas.ControlPanel.Models.CourseManagement.Schema;
using TrungTamTinHoc.Controllers;
using TTTH.Common;
using TTTH.Validate;
using static TTTH.Common.Enums.ConstantsEnum;
using static TTTH.Common.Enums.MessageEnum;

namespace TrungTamTinHoc.Areas.ControlPanel.Controllers
{
    /// <summary>
    /// Class chứa các điều hướng liên quan đến quản lý danh sách khóa học
    /// Author       :   TramHTD - 20/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class CourseManagementController : BaseAdminController
    {
        /// <summary>
        /// Điều hướng đến trang hiển thị danh sách khóa học nếu là request thông thường.
        /// Trả về table chứa danh sách khóa học nếu là Ajax.
        /// Điều hướng về trang lỗi nếu có lỗi xảy ra.
        /// Author       :   TraHTD - 20/07/2018 - create
        /// </summary>
        /// <param name="condition">Đối tượng chứa điều kiện tìm kiếm, tạo thành từ query string</param>
        /// <returns>
        /// Trang danh sách khóa học.
        /// Partial view chứa table danh sách khóa học.
        /// </returns>
        /// <remarks>
        /// Method: GET
        /// RouterName: ListOfCourse
        /// </remarks>
        public ActionResult ListOfCourse(CourseConditionSearch condition)
        {
            try
            {
                if (Request.IsAjaxRequest())
                {
                    return View("Partial/_tableOfCourse", new ListOfCourseModel().GetListOfCourse(condition));
                }
                return View(new ListOfCourseModel().GetListOfCourse(condition));
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }
        /// <summary>
        /// Xóa các khóa học theo danh sách id khóa học được gửi lên.
        /// Author       :   TramHTD - 20/07/2018 - create
        /// </summary>
        /// <param name="ids">Danh sách id các khóa học sẽ xóa</param>
        /// <returns>Đối tượng chứa thông tin về quá trình xóa khóa học</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: DeleteCourses
        /// </remarks>
        public JsonResult DeleteCourses(List<int> ids)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                bool deleted = new ListOfCourseModel().DeleteCourses(ids);
                if (!deleted)
                {
                    response.Code = 201;
                    response.MsgNo = 48;
                }
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Update trạng thái hiển thị trên trang chủ của 1 khóa học.
        /// Author       :   TramHTD - 20/07/2018 - create
        /// </summary>
        /// <param name="id">id của slide sẽ update</param>
        /// <returns>Đối tượng chứa thông tin về quá trình update</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: UpdateShowCourse
        /// </remarks>
        public JsonResult UpdateShow(int id)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                bool updated = new ListOfCourseModel().UpdateShow(id);
                if (!updated)
                {
                    response.Code = 201;
                    response.MsgNo = 48;
                }
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Điều hướng đến trang view nhập thông tin để thêm khóa học.
        /// Author       :   TramHTD - 20/08/2018 - create
        /// </summary>
        /// <returns>Trả về trang view thêm khóa học</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: ViewCreateCourse
        /// </remarks>
        public ActionResult ViewCreateCourse()
        {
            try
            {
                ViewBag.ListChuyenNganh = new CourseMasterModel().GetChuyenNganhs();
                return View("CourseMaster");
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }

        /// <summary>
        /// Điều hướng đến trang view sửa khóa học
        /// Author       :   TramHTD - 20/08/2018 - create
        /// </summary>
        /// <param name="id">Đối tượng chứa thông tin về quá trình chỉnh sửa thông tin</param>
        /// <returns>Trả về trang view sửa khóa học</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: ViewEditCourse
        /// </remarks>
        public ActionResult ViewEditCourse(string id)
        {
            try
            {
                CourseMaster courseMaster = new CourseMasterModel().LoadCourse(id);
                if (courseMaster.Mode == (int)ModeMaster.Insert)
                {
                    return RedirectToAction("ViewCreateCourse");
                }
                ViewBag.ListChuyenNganh = new CourseMasterModel().GetChuyenNganhs();
                return View("CourseMaster", courseMaster);
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }

        /// <summary>
        /// Tạo khóa học hoặc cập nhật khóa học theo thông tin người dùng đưa lên.
        /// Author       :   TramHTD - 20/08/2018 - create
        /// </summary>
        /// <param name="data">Thông tin khóa học mà người dùng nhập vào</param>
        /// <returns>Chuỗi Json chứa kết quả của việc tạo hoặc cập nhật khóa học</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: SaveCourse
        /// </remarks>
        [ValidateInput(false)]
        public JsonResult SaveCourse(Models.CourseManagement.Schema.Course data)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                if (ModelState.IsValid)
                {
                    response = new CourseMasterModel().SaveCourse(data);
                }
                else
                {
                    response.Code = (int)CodeResponse.NotValidate;
                    response.ListError = ModelState.GetModelErrors();
                }
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Lưu thông tin khóa học được gửi lên bằng ngôn ngữ khác ngôn ngữ mặc định
        /// Author       :   TramHTD - 20/08/2018 - create
        /// </summary>
        /// <param name="data">Dữ liệu của đối tượng muốn lưu</param>
        /// <returns></returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: SaveCourse
        /// </remarks>
        [ValidateInput(false)]
        public JsonResult UpdateCourseTranslate(Course_Trans data)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                if (ModelState.IsValid)
                {
                    response = new CourseMasterModel().UpdateCourseTranslate(data);
                }
                else
                {
                    response.Code = (int)CodeResponse.NotValidate;
                    response.ListError = ModelState.GetModelErrors();
                }
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Thay đổi ngôn ngữ
        /// Author       :   TramHTD - 20/08/2018 - create
        /// </summary>
        /// <param name="id">Đối tượng muốn thay đổi ngôn ngữ</param>
        /// <param name="lang">Ngôn ngữ muốn thay đổi</param>
        /// <returns></returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: SaveCourse
        /// </remarks>
        public JsonResult ReferCourseWithLang(int id, string lang)
        {
            try
             {
                 return Json(new CourseMasterModel().ReferCourseWithLang(id, lang), JsonRequestBehavior.AllowGet);
             }
             catch (Exception e)
             {
                 return null;
             }
        }
        /// <summary>
        /// Kiểm tra beautyId đã tồn tại hay chưa.
        /// Author       :   TramHTD - 27/08/2018 - create
        /// </summary>
        /// <param name="value">giá trị của beautyId cần kiểm tra</param>
        /// <returns>Nếu có tồn tại trả về true, ngược lại trả về false</returns>
        /// <remarks>
        /// Method: GET
        /// RouterName: CheckExistBeautyId
        /// </remarks>
        public bool CheckExistBeautyId(string value, string id)
        {
            try
            {
                int Id = Convert.ToInt32(id);
                return new CourseMasterModel().CheckExistBeautyId(value, Id);
            }
            catch
            {
                return false;
            }
        }
    }
}