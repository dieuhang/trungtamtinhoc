﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.ConfigurationManagement.Schema
{
    /// <summary>
    /// Class dùng để chứa thông tin của một cấu hình
    /// Author       :   HaLTH - 13/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class Configuration
    {
        [Required]
        public int SoLanChoPhepDangNhapSai { get; set; }

        [Required]
        public int ThoiGianKhoa { get; set; }

        [Required]
        public int ThoiGianTonTaiToken { get; set; }

        [Required]
        [StringLength(20)]
        public string FbAppId { set; get; }

        [Required]
        [StringLength(80)]
        public string GgClientId { set; get; }
    }
}