﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TrungTamTinHoc.Areas.ControlPanel.Models.UserManagement.Schema;
using TTTH.Common;
using TTTH.Common.Enums;
using TTTH.DataBase;
using TTTH.DataBase.Schema;
using Z.EntityFramework.Plus;
using Account = TrungTamTinHoc.Areas.ControlPanel.Models.UserManagement.Schema.Account;
using Group = TrungTamTinHoc.Areas.ControlPanel.Models.UserManagement.Schema.Group;
using TblAccount = TTTH.DataBase.Schema.Account;
using User = TTTH.DataBase.Schema.User;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.UserManagement
{
    /// <summary>
    /// Class chứa các phương thức liên quan đến việc xử lý tài khoản
    /// Author       :   TramHTD - 10/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class UserMasterModel
    {
        DataContext context;

        public UserMasterModel()
        {
            context = new DataContext();
        }

        /// <summary>
        /// Lấy danh sách các group có trong db
        /// Author       :   TramHTD - 10/08/2018 - create
        /// </summary>
        /// <param></param>
        /// <returns>Danh sách các group</returns>
        public List<Group> GetListGroups()
        {
            string Lang = Common.GetLang();
            return context.GroupTrans.Where(x => x.Lang == Lang && x.Id!=1 && !x.DelFlag ).Select(x => new Group
            {
                IdGroup = x.Id,
                TenGroup = x.GroupName
            }).ToList();
        }

        /// <summary>
        /// Lấy thông tin của 1 User
        /// Author       :   TramHTD - 20/08/2018 - create
        /// </summary>
        /// <param name="id">id của User sẽ được hiển thị</param>
        /// <returns>Thông tin của User</returns>
        public UserMaster LoadUser(string id)
        {
            try
            {
                UserMaster userMaster = new UserMaster();
                int idAccount = 0;
                try
                {
                    idAccount = Convert.ToInt32(id);
                }
                catch { }
                TblAccount account = context.Account.FirstOrDefault(x => x.Id == idAccount && !x.DelFlag);
                if (account != null)
                {
                    userMaster.Mode = (int)ConstantsEnum.ModeMaster.Update;
                    userMaster.User = new Account();
                    userMaster.User.Id = account.Id;
                    userMaster.User.Ho = account.User.Ho;
                    userMaster.User.Ten = account.User.Ten;
                    userMaster.User.Username = account.Username;
                    userMaster.User.Password = account.Password;
                    userMaster.User.GioiTinh = account.User.GioiTinh;
                    userMaster.User.NgaySinh = account.User.NgaySinh;
                    userMaster.User.Email = account.Email;
                    userMaster.User.IdGroup = account.GroupOfAccount.Where(p=>!p.DelFlag).Select(y => y.IdGroup).ToList();
                }
                return userMaster;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Tạo hoặc cập nhật tài khoản dựa vào thông tin đã cung cấp
        /// Author       :   TramHDT - 20/08/2018 - create
        /// </summary>
        /// <param name="newAccount">Thông tin tạo hoặc cập nhật tài khoản</param>
        /// <returns>Thông tin về việc tạo hoặc cập nhật tài khoản thành công hay thất bại</returns>
        public ResponseInfo SaveUser(Account newAccount)
        {
            try
            {
                if (context.Account.FirstOrDefault(x => x.Id == newAccount.Id && !x.DelFlag) != null)
                {
                    return UpadateUser(newAccount);
                }
                return AddUser(newAccount);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Cập nhật lại tài khoản dựa vào thông tin đã cung cấp
        /// Author       :   TramHDT - 20/08/2018 - create
        /// </summary>
        /// <param name="newAccount">Thông tin cập nhật tài khoản</param>
        /// <returns>Thông tin về việc cập nhật tài khoản thành công hay thất bại</returns>
        public ResponseInfo UpadateUser(Account newAccount)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo response = new ResponseInfo();
                
                    context.User.Where(x => x.Id == newAccount.Id && !x.DelFlag)
                        .Update(x => new User
                        {
                            GioiTinh = newAccount.GioiTinh,
                            NgaySinh = newAccount.NgaySinh,
                            Ho = newAccount.Ho,
                            Ten = newAccount.Ten,
                        });
                context.GroupOfAccount.Where(x => x.IdAccount == newAccount.Id && !x.DelFlag)
                    .Update(x => new GroupOfAccount
                    {
                        DelFlag = true
                    });
                TblAccount account = context.Account.FirstOrDefault(x => x.Id == newAccount.Id && !x.DelFlag);
                if (account != null)
                {
                    // Cho tài khoản thuộc vào nhiều group
                    foreach (int idGroup in newAccount.IdGroup)
                    {
                        account.GroupOfAccount.Add(new GroupOfAccount
                        {
                            IdGroup = idGroup
                        });
                    }
                }
                context.SaveChanges();
                transaction.Commit();
                return response;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }


        /// <summary>
        /// Tạo tài khoản dựa vào thông tin đã cung cấp, sau đó gửi mail thông báo tên đăng nhập và mật khẩu cho người dùng
        /// Author       :   TramHDT - 20/08/2018 - create
        /// </summary>
        /// <param name="newAccount">Thông tin tạo tài khoản</param>
        /// <returns>Thông tin về việc tạo tài khoản thành công hay thất bại</returns>
        public ResponseInfo AddUser(Account newAccount)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo result = new ResponseInfo();
                // Kiểm tra xem username đã tồn tại hay chưa
                TblAccount account = context.Account.FirstOrDefault(x => x.Username == newAccount.Username && !x.DelFlag);
                if (account == null)
                {
                    // Kiểm tra xem email đã tồn tại hay chưa
                    account = context.Account.FirstOrDefault(x => x.Email == newAccount.Email && !x.DelFlag);
                    if (account == null)
                    {
                        if (newAccount.Password == null)
                        {
                            newAccount.Password = Common.GetToken(newAccount.Id, 8);
                        }

                        string pass = BaoMat.GetSimpleMD5(newAccount.Password);
                        // Tạo user mới
                        User user = new User
                        {
                            Ho = newAccount.Ho,
                            Ten = newAccount.Ten,
                            Avatar = Common.defaultAvata,
                            GioiTinh = newAccount.GioiTinh,
                            NgaySinh = newAccount.NgaySinh,
                            SoDienThoai = "",
                            CMND = "",
                            DiaChi = ""
                        };
                        // Tạo tài khoản đăng nhập cho user
                        newAccount.Id = context.Account.Count() == 0 ? 1 : context.Account.Max(x => x.Id) + 1;
                        account = new TblAccount
                        {
                            Username = newAccount.Username,
                            Password = BaoMat.GetMD5(pass),
                            Email = newAccount.Email,
                            TokenActive = "",
                            IsActived = true,
                            IsActiveEmail = true,
                            TimeOfToken = null,
                            SoLanDangNhapSai = 0,
                            KhoaTaiKhoanDen = DateTime.Now
                        };
                        // Cho tài khoản thuộc vào nhiều group
                        foreach (int idGroup in newAccount.IdGroup)
                        {
                            account.GroupOfAccount.Add(new GroupOfAccount
                            {
                                IdGroup = idGroup
                            });
                        }

                        user.Account.Add(account);
                        context.User.Add(user);
                        // Lưu vào CSDL
                        context.SaveChanges();
                        // Tiến hành gửi mail
                        SendEmail(newAccount);
                        result.ThongTinBoSung1 = newAccount.Id + "";
                    }
                     else
                     {
                         result.Code = 202;
                         result.MsgNo = 37;
                     }
                 }
                 else
                 {
                     result.Code = 202;
                     result.MsgNo = 36;
                 }
                transaction.Commit();
                return result;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }
        /// <summary>
        /// Kiểm tra email hoặc username đã tồn tại hay chưa.
        /// Author       :   TramHTD - 27/08/2018 - create
        /// </summary>
        /// <param name="value">giá trị của email hoặc username cần kiểm tra</param>
        /// <param name="type">type = 1: kiểm tra usernme; type = 2: kiểm tra email</param>
        /// <returns>Nếu có tồn tại trả về true, ngược lại trả về false</returns>
        public bool CheckExistAccount(string value, string type)
        {
            try
            {
                TblAccount acount = context.Account.FirstOrDefault(x => ((type == "1" && x.Username == value)
                                                                         || (type == "2" && x.Email == value)) && !x.DelFlag);
                if (acount != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Hàm gửi mail thông báo về tạo tài khoản
        /// Author       :   TramHDT - 02/09/2018 - create
        /// </summary>
        /// <param name="account">Thông tin tài khoản vừa được tạo</param>
        public void SendEmail(Account account)
        {
            try
            {
                string lang = Common.GetLang();
                BieuMau bieuMau = context.BieuMau.FirstOrDefault(x => x.Id == (int)ConstantsEnum.TemplateEnum.CreateAccount && x.Lang == lang && !x.DelFlag);
                if (bieuMau != null)
                {
                    bieuMau.NoiDung = bieuMau.NoiDung.Replace("tendangnhap", account.Username);
                    bieuMau.NoiDung = bieuMau.NoiDung.Replace("matkhau", account.Password);
                    bieuMau.NoiDung = bieuMau.NoiDung.Replace("#link", Common.domain);
                    EmailService.Send(account.Email, bieuMau.TenBieuMau, bieuMau.NoiDung);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }


}