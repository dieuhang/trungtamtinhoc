﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.PermissionManagement.Schema
{
    /// <summary>
    /// Class dùng để chứa thông tin của các quyền trong một màn hình
    /// Author       :   AnTM - 23/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class PermissionsInScreen
    {
        public string NameScreen { get; set; }

        public List<Permission> CacPermission { get; set; }
    }
}