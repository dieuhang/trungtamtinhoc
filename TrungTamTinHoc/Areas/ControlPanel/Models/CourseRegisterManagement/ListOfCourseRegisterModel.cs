﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TrungTamTinHoc.Areas.ControlPanel.Models.CourseRegisterManagement.Schema;
using TTTH.Common;
using TTTH.Common.Enums;
using TTTH.DataBase;
using TTTH.DataBase.Schema;
using Z.EntityFramework.Plus;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.CourseRegisterManagement
{
    /// <summary>
    /// Class chứa tất cả các phương thức liên quan đến hiển thị các đăng ký học ở trang quản lý đăng ký học của admin
    /// Author       :   AnTM - 14/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   Home.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ListOfCourseRegisterModel
    {
        private DataContext context;

        public ListOfCourseRegisterModel()
        {
            context = new DataContext();
        }

        /// <summary>
        /// Lấy danh sách các khóa học được phép hiển thị từ DB theo ngôn ngữ.
        /// Author       :   AnTM - 14/07/2018 - create
        /// Author       :   HoangNM - 25/07/2018 - update
        /// </summary>
        /// <returns>Danh sách khóa học có trong DB</returns>
        ///
        public ListOfCourseRegister GetListOfCourseRegister(CourseRegisterConditionSearch condition)
        {
            try
            {
                if (condition == null)
                {
                    condition = new CourseRegisterConditionSearch();
                }
                string lang = Common.GetLang();
                if (context.TrangThai.Where(x => !x.DelFlag && x.Id == condition.idTrangThai).Count() == 0) condition.idTrangThai = 0;
                if (context.LopHoc.Where(x => !x.DelFlag && x.Id == condition.idLopHoc).Count() == 0) condition.idLopHoc = 0;
                if (context.KhoaHoc.Where(x => !x.DelFlag && x.Id == condition.idKhoaHoc).Count() == 0) condition.idKhoaHoc = 0;

                ListOfCourseRegister listOfCourseRegister = new ListOfCourseRegister();
                listOfCourseRegister.Paging = new Paging(context.DangKyLopHoc.Count(x =>
                    (condition.KeySearch == null || (condition.KeySearch != null
                                                    && ((x.HoHocVien + " " + x.TenHocVien).Contains(condition.KeySearch)
                                                    || x.SoDienThoaiHocVien.Contains(condition.KeySearch)
                                                    || x.EmailHocVien.Contains(condition.KeySearch)
                                                    || (x.HoGiamHo + " " + x.TenGiamHo).Contains(condition.KeySearch)
                                                    || x.SoDienThoaiGiamHo.Contains(condition.KeySearch)
                                                    || x.EmailGiamHo.Contains(condition.KeySearch)
                                                    )))
                    && (condition.idTrangThai == 0 || (condition.idTrangThai != 0 && (x.TrangThaiDangKy == condition.idTrangThai)))
                    && (condition.tuNgay == null || (x.ThoiGianDangKy >= condition.tuNgay))
                    && (condition.toiNgay == null || x.ThoiGianDangKy <= condition.toiNgay)
                    && (condition.idKhoaHoc == 0 || (condition.idKhoaHoc != 0 && ((condition.idLopHoc != 0 && (x.IdLopHoc == condition.idLopHoc)) || (condition.idLopHoc == 0 && x.LopHoc.IdKhoaHoc == condition.idKhoaHoc))))
                    && !x.DelFlag), condition.CurrentPage, condition.PageSize);

                listOfCourseRegister.CourseRegisterList = context.DangKyLopHoc.Where(x =>
                    (condition.KeySearch == null || (condition.KeySearch != null
                                                    && ((x.HoHocVien + " " + x.TenHocVien).Contains(condition.KeySearch)
                                                    || x.SoDienThoaiHocVien.Contains(condition.KeySearch)
                                                    || x.EmailHocVien.Contains(condition.KeySearch)
                                                    || (x.HoGiamHo + " " + x.TenGiamHo).Contains(condition.KeySearch)
                                                    || x.SoDienThoaiGiamHo.Contains(condition.KeySearch)
                                                    || x.EmailGiamHo.Contains(condition.KeySearch)
                                                    )))
                    && (condition.idTrangThai == 0 || (condition.idTrangThai != 0 && (x.TrangThaiDangKy == condition.idTrangThai)))
                    && (condition.tuNgay == null || (x.ThoiGianDangKy >= condition.tuNgay))
                    && (condition.toiNgay == null || x.ThoiGianDangKy <= condition.toiNgay)
                    && (condition.idKhoaHoc == 0 || (condition.idKhoaHoc != 0 && ((condition.idLopHoc != 0 && (x.IdLopHoc == condition.idLopHoc)) || (condition.idLopHoc == 0 && x.LopHoc.IdKhoaHoc == condition.idKhoaHoc))))
                    && !x.DelFlag)
                    .OrderBy(x => x.TrangThai.LoaiTrangThai).OrderByDescending(x => x.ThoiGianDangKy).Skip((listOfCourseRegister.Paging.CurrentPage - 1) * listOfCourseRegister.Paging.NumberOfRecord)
                    .Take(listOfCourseRegister.Paging.NumberOfRecord)
                    .Select(x => new CourseRegister()
                    {
                        Id = x.Id,
                        TenLopHoc = x.LopHoc.LopHocTrans.FirstOrDefault(y => y.Lang == lang && !y.DelFlag).TenLop,
                        SoDienThoaiGiamHo = x.SoDienThoaiGiamHo,
                        EmailGiamHo = x.EmailGiamHo,
                        TrangThaiDangKy = x.TrangThaiDangKy,
                        TenHocVien = x.HoHocVien + " " + x.TenHocVien,
                        TenGiamHo = x.HoGiamHo + " " + x.TenGiamHo,
                        EmailHocVien = x.EmailHocVien,
                        SoDienThoaiHocVien = x.SoDienThoaiHocVien,
                        NgayDangKy = x.ThoiGianDangKy,
                        TenKhoaHoc = x.LopHoc.KhoaHoc.KhoaHocTrans.FirstOrDefault(y => y.Lang == lang && !y.DelFlag).TenKhoaHoc
                    })
                    .ToList();
                listOfCourseRegister.Condition = condition;
                listOfCourseRegister.cacTrangThaiDangKy = context.TrangThaiTrans.Where(x => x.Lang == lang && !x.DelFlag && x.TrangThai.LoaiTrangThai == (int)TTTH.Common.Enums.ConstantsEnum.LoaiTrangThai.TrangThaiDangKy)
                    .Select(x => new TrangThaiDangKy()
                    {
                        IDTrangThai = x.Id,
                        TenTrangThai = x.TenTrangThai
                    }).ToList();
                listOfCourseRegister.cacKhoaHoc = context.KhoaHocTrans.Where(x => x.Lang == lang && !x.DelFlag).Select(x => new KhoaHocHienThi()
                {
                    IDKhoaHoc = x.Id,
                    TenKhoaHoc = x.TenKhoaHoc
                }).ToList();
                if (condition.idKhoaHoc != 0)
                {
                    listOfCourseRegister.cacLopHoc = context.LopHoc.Include("LopHocTrans").Where(x => !x.DelFlag && x.IdKhoaHoc == condition.idKhoaHoc && x.LopHocTrans.Count > 0)
                        .Select(x => new LopHocHienThi()
                        {
                            IDLopHoc = x.Id,
                            TenLopHoc = x.LopHocTrans.FirstOrDefault(y => y.Lang == lang).TenLop
                        }).ToList();
                }
                return listOfCourseRegister;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Cập nhật trạng thái hiển thị của danh sách đăng ký khóa học trên trang chủ.
        /// Author       :   HoangNM - 24/07/2018 - create
        /// </summary>
        /// <param name="id">id của danh sách đăng ký khóa học sẽ được cập nhật trạng thái</param>
        /// <param name="newIdTrangThai">idTrangThai của danh sách đăng ký khóa học vừa được cập nhật</param>
        public ResponseInfo UpdateShow(int id, int newIdTrangThai)
        {
            ResponseInfo response = new ResponseInfo();
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                DangKyLopHoc dangKyHocInfo = context.DangKyLopHoc.FirstOrDefault(x => x.Id == id && !x.DelFlag);
                dangKyHocInfo.TrangThaiDangKy = newIdTrangThai;
                if (newIdTrangThai == (int)ConstantsEnum.TrangThaiDangKy.HoanThanh)
                {
                    context.HocVien.Add(new HocVien()
                    {
                        IdUser = dangKyHocInfo.IdUser,
                        IdLopHoc = dangKyHocInfo.IdLopHoc,
                        ThoiGianBatDauHoc = dangKyHocInfo.LopHoc.ThoiGianBatDau,
                        ThoiGianKetThucHoc = dangKyHocInfo.LopHoc.ThoiGianKetThuc,
                        SoTietDaHoc = 0,
                        DaNopTien = false
                    });
                }
                context.SaveChanges();
                transaction.Commit();
                response.ThongTinBoSung5 = context.DangKyLopHoc.Count(x => x.TrangThaiDangKy == (int)ConstantsEnum.TrangThaiDangKy.DangKyMoi);
                return response;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        /// <summary>
        /// Xóa các CouseRegister trong DB.
        /// Author       :   HoangNM - 25/07/2018 - create
        /// </summary>
        /// <param name="ids">Danh sách id của các CourseRegister sẽ xóa</param>
        /// <returns>True nếu xóa thành công, False nếu không còn slide được hiển thị trên trang chủ, Excetion nếu có lỗi</returns>
        public bool DeleteCouseRegister(List<int> ids)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                bool result = true;
                context.DangKyLopHoc.Where(x => ids.Contains(x.Id)).Delete();
                context.SaveChanges();
                transaction.Commit();
                return result;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        /// <summary>
        /// Lấy danh sách các khóa học được phép hiển thị từ DB theo ngôn ngữ.
        /// Author       :   AnTM - 05/08/2018 - create
        /// </summary>
        /// <returns>Danh sách khóa học có trong DB</returns>
        public List<KhoaHocHienThi> LayKhoaHocHienThi()
        {
            try
            {
                string lang = Common.GetLang();
                return context.KhoaHoc.Include("KhoaHocTrans").Where(x => !x.DelFlag).Select(x => new KhoaHocHienThi
                {
                    TenKhoaHoc = x.KhoaHocTrans.FirstOrDefault(y => y.Lang == lang).TenKhoaHoc,
                    IDKhoaHoc = x.Id
                }).ToList();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Lấy danh sách các khóa học được phép hiển thị từ DB theo id khóa học và theo ngôn ngữ
        /// Author       :   AnTM - 05/08/2018 - create
        /// </summary>
        /// <param name="idKhoaHoc">ID khóa học</param>
        /// <returns>Danh sách lớp học có trong DB</returns>
        public List<LopHocHienThi> LayLopHocHienThiTheoIDKhoaHoc(int idKhoaHoc)
        {
            try
            {
                string lang = Common.GetLang();
                return context.KhoaHoc.FirstOrDefault(x => !x.DelFlag && idKhoaHoc == x.Id && x.LopHoc.Count > 0)?
                    .LopHoc.Where(x => x.ChoPhepDangKy && !x.DelFlag && x.LopHocTrans.Count > 0)
                    .Select(x => new LopHocHienThi()
                    {
                        IDLopHoc = x.Id,
                        TenLopHoc = x.LopHocTrans.FirstOrDefault(q => q.Lang == lang)?.TenLop
                    }).ToList();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Lấy thông tin chi tiết của đăng ký học để hiển thị chi tiết
        /// Author       :   AnTM - 08/08/2018 - create
        /// </summary>
        /// <param name="idDangKy">ID đăng ký học</param>
        /// <returns>thông tin chi tiết của 1 record đăng ký học</returns>
        public CourseRegister LayChiTietThongTinDangKyHoc(int idDangKy)
        {
            try
            {
                string lang = Common.GetLang();
                return context.DangKyLopHoc.Where(x => !x.DelFlag && idDangKy == x.Id).Select(x => new CourseRegister()
                {
                    Id = x.Id,
                    DiaChiGiamHo = x.DiaChiGiamHo,
                    DiaChiHocVien = x.DiaChiHocVien,
                    EmailGiamHo = x.EmailGiamHo,
                    EmailHocVien = x.EmailHocVien,
                    GhiChu = x.GhiChu,
                    GioiTinhGiamHo = x.GioiTinhGiamHo,
                    GioiTinhHocVien = x.GioiTinhHocVien,
                    HoGiamHo = x.HoGiamHo,
                    HoHocVien = x.HoHocVien,
                    NgayDangKy = x.ThoiGianDangKy,
                    NgaySinhGiamHo = x.NgaySinhGiamHo,
                    NgaySinhHocVien = x.NgaySinhHocVien,
                    SoCMNDGiamHo = x.CMNDGiamHo,
                    SoCMNDHocVien = x.CMNDHocVien,
                    SoDienThoaiGiamHo = x.SoDienThoaiGiamHo,
                    SoDienThoaiHocVien = x.SoDienThoaiHocVien,
                    TenGiamHo = x.TenGiamHo,
                    TenHocVien = x.TenHocVien,
                    TenLopHoc = x.LopHoc.LopHocTrans.FirstOrDefault(y => y.Lang == lang && !y.DelFlag).TenLop,
                    TenKhoaHoc = x.LopHoc.KhoaHoc.KhoaHocTrans.FirstOrDefault(y => y.Lang == lang && !y.DelFlag).TenKhoaHoc,
                    TrangThaiDangKy = x.TrangThaiDangKy,
                    XaGiamHo = x.XaGiamHo.TenXa,
                    XaHocVien = x.XaHocVien.TenXa,
                    HuyenGiamHo = x.XaGiamHo.Huyen.TenHuyen,
                    HuyenHocVien = x.XaHocVien.Huyen.TenHuyen,
                    TinhGiamHo = x.XaGiamHo.Huyen.Tinh.TenTinh,
                    TinhHocVien = x.XaHocVien.Huyen.Tinh.TenTinh
                }).FirstOrDefault();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Kiểm tra sự tồn tại của đăng ký học
        /// Author       :   AnTM - 08/08/2018 - create
        /// </summary>
        /// <param name="id">ID đăng ký học</param>
        /// <returns>true: tồn tại/ false: không tồn tại</returns>
        public bool CheckExistCourseRegister(int id)
        {
            try
            {
                DangKyLopHoc dangKyHoc = context.DangKyLopHoc.FirstOrDefault(x => !x.DelFlag && x.Id == id);
                if (dangKyHoc != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}