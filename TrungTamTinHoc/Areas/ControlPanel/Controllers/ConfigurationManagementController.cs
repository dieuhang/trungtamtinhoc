﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrungTamTinHoc.Areas.ControlPanel.Models.ConfigurationManagement;
using TrungTamTinHoc.Areas.ControlPanel.Models.ConfigurationManagement.Schema;
using TrungTamTinHoc.Controllers;
using TTTH.Common;
using TTTH.Validate;
using static TTTH.Common.Enums.ConstantsEnum;
using static TTTH.Common.Enums.MessageEnum;

namespace TrungTamTinHoc.Areas.ControlPanel.Controllers
{
    /// <summary>
    /// Class chứa các điều hướng liên quan đến quản lý cấu hình
    /// Author       :   HaLTH - 13/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ConfigurationManagementController : BaseAdminController
    {
        /// <summary>
        /// Điều hướng đến trang hiển thị chỉnh sửa cấu hình nếu là request thông thường.
        /// Điều hướng về trang lỗi nếu có lỗi sảy ra.
        /// Author       :   HaLTH - 13/08/2018 - create
        /// </summary>
        /// Method: GET
        /// RouterName: EditConfiguration
        /// </remarks>
        public ActionResult EditConfiguration()
        {
            try
            {
                ConfigurationMaster configurationMaster = new ConfigurationModel().LoadConfiguration();
                return View("ConfigurationMaster", configurationMaster);
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }

        /// <summary>
        /// Cập nhật cấu hình theo thông tin người dùng đưa lên.
        /// Author       :   HaLTH - 13/08/2018 - create
        /// </summary>
        /// <param name="data">Thông tin cấu hình mà người dùng nhập vào</param>
        /// <returns>Chuỗi Json chứa kết quả của việc cập nhật cấu hình</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: SaveConfiguration
        /// </remarks>
        [ValidateInput(false)]
        public JsonResult SaveConfiguration(Configuration data)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                if (ModelState.IsValid)
                {
                    response = new ConfigurationModel().EditConfiguration(data);
                }
                else
                {
                    response.Code = (int)CodeResponse.NotValidate;
                    response.ListError = ModelState.GetModelErrors();
                }
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}