﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrungTamTinHoc.Areas.Course.Models.Schema
{
    /// <summary>
    /// Class chứa các thuộc tính của 1 khóa học lấy từ DB cho việc hiển thị danh sách các khóa học trên ChiTietkhoaHoc va DanhsachKhoaHoc
    /// 
    /// Author       :   TramHDT - 24/06/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   Home.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class KhoaHoc
    {
        public int Id { get; set; }
        public string BeautyId { set; get; }
        public string TenKhoaHoc { set; get; }
        public string TomTat { set; get; }
        public string AnhMinhHoa { set; get; }
        public DateTime? NgayKhaiGiang { set; get; }
        public int SoLuongView { set; get; }
        public int SoLuongComment { set; get; }
        public int SoLuongDanhGia { set; get; }
        public int DiemDanhGia { set; get; }
        public bool ChoPhepDangKy { set; get; }
    }
    /// <summary>
    /// Class chứa các thuộc tính của 1 khóa học lấy từ DB cho việc hiển thị chi tiết khóa học lên trang ChiTietkhoaHoc
    /// Author       :   TramHDT - 24/06/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   Home.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ChiTietKhoaHoc : KhoaHoc
    {
        public string ChiTiet { get; set; }
        public string DoTuoi { get; set; }
        public int SoLuongDaDangKy { get; set; }
        public string ThoiGian { set; get; }
        public DateTime? ThoiGianKetThuc { set; get; }
        public string LichHoc { set; get; }
        public decimal HocPhi { set; get; }
        public List<CommentKhoaHoc> ListCommentKhoaHocs { set; get; }
        public string AvatarUser  { get; set; }
        public string HotenUser { get; set; }
        public int IdUser { get; set; }
        public int CountComment { get; set; }
    }

    /// <summary>
    /// Class chứa các thuộc tính của 1 Comment khóa học lấy từ DB cho việc hiển thị comment khóa học lên trang ChiTietkhoaHoc
    /// Author       :   TramHDT - 24/06/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   Home.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class CommentKhoaHoc
    {
        public int id { get; set; }
        public string Avatar { get; set; }
        public string HoTen { get; set; }
        [Required]
        public string NoiDung { get; set; }
        public DateTime? ThoiGianCmt { set; get; }
        public int? IdUser { get; set; }
        public List<CommentKhoaHoc> ChildCommentKhoaHocs { set; get; }
        public int CountChildComment { get; set; }
    }

    /// <summary>
    /// Class dùng để chứa thông tin tìm kiếm của danh sách khóa học
    /// Author       :   TramHTD - 06/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class CourseConditionSearch
    {
        public string KeySearch { set; get; }
        public int IdChuyenNganh { set; get; }
        public int CurrentPage { set; get; }
        public int PageSize { set; get; }
        public CourseConditionSearch()
        {
            this.KeySearch = "";
            this.IdChuyenNganh = 0;
            this.PageSize = 9;
            this.CurrentPage = 1;
        }
    }
    /// <summary>
    /// Class dùng để chứa thông tin của chuyên ngành
    /// Author       :   TramHTD - 06/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ChuyenNganh
    {
        public int IdChuyenNganh { set; get; }
        public string TenChuyenNganh { set; get; }
    }
}