﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrungTamTinHoc.Areas.Home.Models;
using TrungTamTinHoc.Areas.Home.Models.Schema;
using TTTH.Validate;
using TTTH.Common;
using TblTokenLogin = TTTH.DataBase.Schema.TokenLogin;
using TblAccount = TTTH.DataBase.Schema.Account;
using static TTTH.Common.Enums.ConstantsEnum;
using static TTTH.Common.Enums.MessageEnum;
using System.Net.Mail;
using System.Net;
using TTTH.DataBase;
using System.Web.Helpers;

namespace TrungTamTinHoc.Areas.Home.Controllers
{
    
    /// <summary>
    /// Class chứa các điều hướng liên quan đến login
    /// Author       :   QuyPN - 28/05/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   Home
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class LoginController : Controller
    {
        /// <summary>
        /// Điều hướng đến trang login.
        /// Author       :   QuyPN - 28/05/2018 - create
        /// </summary>
        /// <returns>Trang view login, nếu có lỗi sẽ rả về trang error</returns>
        /// <remarks>
        /// Method: GET
        /// RouterName: UserLogin
        /// </remarks>
        public ActionResult Index()
        {
            try
            {
                int idAccount = XacThuc.GetIdAccount();
                if (idAccount != 0)
                {
                    return RedirectToAction("Index", "Home", new { area = "home" });
                }
                return View();
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }
        /// <summary>
        /// Xác thực thông tin người dùng gửi lên.
        /// Author: QuyPN - 028/05/2018 - create
        /// </summary>
        /// <param name="account">Đối tượng chưa thông tin tài khaonr của người dùng</param>
        /// <returns>Chỗi Json chứa kết quả kiểm tra</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: homeCheckLogin
        /// </remarks>
        public JsonResult CheckLogin(Account account)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                if (ModelState.IsValid)
                {
                    response = new LoginModel().CheckAccount(account);
                }
                else
                {
                    response.Code = (int)CodeResponse.NotValidate;
                    response.ListError = ModelState.GetModelErrors();
                }
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Xác thực tài khoản thông qua việc login bằng các tài khoản FB hoặc GG.
        /// Author       :   QuyPN - 30/05/2018 - create
        /// </summary>
        /// <param name="accessToken">Thông tin cá nhân lấy được từ FB hoặc GG</param>
        /// <param name="type">type = 1: thông tin từ FB; type = 2: thông tin từ GG</param>
        /// <returns>Chỗi Json chứa kết quả kiểm tra</returns>
        /// <remarks>
        /// Method: GET
        /// RouterName: homeCheckSocialLogin
        /// </remarks>
        public JsonResult LoginBySocialAccount(string accessToken, int type = (int)OtherEnum.TaiKhoanFB)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                LoginModel model = new LoginModel();
                TblTokenLogin token = model.CheckSocialAccount(type == (int)OtherEnum.TaiKhoanFB
                    ? model.LayThongTinFB(accessToken)
                    : model.LayThongTinGG(accessToken), type);
                if (token != null)
                {
                    response.ThongTinBoSung1 = BaoMat.Base64Encode(token.Token);
                }
                else
                {
                    response.Code = (int)CodeResponse.NotAccess;
                    response.MsgNo = (int)MsgNO.XacThucKhongHopLe;
                }
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Điều hướng việc logout khỏi hệ thống.
        /// Author       :   QuyPN - 28/05/2018 - create
        /// </summary>
        /// <returns>Trở về lại trang login, trả về trang error nếu có lỗi</returns>
        /// <remarks>
        /// Method: GET
        /// RouterName: homeLogout
        /// </remarks>
        public ActionResult Logout(string token)
        {
            try
            {
                new LoginModel().RemoveToken(token);
                return RedirectToAction("login");
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }

        /// <summary>
        /// Điều hướng việc logout khỏi hệ thống.
        /// Author       :   QuyPN - 28/05/2018 - create
        /// </summary>
        /// <returns>Trở về lại trang login, trả về trang error nếu có lỗi</returns>
        /// <remarks>
        /// Method: GET
        /// RouterName: homeLogout
        /// </remarks>
        public ActionResult ForgotPassword()
        {
            return View();
        }

        /// <summary>
        /// Kiểm tra email đã tồn tại hay chưa.
        /// Author       :   HaLTH - 27/08/2018 - create
        /// </summary>
        /// </remarks>
        public bool CheckExistEmail(string email)
        {
            try
            {
                return new LoginModel().CheckExistEmail(email);
            }
            catch
            {
                return false;
            }
        }

        [NonAction]
        public void SendVerificationLinkEmail(string emailID, string activationCode, string emailFor = "VerifyAccount")
        {
            var verifyUrl = "/User/" + emailFor + "/" + activationCode;
            var link = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, verifyUrl);

            var fromEmail = new MailAddress("honghalt97@gmail.com", "IPro");
            var toEmail = new MailAddress(emailID);
            var fromEmailPassword = "hongha141197"; // Replace with actual password

            string subject = "";
            string body = "";
            if (emailFor == "VerifyAccount")
            {
                subject = "Your account is successfully created!";
                body = "<br/><br/>We are excited to tell you that your Dotnet Awesome account is" +
                    " successfully created. Please click on the below link to verify your account" +
                    " <br/><br/><a href='" + link + "'>" + link + "</a> ";

            }
            else if (emailFor == "ResetPassword")
            {
                subject = "Reset Password";
                body = "Hi,<br/>br/>We got request for reset your account password. Please click on the below link to reset your password" +
                    "<br/><br/><a href=" + link + ">Reset Password link</a>";
            }


            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
            };

            using (var message = new MailMessage(fromEmail, toEmail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
                smtp.Send(message);
        }

        
        
        //[HttpPost]
        //public ActionResult ForgotPassword(string EmailID)
        //{
        //    //Verify Email ID
        //    //Generate Reset password link 
        //    //Send Email 
        //    string message = "";
        //    bool status = false;

        //      var account = context.Users.Where(a => a.EmailID == EmailID).FirstOrDefault();
        //        if (account != null)
        //        {
        //            //Send email for reset password
        //            string resetCode = Guid.NewGuid().ToString();
        //            SendVerificationLinkEmail(account.EmailID, resetCode, "ResetPassword");
        //            account.ResetPasswordCode = resetCode;
        //            //This line I have added here to avoid confirm password not match issue , as we had added a confirm password property 
        //            //in our model class in part 1
        //            dc.Configuration.ValidateOnSaveEnabled = false;
        //            dc.SaveChanges();
        //            message = "Reset password link has been sent to your email id.";
        //        }
        //        else
        //        {
        //            message = "Account not found";
        //        }
            
        //    ViewBag.Message = message;
        //    return View();
        //}

        //public ActionResult ResetPassword(string id)
        //{
        //    //Verify the reset password link
        //    //Find account associated with this link
        //    //redirect to reset password page
        //    if (string.IsNullOrWhiteSpace(id))
        //    {
        //        return HttpNotFound();
        //    }
        //    var user = dc.Users.Where(a => a.ResetPasswordCode == id).FirstOrDefault();
        //    if (user != null)
        //    {
        //        ResetPasswordModel model = new ResetPasswordModel();
        //        model.ResetCode = id;
        //        return View(model);
        //    }
        //    else
        //    {
        //        return HttpNotFound();
        //    }
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult ResetPassword(ResetPasswordModel model)
        //{
        //    var message = "";
        //    if (ModelState.IsValid)
        //    {
                 
        //    }
        //    else
        //    {
        //        message = "Something invalid";
        //    }
        //    ViewBag.Message = message;
        //    return View(model);
        //}
    }
}