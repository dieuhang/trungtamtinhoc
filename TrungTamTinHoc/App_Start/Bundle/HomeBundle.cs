﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace TrungTamTinHoc.App_Start.Bundle
{
    public class HomeBundle
    {
        public static BundleCollection RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/public/js/login").Include(
                "~/public/js/common/md5.js",
                "~/public/js/home/login/index.js",
                "~/public/js/home/login/socialLogin.js"
            ));

            bundles.Add(new ScriptBundle("~/public/js/registerAccount").Include(
                "~/public/js/common/md5.js",
                "~/public/assets/moment/moment.js",
                "~/public/assets/bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js",
                "~/public/js/home/registerAccount/register.js"
            ));

            bundles.Add(new ScriptBundle("~/public/js/registerSuccess").Include(
                "~/public/js/home/registerAccount/registerSuccess.js"
            ));

            bundles.Add(new ScriptBundle("~/public/js/activeAccount").Include(
                "~/public/js/home/registerAccount/activeAccount.js"
            ));
            bundles.Add(new ScriptBundle("~/public/js/contact").Include(
                "~/public/js/home/contact/contact.js"
            ));
            bundles.Add(new ScriptBundle("~/public/js/tinTuc").Include(
                "~/public/js/home/tinTuc/tinTuc.js"
            ));
            bundles.Add(new StyleBundle("~/public/js/danhSachTinTuc").Include(
                "~/public/assets/pagination/pagination.js",
               "~/public/js/home/tinTuc/danhSachTinTuc.js"
           ));
            bundles.Add(new StyleBundle("~/public/css/registerAccount").Include(
                "~/public/assets/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css"
            ));
            bundles.Add(new StyleBundle("~/public/css/register").Include(
                "~/public/css/home/registerAccount/register.css"
            ));
            bundles.Add(new StyleBundle("~/public/css/tinTuc").Include(
               "~/public/css/home/tinTuc/tinTuc.css"
           ));

            bundles.Add(new StyleBundle("~/public/css/danhSachTinTuc").Include(
               "~/public/css/home/tinTuc/danhSachTinTuc.css"
           ));

            bundles.Add(new ScriptBundle("~/public/js/forgotPassword").Include(
                "~/public/js/common/md5.js",
                "~/public/js/home/login/forgotPassword.js"
            ));

            bundles.Add(new StyleBundle("~/public/css/forgotPassword").Include(
                "~/public/css/home/forgotPassword/forgotPassword.css"
            ));

            bundles.Add(new StyleBundle("~/public/css/contact").Include(
               "~/public/css/home/contact/contact.css"
           ));

            return bundles;
        }
    }
}