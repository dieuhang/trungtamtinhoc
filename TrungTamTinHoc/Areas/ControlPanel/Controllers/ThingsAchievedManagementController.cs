﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TTTH.Common;
using TrungTamTinHoc.Areas.ControlPanel.Models.ThingsAchievedManagement;
using TrungTamTinHoc.Areas.ControlPanel.Models.ThingsAchievedManagement.Schema;
using static TTTH.Common.Enums.ConstantsEnum;
using static TTTH.Common.Enums.MessageEnum;
using TTTH.Validate;
using TrungTamTinHoc.Controllers;

namespace TrungTamTinHoc.Areas.ControlPanel.Controllers
{
    /// <summary>
    /// Class chứa các điều hướng liên quan đến quản lý Những điều đạt được
    /// Author       :   HaLTH - 26/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ThingsAchievedManagementController : BaseAdminController
    {
        /// <summary>
        /// Điều hướng đến trang hiển thị danh sách Những điều đạt được nếu là request thông thường.
        /// Trả về table chứa danh sách Những điều đạt được nếu là Ajax.
        /// Điều hướng về trang lỗi nếu có lỗi sảy ra.
        /// Author       :   HaLTH - 26/07/2018 - create
        /// </summary>
        /// <param name="condition">Đối tượng chứa điều kiện tìm kiếm, tạo thành từ query string</param>
        /// <returns>
        /// Trang danh sách Những điều đạt được.
        /// Partial view chứa table danh sách Những điều đạt được.
        /// </returns>
        /// <remarks>
        /// Method: GET
        /// RouterName: ListOfNhungDieuDatDuoc
        /// </remarks>
        public ActionResult ListOfThingsAchieved (ThingsAchievedConditionSearch condition)
        {
            try
            {
                if (Request.IsAjaxRequest())
                {
                    return View("Partial/_tableOfThingsAchieved", new ListOfThingsAchievedModel().GetListOfThingsAchieved(condition));
                }
                return View(new ListOfThingsAchievedModel().GetListOfThingsAchieved(condition));
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }

        /// <summary>
        /// Xóa những điều đạt được theo danh sách id điều đạt được được gửi lên.
        /// Author       :   HaLTH - 26/07/2018 - create
        /// </summary>
        /// <param name="idn">Danh sách id những điều đạt được sẽ xóa</param>
        /// <returns>Đối tượng chứa thông tin về quá trình xóa những điều đạt được</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: DeleteThingsAchieved
        /// </remarks> 
        public JsonResult DeleteThingsAchieved(List<int> ids)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                bool deleted = new ListOfThingsAchievedModel().DeleteThingsAchieved(ids);
                if (!deleted)
                {
                    response.Code = 201;
                    response.MsgNo = 49;
                }
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Update trạng thái hiển thị trên trang chủ của 1 slide.
        /// Author       :   HaLTH - 26/07/2018 - create
        /// </summary>
        /// <param name="id">id của slide sẽ update</param>
        /// <returns>Đối tượng chứa thông tin về quá trình update</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: UpdateShowSlide
        /// </remarks>
        public JsonResult UpdateShow(int id)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                bool updated = new ListOfThingsAchievedModel().UpdateShow(id);
                if (!updated)
                {
                    response.Code = 201;
                    response.MsgNo = 45;
                }
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Điều hướng đến trang Thêm điều đạt được nếu là request thông thường.
        /// Trả về trang thêm Những điều đạt được nếu là Ajax.
        /// Điều hướng về trang lỗi nếu có lỗi sảy ra.
        /// Author       :   HaLTH - 01/08/2018 - create
        /// View chứa trang thêm Những điều đạt được.
        /// </returns>
        /// <remarks>
        /// Method: GET
        /// RouterName: CreateThingAchieved
        /// </remarks>
        public ActionResult ViewCreateThingAchieved()
        {
            try
            {
                return View("ThingsAchievedMaster");
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }

        /// <summary>
        /// Điều hướng đến trang view sửa Những điều đạt được.
        /// Author       :   HaLTH - 01/08/2018 - create
        /// </summary>
        /// <param name="id">Đối tượng chứa thông tin về quá trình chỉnh sửa thông tin</param>
        /// <returns>Trả về trang view sửa Những điều đạt được</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: ViewEditThingsAchieved
        /// </remarks>
        public ActionResult ViewEditThingsAchieved(int id)
        {
            try
            {
                ThingsAchievedMaster thingsAchievedMaster = new ThingsAchievedMasterModel().LoadThingsAchieved(id);
                if (thingsAchievedMaster.Mode == (int)ModeMaster.Insert)
                {
                    return RedirectToAction("ViewCreateThingAchieved");
                }
                return View("ThingsAchievedMaster", thingsAchievedMaster);
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }

        /// <summary>
        /// Tạo điều đạt được hoặc cập nhật điều đạt được theo thông tin người dùng đưa lên.
        /// Author       :   HaLTH - 01/08/2018 - create
        /// </summary>
        /// <param name="data">Thông tin điều đạt được mà người dùng nhập vào</param>
        /// <returns>Chuỗi Json chứa kết quả của việc tạo hoặc cập nhật điều đạt được</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: SaveThingAchieved
        /// </remarks>
        [HttpPost]
        public JsonResult SaveThingAchieved(ThingsAchieved data)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                if (ModelState.IsValid)
                {
                    response = new ThingsAchievedMasterModel().SaveThingsAchieved(data);
                }
                else
                {
                    response.Code = (int)CodeResponse.NotValidate;
                    response.ListError = ModelState.GetModelErrors();
                }
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Lưu thông tin Những điều đạt được được gửi lên bằng ngôn ngữ khác ngôn ngữ mặc định.
        /// Author       :   HaLTH - 01/08/2018 - create
        /// </summary>
        /// <param name="data">Dữ liệu của đối tượng muốn lưu</param>
        /// <returns></returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: UpdateThingsAchievedTranslate
        /// </remarks>
        public JsonResult UpdateThingsAchievedTranslate(ThingsAchieved_Trans data)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                if (ModelState.IsValid)
                {
                    response = new ThingsAchievedMasterModel().UpdateThingsAchievedTranslate(data);
                }
                else
                {
                    response.Code = (int)CodeResponse.NotValidate;
                    response.ListError = ModelState.GetModelErrors();
                }
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Thay đổi ngôn ngữ
        /// Author       :   HaLTH - 01/08/2018 - create
        /// </summary>
        /// <param name="id">Đối tượng muốn thay đổi ngôn ngữ</param>
        /// <param name="lang">Ngôn ngữ muốn thay đổi</param>
        /// <returns></returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: ReferThingsAchievedWithLang
        /// </remarks>
        public JsonResult ReferThingsAchievedWithLang(int id, string lang)
        {
            try
            {
                return Json(new ThingsAchievedMasterModel().ReferThingsAchievedWithLang(id, lang), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}