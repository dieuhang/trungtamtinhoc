﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TTTH.Common;
using TTTH.DataBase;
using static TTTH.Common.Enums.ConstantsEnum;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.ScheduleManagement.Schema
{
    /// <summary>
    /// Class dùng để chứa thông tin của của Lịch học,dùng để thêm hoặc sửa
    /// Author       :   HoangNM - 06/10/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ScheduleMaster
    {
        public Schedule Schedule { set; get; }
        public KhoaHoc khoahoc { get; set; }
        public List<PhongHocHienThi> cacPhongHoc { get; set; }
        public int Mode { set; get; }
        
        public ScheduleMaster()
        {
            Mode = (int)ModeMaster.Insert;
            Schedule = null;
            khoahoc = null;
            cacPhongHoc = new DataContext().PhongHocTrans.Where(x => x.Lang == Common.defaultLang && !x.DelFlag).Select(x => new PhongHocHienThi()
            {
                IDPhongHoc = x.Id,
                TenPhongHoc = x.TenPhong
            }).ToList();
        }
    }
    public class KhoaHoc
    {
        public List<KhoaHocHienThi> cacKhoaHoc { get; set; }
        public List<LopHocHienThi> cacLopHoc { get; set; }
        public int idKhoaHoc { get; set; }
        public int idLopHoc { get; set; }
        public KhoaHoc()
        {
            idKhoaHoc = 1;
        }
    }
    
}