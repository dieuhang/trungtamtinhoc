﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TrungTamTinHoc.Areas.ControlPanel.Models.ScheduleManagement.Schema;
using TTTH.Common;
using TTTH.DataBase;
using Z.EntityFramework.Plus;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.ScheduleManagement
{
    /// <summary>
    /// Class dùng để xử lý các hoạt động liên quan đến danh sách lịch học.
    /// Author       :   HoangNM - 10/05/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ListOfScheduleModel
    {
        DataContext context;
        public ListOfScheduleModel()
        {
            context = new DataContext();
        }

        /// <summary>
        /// Tìm kiếm các lịch học theo điều kiện cho trước.
        /// Author       :   HoangNM - 05/10/2018 - create
        /// </summary>
        /// <param name="condition">Đối tượng chứa điều kiện tìm kiếm</param>
        /// <returns>Danh sách các lịch học đã tìm kiếm được. Exception nếu có lỗi</returns>
        public ListOfSchedule GetListOfSchedule(ScheduleConditionSearch condition)
        {
            try
            {
                // Nếu không tồn tại điều kiện tìm kiếm thì khởi tạo giá trị tìm kiếm ban đầu
                if (condition == null)
                {
                    condition = new ScheduleConditionSearch();
                }
                string lang = Common.GetLang();
                if (context.LopHoc.Where(x => !x.DelFlag && x.Id == condition.idLopHoc).Count() == 0) condition.idLopHoc = 0;
                if (context.KhoaHoc.Where(x => !x.DelFlag && x.Id == condition.idKhoaHoc).Count() == 0) condition.idKhoaHoc = 0;

                ListOfSchedule listOfSchedule = new ListOfSchedule();
                // Lấy các thông tin dùng để phân trang
                listOfSchedule.Paging = new Paging(context.LichHoc.Count(x =>
                    (condition.idPhongHoc == 0 ||  (condition.idPhongHoc != 0 && (x.IdPhongHoc == condition.idPhongHoc)))
                    && (condition.NgayHoc == 0 || (condition.NgayHoc != 0 && x.NgayHoc==condition.NgayHoc))
                    && (condition.idKhoaHoc == 0 || (condition.idKhoaHoc != 0 && ((condition.idLopHoc != 0 && (x.IdLopHoc == condition.idLopHoc)) || (condition.idLopHoc == 0 && x.LopHoc.IdKhoaHoc == condition.idKhoaHoc))))
                    && !x.DelFlag ), condition.CurrentPage, condition.PageSize);

                // Tìm kiếm và lấy dữ liệu theo trang
                listOfSchedule.ScheduleList = context.LichHoc.Where(x =>
                    (condition.idPhongHoc == 0 || (condition.idPhongHoc != 0 && (x.IdPhongHoc == condition.idPhongHoc)))
                    && (condition.NgayHoc == 0 || (condition.NgayHoc != 0 && x.NgayHoc == condition.NgayHoc))
                    && (condition.idKhoaHoc == 0 || (condition.idKhoaHoc != 0 && ((condition.idLopHoc != 0 && (x.IdLopHoc == condition.idLopHoc)) || (condition.idLopHoc == 0 && x.LopHoc.IdKhoaHoc == condition.idKhoaHoc))))
                    && !x.DelFlag).OrderBy(x => x.Id)
                    .Skip((listOfSchedule.Paging.CurrentPage - 1) * listOfSchedule.Paging.NumberOfRecord)
                    .Take(listOfSchedule.Paging.NumberOfRecord).Select(x => new Schedule
                    {
                        Id = x.Id,
                        KhoaHoc = x.LopHoc.KhoaHoc.KhoaHocTrans.FirstOrDefault(y=> !y.DelFlag).TenKhoaHoc,
                        LopHoc = x.LopHoc.LopHocTrans.FirstOrDefault(y=> !y.DelFlag).TenLop,
                        PhongHoc = x.PhongHoc.PhongHocTrans.FirstOrDefault(y=> !y.DelFlag).TenPhong,
                        NgayHoc= x.NgayHoc,
                        GioBatDau= x.GioBatDau,
                        GioKetThuc= x.GioKetThuc
                    }).ToList();
                listOfSchedule.cacKhoaHoc = context.KhoaHocTrans.Where(x => x.Lang == lang && !x.DelFlag).Select(x => new KhoaHocHienThi()
                {
                    IDKhoaHoc = x.Id,
                    TenKhoaHoc = x.TenKhoaHoc
                }).ToList();
                if (condition.idKhoaHoc != 0)
                {
                    listOfSchedule.cacLopHoc = context.LopHoc.Include("LopHocTrans").Where(x => !x.DelFlag && x.IdKhoaHoc == condition.idKhoaHoc && x.LopHocTrans.Count > 0)
                        .Select(x => new LopHocHienThi()
                        {
                            IDLopHoc = x.Id,
                            TenLopHoc = x.LopHocTrans.FirstOrDefault(y => y.Lang == lang).TenLop
                        }).ToList();
                }
                listOfSchedule.cacPhongHoc=context.PhongHocTrans.Where(x => x.Lang == lang && !x.DelFlag).Select(x => new PhongHocHienThi()
                {
                    IDPhongHoc = x.Id,
                    TenPhongHoc = x.TenPhong
                }).ToList();
                return listOfSchedule;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Xóa các lịch học trong DB.
        /// Author       :   HoangNM - 06/10/2018 - create
        /// </summary>
        /// <param name="ids">Danh sách id của các contact sẽ xóa</param>
        public void DeleteSchedule(List<int> ids)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                context.LichHoc.Where(x => ids.Contains(x.Id)).Delete();
                context.SaveChanges();
                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

    }

}