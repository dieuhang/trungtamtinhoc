﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TrungTamTinHoc.Areas.ControlPanel.Models.SlideManagement.Schema;
using TTTH.Common;
using TTTH.DataBase;
using Z.EntityFramework.Plus;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.SlideManagement
{
    /// <summary>
    /// Class dùng để xử lý các hoạt động liên quan đến slide.
    /// Author       :   QuyPN - 18/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ListOfSlideModel
    {
        private DataContext context;

        public ListOfSlideModel()
        {
            context = new DataContext();
        }

        /// <summary>
        /// Tìm kiếm các slide theo điều kiện cho trước.
        /// Author       :   QuyPN - 18/07/2018 - create
        /// </summary>
        /// <param name="condition">Đối tượng chứa điều kiện tìm kiếm</param>
        /// <returns>Danh sách các slide đã tìm kiếm được. Exception nếu có lỗi</returns>
        public ListOfSlide GetListOfSlide(SlideConditionSearch condition)
        {
            try
            {
                // Nếu không tồn tại điều kiện tìm kiếm thì khởi tạo giá trị tìm kiếm ban đầu
                if (condition == null)
                {
                    condition = new SlideConditionSearch();
                }
                string lang = Common.GetLang();

                ListOfSlide listOfSlide = new ListOfSlide();
                // Lấy các thông tin dùng để phân trang
                listOfSlide.Paging = new Paging(context.Slide.Count(x =>
                    (condition.KeySearch == null ||
                    (condition.KeySearch != null && (x.TieuDe.Contains(condition.KeySearch) ||
                                                   x.ChiTiet.Contains(condition.KeySearch))))
                    && (condition.TrangThai == null || (condition.TrangThai != null &&
                                                       x.HienThi == condition.TrangThai.Value))
                    && !x.DelFlag && x.Lang == lang), condition.CurrentPage, condition.PageSize);

                // Tìm kiếm và lấy dữ liệu theo trang
                listOfSlide.SlideList = context.Slide.Where(x =>
                    (condition.KeySearch == null ||
                    (condition.KeySearch != null && (x.TieuDe.Contains(condition.KeySearch) ||
                                                   x.ChiTiet.Contains(condition.KeySearch))))
                    && (condition.TrangThai == null || (condition.TrangThai != null &&
                                                       x.HienThi == condition.TrangThai.Value))
                    && !x.DelFlag && x.Lang == lang).OrderBy(x => x.Id)
                    .Skip((listOfSlide.Paging.CurrentPage - 1) * listOfSlide.Paging.NumberOfRecord)
                    .Take(listOfSlide.Paging.NumberOfRecord).Select(x => new Slide
                    {
                        Id = x.Id,
                        TieuDe = x.TieuDe,
                        ChiTiet = x.ChiTiet,
                        HienThi = x.HienThi,
                        Link = x.Link,
                        LinkAnh = x.LinkAnh
                    }).ToList();
                listOfSlide.Condition = condition;
                return listOfSlide;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Xóa các slide trong DB.
        /// Author       :   QuyPN - 18/07/2018 - create
        /// </summary>
        /// <param name="ids">Danh sách id của các slide sẽ xóa</param>
        /// <returns>True nếu xóa thành công, False nếu không còn slide được hiển thị trên trang chủ, Excetion nếu có lỗi</returns>
        public bool DeleteSlides(List<int> ids)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                bool result = true;
                if (context.Slide.Count(x => x.HienThi && !ids.Contains(x.Id) && !x.DelFlag) > 1)
                {
                    List<string> fileWillDel = context.Slide.Where(x => ids.Contains(x.Id)).Select(x => x.LinkAnh).ToList();
                    context.Slide.Where(x => ids.Contains(x.Id)).Delete();
                    Common.DeleteFile(fileWillDel);
                    context.SaveChanges();
                }
                else
                {
                    result = false;
                }
                transaction.Commit();
                return result;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        /// <summary>
        /// Cập nhật trạng thái hiển thị của slide trên trang chủ.
        /// Author       :   QuyPN - 18/07/2018 - create
        /// </summary>
        /// <param name="id">id của slide sẽ được cập nhật trạng thái</param>
        /// <returns>True nếu xóa thành công, False nếu không còn slide được hiển thị trên trang chủ, Excetion nếu có lỗi</returns>
        public bool UpdateShow(int id)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                bool result = true;
                if (context.Slide.Count(x => x.HienThi && x.Id != id && !x.DelFlag) > 1)
                {
                    context.Slide.Where(x => x.Id == id && !x.DelFlag)
                        .Update(x => new TTTH.DataBase.Schema.Slide
                        {
                            HienThi = !x.HienThi
                        });
                    context.SaveChanges();
                }
                else
                {
                    result = false;
                }
                transaction.Commit();
                return result;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }
    }
}