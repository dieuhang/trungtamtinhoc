﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrungTamTinHoc.Areas.Course.Models;
using TrungTamTinHoc.Areas.Course.Models.Schema;
using TrungTamTinHoc.Areas.Home.Models;
using TTTH.Common;
using TTTH.Validate;
using static TTTH.Common.Enums.ConstantsEnum;
using static TTTH.Common.Enums.MessageEnum;

namespace TrungTamTinHoc.Areas.Course.Controllers
{
    /// <summary>
    /// Class chứa các điều hướng liên quan đến đăng ký khóa học
    /// Author       :   AnTM - 05/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   Home
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class DangKyKhoaHocController : Controller
    {
        /// <summary>
        /// Điều hướng đến trang đăng ký khóa học.
        /// Author       :   AnTM - 05/07/2018 - create
        /// </summary>
        /// <param name="id">beauty ID của khóa học get từ url</param>
        /// <returns>Trang view đăng ký khóa học, nếu có lỗi sẽ rả về trang error</returns>
        /// <remarks>
        /// Method: GET
        /// RouterName: dang-ky-khoa-hoc
        /// </remarks>
        public ActionResult DangKyHoc(string id)
        {
            try
            {
                DangKyHocModel model = new DangKyHocModel();
                DangKyHocData data = new DangKyHocData();
                int idUser = XacThuc.GetIdUser();
                if (idUser != 0)
                {
                    ThongTinUser thongTinHocVien = model.LayThongTinUserHienThi(idUser);
                    if (thongTinHocVien != null)
                    {
                        data.ThongTinHocVien = thongTinHocVien;
                    }
                    if (thongTinHocVien.IdNguoiGiamHo != null)
                    {
                        data.ThongTinNguoiGiamHo = model.LayThongTinUserHienThi(thongTinHocVien.IdNguoiGiamHo);
                    }
                }
                if (id == null)
                {
                    data.IdKhoaHocDuocChon = -1;
                }
                else
                {
                    data.IdKhoaHocDuocChon = model.GetIdKhoaHocIsSelected(id);
                }
                ViewBag.DangKyHocData = data;
                return View();
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }
        /// <summary>
        /// Lấy danh sách các lớp học theo id khóa học từ client đưa lên
        /// Author       :   AnTM - 07/07/2018 - create
        /// </summary>
        /// <param name="idKhoaHoc">id khóa học chọn từ select box</param>
        /// <returns>Chỗi Json chứa kết quả của việc lấy danh sách lớp học</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: getClassesByIdCourse
        /// </remarks>
        public JsonResult LayDanhSachLopHocTheoIdKhoaHoc(int idKhoaHoc, int type)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                if (type==0)
                {
                    response.ThongTinBoSung4 = new DangKyHocModel().LayKhoaHocHienThi().ToList<object>();
                }
                else if (type == 1)
                {
                    response.ThongTinBoSung4 = new DangKyHocModel().LayLopHocHienThiTheoIDKhoaHoc(idKhoaHoc).ToList<object>();
                }
                else
                {
                    response.Code = (int)CodeResponse.NotAccess;
                    response.MsgNo = (int)MsgNO.XacThucKhongHopLe;
                }
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getAddress(string id, int type)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                DangKyHocModel model = new DangKyHocModel();
                if (type == 0)
                {
                    response.ThongTinBoSung4 = model.LayTatcaTinh().ToList<object>();
                }
                else if (type == 1)
                {
                    response.ThongTinBoSung4 = model.LayCacHuyenTheoIdTinh(id).ToList<object>();
                }
                else if (type == 2)
                {
                    response.ThongTinBoSung4 = model.LayCacXaHienThiTheoIdHuyen(id).ToList<object>();
                }
                else
                {
                    response.Code = (int)CodeResponse.NotAccess;
                    response.MsgNo = (int)MsgNO.XacThucKhongHopLe;
                }
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Đăng ký khóa học theo thông tin người dùng đưa lên.
        /// Author       :   AnTM - 24/06/2018 - create
        /// </summary>
        /// <param name="account">Thông tin đăng ký khóa học mà người dùng nhập vào</param>
        /// <returns>Chỗi Json chứa kết quả của việc đăng ký khóa học</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: homeRegisterCourse
        /// </remarks>
        public JsonResult XuLyDangKyHoc(ThongTinDangKyHoc thongTinDangKy)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                if (ModelState.IsValid)
                {
                    response = new DangKyHocModel().DangKyKhoaHoc(thongTinDangKy);
                }
                else
                {
                    response.Code = (int)CodeResponse.NotValidate;
                    response.ListError = ModelState.GetModelErrors();
                }
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Kiểm tra email hoặc username đã tồn tại hay chưa.
        /// Author       :   AnTM - 23/07/2018 - create
        /// </summary>
        /// <param name="value">giá trị của email hoặc username cần kiểm tra</param>
        /// <param name="type">type = 1: kiểm tra usernme; type = 2: kiểm tra email</param>
        /// <returns>Nếu có tồn tại trả về true, ngược lại trả về false</returns>
        /// <remarks>
        /// Method: GET
        /// RouterName: homeCreateAccount
        /// </remarks>
        public bool CheckExistAccount(string value, string type)
        {
            try
            {
                return new RegisterModel().CheckExistAccount(value, type);
            }
            catch
            {
                return false;
            }
        }
    }
}