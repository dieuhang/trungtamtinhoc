﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TTTH.Common;
using static TTTH.Common.Enums.ConstantsEnum;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.SlideManagement.Schema
{
    public class SlideMaster
    {
        public Slide SlideInfo { set; get; }
        public Slide SlideTrans { set; get; }
        public List<Lang> Language { set; get; }
        public int Mode { set; get; }
        public SlideMaster()
        {
            Mode = (int)ModeMaster.Insert;
            SlideTrans = null;
            SlideInfo = null;
            Language = new List<Lang>();
        }
    }
}