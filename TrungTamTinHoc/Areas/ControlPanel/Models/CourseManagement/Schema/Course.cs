﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.CourseManagement.Schema
{
    /// <summary>
    /// Class dùng để chứa thông tin của một khóa học
    /// Author       :   TramHTD - 20/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class Course
    {
        public int Id { set; get; }
        [Required]
        [StringLength(255)]
        public string BeautyId { get; set; }

        public int IdChuyenNganh { get; set; }

        [Required]
        [StringLength(255)]
        public string AnhMinhHoa { get; set; }

        public HttpPostedFileBase FileImg { set; get; }

        public DateTime? NgayKhaiGiang { get; set; }

        public decimal HocPhi { get; set; }

        public double ChietKhau { get; set; }

        [Required]
        [StringLength(100)]
        public string DoTuoi { set; get; }

        [Required]
        [StringLength(100)]
        public string ThoiGian { set; get; }

        public DateTime? ThoiGianKetThuc { set; get; }

        [Required]
        [StringLength(100)]
        public string LichHoc { set; get; }

        public bool ChoPhepDangKy { set; get; }
        public string CheckChoPhepDangKy { set; get; }

        public bool HienThi { set; get; }
        public string CheckHienThi { set; get; }

        [Required]
        [StringLength(50)]
        public string TenKhoaHoc { get; set; }

        [Required]
        [StringLength(500)]
        public string TomTat { get; set; }

        [Required]
        public string ChiTiet { get; set; }
    }

    public class Course_Trans
    {
        public int Id { set; get; }

        public string LanguageTrans { set; get; }

        [Required]
        [StringLength(50)]
        public string TenKhoaHoc { get; set; }

        [Required]
        [StringLength(500)]
        public string TomTat { get; set; }

        [Required]
        public string ChiTiet { get; set; }
    }
    /// <summary>
    /// Class dùng để chứa thông tin tìm kiếm của danh sách khóa học
    /// Author       :   TramHTD - 20/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class CourseConditionSearch
    {
        public string KeySearch { set; get; }
        public DateTime? StartDate { set; get; }
        public DateTime? EndDate { set; get; }
        public int IdChuyenNganh { set; get; }
        public bool? TrangThai { set; get; }
        public int CurrentPage { set; get; }
        public int PageSize { set; get; }
        public CourseConditionSearch()
        {
            this.KeySearch = "";
            this.StartDate = null;
            this.EndDate = null;
            this.IdChuyenNganh = 0;
            this.TrangThai = null;
            this.PageSize = 10;
            this.CurrentPage = 1;
        }
    }
    /// <summary>
    /// Class dùng để chứa thông tin của chuyên ngành
    /// Author       :   TramHTD - 26/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ChuyenNganh
    {
        public int IdChuyenNganh { set; get; }
        public string TenChuyenNganh { set; get; }
    }
}