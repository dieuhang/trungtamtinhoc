﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TrungTamTinHoc.Areas.ControlPanel.Models.SlideManagement.Schema;
using TTTH.Common;
using TTTH.DataBase;
using Z.EntityFramework.Plus;
using static TTTH.Common.Enums.ConstantsEnum;
using static TTTH.Common.Enums.MessageEnum;
using TblSlide = TTTH.DataBase.Schema.Slide;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.SlideManagement
{
    public class SlideMasterModel
    {
        DataContext context;
        List<string> typeFiles;
        public SlideMasterModel()
        {
            context = new DataContext();
            typeFiles = new List<string>();
            typeFiles.Add(".jpg");
            typeFiles.Add(".png");
        }

        public SlideMaster LoadSlide(string id)
        {
            try
            {
                SlideMaster slideMaster = new SlideMaster();
                int idSlide = 0;
                try
                {
                    idSlide = Convert.ToInt32(id);
                }
                catch { }
                TblSlide slide = context.Slide.FirstOrDefault(x => x.Id == idSlide && !x.DelFlag && x.Lang == Common.defaultLang);
                if(slide != null)
                {
                    slideMaster.Mode = (int)ModeMaster.Update;
                    slideMaster.SlideInfo = new Slide();
                    slideMaster.SlideInfo.Id = slide.Id;
                    slideMaster.SlideInfo.TieuDe = slide.TieuDe;
                    slideMaster.SlideInfo.ChiTiet = slide.ChiTiet;
                    slideMaster.SlideInfo.Link = slide.Link;
                    slideMaster.SlideInfo.LinkAnh = slide.LinkAnh;
                    slideMaster.SlideInfo.HienThi = slide.HienThi;
                    slideMaster.Language = context.Language.Where(x => !x.DelFlag)
                        .Select(x => new Lang
                        {
                            Id = x.Id,
                            Name = x.Name
                        }).OrderBy(x => x.Id).ToList();
                    if(slideMaster.Language.Count > 0)
                    {
                        string lang = slideMaster.Language[0].Id;
                        slide = context.Slide.FirstOrDefault(x => x.Id == idSlide && !x.DelFlag && x.Lang == lang);
                        slideMaster.SlideTrans = new Slide();
                        slideMaster.SlideTrans.Id = slide.Id;
                        slideMaster.SlideTrans.TieuDe = slide.TieuDe;
                        slideMaster.SlideTrans.ChiTiet = slide.ChiTiet;
                        slideMaster.SlideTrans.Link = slide.Link;
                        slideMaster.SlideTrans.LinkAnh = slide.LinkAnh;
                        slideMaster.SlideTrans.HienThi = slide.HienThi;
                    }
                }
                return slideMaster;
            }
            catch(Exception e)
            {
                throw e;
            }
        }
        public ResponseInfo SaveSlide(Slide slide)
        {
            try
            {
                if(context.Slide.FirstOrDefault(x => x.Id == slide.Id && !x.DelFlag) != null)
                {
                    return UpadateSlide(slide);
                }
                return AddSlide(slide);
            }
            catch(Exception e)
            {
                throw e;
            }
        }
        public ResponseInfo UpadateSlide(Slide slide)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo response = new ResponseInfo();
                bool update = true;
                List<string> imgsDelete = new List<string>();
                if (slide.FileImg != null)
                {
                    string linkAnh = Common.SaveFileUpload(slide.FileImg, "/public/img/slider/", "", typeFiles);
                    if (linkAnh == "1")
                    {
                        response.Code = 202;
                        response.MsgNo = (int)MsgNO.FileKhongDungDinhDang;
                        update = false;
                    }
                    else if (linkAnh == "2")
                    {
                        response.Code = 202;
                        response.MsgNo = (int)MsgNO.DungLuongFileQuaLon;
                        update = false;
                    }
                    else if (linkAnh == "")
                    {
                        response.Code = 202;
                        response.MsgNo = (int)MsgNO.TaiFileBiLoi;
                        update = false;
                    }
                    else
                    {
                        imgsDelete.Add(context.Slide.FirstOrDefault(x => x.Id == slide.Id && !x.DelFlag).LinkAnh);
                        slide.LinkAnh = linkAnh;
                    }
                }
                if (update)
                {
                    slide.HienThi = slide.CheckHienThi == "on" ? true : false;
                    context.Slide.Where(x => x.Id == slide.Id && !x.DelFlag)
                        .Update(x => new TTTH.DataBase.Schema.Slide
                        {
                            HienThi = slide.HienThi,
                            Link = slide.Link,
                            LinkAnh = slide.LinkAnh
                        });
                    context.Slide.Where(x => x.Id == slide.Id && x.Lang == Common.defaultLang && !x.DelFlag)
                        .Update(x => new TTTH.DataBase.Schema.Slide
                        {
                            TieuDe = slide.TieuDe,
                            ChiTiet = slide.ChiTiet
                        });
                    context.SaveChanges();
                }
                transaction.Commit();
                Common.DeleteFile(imgsDelete);
                return response;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }
        public ResponseInfo AddSlide(Slide slide)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo response = new ResponseInfo();
                if(slide.FileImg == null)
                {
                    response.Code = 202;
                    response.MsgNo = (int)MsgNO.ChuaChonFile;
                }
                else
                {
                    slide.LinkAnh = Common.SaveFileUpload(slide.FileImg, "/public/img/slider/", "", typeFiles);
                    if(slide.LinkAnh == "1")
                    {
                        response.Code = 202;
                        response.MsgNo = (int)MsgNO.FileKhongDungDinhDang;
                    }
                    else if (slide.LinkAnh == "2")
                    {
                        response.Code = 202;
                        response.MsgNo = (int)MsgNO.DungLuongFileQuaLon;
                    }
                    else if (slide.LinkAnh == "")
                    {
                        response.Code = 202;
                        response.MsgNo = (int)MsgNO.TaiFileBiLoi;
                    }
                    else
                    {
                        List<Lang> listOfLang = context.Language.Where(x => !x.DelFlag).Select(x => new Lang
                        {
                            Id = x.Id,
                            Name = x.Name
                        }).ToList();
                        slide.HienThi = slide.CheckHienThi == "on" ? true : false;
                        slide.Id = context.Slide.Count() == 0 ? 1 : context.Slide.Max(x => x.Id) + 1;
                        foreach(Lang lang in listOfLang)
                        {
                            context.Slide.Add(new TblSlide
                            {
                                Id = slide.Id,
                                Lang = lang.Id,
                                LinkAnh = slide.LinkAnh,
                                Link = slide.Link,
                                TieuDe = slide.TieuDe,
                                ChiTiet = slide.ChiTiet,
                                HienThi = slide.HienThi
                            });
                        }
                        context.SaveChanges();
                        response.ThongTinBoSung1 = slide.Id + "";
                    }
                }
                transaction.Commit();
                return response;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }
        public ResponseInfo UpdateSlideTranslate(Slide_Trans slide)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo response = new ResponseInfo();
                context.Slide.Where(x => x.Id == slide.Id && x.Lang == slide.LanguageTrans && !x.DelFlag)
                    .Update(x => new TTTH.DataBase.Schema.Slide
                    {
                        TieuDe = slide.TieuDe,
                        ChiTiet = slide.ChiTiet
                    });
                context.SaveChanges();
                response.ThongTinBoSung1 = slide.Id + "";
                transaction.Commit();
                return response;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }
        public Slide_Trans ReferSlideWithLang(int id, string lang)
        {
            try
            {
                return context.Slide.Where(x => x.Id == id && x.Lang == lang && !x.DelFlag)
                    .Select(x => new Slide_Trans
                    {
                        TieuDe = x.TieuDe,
                        ChiTiet = x.ChiTiet
                    }).FirstOrDefault();
            }
            catch(Exception e)
            {
                throw e;
            }
        }
    }
}