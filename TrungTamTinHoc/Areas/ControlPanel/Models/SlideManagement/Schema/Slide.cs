﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.SlideManagement.Schema
{
    /// <summary>
    /// Class dùng để chứa thông tin của một slide
    /// Author       :   QuyPN - 18/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class Slide
    {
        public int Id { set; get; }

        [Required]
        [StringLength(255)]
        public string LinkAnh { get; set; }

        [Required]
        [StringLength(100)]
        public string TieuDe { get; set; }

        [Required]
        [StringLength(1000)]
        public string ChiTiet { get; set; }

        [Required]
        [StringLength(255)]
        public string Link { get; set; }

        public HttpPostedFileBase FileImg { set; get; }

        public bool HienThi { get; set; }
        public string CheckHienThi { set; get; }
    }
    public class Slide_Trans
    {
        public int Id { set; get; }

        public string LanguageTrans { set; get; }

        [Required]
        [StringLength(100)]
        public string TieuDe { get; set; }

        [Required]
        [StringLength(1000)]
        public string ChiTiet { get; set; }
    }
    /// <summary>
    /// Class dùng để chứa thông tin tìm kiếm của danh sách slide
    /// Author       :   QuyPN - 18/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class SlideConditionSearch
    {
        public string KeySearch { set; get; }
        public bool? TrangThai { set; get; }
        public int CurrentPage { set; get; }
        public int PageSize { set; get; }
        public SlideConditionSearch()
        {
            this.KeySearch = "";
            this.TrangThai = null;
            this.PageSize = 10;
            this.CurrentPage = 1;
        }
    }
}