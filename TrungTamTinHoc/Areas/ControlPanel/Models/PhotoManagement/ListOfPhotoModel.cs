﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TrungTamTinHoc.Areas.ControlPanel.Models.PhotoManagement.Schema;
using TTTH.Common;
using TTTH.DataBase;
using Z.EntityFramework.Plus;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.PhotoManagement
{
    /// <summary>
    /// Class dùng để xử lý các hoạt động liên quan đến danh sách hình ảnh.
    /// Author       :   HaLTH - 06/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ListOfPhotoModel
    {
        DataContext context;
        public ListOfPhotoModel()
        {
            context = new DataContext();
        }

        /// <summary>
        /// Tìm kiếm các hình ảnh theo điều kiện cho trước.
        /// Author       :   HaLTH - 06/08/2018 - create
        /// </summary>
        /// <param name="condition">Đối tượng chứa điều kiện tìm kiếm</param>
        /// <returns>Danh sách các slide đã tìm kiếm được. Exception nếu có lỗi</returns>
        public ListOfPhoto GetListOfPhoto (PhotoConditionSearch condition)
        {
            try
            {
                // Nếu không tồn tại điều kiện tìm kiếm thì khởi tạo giá trị tìm kiếm ban đầu
                if (condition == null)
                {
                    condition = new PhotoConditionSearch();
                }
                string lang = Common.GetLang();

                ListOfPhoto listOfPhoto = new ListOfPhoto();
                // Lấy các thông tin dùng để phân trang
                listOfPhoto.Paging = new Paging(context.Photos.Count(x =>
                    (condition.KeySearch == null ||
                    (condition.KeySearch != null && (x.Note.Contains(condition.KeySearch))))
                    && !x.DelFlag && x.Lang == lang), condition.CurrentPage, condition.PageSize);

                // Tìm kiếm và lấy dữ liệu theo trang
                listOfPhoto.PhotoList = context.Photos.Where(x =>
                    (condition.KeySearch == null ||
                    (condition.KeySearch != null && (x.Note.Contains(condition.KeySearch))))
                    && !x.DelFlag && x.Lang == lang).OrderBy(x => x.Id)
                    .Skip((listOfPhoto.Paging.CurrentPage - 1) * listOfPhoto.Paging.NumberOfRecord)
                    .Take(listOfPhoto.Paging.NumberOfRecord).Select(x => new Photo
                    {
                        Id = x.Id,
                        LinkAnh = x.Link,
                        Note = x.Note
                    }).ToList();
                listOfPhoto.Condition = condition;
                return listOfPhoto;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Xóa các hình ảnh trong DB.
        /// Author       :   HaLTH - 06/08/2018 - create
        /// </summary>
        /// <param name="ids">Danh sách id của các hình ảnh sẽ xóa</param>
        /// <returns>True nếu xóa thành công, False nếu không còn hình ảnh được hiển thị trên trang chủ, Excetion nếu có lỗi</returns>
        public bool DeletePhotos(List<int> ids)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                bool result = true;
                if (context.Photos.Count(x => !ids.Contains(x.Id) && !x.DelFlag) > 1)
                {
                    context.Photos.Where(x => ids.Contains(x.Id)).Delete();
                    context.SaveChanges();
                }
                else
                {
                    result = false;
                }
                transaction.Commit();
                return result;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }
    }
}