﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrungTamTinHoc.Areas.ControlPanel.Models.SettingManagement;
using TrungTamTinHoc.Areas.ControlPanel.Models.SettingManagement.Schema;
using TrungTamTinHoc.Controllers;
using TTTH.Common;
using TTTH.Common.Filters;
//using TTTH.Common.Filters;
using TTTH.Validate;
using static TTTH.Common.Enums.ConstantsEnum;
using static TTTH.Common.Enums.MessageEnum;

namespace TrungTamTinHoc.Areas.ControlPanel.Controllers
{
    /// <summary>
    /// Class chứa các điều hướng liên quan đến cài đặt hệ thống
    /// Author       :   HoàngNM - 23/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    /// 
    
    public class SettingManagementController : Controller
    {
        /// <summary>
        /// Điều hướng đến trang cập nhật thông tin cài đặt hệ thống đối với lập trình viên 
        /// Author       :   HoangNM - 23/08/2018 - create
        /// </summary>
        /// <remarks>
        /// RouterName: updateSetting
        /// </remarks>

        public ActionResult UpdateSetting()
        {
            try
            {

                SettingMaster settingMaster = new SettingModel().LoadSetting();
                return View("SettingMaster", settingMaster);


            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }

        /// <summary>
        /// lưu thông tin phần cài đặt hệ thống
        /// Author       :   HoangNM - 23/08/2018 - create
        /// </summary>
        /// <param name="data">Đối tượng chứa thông tin phần cài đặt hệ thống cần thay đổi</param>
        /// <remarks>
        /// RouterName: saveSetting
        /// </remarks>
        /// 
        [ValidateInput(false)]

        public JsonResult SaveSetting(Setting data)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {

                if (ModelState.IsValid)
                {
                    response = new SettingModel().UpdateSetting(data);
                }
                else
                {
                    response.Code = (int)CodeResponse.NotValidate;
                    response.ListError = ModelState.GetModelErrors();
                }


            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// lưu thông tin phần cài đặt hệ thống với ngôn ngữ khác
        /// Author       :   HoangNM - 23/08/2018 - create
        /// </summary>
        /// <param name="data">Đối tượng chứa thông tin cài đặt hệ thống với ngôn ngữ khác</param>
        /// <remarks>
        /// RouterName: updatePrivacyPolicyTranslate
        [ValidateInput(false)]
        public JsonResult UpdateSettingTranslate(Setting_Trans data)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {

                if (ModelState.IsValid)
                {
                    response = new SettingModel().UpdateSettingTranslate(data);
                }
                else
                {
                    response.Code = (int)CodeResponse.NotValidate;
                    response.ListError = ModelState.GetModelErrors();
                }

            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// thay đổi thông tin khi thay đổi ngôn ngữ
        /// Author       :   HoangNM - 23/08/2018 - create
        /// </summary>
        /// <param name="data">Đối tượng chứa thông tin cài đặt hệ thống với ngôn ngữ khác</param>
        /// <remarks>
        /// RouterName: updatePrivacyPolicyTranslate
        public JsonResult ReferSettingWithLang(string lang)
        {
            try
            {
                return Json(new SettingModel().ReferSettingWithLang(lang), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        // các phần cài đặt khác ------------------------------------------------------------------------
        //-------------------------------------------------------------------------------------------------

        /// <summary>
        /// Điều hướng đến trang cập nhật thông tin cài đặt hệ thống đối với lập trình viên (phần thông tin bảo mật,hướng dẫn sử dụng,điều khoản sử dụng)
        /// Author       :   HoangNM - 28/08/2018 - create
        /// </summary>
        /// <remarks>
        /// RouterName: updatePrivacyPolicy
        /// </remarks>
        public ActionResult UpdateOtherSettings(int typeSetting)
        {
            try
            {

                switch (typeSetting)
                {
                    case 1:
                        {
                            dataMaster dataMaster = new SettingModel().getPrivacyPolicy();
                            return View("OtherSettings", dataMaster);
                        }
                    case 2:
                        {
                            dataMaster dataMaster = new SettingModel().getUserManual();
                            return View("OtherSettings", dataMaster);
                        }
                    case 3:
                        {
                            dataMaster dataMaster = new SettingModel().getTermsOfUse();
                            return View("OtherSettings", dataMaster);
                        }
                    default:
                        {
                            return RedirectToRoute("adminDashboard");
                        }
                }


            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }

        /// <summary>
        /// lưu thông tin phần cài đặt hệ thống (phần chính sách bảo mật,phần hướng dẫn sử dụng ,phần điều khoản sử dụng)
        /// Author       :   HoangNM - 29/08/2018 - create
        /// </summary>
        /// <param name="data">chứa thông tin chính sách bảo mật</param>
        /// <remarks>
        /// RouterName: savePrivacyPolicy
        /// </remarks>
        [ValidateInput(false)]
        public JsonResult SaveOtherSettings(string data, int typeSetting)
        {
            ResponseInfo response = new ResponseInfo();

            try
            {

                switch (typeSetting)
                {
                    case 1:
                        {
                            response = new SettingModel().UpdatePrivacyPolicy(data);
                            break;
                        }
                    case 2:
                        {
                            response = new SettingModel().UpdateUserManual(data);
                            break;
                        }
                    case 3:
                        {
                            response = new SettingModel().UpdateTermsOfUse(data);
                            break;
                        }
                    default:
                        {
                            response.MsgNo = (int)MsgNO.XacThucKhongHopLe;
                            break;
                        }
                }


            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// lưu thông tin phần cài đặt hệ thống với ngôn ngữ khác(phần chính sách bảo mật)
        /// Author       :   HoangNM - 29/08/2018 - create
        /// </summary>
        /// <param name="dataLang">Đối tượng chứa thông tin chính sách bảo mật với ngôn ngữ khác</param>
        /// <param name="lang">ngôn ngữ khác với ngôn ngữ mặc định và được người dùng chọn</param>
        /// <remarks>
        /// RouterName: updatePrivacyPolicyTranslate
        [ValidateInput(false)]
        public JsonResult UpdateOtherSettingsTranslate(string dataLang, string lang, int typeSettingTrans)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {

                switch (typeSettingTrans)
                {
                    case 1:
                        {
                            response = new SettingModel().UpdatePrivacyPolicyTranslate(dataLang, lang);
                            break;
                        }
                    case 2:
                        {
                            response = new SettingModel().UpdateUserManualTranslate(dataLang, lang);
                            break;
                        }
                    case 3:
                        {
                            response = new SettingModel().UpdateTermsOfUseTranslate(dataLang, lang);
                            break;
                        }
                    default:
                        {
                            response.MsgNo = (int)MsgNO.XacThucKhongHopLe;
                            break;
                        }
                }
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// thay đổi thông tin chính sách bảo mật,hướng dân sử dụng hoặc điều khoản sử dụng khi thay đổi ngôn ngữ
        /// Author       :   HoangNM - 29/08/2018 - create
        /// </summary>
        /// <param name="data">Đối tượng chứa thông tin cài đặt hệ thống với ngôn ngữ khác</param>
        /// <remarks>
        /// RouterName: updatePrivacyPolicyTranslate
        public JsonResult ReferOtherSettingsWithLang(string lang, int typeSettingTrans)
        {
            try
            {
                switch (typeSettingTrans)
                {
                    case 1:
                        {
                            return Json(new SettingModel().ReferPrivacyPolicyWithLang(lang), JsonRequestBehavior.AllowGet);
                        }
                    case 2:
                        {
                            return Json(new SettingModel().ReferUserManualWithLang(lang), JsonRequestBehavior.AllowGet);
                        }
                    case 3:
                        {
                            return Json(new SettingModel().ReferTermsOfUseWithLang(lang), JsonRequestBehavior.AllowGet);
                        }
                    default:
                        {
                            return Json("null", JsonRequestBehavior.AllowGet);
                        }
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}