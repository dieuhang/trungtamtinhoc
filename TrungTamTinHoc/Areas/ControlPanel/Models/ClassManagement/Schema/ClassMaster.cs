﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TTTH.Common;
using TTTH.Common.Enums;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.ClassManagement.Schema
{
    /// <summary>
    /// Class chứa các thông tin liên quan đến lớp học
    /// Author       :   TramHTD - 04/09/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks
    public class ClassMaster
    {
        public Class ClassInfo { set; get; }
        public Class ClassTrans { set; get; }
        public List<Lang> Language { set; get; }
        public int Mode { set; get; }
        public ClassMaster()
        {
            Mode = (int)ConstantsEnum.ModeMaster.Insert;
            ClassInfo = null;
            ClassTrans = null;
            Language = new List<Lang>();
        }
    }
}