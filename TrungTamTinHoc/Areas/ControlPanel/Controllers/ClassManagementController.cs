﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrungTamTinHoc.Areas.ControlPanel.Models.ClassManagement;
using TrungTamTinHoc.Areas.ControlPanel.Models.ClassManagement.Schema;
using TrungTamTinHoc.Controllers;
using TTTH.Common;
using TTTH.Validate;
using static TTTH.Common.Enums.ConstantsEnum;
using static TTTH.Common.Enums.MessageEnum;

namespace TrungTamTinHoc.Areas.ControlPanel.Controllers
{
    /// <summary>
    /// Class chứa các điều hướng liên quan đến quản lý danh sách lớp học
    /// Author       :   HaLTH - 01/09/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ClassManagementController : BaseAdminController
    {
        /// <summary>
        /// Điều hướng đến trang hiển thị danh sách lớp học nếu là request thông thường.
        /// Trả về table chứa danh sách lớp học nếu là Ajax.
        /// Điều hướng về trang lỗi nếu có lỗi sảy ra.
        /// Author       :   HaLTH - 01/09/2018 - create
        /// </summary>
        /// <param name="condition">Đối tượng chứa điều kiện tìm kiếm, tạo thành từ query string</param>
        /// <returns>
        /// Trang danh sách slide.
        /// Partial view chứa table danh sách lớp học.
        /// </returns>
        /// <remarks>
        /// Method: GET
        /// RouterName: ListOfClass
        /// </remarks>
        public ActionResult ListOfClass(ClassConditionSearch condition)
        {
            try
            {
                if (Request.IsAjaxRequest())
                {
                    return View("Partial/_tableOfClass", new ListOfClassModel().GetListOfClass(condition));
                }
                return View(new ListOfClassModel().GetListOfClass(condition));
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }

        /// <summary>
        /// Xóa các lớp học theo danh sách id lớp học được gửi lên.
        /// Author       :   HaLTH - 01/09/2018 - create
        /// </summary>
        /// <param name="ids">Danh sách id các lớp học sẽ xóa</param>
        /// <returns>Đối tượng chứa thông tin về quá trình xóa lớp học</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: DeleteClasses
        /// </remarks>
        public JsonResult DeleteClasses(List<int> ids)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                bool deleted = new ListOfClassModel().DeleteClasses(ids);
                if (!deleted)
                {
                    response.Code = 201;
                    response.MsgNo = 48;
                }
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Điều hướng đến trang view nhập thông tin để thêm lớp học.
        /// Author       :   TramHTD - 04/09/2018 - create
        /// </summary>
        /// <returns>Trả về trang view thêm lớp học</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: ViewCreateClass
        /// </remarks>
        public ActionResult ViewCreateClass()
        {
            try
            {
                ViewBag.ListKhoaHoc = new ClassMasterModel().GetKhoaHocs();
                return View("ClassMaster");
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }

        /// <summary>
        /// Điều hướng đến trang view sửa lớp học
        /// Author       :   TramHTD - 04/09/2018 - create
        /// </summary>
        /// <param name="id">Đối tượng chứa thông tin về quá trình chỉnh sửa thông tin</param>
        /// <returns>Trả về trang view sửa lớp học</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: ViewEditClass
        /// </remarks>
        public ActionResult ViewEditClass(string id)
        {
            try
            {
                ClassMaster classMaster = new ClassMasterModel().LoadClass(id);
                if (classMaster.Mode == (int)ModeMaster.Insert)
                {
                    return RedirectToAction("ViewCreateClass");
                }
                ViewBag.ListKhoaHoc = new ClassMasterModel().GetKhoaHocs();
                return View("ClassMaster", classMaster);
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }

        /// <summary>
        /// Tạo lớp học hoặc cập nhật lớp học theo thông tin người dùng đưa lên.
        /// Author       :   TramHTD - 04/09/2018 - create
        /// </summary>
        /// <param name="data">Thông tin lớp học mà người dùng nhập vào</param>
        /// <returns>Chuỗi Json chứa kết quả của việc tạo hoặc cập nhật lớp học</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: SaveClass
        /// </remarks>
        [ValidateInput(false)]
        public JsonResult SaveClass(Class data)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                if (ModelState.IsValid)
                {
                    response = new ClassMasterModel().SaveClass(data);
                }
                else
                {
                    response.Code = (int)CodeResponse.NotValidate;
                    response.ListError = ModelState.GetModelErrors();
                }
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Lưu thông tin lớp học được gửi lên bằng ngôn ngữ khác ngôn ngữ mặc định
        /// Author       :   TramHTD - 04/09/2018 - create
        /// </summary>
        /// <param name="data">Dữ liệu của đối tượng muốn lưu</param>
        /// <returns></returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: SaveClass
        /// </remarks>
        [ValidateInput(false)]
        public JsonResult UpdateClassTranslate(Class_Trans data)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                if (ModelState.IsValid)
                {
                    response = new ClassMasterModel().UpdateClassTranslate(data);
                }
                else
                {
                    response.Code = (int)CodeResponse.NotValidate;
                    response.ListError = ModelState.GetModelErrors();
                }
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Thay đổi ngôn ngữ
        /// Author       :   TramHTD - 04/09/2018 - create
        /// </summary>
        /// <param name="id">Đối tượng muốn thay đổi ngôn ngữ</param>
        /// <param name="lang">Ngôn ngữ muốn thay đổi</param>
        /// <returns></returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: SaveClass
        /// </remarks>
        public JsonResult ReferClassWithLang(int id, string lang)
        {
            try
            {
                return Json(new ClassMasterModel().ReferClassWithLang(id, lang), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}