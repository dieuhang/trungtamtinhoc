﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrungTamTinHoc.Areas.Information.Models;
using TTTH.Common;
using static TTTH.Common.Enums.ConstantsEnum;
using TblAccount = TTTH.DataBase.Schema.Account;
using TblUser = TTTH.DataBase.Schema.User;
using TrungTamTinHoc.Areas.Information.Models.Schema;
using TTTH.Validate;
using static TTTH.Common.Enums.MessageEnum;
using TrungTamTinHoc.Areas.Home.Models;

namespace TrungTamTinHoc.Areas.Information.Controllers
{
    public class InformationController : Controller
    {
        /// <summary>
        /// Điều hướng đến trang view thông tin cá nhân của tài khoản.
        /// Author       :   HoangNM - 05/07/2018 - create
        /// </summary>
        /// <returns>Trả về trang view thông tin cá nhân</returns>
        /// <remarks>
        /// Method: Post
        /// RouterName: ThongTinCaNhan
        /// </remarks>

        public ActionResult information()
        {
            try
            {
                int idAccount = XacThuc.GetIdAccount();
                InformationModel informationModel = new InformationModel();
                ThongTinCaNhan thongtin= informationModel.GetThongTin(idAccount);
                ViewBag.thongtin = thongtin;
                ViewBag.dsTinh = informationModel.getTinh();
            }
            catch (Exception e)
            {
                throw e;
            }
            return View();
        }

        /// <summary>
        /// Thay đổi password cho  người dùng
        /// Author       :   HoangNM - 05/07/2018 - create
        /// </summary>
        /// <param name="account">Thông tin tài khoản đăng ký mà người dùng nhập vào</param>
        /// <returns>Chỗi Json chứa kết quả của việc thay đổi mật khẩu </returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: changePassword
        /// </remarks>
        /// 
        public JsonResult ChangePassWord(changePass pass)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                if (ModelState.IsValid)
                {
                    response = new InformationModel().CheckAccount(pass);
                }
                else
                {
                    response.Code = (int)CodeResponse.NotValidate;
                    response.ListError = ModelState.GetModelErrors();
                }
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// kiểm tra người dùng nhập đúng password chưa
        /// Author       :   HoangNM - 23/07/2018 - create
        /// </summary>
        /// <param name="pass">chứa password của người dùng</param>
        /// <returns>Chỗi Json chứa kết quả của việc thay đổi mật khẩu </returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: changePassword
        /// </remarks>
        /// 
        public bool CheckPass(string pass)
        {
            try
            {
                return  new InformationModel().CheckPass(pass); 
            }
            catch 
            {
                return false;
            }
            
        }

        /// <summary>
        /// Thêm Username cho người dùng
        /// Author       :   HoangNM - 06/07/2018 - create
        /// </summary>
        /// <param name="account">Thông tin User và password</param>
        /// <returns>Chỗi Json chứa kết quả của việc thay đổi mật khẩu </returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: SaveUsername
        /// </remarks>
        /// 
        public JsonResult SaveUsername(SaveUsername account)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                if (ModelState.IsValid)
                {
                    response = new InformationModel().Save(account);
                }
                else
                {
                    response.Code = (int)CodeResponse.NotValidate;
                    response.ListError = ModelState.GetModelErrors();
                }
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// thay đổi email cho người dùng
        /// Author       :   HoangNM - 07/07/2018 - create
        /// </summary>
        /// <param name="account">Thông tin User và Email</param>
        /// <returns>Chỗi Json chứa kết quả của việc thay đổi mật khẩu </returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: SaveUsername
        /// </remarks>
        /// 
        public JsonResult changeEmail(ChangeEmail account)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                if (ModelState.IsValid)
                {
                    response = new InformationModel().ChangeEmail(account);
                }
                else
                {
                    response.Code = (int)CodeResponse.NotValidate;
                    response.ListError = ModelState.GetModelErrors();
                }
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Kích hoạt email và đưa về trang view thông báo.
        /// Author       :   HoangNM - 08/07/2018 - create
        /// </summary>
        /// <param name="token">Token đang mã hóa Base64 được gửi đi trong mail</param>
        /// <param name="email">email đã thay đổi</param>
        /// <returns>Trả về trang thông báo kích hoạt tài khoản thành công hoặc trang lỗi</returns>
        /// <remarks>
        /// Method: GET
        /// RouterName: KichHoatEmail
        /// </remarks>
        public ActionResult AciveEmail(string email)
        {

            try
            {
                new InformationModel().ActiveEmail(email);
                new InformationModel().RemoveToken();
                return RedirectToRoute("Home");
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }

        /// <summary>
        /// xử lý thông tin Tĩnh,Huyện,Xã
        /// Author       :   HoangNM - 07/07/2018 - create
        /// </summary>
        /// <param name="value">tên của Huyện hoặc Tĩnh</param>
        /// <param name="type">type=1 là tĩnh,type = 2 là huyện </param>
        /// <returns>Chỗi Json chứa kết quả danh sách huyện hoặc xã tương ứng </returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: checkDiaChi
        /// </remarks>
        ///
        public JsonResult CheckDiaChi(string id, string type)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                if (type == "1")
                {
                    response.ThongTinBoSung4 = new InformationModel().getHuyen(id).ToList<object>();
                }
                else
                {
                    response.ThongTinBoSung4 = new InformationModel().getXa(id).ToList<object>();
                }
            }
            catch(Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// kiểm tra userName đã tồn tại chưa
        /// Author       :   HoangNM - 08/07/2018 - create
        /// </summary>
        /// <param name="value">tên Username</param>
        /// <returns>Chỗi Json chứa kết quả danh sách huyện hoặc xã tương ứng </returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: checkUser
        /// </remarks>
        ///
        public bool CheckUser(string value)
        {
            try
            {
                return new InformationModel().CheckUser(value);
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// gửi lại email
        /// Author       :   HoangNM - 08/07/2018 - create
        /// </summary>
        /// <param name="email">tên email</param>
        /// <returns>Chỗi Json chứa kết quả danh sách huyện hoặc xã tương ứng </returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: checkUser
        /// </remarks>
        ///
        public JsonResult GuiLaiMail(string email)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                 new InformationModel().SendEmail(email);
            }
            catch(Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// cập nhật thông tin người dùng
        /// Author       :   HoangNM - 07/07/2018 - create
        /// </summary>
        /// <param name="account">Thông tin tài khoản mà người dùng muốn thay đổi</param>
        /// <param name="FileImg">file ảnh chứa thông tin ảnh avatar người dùng</param>
        /// <param name="FileImgNGH">file ảnh chứa thông tin ảnh avatar người giám hộ</param>
        /// <returns>Chỗi Json chứa kết quả của việc thay đổi mật khẩu </returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: updateInformation
        /// </remarks>
        /// 

        [HttpPost]
        public JsonResult UpdateInformation(UpDate account, HttpPostedFileBase FileImg = null, HttpPostedFileBase FileImgNGH =null)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                if (ModelState.IsValid)
                {
                    response = new InformationModel().UpDate(account, FileImg, FileImgNGH);
                }
                else
                {
                    response.Code = (int)CodeResponse.NotValidate;
                    response.ListError = ModelState.GetModelErrors();
                }
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// lấy lại email
        /// Author       :   HoangNM - 10/07/2018 - create
        /// </summary>
        /// <returns>Chỗi string chứa email của tài khoản </returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: updateInformation
        /// </remarks>
        /// 
        public string GetEmail()
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                string email= new InformationModel().getEmail();
                return  new InformationModel().getEmail();
            }
            catch (Exception e)
            {
                return "";
            }
        }

        /// <summary>
        /// Kiểm tra email đã tồn tại hay chưa.
        /// Author       :   HoangNM - 04/09/2018 - create
        /// </summary>
        /// <param name="value">giá trị của email </param>
        /// <returns>Nếu có tồn tại trả về true, ngược lại trả về false</returns>
        /// <remarks>
        /// Method: GET
        /// RouterName: homeCreateAccount
        /// </remarks>
        public bool CheckExistEmail(string value)
        {
            try
            {
                return new InformationModel().CheckExistEmail(value);
            }
            catch
            {
                return false;
            }
        }
    }
}