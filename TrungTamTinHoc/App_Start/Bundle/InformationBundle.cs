﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace TrungTamTinHoc.App_Start.Bundle
{
    public class InformationBundle
    {
        public static BundleCollection RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/public/js/information").Include(
                "~/public/js/common/md5.js",
                "~/public/assets/moment/moment.js",
                "~/public/assets/bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js",
                "~/public/assets/switchery/dist/switchery.js",
                "~/public/js/information/information/information.js",
                "~/public/js/information/information/changePassword.js"
            ));

            bundles.Add(new StyleBundle("~/public/css/information").Include(
                "~/public/assets/switchery/dist/switchery.css",
                "~/public/assets/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css",
                "~/public/css/Information/Information/Information.css"
            ));
            return bundles;
        }
    }
}