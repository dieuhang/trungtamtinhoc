﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TrungTamTinHoc.Areas.ControlPanel.Models.ClassManagement.Schema;
using TTTH.Common;
using TTTH.Common.Enums;
using TTTH.DataBase;
using TTTH.DataBase.Schema;
using Z.EntityFramework.Plus;
using TblClassTrans = TTTH.DataBase.Schema.LopHocTrans;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.ClassManagement
{
    /// <summary>
    /// Class chứa các phương thức liên quan đến việc xử lý lớp học
    /// Author       :   TramHTD - 04/09/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks
    public class ClassMasterModel
    {
        DataContext context;
        public ClassMasterModel()
        {
            context = new DataContext();
        }
        /// <summary>
        /// Lấy danh sách khóa học có trong db
        /// Author       :   TramHTD - 04/09/2018 - create
        /// </summary>
        /// <returns>danh sách khóa học có trong db</returns>
        public List<Schema.KhoaHoc> GetKhoaHocs()
        {
            return context.KhoaHocTrans.Where(x => x.Lang == Common.defaultLang && !x.DelFlag)
                .Select(x => new Schema.KhoaHoc
                {
                    IdKhoaHoc = x.Id,
                    TenKhoaHoc = x.TenKhoaHoc
                }).ToList();
        }

        /// <summary>
        /// Lấy thông tin của 1 lớp học
        /// Author       :   TramHTD - 04/09/2018 - create
        /// </summary>
        /// <param name="id">id của lớp học sẽ được hiển thị</param>
        /// <returns>Thông tin của lớp học</returns>
        public ClassMaster LoadClass(string id)
        {
            try
            {
                ClassMaster classMaster = new ClassMaster();
                int idClass = 0;
                try
                {
                    idClass = Convert.ToInt32(id);
                }
                catch { }
                TblClassTrans _class = context.LopHocTrans.FirstOrDefault(x => x.Id == idClass && !x.DelFlag && x.Lang == Common.defaultLang);
                if (_class != null)
                {
                    classMaster.Mode = (int)ConstantsEnum.ModeMaster.Update;
                    classMaster.ClassInfo = new Class();
                    classMaster.ClassInfo.Id = _class.Id;
                    classMaster.ClassInfo.IdKhoaHoc = _class.LopHoc.IdKhoaHoc;
                    classMaster.ClassInfo.SoTiet = _class.LopHoc.SoTiet;
                    classMaster.ClassInfo.ThoiGianMoiTiet = _class.LopHoc.ThoiGianMoiTiet;
                    classMaster.ClassInfo.SoLuongHocVien = _class.LopHoc.SoLuongHocVien;
                    classMaster.ClassInfo.ThoiGianBatDau = _class.LopHoc.ThoiGianBatDau;
                    classMaster.ClassInfo.ThoiGianKetThuc = _class.LopHoc.ThoiGianKetThuc;
                    classMaster.ClassInfo.ChoPhepDangKy = _class.LopHoc.ChoPhepDangKy;
                    classMaster.ClassInfo.TenLop = _class.TenLop;
                    classMaster.ClassInfo.TomTat = _class.TomTat;
                    classMaster.Language = context.Language.Where(x => !x.DelFlag)
                        .Select(x => new Lang
                        {
                            Id = x.Id,
                            Name = x.Name
                        }).OrderBy(x => x.Id).ToList();
                    if (classMaster.Language.Count > 0)
                    {
                        string lang = classMaster.Language[0].Id;
                        _class = context.LopHocTrans.FirstOrDefault(x => x.Id == idClass && !x.DelFlag && x.Lang == lang);
                        classMaster.Mode = (int)ConstantsEnum.ModeMaster.Update;
                        classMaster.ClassTrans = new Class();
                        classMaster.ClassTrans.Id = _class.Id;
                        classMaster.ClassTrans.IdKhoaHoc = _class.LopHoc.IdKhoaHoc;
                        classMaster.ClassTrans.SoTiet = _class.LopHoc.SoTiet;
                        classMaster.ClassTrans.ThoiGianMoiTiet = _class.LopHoc.ThoiGianMoiTiet;
                        classMaster.ClassTrans.SoLuongHocVien = _class.LopHoc.SoLuongHocVien;
                        classMaster.ClassTrans.ThoiGianBatDau = _class.LopHoc.ThoiGianBatDau;
                        classMaster.ClassTrans.ThoiGianKetThuc = _class.LopHoc.ThoiGianKetThuc;
                        classMaster.ClassTrans.ChoPhepDangKy = _class.LopHoc.ChoPhepDangKy;
                        classMaster.ClassTrans.TenLop = _class.TenLop;
                        classMaster.ClassTrans.TomTat = _class.TomTat;
                    }
                }
                return classMaster;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Tạo hoặc cập nhật lại lớp học từ thông tin mà người dùng gửi lên
        /// Author       :   TramHTD - 04/09/2018 - create
        /// </summary>
        /// <param name="_class">Các thông tin của lớp học muốn tạo hoặc cập nhật</param>
        /// <returns>Thông tin về việc tạo hoặc cập nhật khóa học thành công hay thất bại</returns>
        public ResponseInfo SaveClass(Class _class)
        {
            try
            {
                if (context.LopHoc.FirstOrDefault(x => x.Id == _class.Id && !x.DelFlag) != null)
                {
                    return UpadateClass(_class);
                }
                return AddClass(_class);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Cập nhật lại  lớp học từ thông tin mà người dùng gửi lên
        /// Author       :   TramHTD - 04/09/2018 - create
        /// </summary>
        /// <param name="course">Các thông tin của lớp học muốn cập nhật</param>
        /// <returns>Thông tin về việc cập nhật lớp học thành công hay thất bại</returns>
        public ResponseInfo UpadateClass(Class _class)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo response = new ResponseInfo();
                _class.ChoPhepDangKy = _class.CheckChoPhepDangKy == "on" ? true : false;
                context.LopHoc.Where(x => x.Id == _class.Id && !x.DelFlag)
                        .Update(x => new TTTH.DataBase.Schema.LopHoc
                        {
                            IdKhoaHoc = _class.IdKhoaHoc,
                            SoTiet = _class.SoTiet,
                            ThoiGianMoiTiet = _class.ThoiGianMoiTiet,
                            SoLuongHocVien = _class.SoLuongHocVien,
                            ThoiGianBatDau = _class.ThoiGianBatDau,
                            ThoiGianKetThuc = _class.ThoiGianKetThuc,
                            ChoPhepDangKy = _class.ChoPhepDangKy,
                        });
                context.LopHocTrans.Where(x => x.Id == _class.Id && x.Lang == Common.defaultLang && !x.DelFlag)
                    .Update(x => new TTTH.DataBase.Schema.LopHocTrans
                    {
                        TenLop = _class.TenLop,
                        TomTat = _class.TomTat
                    });

                context.SaveChanges();
                transaction.Commit();
                return response;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }
        /// <summary>
        /// Tạo  lớp học từ thông tin mà người dùng gửi lên
        /// Author       :   TramHTD - 04/09/2018 - create
        /// </summary>
        /// <param name="_class">Các thông tin của lớp học muốn tạo</param>
        /// <returns>Thông tin về việc tạo  khóa học thành công hay thất bại</returns>
        public ResponseInfo AddClass(Class _class)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo response = new ResponseInfo();
                List<Lang> listOfLang = context.Language.Where(x => !x.DelFlag).Select(x => new Lang
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList();
                _class.ChoPhepDangKy = _class.CheckChoPhepDangKy == "on" ? true : false;
                _class.Id = context.LopHoc.Count() == 0 ? 1 : context.LopHoc.Max(x => x.Id) + 1;
                context.LopHoc.Add(new LopHoc
                {
                    Id = _class.Id,
                    IdKhoaHoc = _class.IdKhoaHoc,
                    SoTiet = _class.SoTiet,
                    ThoiGianMoiTiet = _class.ThoiGianMoiTiet,
                    SoLuongHocVien = _class.SoLuongHocVien,
                    ThoiGianBatDau = _class.ThoiGianBatDau,
                    ThoiGianKetThuc = _class.ThoiGianKetThuc,
                    ChoPhepDangKy = _class.ChoPhepDangKy,
                });
                foreach (Lang lang in listOfLang)
                {
                    context.LopHocTrans.Add(new LopHocTrans
                    {
                        Id = _class.Id,
                        Lang = lang.Id,
                        TenLop = _class.TenLop,
                        TomTat = _class.TomTat
                    });
                }
                context.SaveChanges();
                response.ThongTinBoSung1 = _class.Id + "";
                transaction.Commit();
                return response;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        /// <summary>
        /// Cập nhật lại lớp học từ thông tin mà người dùng gửi lên bằng ngôn ngữ khác
        /// Author       :   TramHTD - 27/08/2018 - create
        /// </summary>
        /// <param name="_class">Các thông tin ngôn ngữ khác của lớp học muốn cập nhật</param>
        /// <returns>Thông tin về việc cập nhật lớp học thành công hay thất bại</returns>
        public ResponseInfo UpdateClassTranslate(Class_Trans _class)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo response = new ResponseInfo();
                context.LopHocTrans.Where(x => x.Id == _class.Id && x.Lang == _class.LanguageTrans && !x.DelFlag)
                    .Update(x => new TTTH.DataBase.Schema.LopHocTrans
                    {
                        TenLop = _class.TenLop,
                        TomTat = _class.TomTat,
                    });
                context.SaveChanges();
                response.ThongTinBoSung1 = _class.Id + "";
                transaction.Commit();
                return response;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        /// <summary>
        /// Lấy thông tin của lóp học bởi ngôn ngữ khác ngôn ngữ mặc định
        /// Author       :   TramHTD - 04/09/2018 - create
        /// </summary>
        /// <param name="id">Id của lớp học muốn lấy</param>
        /// <param name="lang">Ngôn ngữ muốn lấy thông tin liên quan</param>
        /// <returns>Thông tin của lớp học có nhiều ngôn ngữ</returns>
        public Class_Trans ReferClassWithLang(int id, string lang)
        {
            try
            {
                return context.LopHocTrans.Where(x => x.Id == id && x.Lang == lang && !x.DelFlag)
                    .Select(x => new Class_Trans()
                    {
                        TenLop = x.TenLop,
                        TomTat = x.TomTat,
                    }).FirstOrDefault();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}