﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TTTH.Common;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.ThingsAchievedManagement.Schema
{
    /// <summary>
    /// Class dùng để lấy danh sách slide trả về cho trang danh sách Những điều đạt được
    /// Author       :   HaLTH - 26/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ListOfThingsAchieved
    {
        public List<ThingsAchieved> ThingsAchievedList { set; get; }
        public Paging Paging { set; get; }
        public ThingsAchievedConditionSearch Condition { set; get; }
        public ListOfThingsAchieved()
        {
            this.ThingsAchievedList = new List<ThingsAchieved>();
            this.Condition = new ThingsAchievedConditionSearch();
            this.Paging = new Paging();
        }
    }
}