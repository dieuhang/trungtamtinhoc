﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.EmailSubscriptionManagement.Schema
{
    /// <summary>
    /// Class dùng để chứa thông tin của một đăng ký theo dõi qua email
    /// Author       :   HaLTH - 19/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class EmailSubscription
    {
        public int Id { set; get; }

        [Required]
        [StringLength(255)]
        public string Email { get; set; }
    }

    /// <summary>
    /// Class dùng để chứa thông tin tìm kiếm của danh sách đăng ký theo dõi qua email
    /// Author       :   HaLTH - 19/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class EmailSubscriptionConditionSearch
    {
        public string KeySearch { set; get; }
        public int CurrentPage { set; get; }
        public int PageSize { set; get; }
        public EmailSubscriptionConditionSearch()
        {
            this.KeySearch = "";
            this.PageSize = 10;
            this.CurrentPage = 1;
        }
    }
}