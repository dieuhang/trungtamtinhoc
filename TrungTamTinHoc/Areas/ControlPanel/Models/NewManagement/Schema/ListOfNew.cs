﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TTTH.Common;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.NewsManagement.Schema
{
    /// <summary>
    /// Class dùng để lấy danh sách tin tức trả về cho trang danh sách tin tức
    /// Author       :   HangNTD - 20/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ListOfNews
    {
        public List<News> TinTucList { set; get; }
        public Paging Paging { set; get; }
        public NewsConditionSearch Condition { set; get; }
        public ListOfNews()
        {
            this.TinTucList = new List<News>();
            this.Condition = new NewsConditionSearch();
            this.Paging = new Paging();
        }
    }
}