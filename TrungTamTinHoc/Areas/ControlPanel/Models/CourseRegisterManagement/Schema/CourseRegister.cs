﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.CourseRegisterManagement.Schema
{
    /// <summary>
    /// Class chứa tất cả dữ liệu cho việc hiển thị các đăng ký học ở trang quản lý đăng ký học của admin
    /// Author       :   AnTM - 14/07/2018 - create
    /// Author       :   HoangNM - 25/07/2018 - update
    /// </summary>
    /// <remarks>
    /// Package      :   Home.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class CourseRegister
    {
        public int IDLopHoc { get; set; }

        public string HoHocVien { get; set; }

        public string TenHocVien { get; set; }

        public bool GioiTinhHocVien { get; set; }

        public DateTime NgaySinhHocVien { get; set; }

        public string DiaChiHocVien { get; set; }

        public string XaHocVien { get; set; }

        public string HuyenHocVien { get; set; }

        public string TinhHocVien { get; set; }

        public string SoCMNDHocVien { get; set; }

        public string SoDienThoaiHocVien { get; set; }

        public string EmailHocVien { get; set; }

        public string HoGiamHo { get; set; }

        public string TenGiamHo { get; set; }

        public bool GioiTinhGiamHo { get; set; }

        public DateTime? NgaySinhGiamHo { get; set; }

        public string DiaChiGiamHo { get; set; }

        public string XaGiamHo { get; set; }

        public string HuyenGiamHo { get; set; }

        public string TinhGiamHo { get; set; }

        public string SoCMNDGiamHo { get; set; }

        public string SoDienThoaiGiamHo { get; set; }

        public string EmailGiamHo { get; set; }

        public string GhiChu { get; set; }

        public int Id { set; get; }

        public int IdUser { get; set; }
        
        public string TenLopHoc { get; set; }

        public string TenKhoaHoc { get; set; }

        public DateTime NgayDangKy { get; set; }

        public int TrangThaiDangKy { get; set; }
    }
    /// <summary>
    /// Class chứa các thuộc tính là điều kiện để get các đăng ký hocj lấy từ DB cho việc fill dữ liệu lên table ở trang admin
    /// Author       :   AnTM - 14/07/2018 - create
    /// Author       :   HoangNM - 25/07/2018 - update
    /// </summary>
    /// <remarks>
    /// Package      :   Home.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class CourseRegisterConditionSearch
    {
        public string KeySearch { set; get; }
        public int idTrangThai { set; get; }
        public int CurrentPage { set; get; }
        public int PageSize { set; get; }
        public DateTime? tuNgay { set; get; }
        public DateTime? toiNgay { set; get; }
        public int idKhoaHoc { get; set; }
        public int idLopHoc { get; set; }
        public CourseRegisterConditionSearch()
        {
            this.KeySearch = "";
            this.idTrangThai = 0;
            this.PageSize = 10;
            this.CurrentPage = 1;
            this.tuNgay = null;
            this.toiNgay = null;
            this.idKhoaHoc = 0;
            this.idLopHoc = 0;
        }
    }

}