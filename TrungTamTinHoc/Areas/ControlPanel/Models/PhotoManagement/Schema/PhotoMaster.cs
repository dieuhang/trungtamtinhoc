﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TTTH.Common;
using static TTTH.Common.Enums.ConstantsEnum;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.PhotoManagement.Schema
{
    /// <summary>
    /// Class dùng để chứa thông tin của của album ảnh,dùng để thêm hoặc sửa
    /// Author       :   HoangNM - 09/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class PhotoMaster
    {
        public Photo PhotoInfo { set; get; }
        public Photo PhotoTrans { set; get; }
        public List<Lang> Language { set; get; }
        public int Mode { set; get; }
        public PhotoMaster()
        {
            Mode = (int)ModeMaster.Insert;
            PhotoInfo = null;
            PhotoTrans = null;
            Language = new List<Lang>();
        }
    }
}