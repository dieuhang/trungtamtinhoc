﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TTTH.Common;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.CourseManagement.Schema
{
    /// <summary>
    /// Class dùng để lấy danh sách khóa học trả về cho trang danh sách khóa học
    /// Author       :   TramHTD - 20/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ListOfCourse
    {
        public List<Course> CourseList { set; get; }
        public List<ChuyenNganh> ChuyenNganh { set; get; }
        public Paging Paging { set; get; }
        public CourseConditionSearch Condition { set; get; }

        public ListOfCourse()
        {
            this.CourseList = new List<Course>();
            this.ChuyenNganh = new List<ChuyenNganh>();
            this.Condition = new CourseConditionSearch();
            this.Paging = new Paging();
        }
    }
}