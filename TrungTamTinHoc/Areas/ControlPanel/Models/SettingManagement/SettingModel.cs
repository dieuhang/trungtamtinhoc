﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TrungTamTinHoc.Areas.ControlPanel.Models.SettingManagement.Schema;
using TTTH.Common;
using TTTH.DataBase;
using Z.EntityFramework.Plus;
using TblCaiDatHeThong = TTTH.DataBase.Schema.CaiDatHeThong;
using Account = TTTH.DataBase.Schema.Account;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.SettingManagement
{
    /// <summary>
    /// Class dùng để xử lý các hoạt động liên quan đến hiển thị cài đặt hệ thống
    /// Author       :   HoangNM - 17/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class SettingModel
    {
        DataContext context;
        public SettingModel()
        {
            context = new DataContext();
        }

        // phần cài đặt hệ thống ---------------------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Lấy thông tin của Cài đặt hệ thống dùng để cập nhật thông tin
        /// Author       :   HoangNM - 18/08/2018 - create
        /// </summary>

        public SettingMaster LoadSetting()
        {
            try
            {
                SettingMaster settingMaster = new SettingMaster();

                TblCaiDatHeThong setting = context.CaiDatHeThong.FirstOrDefault(x => !x.DelFlag && x.Lang == Common.defaultLang);
                if (setting != null)
                {
                    settingMaster.SettingInfo = new Setting();
                    settingMaster.SettingInfo.DiaChi = setting.DiaChi;
                    settingMaster.SettingInfo.SoDienThoai = setting.SoDienThoai;
                    settingMaster.SettingInfo.LinkFB = setting.LinkFB;
                    settingMaster.SettingInfo.LinkGoogle = setting.LinkGoogle;
                    settingMaster.SettingInfo.Email = setting.Email;
                    settingMaster.SettingInfo.Skype = setting.Skype;
                    settingMaster.SettingInfo.MucBaoHiemXH = setting.MucBaoHiemXH;
                    settingMaster.SettingInfo.ThueTNCN1 = setting.ThueTNCN1;
                    settingMaster.SettingInfo.ThueTNCN2 = setting.ThueTNCN2;
                    settingMaster.SettingInfo.ThueTNCN3 = setting.ThueTNCN3;
                    settingMaster.SettingInfo.GioiHanThueTNCN1 = setting.GioiHanThueTNCN1;
                    settingMaster.SettingInfo.GioiHanThueTNCN2 = setting.GioiHanThueTNCN2;
                    settingMaster.SettingInfo.GioiHanThueTNCN3 = setting.GioiHanThueTNCN3;
                    settingMaster.SettingInfo.About = setting.About;
                    settingMaster.SettingInfo.WhyUs = setting.WhyUs;
                    settingMaster.SettingInfo.TomTat = setting.TomTat;
                    settingMaster.SettingInfo.Keyword = setting.Keyword;
                    settingMaster.SettingInfo.Author = setting.Author;
                    settingMaster.SettingInfo.Link = setting.Link;
                    settingMaster.SettingInfo.KinhDo = setting.KinhDo;
                    settingMaster.SettingInfo.ViDo = setting.ViDo;
                    settingMaster.SettingInfo.GioiThieuChungKhoaHoc = setting.GioiThieuChungKhoaHoc;
                    settingMaster.SettingInfo.EmailHeThong = setting.EmailHeThong;
                    settingMaster.SettingInfo.MatKhauEmail = setting.MatKhauEmail;
                    settingMaster.Language = context.Language.Where(x => !x.DelFlag)
                        .Select(x => new Lang
                        {
                            Id = x.Id,
                            Name = x.Name
                        }).OrderBy(x => x.Id).ToList();
                    if (settingMaster.Language.Count > 0)
                    {
                        string lang = settingMaster.Language[0].Id;
                        setting = context.CaiDatHeThong.FirstOrDefault(x => !x.DelFlag && x.Lang == lang);
                        settingMaster.SettingTrans = new Setting();
                        settingMaster.SettingTrans.DiaChi = setting.DiaChi;
                        settingMaster.SettingTrans.About = setting.About;
                        settingMaster.SettingTrans.WhyUs = setting.WhyUs;
                        settingMaster.SettingTrans.TomTat = setting.TomTat;
                        settingMaster.SettingTrans.GioiThieuChungKhoaHoc = setting.GioiThieuChungKhoaHoc;
                    }
                }
                return settingMaster;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Cập nhật thông tin của Cài đặt hệ thống
        /// Author       :   HoangNM - 18/08/2018 - create
        /// </summary>
        /// <param name="setting">chứa thông tin phần cài đặt hệ thống</param>
        public ResponseInfo UpdateSetting(Setting setting)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo response = new ResponseInfo();

                context.CaiDatHeThong.Where(x => x.Id == 1 && !x.DelFlag)
                    .Update(x => new TTTH.DataBase.Schema.CaiDatHeThong
                    {
                        SoDienThoai = setting.SoDienThoai,
                        LinkFB = setting.LinkFB,
                        LinkGoogle = setting.LinkGoogle,
                        Email = setting.Email,
                        Skype = setting.Skype,
                        MucBaoHiemXH = setting.MucBaoHiemXH,
                        ThueTNCN1 = setting.ThueTNCN1,
                        ThueTNCN2 = setting.ThueTNCN2,
                        ThueTNCN3 = setting.ThueTNCN3,
                        GioiHanThueTNCN1 = setting.GioiHanThueTNCN1,
                        GioiHanThueTNCN2 = setting.GioiHanThueTNCN2,
                        GioiHanThueTNCN3 = setting.GioiHanThueTNCN3,
                        Keyword = setting.Keyword,
                        Author = setting.Author,
                        Link = setting.Link,
                        KinhDo = setting.KinhDo,
                        ViDo = setting.ViDo,
                        EmailHeThong = setting.EmailHeThong,
                        MatKhauEmail = setting.MatKhauEmail
                    });
                context.CaiDatHeThong.Where(x => x.Id == 1 && x.Lang == Common.defaultLang && !x.DelFlag)
                    .Update(x => new TTTH.DataBase.Schema.CaiDatHeThong
                    {
                        DiaChi = setting.DiaChi,
                        About = setting.About,
                        WhyUs = setting.WhyUs,
                        TomTat = setting.TomTat,
                        GioiThieuChungKhoaHoc = setting.GioiThieuChungKhoaHoc
                    });
                context.SaveChanges();
                transaction.Commit();
                return response;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        /// <summary>
        /// Cập nhật thông tin của Cài đặt hệ thống với ngôn ngữ khác
        /// Author       :   HoangNM - 18/08/2018 - create
        /// </summary>
        /// <param name="setting">chứa thông tin phần cài đặt hệ thống</param>
        public ResponseInfo UpdateSettingTranslate(Setting_Trans setting)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo response = new ResponseInfo();
                context.CaiDatHeThong.Where(x => x.Id == 1 && x.Lang == setting.LanguageTrans && !x.DelFlag)
                    .Update(x => new TTTH.DataBase.Schema.CaiDatHeThong
                    {
                        DiaChi = setting.diaChi,
                        About = setting.About,
                        WhyUs = setting.WhyUs,
                        TomTat = setting.TomTat,
                        GioiThieuChungKhoaHoc = setting.GioiThieuChungKhoaHoc
                    });
                context.SaveChanges();
                transaction.Commit();
                return response;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }
        /// <summary>
        /// Cập nhật thông tin của Cài đặt hệ thống với ngôn ngữ khác
        /// Author       :   HoangNM - 29/08/2018 - create
        /// </summary>
        /// <param name="setting">chứa thông tin phần cài đặt hệ thống</param>
        public Setting_Trans ReferSettingWithLang(string lang)
        {
            try
            {
                return context.CaiDatHeThong.Where(x => x.Id == 1 && x.Lang == lang && !x.DelFlag)
                    .Select(x => new Setting_Trans
                    {
                        diaChi = x.DiaChi,
                        About = x.About,
                        WhyUs = x.WhyUs,
                        TomTat = x.TomTat,
                        GioiThieuChungKhoaHoc = x.GioiThieuChungKhoaHoc,
                    }).FirstOrDefault();
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        // phần chính sách bảo mật -------------------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Lấy thông tin của phần chính sách bảo mật
        /// Author       :   HoangNM - 28/08/2018 - create
        /// </summary>
        /// 
        public dataMaster getPrivacyPolicy()
        {
            try
            {
                dataMaster settingMaster = new dataMaster();
                settingMaster.typeSetting = 1;
                TblCaiDatHeThong setting = context.CaiDatHeThong.FirstOrDefault(x => !x.DelFlag && x.Lang == Common.defaultLang);
                if (setting != null)
                {
                    settingMaster.data = setting.ChinhSachBaoMat;
                    settingMaster.Language = context.Language.Where(x => !x.DelFlag)
                        .Select(x => new Lang
                        {
                            Id = x.Id,
                            Name = x.Name
                        }).OrderBy(x => x.Id).ToList();
                    if (settingMaster.Language.Count > 0)
                    {
                        string lang = settingMaster.Language[0].Id;
                        settingMaster.dataLang = context.CaiDatHeThong.FirstOrDefault(x => !x.DelFlag && x.Lang == lang).ChinhSachBaoMat;
                    }
                }
                return settingMaster;
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        /// <summary>
        /// Cập nhật thông tin chính sách bảo mật
        /// Author       :   HoangNM - 28/08/2018 - create
        /// </summary>
        /// <param name="data">chứa thông tin của chính sách bảo mật</param>
        public ResponseInfo UpdatePrivacyPolicy(string data)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo response = new ResponseInfo();

                context.CaiDatHeThong.Where(x => x.Id == 1 && x.Lang == Common.defaultLang && !x.DelFlag)
                    .Update(x => new TTTH.DataBase.Schema.CaiDatHeThong
                    {
                        ChinhSachBaoMat = data
                    });
                context.SaveChanges();
                transaction.Commit();
                return response;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }


        /// <summary>
        /// Cập nhật thông tin chính sách bảo mật với ngôn ngữ khác
        /// Author       :   HoangNM - 28/08/2018 - create
        /// </summary>
        /// <param name="data">chứa thông tin chính sách bảo mật theo ngôn ngữ khác</param>
        /// <param name="lang">ngôn ngữ khác</param>
        public ResponseInfo UpdatePrivacyPolicyTranslate(string data, string lang)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo response = new ResponseInfo();
                context.CaiDatHeThong.Where(x => x.Id == 1 && x.Lang == lang && !x.DelFlag)
                    .Update(x => new TTTH.DataBase.Schema.CaiDatHeThong
                    {
                        ChinhSachBaoMat = data
                    });
                context.SaveChanges();
                transaction.Commit();
                return response;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        /// <summary>
        /// Cập nhật thông tin chính sách bảo mật khi thay đổi ngôn ngữ
        /// Author       :   HoangNM - 29/08/2018 - create
        /// </summary>
        /// <param name="lang">ngôn ngữ</param>
        public string ReferPrivacyPolicyWithLang(string lang)
        {
            try
            {
                return context.CaiDatHeThong.Where(x => x.Id == 1 && x.Lang == lang && !x.DelFlag)
                    .Select(x => x.ChinhSachBaoMat).FirstOrDefault();
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        // hướng dẫn sử dụng ---------------------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------------------------------------------


        /// <summary>
        /// Lấy thông tin của hướng dẫn sử dụng
        /// Author       :   HoangNM - 28/08/2018 - create
        /// </summary>
        /// 
        public dataMaster getUserManual()
        {
            try
            {
                dataMaster settingMaster = new dataMaster();
                settingMaster.typeSetting = 2;
                TblCaiDatHeThong setting = context.CaiDatHeThong.FirstOrDefault(x => !x.DelFlag && x.Lang == Common.defaultLang);
                if (setting != null)
                {
                    settingMaster.data = setting.HuongDanSuDung;
                    settingMaster.Language = context.Language.Where(x => !x.DelFlag)
                        .Select(x => new Lang
                        {
                            Id = x.Id,
                            Name = x.Name
                        }).OrderBy(x => x.Id).ToList();
                    if (settingMaster.Language.Count > 0)
                    {
                        string lang = settingMaster.Language[0].Id;
                        settingMaster.dataLang = context.CaiDatHeThong.FirstOrDefault(x => !x.DelFlag && x.Lang == lang).HuongDanSuDung;
                    }
                }
                return settingMaster;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Cập nhật thông tin hướng dẫn sử dụng 
        /// Author       :   HoangNM - 28/08/2018 - create
        /// </summary>
        /// <param name="data">chứa thông tin hướng dẫn sử dụng</param>
        public ResponseInfo UpdateUserManual(string data)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo response = new ResponseInfo();

                context.CaiDatHeThong.Where(x => x.Id == 1 && x.Lang == Common.defaultLang && !x.DelFlag)
                    .Update(x => new TTTH.DataBase.Schema.CaiDatHeThong
                    {
                        HuongDanSuDung = data
                    });
                context.SaveChanges();
                transaction.Commit();
                return response;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        /// <summary>
        /// Cập nhật thông tin hướng dẫn sử dụng với ngôn ngữ khác
        /// Author       :   HoangNM - 28/08/2018 - create
        /// </summary>
        /// <param name="data">chứa thông tin hướng dẫn sử dụng theo ngôn ngữ khác</param>
        /// <param name="lang">ngôn ngữ khác</param>
        public ResponseInfo UpdateUserManualTranslate(string data, string lang)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo response = new ResponseInfo();
                context.CaiDatHeThong.Where(x => x.Id == 1 && x.Lang == lang && !x.DelFlag)
                    .Update(x => new TTTH.DataBase.Schema.CaiDatHeThong
                    {
                        HuongDanSuDung = data
                    });
                context.SaveChanges();
                transaction.Commit();
                return response;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }
        /// <summary>
        /// Cập nhật thông tin của hướng dẫn sử dụng với ngôn ngữ khác
        /// Author       :   HoangNM - 29/08/2018 - create
        /// </summary>
        /// <param name="lang">ngôn ngữ</param>
        public string ReferUserManualWithLang(string lang)
        {
            try
            {
                return context.CaiDatHeThong.Where(x => x.Id == 1 && x.Lang == lang && !x.DelFlag)
                    .Select(x => x.HuongDanSuDung).FirstOrDefault();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        // phần điều khoản sử dụng -------------------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Lấy thông tin của phần điều khoản sử dụng
        /// Author       :   HoangNM - 28/08/2018 - create
        /// </summary>
        /// 
        public dataMaster getTermsOfUse()
        {
            try
            {
                dataMaster settingMaster = new dataMaster();
                settingMaster.typeSetting = 3;
                TblCaiDatHeThong setting = context.CaiDatHeThong.FirstOrDefault(x => !x.DelFlag && x.Lang == Common.defaultLang);
                if (setting != null)
                {
                    settingMaster.data = setting.DieuKhoanSuDung;
                    settingMaster.Language = context.Language.Where(x => !x.DelFlag)
                        .Select(x => new Lang
                        {
                            Id = x.Id,
                            Name = x.Name
                        }).OrderBy(x => x.Id).ToList();
                    if (settingMaster.Language.Count > 0)
                    {
                        string lang = settingMaster.Language[0].Id;
                        settingMaster.dataLang = context.CaiDatHeThong.FirstOrDefault(x => !x.DelFlag && x.Lang == lang).DieuKhoanSuDung;
                    }
                }
                return settingMaster;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Cập nhật thông tin điều khoản sử dụng
        /// Author       :   HoangNM - 28/08/2018 - create
        /// </summary>
        /// <param name="data">chứa thông tin điều khoản sử dụng</param>
        public ResponseInfo UpdateTermsOfUse(string data)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo response = new ResponseInfo();

                context.CaiDatHeThong.Where(x => x.Id == 1 && x.Lang == Common.defaultLang && !x.DelFlag)
                    .Update(x => new TTTH.DataBase.Schema.CaiDatHeThong
                    {
                        DieuKhoanSuDung = data
                    });
                context.SaveChanges();
                transaction.Commit();
                return response;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        /// <summary>
        /// Cập nhật thông tin điều khoản sử dụng với ngôn ngữ khác
        /// Author       :   HoangNM - 28/08/2018 - create
        /// </summary>
        /// <param name="data">chứa thông tin điều khoản sử dụng theo ngôn ngữ khác</param>
        /// <param name="lang">ngôn ngữ khác</param>
        public ResponseInfo UpdateTermsOfUseTranslate(string data, string lang)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo response = new ResponseInfo();
                context.CaiDatHeThong.Where(x => x.Id == 1 && x.Lang == lang && !x.DelFlag)
                    .Update(x => new TTTH.DataBase.Schema.CaiDatHeThong
                    {
                        DieuKhoanSuDung = data
                    });
                context.SaveChanges();
                transaction.Commit();
                return response;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        /// <summary>
        /// Cập nhật thông tin của điều khoản sử dụng với ngôn ngữ khác
        /// Author       :   HoangNM - 29/08/2018 - create
        /// </summary>
        /// <param name="lang">ngôn ngữ</param>
        public string ReferTermsOfUseWithLang(string lang)
        {
            try
            {
                return context.CaiDatHeThong.Where(x => x.Id == 1 && x.Lang == lang && !x.DelFlag)
                    .Select(x => x.DieuKhoanSuDung).FirstOrDefault();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}