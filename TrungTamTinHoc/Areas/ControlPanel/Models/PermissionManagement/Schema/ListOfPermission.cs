﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.PermissionManagement.Schema
{
    /// <summary>
    /// Class chứa tất cả dữ liệu cho việc hiển thị list các permission của từng group lên table quản lý ở trang admin.
    /// Author       :   AnTM - 13/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ListOfPermission
    {
        public int IdGroupSelected { get; set; }
        public List<PermissionsInScreen> cacManHinh { get; set; }
        public List<Group> ListGroup { get; set; }

        public ListOfPermission()
        {
            cacManHinh = new List<PermissionsInScreen>();
            ListGroup = new List<Group>();
        }
    }
}