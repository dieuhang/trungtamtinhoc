﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.PhotoManagement.Schema
{
    /// <summary>
    /// Class dùng để chứa thông tin của một hình ảnh
    /// Author       :   HaLTH - 06/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class Photo
    {
        public int Id { set; get; }

        [StringLength(255)]
        public string LinkAnh { set; get; }

        [StringLength(500)]
        public string Note { set; get; }

        public HttpPostedFileBase FileImg { set; get; }
    }

    public class Photos_Trans
    {
        public int Id { set; get; }

        public string LanguageTrans { set; get; }

        [Required]
        [StringLength(1000)]
        public string Note { get; set; }
    }

    /// <summary>
    /// Class dùng để chứa thông tin tìm kiếm của danh sách hình ảnh
    /// Author       :   HaLTH - 06/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class PhotoConditionSearch
    {
        public string KeySearch { set; get; }
        public int CurrentPage { set; get; }
        public int PageSize { set; get; }
        public PhotoConditionSearch()
        {
            this.KeySearch = "";
            this.PageSize = 10;
            this.CurrentPage = 1;
        }
    }
}