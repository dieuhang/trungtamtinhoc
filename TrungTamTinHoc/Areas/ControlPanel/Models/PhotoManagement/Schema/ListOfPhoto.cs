﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TTTH.Common;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.PhotoManagement.Schema
{
    /// <summary>
    /// Class dùng để lấy danh sách hình ảnh trả về cho trang danh sách danh sách hình ảnh
    /// Author       :   HaLTH - 06/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ListOfPhoto
    {
        public List<Photo> PhotoList { set; get; }
        public Paging Paging { set; get; }
        public PhotoConditionSearch Condition { set; get; }
        public ListOfPhoto()
        {
            this.PhotoList = new List<Photo>();
            this.Condition = new PhotoConditionSearch();
            this.Paging = new Paging();
        }
    }
}