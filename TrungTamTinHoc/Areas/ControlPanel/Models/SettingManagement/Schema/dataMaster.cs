﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TTTH.Common;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.SettingManagement.Schema
{
    /// <summary>
    /// Class dùng để chứa thông tin của phần chinh sách bảo mật / hướng dẫn sử dụng / hoặc điều khoản ứng dụng
    /// Author       :   HoangNM - 29/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    /// 
    public class dataMaster
    {
        //chứa dữ liệu ngôn ngữ chính (tiếng việt)
        public string data { get; set; }
        //chứa dữ liệu ngôn ngữ khác
        public string dataLang { get; set; }
        public List<Lang> Language { set; get; }
        public int typeSetting { get; set; }
        public dataMaster()
        {
            data = null;
            dataLang = null;
            Language = new List<Lang>();
        }
    }

}