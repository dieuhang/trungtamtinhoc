﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrungTamTinHoc.Areas.Information.Models.Schema
{
    /// <summary>
    /// Class thông tin của việc thay đổi email.
    /// Author       :   Hoang - 07/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   Information.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ChangeEmail
    {
        [Required(ErrorMessage = "1")]
        [MaxLength(255, ErrorMessage = "2")]
        [EmailAddress(ErrorMessage = "5")]
        public string Email { set; get; }
    }
}