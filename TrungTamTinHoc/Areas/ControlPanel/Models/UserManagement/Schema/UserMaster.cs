﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TTTH.Common.Enums;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.UserManagement.Schema
{
    public class UserMaster
    {
        public Account User { set; get; }
        public int Mode { set; get; }
        public UserMaster()
        {
            Mode = (int)ConstantsEnum.ModeMaster.Insert;
            User = null;
        }
    }
}