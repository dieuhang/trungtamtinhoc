﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace TrungTamTinHoc.App_Start.Bundle
{
    public class CourseBundle
    {
        public static BundleCollection RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/public/js/registerCourse").Include(
                "~/public/js/common/md5.js",
                "~/public/assets/moment/moment.js",
                "~/public/assets/bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js",
                "~/public/js/course/registerCourse/register.js"
            ));

            bundles.Add(new ScriptBundle("~/public/js/danhSachKhoaHoc").Include(
                "~/public/assets/pagination/pagination.js",
                "~/public/js/course/khoaHoc/danhSachKhoaHoc.js"
            ));

            bundles.Add(new StyleBundle("~/public/css/registerCourse").Include(
                "~/public/assets/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css",
                "~/public/css/course/registerCourse/register.css"
            ));

            bundles.Add(new ScriptBundle("~/public/js/courseDetails").Include(
                "~/public/js/course/courseDetails/index.js"
            ));
            return bundles;
        }
    }
}