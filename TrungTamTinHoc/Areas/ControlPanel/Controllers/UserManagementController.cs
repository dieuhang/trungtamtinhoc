﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrungTamTinHoc.Areas.ControlPanel.Models.UserManagement;
using TrungTamTinHoc.Areas.ControlPanel.Models.UserManagement.Schema;
using TrungTamTinHoc.Controllers;
using TTTH.Common;
using TTTH.Common.Enums;
using TTTH.Validate;

namespace TrungTamTinHoc.Areas.ControlPanel.Controllers
{
    /// <summary>
    /// Class chứa các điều hướng liên quan đến quản lý danh sách User
    /// Author       :   TramHTD -05/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class UserManagementController : BaseAdminController
    {
        /// <summary>
        /// Điều hướng đến trang hiển thị danh sách User nếu là request thông thường.
        /// Trả về table chứa danh sách User nếu là Ajax.
        /// Điều hướng về trang lỗi nếu có lỗi xảy ra.
        /// Author       :   TraHTD - 20/07/2018 - create
        /// </summary>
        /// <param name="condition">Đối tượng chứa điều kiện tìm kiếm, tạo thành từ query string</param>
        /// <returns>
        /// Trang danh sách User.
        /// Partial view chứa table danh sách User.
        /// </returns>
        /// <remarks>
        /// Method: GET
        /// RouterName: ListOfUser
        /// </remarks>
        public ActionResult ListOfUser(UserConditionSearch condition)
        {
            try
            {
                if (Request.IsAjaxRequest())
                {
                    return View("Partial/_tableOfUser", new ListOfUserModel().GetListOfUser(condition));
                }
                return View(new ListOfUserModel().GetListOfUser(condition));
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }
        /// <summary>
        /// Thay đổi mật khẩu theo id account được gửi lên.
        /// Author       :   TramHTD - 06/08/2018 - create
        /// </summary>
        /// <param name="id">id account sẽ thay đổi mật khẩu</param>
        /// <returns>Đối tượng chứa thông tin về quá trình thay đổi mật khẩu</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: ResetPasswordAccount
        /// </remarks>
        public JsonResult ResetPasswordAccount(int id)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                bool reset = new ListOfUserModel().ResetPasswordAccount(id);
                if (!reset)
                {
                    response.Code = 201;
                    response.MsgNo = 50;
                }
            }
            catch (Exception e)
            {
                response.Code = (int)ConstantsEnum.CodeResponse.ServerError;
                response.MsgNo = (int)MessageEnum.MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Điều hướng đến trang view nhập thông tin để thêm tài khoản.
        /// Author       :   TramHTD - 10/08/2018 - create
        /// </summary>
        /// <returns>Trả về trang view thêm tài khoản</returns>
        /// <remarks>
        /// Method: GET
        /// RouterName: ViewCreateUser
        /// </remarks>

        public ActionResult ViewCreateUser()
        {
            try
            {
                ViewBag.GroupList = new UserMasterModel().GetListGroups();
                return View("UserMaster");
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }

        /// <summary>
        /// Điều hướng đến trang view nhập thông tin để sửa tài khoản.
        /// Author       :   TramHTD - 10/08/2018 - create
        /// </summary>
        /// <param name="id">Đối tượng chứa thông tin về quá trình chỉnh sửa thông tin</param>
        /// <returns>Trả về trang view sửa tài khoản</returns>
        /// <remarks>
        /// Method: GET
        /// RouterName: ViewEditUser
        /// </remarks>
        public ActionResult ViewEditUser(string id)
        {
            try
            {
                UserMaster userMaster = new UserMasterModel().LoadUser(id);
                if (userMaster.Mode == (int)ConstantsEnum.ModeMaster.Insert)
                {
                    return RedirectToAction("ViewCreateUser");
                }
                ViewBag.GroupList = new UserMasterModel().GetListGroups();
                return View("UserMaster", userMaster);
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }
        /// <summary>
        /// Tạo tài khoản hoặc cập nhật tài khoản theo thông tin người dùng đưa lên.
        /// Author       :   TramHTD - 10/08/2018 - create
        /// </summary>
        /// <param name="data">Thông tin tài khoản đăng ký mà người dùng nhập vào</param>
        /// <returns>Chuỗi Json chứa kết quả của việc tạo tài khoản</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: SaveUser
        /// </remarks>
        public JsonResult SaveUser(Account data)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                if (ModelState.IsValid)
                {
                    response = new UserMasterModel().SaveUser(data);
                }
                else
                {
                    response.Code = (int)ConstantsEnum.CodeResponse.NotValidate;
                    response.ListError = ModelState.GetModelErrors();
                }
            }
            catch (Exception e)
            {
                response.Code = (int)ConstantsEnum.CodeResponse.ServerError;
                response.MsgNo = (int)MessageEnum.MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// Kiểm tra email hoặc username đã tồn tại hay chưa.
        /// Author       :   TramHTD - 10/08/2018 - create
        /// </summary>
        /// <param name="value">giá trị của email hoặc username cần kiểm tra</param>
        /// <param name="type">type = 1: kiểm tra usernme; type = 2: kiểm tra email</param>
        /// <returns>Nếu có tồn tại trả về true, ngược lại trả về false</returns>
        /// <remarks>
        /// Method: GET
        /// RouterName: homeCreateAccount
        /// </remarks>
        public bool CheckExistAccount(string value, string type)
        {
            try
            {
                return new UserMasterModel().CheckExistAccount(value, type);
            }
            catch
            {
                return false;
            }
        }
    }
}