﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TTTH.Common;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.EmailSubscriptionManagement.Schema
{
    /// <summary>
    /// Class dùng để lấy danh sách đăng ký theo dõi qua email trả về cho trang danh sách đăng ký theo dõi qua email
    /// Author       :   HaLTH - 19/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ListOfEmailSubscription
    {
        public List<EmailSubscription> EmailSubscriptionList { set; get; }
        public Paging Paging { set; get; }
        public EmailSubscriptionConditionSearch Condition { set; get; }
        public ListOfEmailSubscription()
        {
            this.EmailSubscriptionList = new List<EmailSubscription>();
            this.Condition = new EmailSubscriptionConditionSearch();
            this.Paging = new Paging();
        }
    }
}