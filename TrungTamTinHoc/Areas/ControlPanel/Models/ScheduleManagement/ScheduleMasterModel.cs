﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TrungTamTinHoc.Areas.ControlPanel.Models.ScheduleManagement.Schema;
using TTTH.Common;
using TTTH.DataBase;
using Z.EntityFramework.Plus;
using static TTTH.Common.Enums.ConstantsEnum;
using TblSchedule = TTTH.DataBase.Schema.LichHoc;
using TblPhongHoc = TTTH.DataBase.Schema.PhongHoc;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.ScheduleManagement
{
    public class ScheduleMasterModel
    {
        DataContext context;
        public ScheduleMasterModel()
        {
            context = new DataContext();
        }

        /// <summary>
        /// Dùng để load thông tin của ErrorMsg
        /// Author       :   HoangNM - 13/08/2018 - create
        /// </summary>
        /// <param name="id">id của errorMgs cần lấy thông tin</param>
        public ScheduleMaster LoadSchedule(string id)
        {
            try
            {
                ScheduleMaster scheduleMaster = new ScheduleMaster();
                int idSchedule = 0;
                try
                {
                    idSchedule = Convert.ToInt32(id);
                }
                catch { }
                TblSchedule schedule = context.LichHoc.FirstOrDefault(x => x.Id == idSchedule && !x.DelFlag );
                if (schedule != null)
                {
                    scheduleMaster.Mode = (int)ModeMaster.Update;
                    scheduleMaster.Schedule = new Schedule();
                    scheduleMaster.Schedule.Id = schedule.Id;
                    scheduleMaster.Schedule.KhoaHoc = schedule.LopHoc.KhoaHoc.KhoaHocTrans.FirstOrDefault(y=>!y.DelFlag && y.Lang == Common.defaultLang).TenKhoaHoc;
                    scheduleMaster.Schedule.LopHoc = schedule.LopHoc.LopHocTrans.FirstOrDefault(y=>!y.DelFlag && y.Lang == Common.defaultLang).TenLop;
                    scheduleMaster.Schedule.PhongHoc = schedule.PhongHoc.PhongHocTrans.FirstOrDefault(y=>!y.DelFlag && y.Lang == Common.defaultLang).TenPhong;
                    scheduleMaster.Schedule.NgayHoc = schedule.NgayHoc;
                    scheduleMaster.Schedule.GioBatDau = schedule.GioBatDau;
                    scheduleMaster.Schedule.GioKetThuc = schedule.GioKetThuc;
                    scheduleMaster.Schedule.idPhongHoc = schedule.IdPhongHoc;
                }
                return scheduleMaster;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// Dùng để load thông tin của ErrorMsg
        /// Author       :   HoangNM - 13/08/2018 - create
        /// </summary>
        /// <param name="id">id của errorMgs cần lấy thông tin</param>
        /// 
        public KhoaHoc loadKhoaHoc()
        {
            KhoaHoc khoaHoc = new KhoaHoc();
            khoaHoc.cacKhoaHoc = context.KhoaHocTrans.Where(x => x.Lang == Common.defaultLang && !x.DelFlag).Select(x => new KhoaHocHienThi()
            {
                IDKhoaHoc = x.Id,
                TenKhoaHoc = x.TenKhoaHoc
            }).ToList();
            khoaHoc.cacLopHoc = context.LopHoc.Include("LopHocTrans").Where(x => !x.DelFlag && x.IdKhoaHoc == khoaHoc.idKhoaHoc && x.LopHocTrans.Count > 0)
                    .Select(x => new LopHocHienThi()
                    {
                        IDLopHoc = x.Id,
                        TenLopHoc = x.LopHocTrans.FirstOrDefault(y => y.Lang == Common.defaultLang).TenLop
                    }).ToList();
            return khoaHoc;
        }

        /// <summary>
        /// Dùng để thêm hoăc sửa thông tin của lịch học
        /// Author       :   HoangNM - 06/10/2018 - create
        /// </summary>
        /// <param name="schedule">thông tin của lịch học gửi lên để thêm hoặc sửa</param>
        public ResponseInfo SaveSchedule(Schedule schedule)
        {
            try
            {
                if (context.LichHoc.FirstOrDefault(x => x.Id == schedule.Id && !x.DelFlag) != null)
                {
                    return UpadateSchedule(schedule);
                }
                return AddSchedule(schedule);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// Dùng để thêm hoăc sửa thông tin của ErrorMsg
        /// Author       :   HoangNM - 13/08/2018 - create
        /// </summary>
        /// <param name="errorMsg">thông tin của errorMsg gửi lên để thêm hoặc sửa</param>
        public ResponseInfo UpadateSchedule(Schedule schedule)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo response = new ResponseInfo();
                bool update = true;

                if (update)
                {

                    context.LichHoc.Where(x => x.Id == schedule.Id && !x.DelFlag)
                        .Update(x => new TTTH.DataBase.Schema.LichHoc
                        {
                            NgayHoc=schedule.NgayHoc,
                            GioKetThuc=schedule.GioKetThuc,
                            GioBatDau=schedule.GioBatDau
                        });
                    
                    context.SaveChanges();
                }
                transaction.Commit();
                return response;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }
        /// <summary>
        /// Dùng để thêm thông tin của lịch học
        /// Author       :   HoangNM - 06/10/2018 - create
        /// </summary>
        /// <param name="schedule">thông tin của lịch học gửi lên để thêm hoặc sửa</param>
        public ResponseInfo AddSchedule(Schedule schedule)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo response = new ResponseInfo();

                schedule.Id = context.LichHoc.Count() == 0 ? 1 : context.LichHoc.Max(x => x.Id) + 1;
                
                context.LichHoc.Add(new TblSchedule
                {
                    Id = schedule.Id,
                    NgayHoc = schedule.NgayHoc,
                    GioBatDau = schedule.GioBatDau,
                    GioKetThuc = schedule.GioKetThuc,
                    IdLopHoc=schedule.idLopHoc,
                    IdPhongHoc=schedule.idPhongHoc
                });

                
                context.SaveChanges();
                response.ThongTinBoSung1 = schedule.Id + "";


                transaction.Commit();
                return response;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }
    }
}