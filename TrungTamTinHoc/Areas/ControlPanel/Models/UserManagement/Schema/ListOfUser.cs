﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TTTH.Common;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.UserManagement.Schema
{
    /// <summary>
    /// Class dùng để lấy danh sách User trả về cho trang danh sách User
    /// Author       :   TramHTD - 03/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ListOfUser
    {
        public List<User> UserList { set; get; }
        public List<Group> Group { set; get; }
        public Paging Paging { set; get; }
        public UserConditionSearch Condition { set; get; }

        public ListOfUser()
        {
            this.UserList = new List<User>();
            this.Group = new List<Group>();
            this.Condition = new UserConditionSearch();
            this.Paging = new Paging();
        }
    }
}