﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrungTamTinHoc.Areas.ControlPanel.Models.ContactManagement;
using TrungTamTinHoc.Areas.ControlPanel.Models.ContactManagement.Schema;
using TrungTamTinHoc.Controllers;
using TTTH.Common;
using static TTTH.Common.Enums.ConstantsEnum;
using static TTTH.Common.Enums.MessageEnum;

namespace TrungTamTinHoc.Areas.ControlPanel.Controllers
{
    /// <summary>
    /// Class chứa các điều hướng liên quan đến quản lý danh sách liên hệ
    /// Author       :   HoàngNM - 10/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ContactManagementController : BaseAdminController
    {
        /// <summary>
        /// Điều hướng đến trang hiển thị danh sách liên hệ nếu là request thông thường.
        /// Trả về table chứa danh sách liên hệ nếu là Ajax.
        /// Điều hướng về trang lỗi nếu có lỗi sảy ra.
        /// Author       :   HoangNM - 10/07/2018 - create
        /// </summary>
        /// <param name="condition">Đối tượng chứa điều kiện tìm kiếm, tạo thành từ query string</param>
        /// <returns>
        /// Trang danh sách liên hệ.
        /// Partial view chứa table danh sách liên hệ.
        /// </returns>
        /// <remarks>
        /// Method: GET
        /// RouterName: ListOfContact
        /// </remarks>
        public ActionResult ListOfContact(ContactConditionSearch condition)
        {
            try
            {
                ViewBag.lienHeMoi = new ListOfContactModel().getLienHeMoi();
                if (Request.IsAjaxRequest())
                {
                    return View("Partial/_tableOfContact", new ListOfContactModel().GetListOfContact(condition));
                }
                return View(new ListOfContactModel().GetListOfContact(condition));
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }

        /// <summary>
        /// Update trạng thái  trên trang chủ của 1 liên hệ.
        /// Author       :   HoangNM - 23/07/2018 - create
        /// </summary>
        /// <param name="id">id của contact sẽ update</param>
        /// <param name="idTrangThai">id trạng thái của contact sẽ update</param>
        /// <returns>Đối tượng chứa thông tin về quá trình update</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: UpdateShowContact
        /// </remarks>
        public JsonResult UpdateShow(int id ,int idTrangThai)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                new ListOfContactModel().UpdateShow(id,idTrangThai);
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            response.ThongTinBoSung2 = new ListOfContactModel().getLienHeMoi().ToString();
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Xóa các contact theo danh sách id contact được gửi lên.
        /// Author       :   HoangNM - 23/07/2018 - create
        /// </summary>
        /// <param name="ids">Danh sách id sẽ xóa</param>
        /// <returns>Đối tượng chứa thông tin về quá tring xóa contact</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: DeleteContacts
        /// </remarks>
        public JsonResult DeleteContacts(List<int> ids)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                new ListOfContactModel().DeleteContacts(ids);
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}