﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TTTH.Common;
using static TTTH.Common.Enums.ConstantsEnum;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.GroupManagement.Schema
{
    public class GroupMaster
    {
        public Group GroupInfo { set; get; }
        public Group GroupTrans { set; get; }
        public List<Lang> Language { set; get; }
        public int Mode { set; get; }
        public GroupMaster()
        {
            Mode = (int)ModeMaster.Insert;
            GroupTrans = null;
            GroupInfo = null;
            Language = new List<Lang>();
        }
    }
}