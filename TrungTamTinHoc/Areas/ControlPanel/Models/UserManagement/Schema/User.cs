﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.UserManagement.Schema
{
    /// <summary>
    /// Class dùng để chứa thông tin của một User
    /// Author       :   TramHTD - 03/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class User
    {
        public int Id { set; get; }

        [Required]
        [StringLength(50)]
        public string Ho { get; set; }

        [Required]
        [StringLength(50)]
        public string Ten { get; set; }

        [Required]
        [StringLength(255)]
        public string Avatar { get; set; }

        public bool GioiTinh { get; set; }

        public DateTime? NgaySinh { get; set; }

        [StringLength(15)]
        public string SoDienThoai { get; set; }

        [StringLength(50)]
        public string Username { get; set; }

        [StringLength(50)]
        public string Password { get; set; }

        [StringLength(255)]
        public string Email { get; set; }

        public List<Group> GroupList { set; get; }
    }
    /// <summary>
    /// Class dùng để chứa thông tin tìm kiếm của danh sách User
    /// Author       :   TramHTD - 03/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class UserConditionSearch
    {
        public string Ho { set; get; }
        public string Ten { set; get; }
        public string Email { set; get; }
        public string SoDienThoai { set; get; }
        public string UserName { set; get; }
        public int IdGroup { set; get; }
        public int CurrentPage { set; get; }
        public int PageSize { set; get; }
        public UserConditionSearch()
        {
            this.Ho = "";
            this.Ten = "";
            this.Email = "";
            this.SoDienThoai = "";
            this.UserName = "";
            this.IdGroup = 0;
            this.PageSize = 10;
            this.CurrentPage = 1;
        }
    }

    /// <summary>
    /// Class dùng để chứa thông tin của Group
    /// Author       :   TramHTD - 03/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class Group
    {
        public int IdGroup { set; get; }
        public string TenGroup { set; get; }
    }
}