﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TrungTamTinHoc.Areas.ControlPanel.Models.ThingsAchievedManagement.Schema;
using TTTH.Common;
using TTTH.DataBase;
using Z.EntityFramework.Plus;
using static TTTH.Common.Enums.ConstantsEnum;
using static TTTH.Common.Enums.MessageEnum;
using TblThingsAchieved = TTTH.DataBase.Schema.NhungDieuDatDuoc;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.ThingsAchievedManagement
{
    /// <summary>
    /// Class chứa các phương thức liên quan đến việc xử lý Những điều đạt được.
    /// Author       :   HaLTH - 26/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ThingsAchievedMasterModel
    {
        DataContext context;
        List<string> typeFiles;
        public ThingsAchievedMasterModel()
        {
            context = new DataContext();
            typeFiles = new List<string>();
            typeFiles.Add(".jpg");
            typeFiles.Add(".png");
        }

        /// <summary>
        /// Lấy thông tin của 1 điều đạt được.
        /// Author       :   HaLTH - 26/07/2018 - create
        /// </summary>
        /// <param name="id">id của điều đạt được sẽ được hiển thị</param>
        /// <returns>Thông tin của điều đạt được</returns>
        public ThingsAchievedMaster LoadThingsAchieved(int id)
        {
            try
            {
                ThingsAchievedMaster thingsAchievedMaster = new ThingsAchievedMaster();
                int idThingsAchieved = 0;
                try
                {
                    idThingsAchieved = Convert.ToInt32(id);
                }
                catch { }
                TblThingsAchieved thingsachieved = context.NhungDieuDatDuoc.FirstOrDefault(x => x.Id == idThingsAchieved && !x.DelFlag && x.Lang == Common.defaultLang);
                if (thingsachieved != null)
                {
                    thingsAchievedMaster.Mode = (int)ModeMaster.Update;
                    thingsAchievedMaster.ThingsAchievedInfo = new ThingsAchieved();
                    thingsAchievedMaster.ThingsAchievedInfo.Id = thingsachieved.Id;
                    thingsAchievedMaster.ThingsAchievedInfo.Title = thingsachieved.Title;
                    thingsAchievedMaster.ThingsAchievedInfo.Content = thingsachieved.Content;
                    thingsAchievedMaster.ThingsAchievedInfo.Icon = thingsachieved.Icon;
                    thingsAchievedMaster.ThingsAchievedInfo.IsShow = thingsachieved.IsShow;
                    thingsAchievedMaster.Language = context.Language.Where(x => !x.DelFlag)
                        .Select(x => new Lang
                        {
                            Id = x.Id,
                            Name = x.Name
                        }).OrderBy(x => x.Id).ToList();
                    if (thingsAchievedMaster.Language.Count > 0)
                    {
                        string lang = thingsAchievedMaster.Language[0].Id;
                        thingsachieved = context.NhungDieuDatDuoc.FirstOrDefault(x => x.Id == idThingsAchieved && !x.DelFlag && x.Lang == lang);
                        thingsAchievedMaster.ThingsAchievedTrans = new ThingsAchieved();
                        thingsAchievedMaster.ThingsAchievedTrans.Id = thingsachieved.Id;
                        thingsAchievedMaster.ThingsAchievedTrans.Title = thingsachieved.Title;
                        thingsAchievedMaster.ThingsAchievedTrans.Content = thingsachieved.Content;
                        thingsAchievedMaster.ThingsAchievedTrans.Icon = thingsachieved.Icon;
                        thingsAchievedMaster.ThingsAchievedTrans.IsShow = thingsachieved.IsShow;
                    }
                }
                return thingsAchievedMaster;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Tạo hoặc cập nhật lại điều đạt được từ thông tin mà người dùng gửi lên
        /// Author       :   HaLTH - 26/07/2018 - create
        /// </summary>
        /// <param name="course">Các thông tin của điều đạt được muốn tạo hoặc cập nhật</param>
        /// <returns>Thông tin về việc tạo hoặc cập nhật điều đạt được thành công hay thất bại</returns>
        public ResponseInfo SaveThingsAchieved(ThingsAchieved thingsachieved)
        {
            try
            {
                if (context.NhungDieuDatDuoc.FirstOrDefault(x => x.Id == thingsachieved.Id && !x.DelFlag) != null)
                {
                    return UpadateThingsAchieved(thingsachieved);
                }
                return AddThingsAchieved(thingsachieved);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Cập nhật lại điều đạt được từ thông tin mà người dùng gửi lên
        /// Author       :   HaLTH - 26/07/2018 - create
        /// </summary>
        /// <param name="course">Các thông tin của điều đạt được muốn cập nhật</param>
        /// <returns>Thông tin về việc cập nhật điều đạt được thành công hay thất bại</returns>
        public ResponseInfo UpadateThingsAchieved(ThingsAchieved thingsachieved)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo response = new ResponseInfo();
                bool update = true;
                List<string> imgsDelete = new List<string>();
                if (thingsachieved.FileImg != null)
                {
                    string icon = Common.SaveFileUpload(thingsachieved.FileImg, "/public/img/testimonial/", "", typeFiles);
                    if (icon == "1")
                    {
                        response.Code = 202;
                        response.MsgNo = (int)MsgNO.FileKhongDungDinhDang;
                        update = false;
                    }
                    else if (icon == "2")
                    {
                        response.Code = 202;
                        response.MsgNo = (int)MsgNO.DungLuongFileQuaLon;
                        update = false;
                    }
                    else if (icon == "")
                    {
                        response.Code = 202;
                        response.MsgNo = (int)MsgNO.TaiFileBiLoi;
                        update = false;
                    }
                    else
                    {
                        imgsDelete.Add(context.NhungDieuDatDuoc.FirstOrDefault(x => x.Id == thingsachieved.Id && !x.DelFlag).Icon);
                        thingsachieved.Icon = icon;
                    }
                }
                if (update)
                {
                    thingsachieved.IsShow = thingsachieved.CheckHienThi == "on" ? true : false;

                    context.NhungDieuDatDuoc.Where(x => x.Id == thingsachieved.Id && !x.DelFlag)
                        .Update(x => new TTTH.DataBase.Schema.NhungDieuDatDuoc
                        {
                            IsShow = thingsachieved.IsShow,
                            Icon = thingsachieved.Icon
                        });

                    context.NhungDieuDatDuoc.Where(x => x.Id == thingsachieved.Id && x.Lang == Common.defaultLang && !x.DelFlag)
                        .Update(x => new TTTH.DataBase.Schema.NhungDieuDatDuoc
                        {
                            Title = thingsachieved.Title,
                            Content = thingsachieved.Content
                        });
                    context.SaveChanges();
                }
                transaction.Commit();
                Common.DeleteFile(imgsDelete);
                return response;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        /// <summary>
        /// Tạo điều đạt được từ thông tin mà người dùng gửi lên
        /// Author       :   HaLTH - 26/07/2018 - create
        /// </summary>
        /// <param name="course">Các thông tin của điều đạt được muốn tạo</param>
        /// <returns>Thông tin về việc tạo điều đạt được thành công hay thất bại</returns>
        public ResponseInfo AddThingsAchieved (ThingsAchieved thingsachieved)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo response = new ResponseInfo();
                if (thingsachieved.FileImg == null)
                {
                    response.Code = 202;
                    response.MsgNo = (int)MsgNO.ChuaChonFile;
                }
                else
                {
                    thingsachieved.Icon = Common.SaveFileUpload(thingsachieved.FileImg, "/public/img/testimonial/", "", typeFiles);
                    if (thingsachieved.Icon == "1")
                    {
                        response.Code = 202;
                        response.MsgNo = (int)MsgNO.FileKhongDungDinhDang;
                    }
                    else if (thingsachieved.Icon == "2")
                    {
                        response.Code = 202;
                        response.MsgNo = (int)MsgNO.DungLuongFileQuaLon;
                    }
                    else if (thingsachieved.Icon == "")
                    {
                        response.Code = 202;
                        response.MsgNo = (int)MsgNO.TaiFileBiLoi;
                    }
                    else
                    {
                        List<Lang> listOfLang = context.Language.Where(x => !x.DelFlag).Select(x => new Lang
                        {
                            Id = x.Id,
                            Name = x.Name
                        }).ToList();
                        thingsachieved.IsShow = thingsachieved.CheckHienThi == "on" ? true : false;
                        thingsachieved.Id = context.NhungDieuDatDuoc.Count() == 0 ? 1 : context.NhungDieuDatDuoc.Max(x => x.Id) + 1;
                        foreach (Lang lang in listOfLang)
                        {
                            context.NhungDieuDatDuoc.Add(new TblThingsAchieved
                            {
                                Id = thingsachieved.Id,
                                Lang = lang.Id,
                                Icon = thingsachieved.Icon,
                                Title = thingsachieved.Title,
                                Content = thingsachieved.Content,
                                IsShow = thingsachieved.IsShow
                            });
                        }
                        context.SaveChanges();
                        response.ThongTinBoSung1 = thingsachieved.Id + "";
                    }
                }
                transaction.Commit();
                return response;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        /// <summary>
        /// Cập nhật lại điều đạt được từ thông tin mà người dùng gửi lên bằng ngôn ngữ khác
        /// Author       :   HaLTH - 26/07/2018 - create
        /// </summary>
        /// <param name="course">Các thông tin ngôn ngữ khác của điều đạt được muốn cập nhật</param>
        /// <returns>Thông tin về việc cập nhật điều đạt được thành công hay thất bại</returns>
        public ResponseInfo UpdateThingsAchievedTranslate(ThingsAchieved_Trans thingsachieved)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo response = new ResponseInfo();
                context.NhungDieuDatDuoc.Where(x => x.Id == thingsachieved.Id && x.Lang == thingsachieved.LanguageTrans && !x.DelFlag)
                    .Update(x => new TTTH.DataBase.Schema.NhungDieuDatDuoc
                    {
                        Title = thingsachieved.Title,
                        Content = thingsachieved.Content
                    });
                context.SaveChanges();
                response.ThongTinBoSung1 = thingsachieved.Id + "";
                transaction.Commit();
                return response;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        /// <summary>
        /// Thay đổi lại ngôn ngữ
        /// Author       :   HaLTH - 26/07/2018 - create
        /// </summary>
        /// <param name="id">id của điều đat được</param>
        /// <param name="lang">Ngôn ngữ được chọn</param>
        /// <returns>Điều đat được đã chuyển ngôn ngữ</returns>
        /// <remarks>
        /// </remarks>
        public ThingsAchieved_Trans ReferThingsAchievedWithLang(int id, string lang)
        {
            try
            {
                return context.NhungDieuDatDuoc.Where(x => x.Id == id && x.Lang == lang && !x.DelFlag)
                    .Select(x => new ThingsAchieved_Trans
                    {
                        Title = x.Title,
                        Content = x.Content
                    }).FirstOrDefault();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}