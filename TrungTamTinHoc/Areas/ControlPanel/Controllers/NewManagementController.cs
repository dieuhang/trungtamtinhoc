﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrungTamTinHoc.Areas.ControlPanel.Models.NewsManagement;
using TrungTamTinHoc.Areas.ControlPanel.Models.NewsManagement.Schema;
using TrungTamTinHoc.Controllers;
using TTTH.Common;
using TTTH.Validate;
using static TTTH.Common.Enums.ConstantsEnum;
using static TTTH.Common.Enums.MessageEnum;

namespace TrungTamTinHoc.Areas.ControlPanel.Controllers
{
    public class NewManagementController : BaseAdminController
    {
        /// <summary>
        /// Điều hướng đến trang hiển thị danh sách new nếu là request thông thường.
        /// Trả về table chứa danh sách News nếu là Ajax.
        /// Điều hướng về trang lỗi nếu có lỗi sảy ra.
        /// Author       :   HangNTD - 20/07/2018 - create
        /// </summary>
        /// <param name="condition">Đối tượng chứa điều kiện tìm kiếm, tạo thành từ query string</param>
        /// <returns>
        /// Trang danh sách new.
        /// Partial view chứa table danh sách new.
        /// </returns>
        /// <remarks>
        /// Method: GET
        /// RouterName: ListOfNews
        /// </remarks>
        public ActionResult ListOfNew(NewsConditionSearch condition)

        {
            try
            {
                if (Request.IsAjaxRequest())
                {
                    return View("Partial/_tableOfNew", new ListOfNewsModel().GetListOfNews(condition));
                }
                return View(new ListOfNewsModel().GetListOfNews(condition));
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }

        /// <summary>
        /// Xóa các New theo danh sách id tintuc được gửi lên.
        /// Author       :   HangNTD - 25/07/2018 - create
        /// </summary>
        /// <param name="ids">Danh sách id các New sẽ xóa</param>
        /// <returns>Đối tượng chứa thông tin về quá trình xóa New</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: DeleteNews
        /// </remarks>
        public JsonResult DeleteNews(List<int> ids)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                bool deleted = new ListOfNewsModel().DeleteNews(ids);
                if (!deleted)
                {
                    response.Code = 201;
                    response.MsgNo = 49;
                }
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Update trạng thái hiển thị trên trang chủ của 1 tin tức.
        /// Author       :   HangNTD - 25/07/2018 - create
        /// </summary>
        /// <param name="id">id của tin tức sẽ update</param>
        /// <returns>Đối tượng chứa thông tin về quá trình update</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: UpdateShowNew
        /// </remarks>
        public JsonResult UpdateShow(int id)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                bool updated = new ListOfNewsModel().UpdateShow(id);
                if (!updated)
                {
                    response.Code = 201;
                    response.MsgNo = 45;
                }
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Điều hướng đến trang view nhập thông tin để thêm news.
        /// Author       :   HangNTD - 25/07/2018 - create
        /// </summary>
        /// <returns>Trả về trang view thêm news</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: ViewCreateNews
        /// </remarks>
        public ActionResult ViewCreateNews()
        {
            try
            {
                return View("NewsMaster");
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }
        /// <summary>
        /// Điều hướng đến trang view sửa news
        /// Author       :   HangNTD - 25/07/2018 - create
        /// </summary>
        /// <param name="id">Đối tượng chứa thông tin về quá trình chỉnh sửa thông tin</param>
        /// <returns>Trả về trang view sửa news</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: ViewEditNews
        /// </remarks>
        public ActionResult ViewEditNews(string id)
        {
            try
            {
                NewsMaster newsMaster = new NewsMasterModel().LoadNews(id);
                if (newsMaster.Mode == (int)ModeMaster.Insert)
                {
                    return RedirectToAction("ViewCreateNews");
                }
                return View("NewsMaster", newsMaster);
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }
        /// <summary>
        /// Tạo news hoặc cập nhật news theo thông tin người dùng đưa lên.
        /// Author       :   HangNTD - 25/07/2018 - create
        /// </summary>
        /// <param name="data">Thông tin news mà người dùng nhập vào</param>
        /// <returns>Chuỗi Json chứa kết quả của việc tạo hoặc cập nhật news</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: SaveNews
        /// </remarks>
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult SaveNews(News data)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                if (ModelState.IsValid)
                {
                    response = new NewsMasterModel().SaveNews(data);
                }
                else
                {
                    response.Code = (int)CodeResponse.NotValidate;
                    response.ListError = ModelState.GetModelErrors();
                }
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Lưu thông tin news được gửi lên bằng ngôn ngữ khác ngôn ngữ mặc định
        /// Author       :   HangNTD - 25/07/2018 - create
        /// </summary>
        /// <param name="data">Dữ liệu của đối tượng muốn lưu</param>
        /// <returns></returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: SaveNews
        /// </remarks>
        [ValidateInput(false)]
        public JsonResult UpdateNewsTranslate(News_Trans data)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                if (ModelState.IsValid)
                {
                    response = new NewsMasterModel().UpdateNewsTranslate(data);
                }
                else
                {
                    response.Code = (int)CodeResponse.NotValidate;
                    response.ListError = ModelState.GetModelErrors();
                }
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Thay đổi ngôn ngữ
        /// Author       :   HangNTD- 25/07/2018 - create
        /// </summary>
        /// <param name="id">Đối tượng muốn thay đổi ngôn ngữ</param>
        /// <param name="lang">Ngôn ngữ muốn thay đổi</param>
        /// <returns></returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: SaveNews
        /// </remarks>
        public JsonResult ReferNewsWithLang(int id, string lang)
        {
            try
            {
                return Json(new NewsMasterModel().ReferNewsWithLang(id, lang), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        /// <summary>
        /// Kiểm tra url của news đã tồn tại chưa
        /// Author       :   HangNTD - 25/07/2018 - create
        /// </summary>
        /// <param name="value">url của news</param>
        /// <returns>true: nếu đã tồn tại, false nếu chưa tồn tại</returns>
        /// <remarks>
        /// </remarks>
        public bool CheckExistUrlNews(string value)
        {
            try
            {
                return new NewsMasterModel().CheckExistUrlNews(value);
            }
            catch
            {
                return false;
            }
        }
    }
}