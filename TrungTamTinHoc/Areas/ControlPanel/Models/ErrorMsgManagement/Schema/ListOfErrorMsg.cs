﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TTTH.Common;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.ErrorMsgManagement.Schema
{
    /// <summary>
    /// Class dùng để hiển thị thông tin của của ErrorMgs
    /// Author       :   HoangNM - 03/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ListOfErrorMsg
    {
        public List<ErrorMsg> ErrorMsgList { set; get; }
        public Paging Paging { set; get; }
        public ContactConditionSearch Condition { set; get; }
        public ListOfErrorMsg()
        {
            this.ErrorMsgList = new List<ErrorMsg>();
            this.Condition = new ContactConditionSearch();
            this.Paging = new Paging();
            
        }
    }
}