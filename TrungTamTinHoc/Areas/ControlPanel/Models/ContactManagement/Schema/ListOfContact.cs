﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TTTH.Common;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.ContactManagement.Schema
{
    /// <summary>
    /// Class dùng để chứa thông tin của danh sách liên hệ,và dùng để phân trang t
    /// Author       :   HoangNM - 23/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ListOfContact
    {
        public List<Contact> ContactList { set; get; }
        public Paging Paging { set; get; }
        public ContactConditionSearch Condition { set; get; }
        public List<TrangThai> tenTrangThai { set; get; }
        public ListOfContact()
        {
            this.ContactList = new List<Contact>();
            this.Condition = new ContactConditionSearch();
            this.Paging = new Paging();
        }
    }
}