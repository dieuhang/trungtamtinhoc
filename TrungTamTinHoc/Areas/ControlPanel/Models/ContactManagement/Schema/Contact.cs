﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.ContactManagement.Schema
{
    public class Contact
    {
        public int Id { set; get; }
        [Required]
        [StringLength(50)]
        public string HoTen { get; set; }

        [StringLength(15)]
        public string Sdt { get; set; }

        [Required]
        [StringLength(255)]
        public string Email { get; set; }

        [Required]
        [StringLength(1000)]
        public string NoiDung { get; set; }

        public int idTrangThai { get; set; }

     

    }

    public class ContactConditionSearch
    {
        public string SearchName { set; get; }
        public int TrangThai { set; get; }
        public int CurrentPage { set; get; }
        public int PageSize { set; get; }
        public DateTime? tuNgay { set; get; }
        public DateTime? toiNgay { set; get; }
        public string SearchSdt { get; set; }
        public string SearchEmail { get; set; }
        public ContactConditionSearch()
        {
            this.SearchName = "";
            this.SearchSdt = "";
            this.SearchEmail = "";
            this.TrangThai = 0;
            this.PageSize = 10;
            this.CurrentPage = 1;
        }
    }

    public class TrangThai
    {
        public int Id { get; set; }
        public string tenTrangThai { get; set; }
    }
}