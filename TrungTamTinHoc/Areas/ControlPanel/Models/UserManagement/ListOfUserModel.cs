﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using EntityFramework.Extensions;
using TrungTamTinHoc.Areas.ControlPanel.Models.UserManagement.Schema;
using TTTH.Common;
using TTTH.Common.Enums;
using TTTH.DataBase;
using TTTH.DataBase.Schema;
using Group = TrungTamTinHoc.Areas.ControlPanel.Models.UserManagement.Schema.Group;
using User = TrungTamTinHoc.Areas.ControlPanel.Models.UserManagement.Schema.User;
using TblAccount = TTTH.DataBase.Schema.Account;
using TblBieuMau = TTTH.DataBase.Schema.BieuMau;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.UserManagement
{
    /// <summary>
    /// Class dùng để xử lý các hoạt động liên quan đến User 
    /// Author       :   TramHTD - 03/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ListOfUserModel
    {
        DataContext context;
        public ListOfUserModel()
        {
            context = new DataContext();
        }

        /// <summary>
        /// Tìm kiếm các User theo điều kiện cho trước.
        /// Author       :   TramHTD - 03/08/2018 - create
        /// </summary>
        /// <param name="condition">Đối tượng chứa điều kiện tìm kiếm</param>
        /// <returns>Danh sách các User đã tìm kiếm được. Exception nếu có lỗi</returns>
        public ListOfUser GetListOfUser(UserConditionSearch condition)
        {
            try
            {
                // Nếu không tồn tại điều kiện tìm kiếm thì khởi tạo giá trị tìm kiếm ban đầu
                if (condition == null)
                {
                    condition = new UserConditionSearch();
                }
                string lang = Common.GetLang();
                ListOfUser listOfUser = new ListOfUser();

                // Lấy các thông tin dùng để phân trang
                listOfUser.Paging = new Paging(context.Account.Count(x =>
                    (condition.Ho == null ||
                    (condition.Ho != null && (x.User.Ho.Contains(condition.Ho))))
                    && (condition.Ten == null ||
                    (condition.Ten != null && (x.User.Ten.Contains(condition.Ten))))
                    && (condition.Email == null ||
                    (condition.Email != null && (x.Email.Contains(condition.Email))))
                    && (condition.UserName == null ||
                    (condition.UserName != null && (x.Username.Contains(condition.UserName))))
                    && (condition.SoDienThoai == null ||
                    (condition.SoDienThoai != null && (x.User.SoDienThoai.Contains(condition.SoDienThoai))))
                    && (condition.IdGroup ==0 ||
                    (condition.IdGroup != 0 && (x.GroupOfAccount.Count(y=>y.IdGroup==condition.IdGroup)>0)))
                    && !x.DelFlag && (x.GroupOfAccount.Count(y => y.IdGroup == 1) <= 0)), condition.CurrentPage, condition.PageSize);

                // Tìm kiếm và lấy dữ liệu theo trang
                listOfUser.UserList = context.Account.Where(x =>
                    (condition.Ho == null ||
                    (condition.Ho != null && (x.User.Ho.Contains(condition.Ho))))
                    && (condition.Ten == null ||
                    (condition.Ten != null && (x.User.Ten.Contains(condition.Ten))))
                    && (condition.Email == null ||
                    (condition.Email != null && (x.Email.Contains(condition.Email))))
                    && (condition.UserName == null ||
                    (condition.UserName != null && (x.Username.Contains(condition.UserName))))
                    && (condition.SoDienThoai == null ||
                    (condition.SoDienThoai != null && (x.User.SoDienThoai.Contains(condition.SoDienThoai))))
                    && (condition.IdGroup == 0 ||
                    (condition.IdGroup != 0 && (x.GroupOfAccount.Count(y => y.IdGroup == condition.IdGroup) > 0)))
                    && !x.DelFlag && (x.GroupOfAccount.Count(y=>y.IdGroup==1)<=0)).OrderBy(x => x.Id).Skip((listOfUser.Paging.CurrentPage - 1) * listOfUser.Paging.NumberOfRecord)
                    .Take(listOfUser.Paging.NumberOfRecord).Select(x => new User
                    {
                        Id = x.Id,
                        Ho = x.User.Ho,
                        Ten = x.User.Ten,
                        Username = x.Username,
                        Password = x.Password,
                        GioiTinh = x.User.GioiTinh,
                        SoDienThoai = x.User.SoDienThoai,
                        Avatar = x.User.Avatar,
                        NgaySinh = x.User.NgaySinh,
                        Email = x.Email,
                        GroupList = x.GroupOfAccount.Where(q=>!q.DelFlag).Select(y => new Group
                        {
                            IdGroup = y.Id,
                            TenGroup = y.Group.GroupTrans.FirstOrDefault(p => y.IdGroup == p.Id && p.Lang == lang && !p.DelFlag).GroupName
                        }).ToList()
                    }).ToList();
                listOfUser.Condition = condition;
                listOfUser.Group = context.GroupTrans.Where(x => x.Lang == lang && !x.DelFlag).Select(x => new Group
                {
                    IdGroup = x.Id,
                    TenGroup = x.GroupName
                }).ToList();
                return listOfUser;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Thay đổi mật khẩu của  account
        /// Author       :   TramHTD - 06/08/2018 - create
        /// </summary>
        /// <param name="idAccount">id của account muốn thay đổi mật khẩu</param>
        /// <returns>Danh sách User sau khi thay đổi mật khẩu. Exception nếu có lỗi</returns>
        public bool ResetPasswordAccount(int id)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                string NewPassword = Common.GetToken(id,8);
                
                TblAccount taiKhoan = context.Account.FirstOrDefault(x => x.Id == id && !x.DelFlag);
                if (taiKhoan == null)
                {
                    return false;
                }
                else
                {
                    string pass = BaoMat.GetSimpleMD5(NewPassword);
                    taiKhoan.Password = BaoMat.GetMD5(pass);
                    context.SaveChanges();
                    SendEmail(NewPassword, taiKhoan.Email);
                }
                transaction.Commit();
                return true;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
       
        }
        /// <summary>
        /// Hàm gửi mail thông báo về thông tin thay đổi mật khẩu
        /// Author       :   TramHDT - 02/09/2018 - create
        /// </summary>
        /// <param name="password">Mật khẩu vừa mới được thay đổi</param>
        /// <param name="email">Email của người dùng được gửi thông tin</param>
        public void SendEmail(string password, string email)
        {
            try
            {
                string lang = Common.GetLang();
                TblBieuMau bieuMau = context.BieuMau.FirstOrDefault(x => x.Id == (int)ConstantsEnum.TemplateEnum.ResetPassword && x.Lang == lang && !x.DelFlag);
                if (bieuMau != null)
                {
                    bieuMau.NoiDung = bieuMau.NoiDung.Replace("matkhau", password);
                    bieuMau.NoiDung = bieuMau.NoiDung.Replace("ngay", DateTime.Now.ToString());
                    EmailService.Send(email, bieuMau.TenBieuMau, bieuMau.NoiDung);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}