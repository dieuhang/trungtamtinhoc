﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.ScheduleManagement.Schema
{
    /// <summary>
    /// Class dùng để chứa thông tin của một lịch học
    /// Author       :   HoangNM - 05/10/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class Schedule
    {
        public int Id { set; get; }
        public string PhongHoc { set; get; }
        public string KhoaHoc { set; get; }
        public string LopHoc { set; get; }
        public int idPhongHoc { get; set; }
        public int idLopHoc { get; set; }
        public int NgayHoc { set; get; }
        public TimeSpan GioBatDau { get; set; }
        public TimeSpan GioKetThuc { get; set; }

    }
    /// <summary>
    /// Class dùng để chứa thông tin tìm kiếm của danh sách hình ảnh
    /// Author       :   HoangNM - 05/10/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ScheduleConditionSearch
    {
        public int CurrentPage { set; get; }
        public int PageSize { set; get; }
        public int idKhoaHoc { get; set; }
        public int idLopHoc { get; set; }
        public int idPhongHoc { get; set; }
        public int NgayHoc { get; set; }
        public ScheduleConditionSearch()
        {
            this.idPhongHoc = 0;
            this.PageSize = 10;
            this.CurrentPage = 1;
            this.idLopHoc = 0;
            this.idKhoaHoc = 0;
            this.NgayHoc = 0;
        }
    }
}