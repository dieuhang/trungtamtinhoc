﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TrungTamTinHoc.Areas.Course.Models.Schema;
using TTTH.Common;

namespace TrungTamTinHoc.Areas.Home.Models.Schema
{
    /// <summary>
    /// Class chứa các thuộc tính của 1 TinTuc lấy từ DB cho việc hiển thị TinTuc.
    /// Author       :   HangNTD - 23/06/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   Home.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class TinTuc
    {
        public int Id { get; set; }
        public string BeautyId { set; get; }
        public string AnhMinhHoa { set; get; }
        public string TieuDe { set; get; }
        public string TomTat { set; get; }
        public DateTime? NgayDang { set; get; }
        public int SoLuongView { set; get; }
        public int SoLuongComment { get; set; }
        public int SoLuongDanhGia { set; get; }
        public int DiemDanhGia { set; get; }
    }

    /// <summary>
    /// Class chứa các thuộc tínhchi tiết của 1 TinTuc lấy từ DB cho việc hiển thị TinTuc.
    /// Author       :   HangNTD - 23/06/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   Home.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ChiTietTinTuc : TinTuc
    {
        public string AnhChinh { get; set; }
        public string NoiDung { set; get; }
        public List<TinTuc> ListReccentNew { get; set; }
        public List<CommentTinTuc> ListComment { get; set; }
        public string AvatarUser { get; set; }
        public string HotenUser { get; set; }
        public int IdUser { get; set; }
        public int CountComment { get; set; }
    }

    /// <summary>
    /// Class chứa danh sách chi tiết tin tức cần hieernt thị và thông tin các tin tức liên quan.
    /// Author       :   HangNTD - 23/06/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   Home.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>

    public class CommentTinTuc
    {
        public int Id { get; set; }
        public int IdComment { get; set; }
        public string Avatar { get; set; }
        public string HoTen { get; set; }
        [Required]
        public string NoiDung { get; set; }
        public DateTime? ThoiGianCmt { set; get; }
        public float Rating { get; set; }
        public int? IdUser { get; set; }
        public List<CommentTinTuc> ListChildComment { set; get; }
        public int CountChildComment { get; set; }
    }
    /// <summary>
    /// Class dùng để lấy danh sách tin tức trả về cho trang danh sách tin tức
    /// Author       :   HangNTD - 23/06/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   Home.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>

    public class TinTucList
    {
        public List<TinTuc> TinTucs { set; get; }
        public Paging Paging { set; get; }
        public NewsConditionSearch Condition { set; get; }

        public TinTucList()
        {
            this.TinTucs = new List<TinTuc>();
            this.Condition = new NewsConditionSearch();
            this.Paging = new Paging();
        }
    }
    /// <summary>
    /// Class dùng để chứa thông tin tìm kiếm của danh sách tin tức
    /// Author       :   HangNTD - 12/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class NewsConditionSearch
    {
        public string KeySearch { set; get; }
        public int CurrentPage { set; get; }
        public int PageSize { set; get; }
        public NewsConditionSearch()
        {
            this.KeySearch = "";
            this.PageSize = 8;
            this.CurrentPage = 1;
        }
    }
}