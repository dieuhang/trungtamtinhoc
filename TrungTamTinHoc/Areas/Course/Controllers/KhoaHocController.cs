﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrungTamTinHoc.Areas.Course.Models;
using TrungTamTinHoc.Areas.Course.Models.Schema;
using TTTH.Common;
using TTTH.Common.Enums;
using TTTH.Validate;

namespace TrungTamTinHoc.Areas.Course.Controllers
{
    public class KhoaHocController : Controller
    {
        /// <summary>
        /// Điều hướng đến trang chi tiết khóa học
        /// Author       :   TramHDT - 24/06/2018 - create
        /// </summary>
        /// <returns>Trang view ChiTietKhoaHoc, nếu có lỗi sẽ rả về trang error</returns>
        /// <remarks>
        /// Method: GET
        /// RouterName: ChiTietKhoaHoc
        /// </remarks>
        public ActionResult ChiTietKhoaHoc(string id)
        {
            try
            {
                ChiTietKhoaHoc chiTietKhoaHoc = new KhoaHocModel().LoadChiTietKhoaHoc(id);
                if (chiTietKhoaHoc == null)
                {
                    return RedirectToAction("DanhSachKhoaHoc");
                }
                else
                {
                    chiTietKhoaHoc.IdUser = XacThuc.GetIdUser();
                    return View("ChiTietKhoaHoc", chiTietKhoaHoc);
                }
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }
        /// <summary>
        /// Xử lý sau khi nhấn nút xem thêm ở bình luận
        /// Author       :   TramHDT - 24/07/2018 - create
        /// </summary>
        /// <returns>
        /// Partial view chứa 5 comment lớn tiếp theo
        /// Partial view chứa 2 comment nhỏ trong comment lớn nếu có id của comment lớn đó
        /// </returns>
        /// <remarks>
        /// Method: GET
        /// RouterName: seeMore
        /// </remarks>
        public ActionResult SeeMore(int countComment,int childComment,int idParentComment,int idKhoaHoc)
        {
            try
            {
                if (idParentComment != 0)
                {
                    return View("Partial/_listCommentReply", new KhoaHocModel().GetListCommentReply(idKhoaHoc, childComment, idParentComment));
                }
                return View("Partial/_listComment", new KhoaHocModel().GetListComment(idKhoaHoc, countComment));
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }
        /// <summary>
        /// hiển thị danh sách khóa học
        /// Author       :   HoangNM - 23/06/2018 - create
        ///              :   TramHTD - 06/08/2018 - update
        /// </summary>
        /// <returns>Trả lại trang view DanhSachKhoaHoc, nếu có lỗi thì trả về trang error</returns>
        /// <remarks>
        /// Method: GET
        /// RouterName: DanhSachKhoaHoc
        /// </remarks>
        /// 
        public ActionResult DanhSachKhoaHoc(CourseConditionSearch condition)
        {
            try
            {
                if (Request.IsAjaxRequest())
                {
                    return View("Partial/_listCourse", new KhoaHocModel().LoadKhoaHoc(condition));
                }
                return View(new KhoaHocModel().LoadKhoaHoc(condition));

            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }

        }

        /// <summary>
        /// Cập nhật trả lời bình luận theo thông tin người dùng đưa lên.
        /// Author       :   TramHDT - 12/07/2018 - create
        /// </summary>
        /// <param> </param>
        /// <returns>Chuỗi Json chứa kết quả của việc Cập nhật trả lời bình luận</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: commentReply
        /// </remarks>
        public ActionResult CommentReply(int idParentComment,string contentReply,int idKhoaHoc, int starRating, int IdUser)
        {
            try
            {
                if (idParentComment != 0)
                {
                    return View("Partial/_commentReply", new KhoaHocModel().CommentReply(idParentComment, contentReply, idKhoaHoc,IdUser,starRating));
                }
                return View("Partial/_comment", new KhoaHocModel().CommentReply(idParentComment, contentReply, idKhoaHoc, IdUser, starRating));
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }
    }
}