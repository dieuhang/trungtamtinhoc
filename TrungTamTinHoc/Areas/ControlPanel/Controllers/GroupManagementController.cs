﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TTTH.Common;
using TrungTamTinHoc.Areas.ControlPanel.Models.GroupManagement;
using TrungTamTinHoc.Areas.ControlPanel.Models.GroupManagement.Schema;
using static TTTH.Common.Enums.ConstantsEnum;
using static TTTH.Common.Enums.MessageEnum;
using TTTH.Validate;
using TrungTamTinHoc.Controllers;

namespace TrungTamTinHoc.Areas.ControlPanel.Controllers
{
    /// <summary>
    /// Class chứa các điều hướng liên quan đến quản lý danh sách Group
    /// Author       :   HangNTD - 02/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class GroupManagementController : BaseAdminController
    {
        /// <summary>
        /// Điều hướng đến trang hiển thị danh sách Group nếu là request thông thường.
        /// Trả về table chứa danh sách Group nếu là Ajax.
        /// Điều hướng về trang lỗi nếu có lỗi sảy ra.
        /// Author       :   HangNTD - 02/08/2018 - create
        /// </summary>
        /// <param name="condition">Đối tượng chứa điều kiện tìm kiếm, tạo thành từ query string</param>
        /// <returns>
        /// Trang danh sách Group.
        /// Partial view chứa table danh sách Group.
        /// </returns>
        /// <remarks>
        /// Method: GET
        /// RouterName: ListOfGroup
        /// </remarks>
        public ActionResult ListOfGroup(GroupConditionSearch condition)
        {
            try
            {
                if (Request.IsAjaxRequest())
                {
                    return View("Partial/_tableOfGroup", new ListOfGroupModel().GetListOfGroup(condition));
                }
                return View(new ListOfGroupModel().GetListOfGroup(condition));
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }
        /// <summary>
        /// Xóa các Group theo danh sách id Group được gửi lên.
        /// Author       :   HangNTD - 02/08/2018 - create
        /// </summary>
        /// <param name="ids">Danh sách id các group sẽ xóa</param>
        /// <returns>Đối tượng chứa thông tin về quá tringh xóa Group</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: DeleteGroups
        /// </remarks>
        public JsonResult DeleteGroups(List<int> ids)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                bool deleted = new ListOfGroupModel().DeleteGroups(ids);
                if (!deleted)
                {
                    response.Code = 201;
                    response.MsgNo = 45;
                }
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Update trạng thái hiển thị trên trang chủ của 1 Group.
        /// Author       :   HangNTD - 02/08/2018 - create
        /// </summary>
        /// <param name="id">id của Group sẽ update</param>
        /// <returns>Đối tượng chứa thông tin về quá trình update</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: UpdateShowGroup
        /// </remarks>
        public JsonResult UpdateShow(int id)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                bool updated = new ListOfGroupModel().UpdateShow(id);
                if (!updated)
                {
                    response.Code = 201;
                    response.MsgNo = 45;
                }
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Trả về view add group khi click add group.
        /// Author       :   HangNTD - 02/08/2018 - create
        /// </summary>
        /// </remarks>
        public ActionResult ViewCreateGroup()
        {
            try
            {
                return View("GroupMaster");
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }
        /// <summary>
        /// Điều hướng đến trang view sửa group
        /// Author       :   HangNTD - 25/07/2018 - create
        /// </summary>
        /// <param name="id">Đối tượng chứa thông tin về quá trình chỉnh sửa thông tin</param>
        /// <returns>Trả về trang view sửa group</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: ViewEditgroup
        /// </remarks>
        public ActionResult ViewEditGroup(string id)
        {
            try
            {
                GroupMaster groupMaster = new GroupMasterModel().LoadGroup(id);
                if (groupMaster.Mode == (int)ModeMaster.Insert)
                {
                    return RedirectToAction("ViewCreateGroup");
                }
                return View("GroupMaster", groupMaster);
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }
        /// <summary>
        /// Tạo group hoặc cập nhật group theo thông tin người dùng đưa lên.
        /// Author       :   HangNTD - 25/07/2018 - create
        /// </summary>
        /// <param name="data">Thông tin group mà người dùng nhập vào</param>
        /// <returns>Chuỗi Json chứa kết quả của việc tạo hoặc cập nhật group</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: Savegroup
        /// </remarks>
        [HttpPost]
        public JsonResult SaveGroup(Group data)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                if (ModelState.IsValid)
                {
                    response = new GroupMasterModel().SaveGroup(data);
                }
                else
                {
                    response.Code = (int)CodeResponse.NotValidate;
                    response.ListError = ModelState.GetModelErrors();
                }
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Lưu thông tin group được gửi lên bằng ngôn ngữ khác ngôn ngữ mặc định
        /// Author       :   HangNTD - 25/07/2018 - create
        /// </summary>
        /// <param name="data">Dữ liệu của đối tượng muốn lưu</param>
        /// <returns></returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: Savegroup
        /// </remarks>
        public JsonResult UpdateGroupTranslate(Group_Trans data)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                if (ModelState.IsValid)
                {
                    response = new GroupMasterModel().UpdateGroupTranslate(data);
                }
                else
                {
                    response.Code = (int)CodeResponse.NotValidate;
                    response.ListError = ModelState.GetModelErrors();
                }
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Thay đổi ngôn ngữ
        /// Author       :   HangNTD- 25/07/2018 - create
        /// </summary>
        /// <param name="id">Đối tượng muốn thay đổi ngôn ngữ</param>
        /// <param name="lang">Ngôn ngữ muốn thay đổi</param>
        /// <returns></returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: SaveGroup
        /// </remarks>
        public JsonResult ReferGroupWithLang(int id, string lang)
        {
            try
            {
                return Json(new GroupMasterModel().ReferGroupWithLang(id, lang), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}