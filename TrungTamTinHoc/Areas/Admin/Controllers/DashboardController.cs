﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrungTamTinHoc.Areas.Admin.Models;
using TrungTamTinHoc.Areas.ControlPanel.Models.ContactManagement;
using TrungTamTinHoc.Controllers;

namespace TrungTamTinHoc.Areas.Admin.Controllers
{
    public class DashboardController : BaseAdminController
    {
        // GET: Admin/Dashboard
        public ActionResult Index()
        {
            return View();
        }
    }
}