﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrungTamTinHoc.Areas.ControlPanel.Models.ContactManagement.Schema;
using TrungTamTinHoc.Areas.ControlPanel.Models.ScheduleManagement;
using TrungTamTinHoc.Areas.ControlPanel.Models.ScheduleManagement.Schema;
using TTTH.Common;
using TTTH.Validate;
using static TTTH.Common.Enums.ConstantsEnum;
using static TTTH.Common.Enums.MessageEnum;

namespace TrungTamTinHoc.Areas.ControlPanel.Controllers
{
    /// <summary>
    /// Class chứa các điều hướng liên quan đến quản lý lịch học
    /// Author       :   HoàngNM - 5/10/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ScheduleManagementController : Controller
    {
        /// <summary>
        /// Điều hướng đến trang hiển thị danh sách lịch học nếu là request thông thường.
        /// Trả về table chứa danh sách liên hệ nếu là Ajax.
        /// Điều hướng về trang lỗi nếu có lỗi sảy ra.
        /// Author       :   HoangNM - 05/10/2018 - create
        /// </summary>
        /// <param name="condition">Đối tượng chứa điều kiện tìm kiếm, tạo thành từ query string</param>
        /// <returns>
        /// Trang danh sách lịch học.
        /// Partial view chứa table danh sách lịch học .
        /// </returns>
        /// <remarks>
        /// Method: GET
        /// RouterName: ListOfSchedule
        /// </remarks>
        public ActionResult ListOfSchedule(ScheduleConditionSearch condition)
        {
            try
            {
                if (Request.IsAjaxRequest())
                {
                    return View("Partial/_tableOfSchedule", new ListOfScheduleModel().GetListOfSchedule(condition));
                }
                return View(new ListOfScheduleModel().GetListOfSchedule(condition));
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }

        /// <summary>
        /// Xóa các lịch học theo danh sách id lịch học được gửi lên.
        /// Author       :   HoangNM - 05/10/2018 - create
        /// </summary>
        /// <param name="ids">Danh sách id các lịch sẽ xóa</param>
        /// <returns>Đối tượng chứa thông tin về quá trình xóa lịch học </returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: DeleteSchedule
        /// </remarks>


        public JsonResult DeleteSchedule(List<int> ids)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                new ListOfScheduleModel().DeleteSchedule(ids);

            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Thêm Lịch học
        /// Author       :   HoangNM - 06/10/2018 - create
        /// </summary>

        public ActionResult ViewCreateSchedule()
        {
            try
            {
                KhoaHoc khoaHoc = new ScheduleMasterModel().loadKhoaHoc();
                ScheduleMaster scheduleMaster = new ScheduleMaster();
                scheduleMaster.khoahoc = khoaHoc;
                return View("ScheduleMaster",scheduleMaster);
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }

        /// <summary>
        /// thay đổi thông tin của lịch học
        /// Author       :   HoangNM - 07/10/2018 - create
        /// </summary>
        /// <param name="id">id của lịch học muốn update</param>

        public ActionResult ViewEditSchedule(string id)
        {
            try
            {
                ScheduleMaster scheduleMaster = new ScheduleMasterModel().LoadSchedule(id);
                if (scheduleMaster.Mode == (int)ModeMaster.Insert)
                {
                    return RedirectToAction("ViewCreateSchedule");
                }
                return View("ScheduleMaster", scheduleMaster);
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }

        /// <summary>
        /// dùng để lưu thông tin của lịch học
        /// Author       :   HoangNM - 07/10/2018 - create
        /// </summary>
        /// <param name="data">data chứa thông tin của lịch học</param>
        [HttpPost]
        public JsonResult SaveSchedule(Schedule data)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                if (ModelState.IsValid)
                {
                    response = new ScheduleMasterModel().SaveSchedule(data);
                }
                else
                {
                    response.Code = (int)CodeResponse.NotValidate;
                    response.ListError = ModelState.GetModelErrors();
                }
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

    }
}