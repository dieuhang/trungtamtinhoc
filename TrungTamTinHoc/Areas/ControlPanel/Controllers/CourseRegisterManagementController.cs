﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrungTamTinHoc.Areas.ControlPanel.Models.CourseRegisterManagement;
using TrungTamTinHoc.Areas.ControlPanel.Models.CourseRegisterManagement.Schema;
using TrungTamTinHoc.Controllers;
using TTTH.Common;
using static TTTH.Common.Enums.ConstantsEnum;
using static TTTH.Common.Enums.MessageEnum;

namespace TrungTamTinHoc.Areas.ControlPanel.Controllers
{
    /// <summary>
    /// Class chứa các điều hướng liên quan đến trang quản lý đăng ký khóa học của admin
    /// Author       :   AnTM - 14/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   Home
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class CourseRegisterManagementController : BaseAdminController
    {
        /// <summary>
        /// Điều hướng đến quản lý khóa học nếu là request thông thường
        /// Author       :   AnTM - 17/07/2018 - create
        /// </summary>
        /// <param name="condition">đối tượng chưa điều kiện tìm kiếm</param>
        /// <returns>
        /// Trang danh sách đăng ký khóa học
        /// Partial view chứa danh sách đăng ký khóa học
        /// </returns>
        /// <remarks>
        /// Method: GET
        /// RouterName: ListOfCourseRegister
        /// </remarks>
        public ActionResult ListOfCourseRegister(CourseRegisterConditionSearch condition)
        {
            try
            {
                if (Request.IsAjaxRequest())
                {
                    return View("Partial/_tableOfCourseRegister", new ListOfCourseRegisterModel().GetListOfCourseRegister(condition));
                }
                return View(new ListOfCourseRegisterModel().GetListOfCourseRegister(condition));
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }

        /// <summary>
        /// Update trạng thái  trên trang chủ của danh sách đăng ký khóa học.
        /// Author       :   HoangNM - 24/07/2018 - create
        /// </summary>
        /// <param name="id">id của danh sách đăng ký khóa học sẽ update</param>
        /// <returns>Đối tượng chứa thông tin về quá trình update</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: UpdateShowCourseRegister
        /// </remarks>
        public JsonResult UpdateShow(int id, int idTrangThai)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                response = new ListOfCourseRegisterModel().UpdateShow(id, idTrangThai);
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Xóa các thông tin đăng ký khóa học theo danh sách id  được gửi lên.
        /// Author       :   HoangNM - 25/07/2018 - create
        /// </summary>
        /// <param name="ids">Danh sách id  sẽ xóa</param>
        /// <returns>Đối tượng chứa thông tin về quá tring xóa contact</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: DeleteCourse
        /// </remarks>
        public JsonResult DeleteCourseRegister(List<int> ids)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                bool deleted = new ListOfCourseRegisterModel().DeleteCouseRegister(ids);
                if (!deleted)
                {
                    response.Code = 201;
                    response.MsgNo = 45;
                }
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Lấy danh sách các lớp học theo id khóa học từ client đưa lên
        /// Author       :   AnTM - 07/08/2018 - create
        /// </summary>
        /// <param name="idKhoaHoc">id khóa học chọn từ select box</param>
        /// <returns>Chỗi Json chứa kết quả của việc lấy danh sách lớp học</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: getClassesByIdCourse
        /// </remarks>
        public JsonResult LayDanhSachLopHocTheoIdKhoaHoc(int idKhoaHoc)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                response.ThongTinBoSung4 = new ListOfCourseRegisterModel().LayLopHocHienThiTheoIDKhoaHoc(idKhoaHoc).ToList<object>();
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Lấy danh sách các lớp học theo id khóa học từ client đưa lên
        /// Author       :   AnTM - 07/08/2018 - create
        /// </summary>
        /// <param name="id">ID đăng ký học gửi từ client</param>
        /// <returns>Chỗi Json chứa kết quả của việc lấy thông tin chi tiết đăng ký học</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: DetailCourseRegister
        /// </remarks>
        public ActionResult LayChiTietThongTinDangKyHoc(int id)
        {
            try
            {
                ListOfCourseRegisterModel model = new ListOfCourseRegisterModel();
                if (model.CheckExistCourseRegister(id))
                {
                    return View("Partial/_modalPopupCourseRegisterDetail",
                        model.LayChiTietThongTinDangKyHoc(id));
                }
                return RedirectToAction("Error", "Error", new { area = "error", error = "" });
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }
    }
}