﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TrungTamTinHoc.Areas.ControlPanel.Models.EmailSubscriptionManagement.Schema;
using TTTH.Common;
using TTTH.DataBase;
using Z.EntityFramework.Plus;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.EmailSubscriptionManagement
{
    /// <summary>
    /// Class dùng để xử lý các hoạt động liên quan đến đăng ký theo dõi qua email.
    /// Author       :   HaLTH - 19/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ListOfEmailSubscriptionModel
    {
        DataContext context;
        public ListOfEmailSubscriptionModel()
        {
            context = new DataContext();
        }

        /// <summary>
        /// Tìm kiếm các email theo điều kiện cho trước.
        /// Author       :   HaLTH - 19/07/2018 - create
        /// </summary>
        /// <param name="condition">Đối tượng chứa điều kiện tìm kiếm</param>
        /// <returns>Danh sách các email đã tìm kiếm được. Exception nếu có lỗi</returns>
        public ListOfEmailSubscription GetListOfEmailSubscription(EmailSubscriptionConditionSearch condition)
        {
            try
            {
                // Nếu không tồn tại điều kiện tìm kiếm thì khởi tạo giá trị tìm kiếm ban đầu
                if (condition == null)
                {
                    condition = new EmailSubscriptionConditionSearch();
                }
                string lang = Common.GetLang();

                ListOfEmailSubscription listOfEmailSubscription = new ListOfEmailSubscription();
                // Lấy các thông tin dùng để phân trang
                listOfEmailSubscription.Paging = new Paging(context.DangKyTheoDoi.Count(x =>
                    (condition.KeySearch == null ||
                    (condition.KeySearch != null && (x.Email.Contains(condition.KeySearch))))
                    && !x.DelFlag), condition.CurrentPage, condition.PageSize);

                // Tìm kiếm và lấy dữ liệu theo trang
                listOfEmailSubscription.EmailSubscriptionList = context.DangKyTheoDoi.Where(x =>
                    (condition.KeySearch == null ||
                    (condition.KeySearch != null && (x.Email.Contains(condition.KeySearch))))
                    && !x.DelFlag).OrderBy(x => x.Id)
                    .Skip((listOfEmailSubscription.Paging.CurrentPage - 1) * listOfEmailSubscription.Paging.NumberOfRecord)
                    .Take(listOfEmailSubscription.Paging.NumberOfRecord).Select(x => new EmailSubscription
                    {
                        Id = x.Id,
                        Email = x.Email
                    }).ToList();
                listOfEmailSubscription.Condition = condition;
                return listOfEmailSubscription;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Xóa các email trong DB.
        /// Author       :   HaLTH - 19/07/2018 - create
        /// </summary>
        /// <param name="ide">Danh sách id của các email sẽ xóa</param>
        /// <returns>True nếu xóa thành công, False nếu không còn email được hiển thị, Excetion nếu có lỗi</returns>
        /// 
        public void DeleteEmailSubscriptions(List<int> ids)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                context.DangKyTheoDoi.Where(x => ids.Contains(x.Id)).Delete();
                context.SaveChanges();
                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }
    }
}