﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.ThingsAchievedManagement.Schema
{
    /// <summary>
    /// Class dùng để chứa thông tin của Những điều đạt được
    /// Author       :   HaLTH - 26/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ThingsAchieved
    {
        public int Id { set; get; }
        [Required]
        [StringLength(255)]
        public string Icon { get; set; }

        [Required]
        [StringLength(50)]
        public string Title { get; set; }

        [Required]
        [StringLength(1000)]
        public string Content { get; set; }

        public HttpPostedFileBase FileImg { set; get; }

        public bool IsShow { set; get; }
        public string CheckHienThi { set; get; }
    }

    public class ThingsAchieved_Trans
    {
        public int Id { set; get; }

        public string LanguageTrans { set; get; }

        [Required]
        [StringLength(50)]
        public string Title { get; set; }

        [Required]
        [StringLength(1000)]
        public string Content { get; set; }
    }

    /// <summary>
    /// Class dùng để chứa thông tin tìm kiếm của danh sách Những điều đạt được
    /// Author       :   HaLTH - 26/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ThingsAchievedConditionSearch
    {
        public string KeySearch { set; get; }
        public bool? TrangThai { set; get; }
        public int CurrentPage { set; get; }
        public int PageSize { set; get; }
        public ThingsAchievedConditionSearch()
        {
            this.KeySearch = "";
            this.TrangThai = null;
            this.PageSize = 10;
            this.CurrentPage = 1;
        }
    }
}