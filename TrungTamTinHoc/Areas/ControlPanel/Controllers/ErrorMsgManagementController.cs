﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrungTamTinHoc.Areas.ControlPanel.Models.ErrorMsgMagement;
using TTTH.Common;
using static TTTH.Common.Enums.ConstantsEnum;
using static TTTH.Common.Enums.MessageEnum;
using TrungTamTinHoc.Areas.ControlPanel.Models.ErrorMsgManagement.Schema;
using TrungTamTinHoc.Areas.ControlPanel.Models.ErrorMsgManagement;
using TTTH.Validate;
using TrungTamTinHoc.Areas.ControlPanel.Models.ErrorMgsManagement;
using TrungTamTinHoc.Controllers;

namespace TrungTamTinHoc.Areas.ControlPanel.Controllers
{
    /// <summary>
    /// Class chứa các điều hướng liên quan đến quản lý danh sách Lỗi
    /// Author       :   HoàngNM - 01/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ErrorMsgManagementController : DevelopController
    {
        /// <summary>
        /// Điều hướng đến trang hiển thị danh sách lỗi nếu là request thông thường.
        /// Trả về table chứa danh sách lỗi nếu là Ajax.
        /// Điều hướng về trang lỗi nếu có lỗi sảy ra.
        /// Author       :   HoangNM - 01/08/2018 - create
        /// </summary>
        /// <param name="condition">Đối tượng chứa điều kiện tìm kiếm, tạo thành từ query string</param>
        /// <returns>
        /// Trang danh sách lỗi.
        /// Partial view chứa table danh sách lỗi.
        /// </returns>
        /// <remarks>
        /// Method: GET
        /// RouterName: ListOfContact
        /// </remarks>
        public ActionResult ListOfErrorMsg(ContactConditionSearch condition)
        {
            try
            {
                if(new ErrorMsgMasterModel().ChekAccount())
                {
                    if (Request.IsAjaxRequest())
                    {
                        return View("Partial/_tableOfErrorMsg", new ListOfErrorMsgModel().GetListOfErrorMsg(condition));
                    }
                    return View(new ListOfErrorMsgModel().GetListOfErrorMsg(condition));
                }
                else
                {
                    return RedirectToRoute("adminDashboard");
                }
               
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }/// <summary>
         /// Update Type của errorMgs trên db.
         /// Author       :   HoangNM - 03/08/2018 - create
         /// </summary>
         /// <param name="id">id của contact sẽ update</param>
         /// <param name="Type">id trạng thái của contact sẽ update</param>
         /// <returns>Đối tượng chứa thông tin về quá trình update</returns>
         /// <remarks>
         /// Method: POST
         /// RouterName: UpdateType
         /// </remarks>
        public JsonResult UpdateType(int id, int Type)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                if(new ErrorMsgMasterModel().ChekAccount())
                {
                    new ListOfErrorMsgModel().UpdateType(id, Type);
                }else
                {
                    response.Code = 403;
                    response.MsgNo = (int)MsgNO.permissions;
                }
                
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Xóa các errorMgs theo danh sách id error được gửi lên.
        /// Author       :   HoangNM - 03/08/2018 - create
        /// </summary>
        /// <param name="ids">Danh sách id sẽ xóa</param>
        /// <returns>Đối tượng chứa thông tin về quá tring xóa errorMgs</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: DeleteErrorMgs
        /// </remarks>
        public JsonResult DeleteErrorMsg(List<int> ids)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                if (new ErrorMsgMasterModel().ChekAccount())
                {
                    new ListOfErrorMsgModel().DeleteErrorMsg(ids);
                }
                else
                {
                    response.Code = 403;
                    response.MsgNo = (int)MsgNO.permissions;
                }
                
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// cập nhật file js vào code .
        /// Author       :   HoangNM - 03/08/2018 - create
        /// </summary>
        /// <remarks>
        /// RouterName: ListOfErrorMgsUpdateFile
        /// </remarks>
        public JsonResult UpdateFile()
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                if (new ErrorMsgMasterModel().ChekAccount())
                {
                    response = new ListOfErrorMsgModel().UpdateFile();
                }
                else
                {
                    response.Code = 403;
                    response.MsgNo = (int)MsgNO.permissions;
                }
                

            }
            catch(Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Thêm errorMgs
        /// Author       :   HoangNM - 13/08/2018 - create
        /// </summary>

        public ActionResult ViewCreateErrorMsg()
        {
            try
            {
                if(new ErrorMsgMasterModel().ChekAccount())
                {
                    return View("ErrorMsgMaster");
                }
                else
                {
                    return RedirectToRoute("adminDashboard");
                }
                
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }

        /// <summary>
        /// thay đổi thông tin của errorMgs
        /// Author       :   HoangNM - 13/08/2018 - create
        /// </summary>
        /// <param name="id">id của errorMgs muốn update</param>

        public ActionResult ViewEditErrorMsg(string id)
        {
            try
            {
                ErrorMsgMaster errorMgsMaster = new ErrorMsgMasterModel().LoadErrorMsg(id);
                if (errorMgsMaster.Mode == (int)ModeMaster.Insert)
                {
                    return RedirectToAction("ViewCreateErrorMsg");
                }
                return View("ErrorMsgMaster", errorMgsMaster);
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }

        /// <summary>
        /// dùng để lưu thông tin của ErrorMsg
        /// Author       :   HoangNM - 13/08/2018 - create
        /// </summary>
        /// <param name="data">data chứa thông tin của errorMsg</param>
        [HttpPost]
        public JsonResult SaveErrorMsg(ErrorMsg data)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                if (ModelState.IsValid)
                {
                    if (new ErrorMsgMasterModel().ChekAccount())
                    {
                        response = new ErrorMsgMasterModel().SaveErrorMsg(data);
                    }
                    else
                    {
                        response.Code = 403;
                        response.MsgNo = (int)MsgNO.permissions;
                    }
                   
                }
                else
                {
                    response.Code = (int)CodeResponse.NotValidate;
                    response.ListError = ModelState.GetModelErrors();
                }
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// dùng để lưu thông tin của ErrorMsg bằng ngôn ngữ khác
        /// Author       :   HoangNM - 13/08/2018 - create
        /// </summary>
        /// <param name="data">data chứa thông tin của errorMsg</param>
        public JsonResult UpdateErrorMsgTranslate(ErrorMsg_Trans data)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                if (ModelState.IsValid)
                {
                    if (new ErrorMsgMasterModel().ChekAccount())
                    {
                        response = new ErrorMsgMasterModel().UpdateErrorMsgTranslate(data);
                    }
                    else
                    {
                        response.Code = 403;
                        response.MsgNo = (int)MsgNO.permissions;
                    }
                    
                }
                else
                {
                    response.Code = (int)CodeResponse.NotValidate;
                    response.ListError = ModelState.GetModelErrors();
                }
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// dùng để thay đổi ngôn ngữ
        /// Author       :   HoangNM - 13/08/2018 - create
        /// </summary>
        public JsonResult ReferErrorMsgWithLang(int id, string lang)
        {
            try
            {
                return Json(new ErrorMsgMasterModel().ReferErrorMsgWithLang(id, lang), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}