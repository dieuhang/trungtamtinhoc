﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TTTH.Common;
using TTTH.DataBase;
using TTTH.DataBase.Schema;
using TblAccount = TrungTamTinHoc.Areas.Home.Models.Schema.Account;
using static TTTH.Common.Enums.ConstantsEnum;
using static TTTH.Common.Enums.MessageEnum;
using Z.EntityFramework.Plus;
using TblCauHinh = TTTH.DataBase.Schema.CauHinh;
using TrungTamTinHoc.Areas.ControlPanel.Models.ConfigurationManagement.Schema;
using System.Data.Entity;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.ConfigurationManagement
{
    /// <summary>
    /// Class dùng để xử lý các hoạt động liên quan đến cấu hình.
    /// Author       :   HaLTH - 13/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ConfigurationModel
    {
        DataContext context;
        public ConfigurationModel()
        {
            context = new DataContext();
        }

        /// <summary>
        /// Lấy thông tin của Cấu hình dùng để cập nhật thông tin
        /// Author       :   HaLTH - 13/08/2018 - create
        /// </summary>
        public ConfigurationMaster LoadConfiguration()
        {
            try
            {
                ConfigurationMaster configurationMaster = new ConfigurationMaster();

                TblCauHinh configuration = context.CauHinh.FirstOrDefault(x => !x.DelFlag);
                if (configuration != null)
                {
                    configurationMaster.ConfigurationInfo = new Configuration();
                    configurationMaster.ConfigurationInfo.SoLanChoPhepDangNhapSai = configuration.SoLanChoPhepDangNhapSai;
                    configurationMaster.ConfigurationInfo.ThoiGianKhoa = configuration.ThoiGianKhoa;
                    configurationMaster.ConfigurationInfo.ThoiGianTonTaiToken = configuration.ThoiGianTonTaiToken;
                    configurationMaster.ConfigurationInfo.FbAppId = configuration.FbAppId;
                    configurationMaster.ConfigurationInfo.GgClientId = configuration.GgClientId;
                }
                return configurationMaster;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Cập nhật lại Cấu hình từ thông tin mà người dùng gửi lên
        /// Author       :   HaLTH - 13/08/2018 - create
        /// </summary>
        /// <param name="course">Các thông tin của Cấu hình muốn cập nhật</param>
        /// <returns>Thông tin về việc cập nhật Cấu hình thành công hay thất bại</returns>
        public ResponseInfo EditConfiguration(Configuration configuration)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo response = new ResponseInfo();

                context.CauHinh.Where(x => x.Id == 1 && !x.DelFlag)
                    .Update(x => new TTTH.DataBase.Schema.CauHinh
                    {
                        SoLanChoPhepDangNhapSai = configuration.SoLanChoPhepDangNhapSai,
                        ThoiGianKhoa = configuration.ThoiGianKhoa,
                        ThoiGianTonTaiToken = configuration.ThoiGianTonTaiToken,
                        FbAppId = configuration.FbAppId,
                        GgClientId = configuration.GgClientId
                    });

                context.SaveChanges();
                transaction.Commit();
                return response;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

    }
}