﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TTTH.Common;
using TTTH.Common.Enums;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.CourseManagement.Schema
{
    /// <summary>
    /// Class chứa các thông tin liên quan đến khóa học
    /// Author       :   TramHTD - 27/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks
    public class CourseMaster
    {
        public Course CourseInfo { set; get; }
        public Course CourseTrans { set; get; }
        public List<Lang> Language { set; get; }
        public int Mode { set; get; }
        public CourseMaster()
        {
            Mode = (int)ConstantsEnum.ModeMaster.Insert;
            CourseInfo = null;
            CourseTrans = null;
            Language = new List<Lang>();
        }
    }
}