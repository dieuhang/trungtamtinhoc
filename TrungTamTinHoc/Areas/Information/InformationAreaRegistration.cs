﻿using System.Web.Mvc;

namespace TrungTamTinHoc.Areas.Information
{
    public class InformationAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Information";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "changePassword",
                "information/change-password",
                new { controller = "Information", action = "ChangePassWord", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "SaveUsername",
                "information/add-Username",
                new { controller = "Information", action = "SaveUsername", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "changeEmail",
                "information/change-email",
                new { controller = "Information", action = "ChangeEmail", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "SendMail",
                "information/send-email",
                new { controller = "Information", action = "GuiLaiMail", id = UrlParameter.Optional }
            );

            
            context.MapRoute(
                "checkDiaChi",
                "information/update-diachi",
                new { controller = "Information", action = "CheckDiaChi", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "checkUser",
                "information/check-user",
                new { controller = "Information", action = "CheckUser", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "updateInformation",
                "information/update-information",
                new { controller = "Information", action = "UpdateInformation", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "getEmail",
                "information/getEmail",
                new { controller = "Information", action = "GetEmail", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "changePass",
                "information/change-pass",
                new { controller = "Information", action = "CheckPass", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "checkEmail",
                "information/check-mail",
                new { controller = "Information", action = "CheckExistEmail", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "Information_default",
                "Information/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}