﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrungTamTinHoc.Areas.ControlPanel.Models.PhotoManagement;
using TrungTamTinHoc.Areas.ControlPanel.Models.PhotoManagement.Schema;
using TrungTamTinHoc.Controllers;
using TTTH.Common;
using TTTH.Validate;
using static TTTH.Common.Enums.ConstantsEnum;
using static TTTH.Common.Enums.MessageEnum;

namespace TrungTamTinHoc.Areas.ControlPanel.Controllers
{
    /// <summary>
    /// Class chứa các điều hướng liên quan đến quản lý danh sách hình ảnh
    /// Author       :   HaLTH - 06/08/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class PhotoManagementController : BaseAdminController
    {
        /// <summary>
        /// Điều hướng đến trang hiển thị danh sách hình ảnh nếu là request thông thường.
        /// Trả về table chứa danh sách hình ảnh nếu là Ajax.
        /// Điều hướng về trang lỗi nếu có lỗi sảy ra.
        /// Author       :   HaLTH - 06/08/2018 - create
        /// </summary>
        /// <param name="condition">Đối tượng chứa điều kiện tìm kiếm, tạo thành từ query string</param>
        /// <returns>
        /// Trang danh sách hình ảnh.
        /// Partial view chứa table danh sách hình ảnh.
        /// </returns>
        /// <remarks>
        /// Method: GET
        /// RouterName: ListOfPhoto
        /// </remarks>
        public ActionResult ListOfPhoto(PhotoConditionSearch condition)
        {
            try
            {
                if (Request.IsAjaxRequest())
                {
                    return View("Partial/_tableOfPhoto", new ListOfPhotoModel().GetListOfPhoto(condition));

                }
                return View(new ListOfPhotoModel().GetListOfPhoto(condition));
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }

        /// <summary>
        /// Xóa các hình ảnh theo danh sách id hình ảnh được gửi lên.
        /// Author       :   HaLTH - 06/08/2018 - create
        /// </summary>
        /// <param name="ids">Danh sách id các hình ảnh sẽ xóa</param>
        /// <returns>Đối tượng chứa thông tin về quá trình xóa hình ảnh</returns>
        /// <remarks>
        /// Method: POST
        /// RouterName: DeletePhotos
        /// </remarks>
        public JsonResult DeletePhotos(List<int> ids)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                new ListOfPhotoModel().DeletePhotos(ids);

            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Thêm hình ảnh vào album (photo)
        /// Author       :   HoangNM - 09/08/2018 - create
        /// </summary>

        public ActionResult ViewCreatePhotos()
        {
            try
            {
                return View("PhotoMaster");
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }

        /// <summary>
        /// thay đổi thông tin của album ảnh (photo)
        /// Author       :   HoàngNM - 09/08/2018 - create
        /// </summary>
        /// <param name="id">id của hình ảnh muốn update</param>
        
        public ActionResult ViewEditPhoTos(string id)
        {
            try
            {
                PhotoMaster photoMaster = new PhotoMasterModel().LoadPhoto(id);
                if (photoMaster.Mode == (int)ModeMaster.Insert)
                {
                    return RedirectToAction("ViewCreatePhotos");
                }
                return View("PhotoMaster", photoMaster);
            }
            catch (Exception e)
            {
                return RedirectToAction("Error", "Error", new { area = "error", error = e.Message });
            }
        }

        /// <summary>
        /// lưu thay đổi hoặc add thông tin của album ảnh (photo)
        /// Author       :   HoàngNM - 09/08/2018 - create
        /// </summary>
        /// <param name="data">data chứa thông tin của album ảnh</param>
        [HttpPost]
        public JsonResult SavePhotos(Photo data)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                if (ModelState.IsValid)
                {
                    response = new PhotoMasterModel().SavePhotos(data);
                }
                else
                {
                    response.Code = (int)CodeResponse.NotValidate;
                    response.ListError = ModelState.GetModelErrors();
                }
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// dùng để lưu thay đổi thông tin của album ảnh (photo) cho ngôn ngử mình chọn
        /// Author       :   HoàngNM - 09/08/2018 - create
        /// </summary>
        /// <param name="data">data chứa các thuộc tính cần thay đổi của album ảnh</param>
        public JsonResult UpdatePhotoTranslate(Photos_Trans data)
        {
            ResponseInfo response = new ResponseInfo();
            try
            {
                if (ModelState.IsValid)
                {
                    response = new PhotoMasterModel().UpdatePhotosTranslate(data);
                }
                else
                {
                    response.Code = (int)CodeResponse.NotValidate;
                    response.ListError = ModelState.GetModelErrors();
                }
            }
            catch (Exception e)
            {
                response.Code = (int)CodeResponse.ServerError;
                response.MsgNo = (int)MsgNO.ServerError;
                response.ThongTinBoSung1 = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// dùng để thay đổi ngôn ngữ
        /// Author       :   HoàngNM - 11/08/2018 - create
        /// </summary>
        public JsonResult ReferPhotoWithLang(int id, string lang)
        {
            try
            {
                return Json(new PhotoMasterModel().ReferPhotoWithLang(id, lang), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}