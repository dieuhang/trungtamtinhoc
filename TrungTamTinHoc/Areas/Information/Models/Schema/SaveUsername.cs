﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrungTamTinHoc.Areas.Information.Models.Schema
{
    public class SaveUsername
    {
        [Required(ErrorMessage = "1")]
        [MaxLength(50, ErrorMessage = "2")]
        [RegularExpression("^[a-zA-Z0-9_.-]{6,50}$", ErrorMessage = "34")]
        public string Username { set; get; }

        [Required(ErrorMessage = "1")]
        [MaxLength(50, ErrorMessage = "2")]
        [RegularExpression("^(?=.*\\d)(?=.*[a-zA-Z])[a-zA-Z0-9]{8,50}$", ErrorMessage = "19")]
        public string Password { set; get; }
    }
}