﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TrungTamTinHoc.Areas.Course.Models.Schema;
using TTTH.Common;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.ClassManagement.Schema
{
    /// <summary>
    /// Class dùng để lấy danh sách lớp học trả về cho trang danh sách lớp học
    /// Author       :   HaLTH - 01/09/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ListOfClass
    {
        public List<Class> ClassList { set; get; }
        public List<KhoaHoc> KhoaHocList { set; get; }
        public Paging Paging { set; get; }
        public ClassConditionSearch Condition { set; get; }
        public ListOfClass()
        {
            this.ClassList = new List<Class>();
            this.Condition = new ClassConditionSearch();
            this.Paging = new Paging();
        }
    }
}