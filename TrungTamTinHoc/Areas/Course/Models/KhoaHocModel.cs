﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Web;
using TrungTamTinHoc.Areas.Course.Models.Schema;
using TTTH.Common;
using TTTH.Common.Enums;
using TTTH.DataBase;
using Z.EntityFramework.Plus;
using TblUser = TTTH.DataBase.Schema.User;
using TblCommentKhoaHoc = TTTH.DataBase.Schema.CommentKhoaHoc;
using TblDanhGiaKhoaHoc = TTTH.DataBase.Schema.DanhGiaKhoaHoc;
namespace TrungTamTinHoc.Areas.Course.Models
{
    public class KhoaHocModel
    {
        DataContext context;
        public KhoaHocModel()
        {
            context = new DataContext();
        }

        /// <summary>
        /// Lấy danh sách khóa học được phép hiển thị từ DB theo ngôn ngữ.
        /// Author       :   TramHDT - 24/07/2018 - create
        /// </summary>
        /// <returns>Danh sách khóa học có trong DB</returns>
        public KhoaHocList LoadKhoaHoc(CourseConditionSearch condition)
        {
            try
            {
                // Nếu không tồn tại điều kiện tìm kiếm thì khởi tạo giá trị tìm kiếm ban đầu
                if (condition == null)
                {
                    condition = new CourseConditionSearch();
                }
                string lang = Common.GetLang();
                KhoaHocList ListOfCourse = new KhoaHocList();
                // Lấy các thông tin dùng để phân trang
                ListOfCourse.Paging = new Paging(context.KhoaHocTrans.Count(x =>
                    (condition.KeySearch == null ||
                     (condition.KeySearch != null && (x.TenKhoaHoc.Contains(condition.KeySearch))))
                    && (condition.IdChuyenNganh == 0 ||
                        (condition.IdChuyenNganh != 0 && x.KhoaHoc.IdChuyenNganh == condition.IdChuyenNganh))
                    && !x.DelFlag && x.Lang == lang), condition.CurrentPage, condition.PageSize);

                ListOfCourse.KhoaHocs = context.KhoaHocTrans.Where(x =>
                    (condition.KeySearch == null ||
                     (condition.KeySearch != null && (x.TenKhoaHoc.Contains(condition.KeySearch))))
                    && (condition.IdChuyenNganh == 0 ||
                        (condition.IdChuyenNganh != 0 && x.KhoaHoc.IdChuyenNganh == condition.IdChuyenNganh))
                    && !x.DelFlag && x.Lang == lang).Select(x => new KhoaHoc
                 {
                     Id = x.Id,
                     BeautyId = x.KhoaHoc.BeautyId,
                     TenKhoaHoc = x.TenKhoaHoc,
                     TomTat = x.TomTat,
                     AnhMinhHoa = x.KhoaHoc.AnhMinhHoa,
                     NgayKhaiGiang = x.KhoaHoc.NgayKhaiGiang,
                     SoLuongView = x.KhoaHoc.SoLuongView,
                     SoLuongComment = x.KhoaHoc.CommentKhoaHoc.Count,
                     SoLuongDanhGia = x.KhoaHoc.DanhGiaKhoaHoc.Count,
                     DiemDanhGia = x.KhoaHoc.DanhGiaKhoaHoc.Count != 0 ? x.KhoaHoc.DanhGiaKhoaHoc.Sum(y => y.DiemDanhGia) : 0,
                     ChoPhepDangKy = x.KhoaHoc.ChoPhepDangKy
                 }).OrderBy(x => x.Id).Skip((ListOfCourse.Paging.CurrentPage - 1) * ListOfCourse.Paging.NumberOfRecord)
                    .Take(ListOfCourse.Paging.NumberOfRecord).ToList();
                ListOfCourse.ChuyenNganh = context.ChuyenNganhTrans.Where(x => x.Lang == lang && !x.DelFlag)
                    .Select(x => new ChuyenNganh
                    {
                        IdChuyenNganh = x.Id,
                        TenChuyenNganh = x.TenChuyenNganh
                    }).ToList();
                ListOfCourse.Condition = condition;
                return ListOfCourse;

            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Lấy danh sách 5 comment được phép hiển thị từ DB.
        /// Author       :   TramHDT - 24/07/2018 - create
        /// </summary>
        /// <param name="CountComment"></param>
        /// <returns>khóa học có trong DB theo beautyId.</returns>
        public List<CommentKhoaHoc> GetListComment(int idKhoaHoc, int CountComment)
        {
            try
            {

                List<CommentKhoaHoc> listComment= context.CommentKhoaHoc
                    .Where(x => x.IdKhoaHoc == idKhoaHoc && x.IdComment == null && !x.DelFlag)
                    .Select(p => new CommentKhoaHoc
                    {
                        id = p.Id,
                        NoiDung = p.NoiDung,
                        ThoiGianCmt = p.Created_at,
                        IdUser = p.IdUser,
                        Avatar = p.IdUser != 0 ? p.User.Avatar : null,
                        HoTen = p.IdUser == null ? "No Name":(p.User.Ho + " " + p.User.Ten)
                    }).OrderByDescending(y => y.ThoiGianCmt).Skip(CountComment).Take(5).ToList();

                foreach (var cmt in listComment)
                {
                    cmt.CountChildComment = context.CommentKhoaHoc.Count(x => x.IdKhoaHoc == idKhoaHoc && x.IdComment == cmt.id && !x.DelFlag);
                    cmt.ChildCommentKhoaHocs = cmt.CountChildComment < 1 ? null : GetListCommentReply(idKhoaHoc, 0, cmt.id);
                }

                return listComment;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Lấy danh sách 2 comment con được phép hiển thị từ DB theo IdParentComment.
        /// Author       :   TramHDT - 23/07/2018 - create
        /// </summary>
        /// <param name="IdParentComment"></param>
        /// <returns>khóa học có trong DB theo beautyId.</returns>
        public List<CommentKhoaHoc> GetListCommentReply(int  idKhoaHoc, int childComment, int idParentComment)
        {
            try
            {

                List<CommentKhoaHoc> listCommentReply = context.CommentKhoaHoc
                    .Where(x => x.IdKhoaHoc == idKhoaHoc && x.IdComment == idParentComment &&  !x.DelFlag)
                    .Select(p => new CommentKhoaHoc
                    {
                        NoiDung = p.NoiDung,
                        ThoiGianCmt = p.Created_at,
                        IdUser = p.IdUser,
                        Avatar = p.IdUser!=0? p.User.Avatar: null,
                        HoTen = p.IdUser==null? "No Name":(p.User.Ho+ " " + p.User.Ten)
                    }).OrderByDescending(y => y.ThoiGianCmt).Skip(childComment).Take(2).OrderBy(y => y.ThoiGianCmt).ToList();

                return listCommentReply;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Lấy khóa học được phép hiển thị từ DB theo ngôn ngữ và beautyId.
        /// Author       :   TramHDT - 24/06/2018 - create
        /// </summary>
        /// <param name="beautyId"></param>
        /// <returns>khóa học có trong DB theo beautyId.</returns>
        public ChiTietKhoaHoc LoadChiTietKhoaHoc(string beautyId)
        {
            try
            {
                string lang = Common.GetLang();
                if (context.KhoaHoc.Count(x => x.BeautyId == beautyId && !x.DelFlag) < 1)
                {
                    return null;
                }

                ChiTietKhoaHoc chiTietKhoaHoc = context.KhoaHoc.Include("KhoaHocTrans")
                    .Where(x => x.BeautyId == beautyId && !x.DelFlag).Select(x => new ChiTietKhoaHoc
                    {
                        Id = x.Id,
                        BeautyId = x.BeautyId,
                        TenKhoaHoc = x.KhoaHocTrans.FirstOrDefault(y => y.Lang == lang).TenKhoaHoc,
                        TomTat = x.KhoaHocTrans.FirstOrDefault(y => y.Lang == lang).TomTat,
                        AnhMinhHoa = x.AnhMinhHoa,
                        NgayKhaiGiang = x.NgayKhaiGiang,
                        SoLuongDanhGia = x.DanhGiaKhoaHoc.Count,
                        DiemDanhGia = x.DanhGiaKhoaHoc.Count != 0 ? x.DanhGiaKhoaHoc.Sum(y => y.DiemDanhGia) : 0,
                        ChoPhepDangKy = x.ChoPhepDangKy,
                        ChiTiet = x.KhoaHocTrans.FirstOrDefault(y => y.Lang == lang).ChiTiet,
                        DoTuoi = x.DoTuoi,
                        SoLuongDaDangKy = x.SoLuongDaDangKy,
                        ThoiGian = x.ThoiGian,
                        ThoiGianKetThuc = x.ThoiGianKetThuc,
                        LichHoc = x.LichHoc,
                        HocPhi = x.HocPhi,
                        SoLuongView = x.SoLuongView,

                    }).FirstOrDefault();

                if (chiTietKhoaHoc != null)
                {
                    chiTietKhoaHoc.CountComment = context.CommentKhoaHoc
                        .Count(x => x.IdKhoaHoc == chiTietKhoaHoc.Id && x.IdComment == null && !x.DelFlag);
                    chiTietKhoaHoc.ListCommentKhoaHocs = chiTietKhoaHoc.CountComment < 1 ? null : GetListComment(chiTietKhoaHoc.Id, 0);
                    int idUser = XacThuc.GetIdUser();
                    if (idUser != 0)
                    {
                        chiTietKhoaHoc.AvatarUser =
                            context.User.FirstOrDefault(x => x.Id == idUser && !x.DelFlag)?.Avatar;
                        chiTietKhoaHoc.HotenUser = context.User.FirstOrDefault(x => x.Id == idUser && !x.DelFlag)?.Ho +
                                                   context.User.FirstOrDefault(x => x.Id == idUser)?.Ten;
                    }
                    else
                    {
                        chiTietKhoaHoc.AvatarUser = null;
                        chiTietKhoaHoc.HotenUser = "No Name";
                    }
                }
                //Tăng view lên 1
                context.KhoaHoc.Where(x => x.BeautyId == beautyId && !x.DelFlag)
                        .Update(x => new TTTH.DataBase.Schema.KhoaHoc
                        {
                            SoLuongView = chiTietKhoaHoc.SoLuongView + 1
                        });
                context.SaveChanges();

                return chiTietKhoaHoc;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Xử lý khi có thông tin bình luận và đánh giá của người dùng
        /// Author       :   TramHDT - 24/06/2018 - create
        /// </summary>
        /// <param></param>
        /// <returns>Thông tin về việc bình luận thành công hay thất bại</returns>
        public CommentKhoaHoc CommentReply(int idParentComment, string contentReply, int idKhoaHoc, int idUser, int starRating)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                if (contentReply.Length>0)
                {
                    TblCommentKhoaHoc comment = new TblCommentKhoaHoc
                    {
                        IdKhoaHoc = idKhoaHoc,
                        NoiDung = contentReply
                    };
                    if(idUser!=0)
                        comment.IdUser = idUser;
                    if (idParentComment != 0)
                    {
                        comment.IdComment = idParentComment;
                    }
                    context.CommentKhoaHoc.Add(comment);
                }

                if (starRating > 0)  
                    {
                        if (context.DanhGiaKhoaHoc.Count(x => x.IdUser == idUser && x.IdKhoaHoc == idKhoaHoc && !x.DelFlag) > 0)
                        {
                            context.DanhGiaKhoaHoc.Where(x => x.IdUser == idUser && x.IdKhoaHoc == idKhoaHoc && !x.DelFlag)
                                .Update(x => new TblDanhGiaKhoaHoc
                                {
                                    DiemDanhGia = starRating
                                });
                        }
                        else
                        {
                        TblDanhGiaKhoaHoc rating = new TblDanhGiaKhoaHoc
                            {
                                IdKhoaHoc = idKhoaHoc,
                                DiemDanhGia = starRating,
                                IdUser = idUser
                            };
                            context.DanhGiaKhoaHoc.Add(rating);
                        }
                }
                context.SaveChanges();
                transaction.Commit();
                if (contentReply.Length > 0)
                {
                    CommentKhoaHoc commentKhoaHoc = context.CommentKhoaHoc.Select(x => new CommentKhoaHoc
                    {
                        id = x.Id,
                        ThoiGianCmt = x.Created_at,
                    }).OrderByDescending(y=>y.id).FirstOrDefault();

                    if (commentKhoaHoc != null)
                    {
                        commentKhoaHoc.NoiDung = contentReply;
                        if (idUser != 0)
                        {
                            commentKhoaHoc.Avatar =
                                context.User.FirstOrDefault(x => x.Id == idUser && !x.DelFlag)?.Avatar;
                            commentKhoaHoc.HoTen = context.User.FirstOrDefault(x => x.Id == idUser && !x.DelFlag)?.Ho +
                                                   context.User.FirstOrDefault(x => x.Id == idUser)?.Ten;
                        }
                        else
                        {
                            commentKhoaHoc.Avatar = null;
                            commentKhoaHoc.HoTen = "No Name";
                        }
                    }
                    return commentKhoaHoc;
                }
                return null;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }
    }
}