﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.GroupManagement.Schema
{
    public class Group
    {
        [Required]
        public int Id { set; get; }
        
        [Required]
        [StringLength(100)]
        public string GroupName { get; set; }

        [Required]
        [StringLength(1000)]
        public string MoTa { get; set; }

        [Required]
        public bool IsDefault { get; set; }

        public string CheckIsDefault { set; get; }
        [Required]
        public bool IsSystem { get; set; }
        public string CheckIsSystem { set; get; }
    }
    public class Group_Trans
    {
        public int Id { set; get; }

        public string LanguageTrans { set; get; }

        [Required]
        [StringLength(100)]
        public string GroupName { get; set; }

        [Required]
        [StringLength(1000)]
        public string MoTa { get; set; }
    }

    /// <summary>
    /// Class dùng để chứa thông tin tìm kiếm của danh sách group
    /// Author       :   HangNTD - 20/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class GroupConditionSearch
    {
        public string KeySearch { set; get; }
        public bool? TrangThai { set; get; }
        public int CurentPage { set; get; }
        public int PageSize { set; get; }
        public GroupConditionSearch()
        {
            this.KeySearch = "";
            this.TrangThai = null;
            this.PageSize = 10;
            this.CurentPage = 1;
        }
    }
}