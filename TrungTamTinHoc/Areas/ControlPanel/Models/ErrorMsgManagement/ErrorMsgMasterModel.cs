﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TrungTamTinHoc.Areas.ControlPanel.Models.ErrorMsgManagement.Schema;
using TTTH.Common;
using TTTH.DataBase;
using Z.EntityFramework.Plus;
using static TTTH.Common.Enums.ConstantsEnum;
using TblErrorMgs = TTTH.DataBase.Schema.ErrorMsg;
using Account = TTTH.DataBase.Schema.Account;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.ErrorMgsManagement
{

    public class ErrorMsgMasterModel
    {
        DataContext context;
        public ErrorMsgMasterModel()
        {
            context = new DataContext();
        }

        /// <summary>
        /// Dùng để load thông tin của ErrorMsg
        /// Author       :   HoangNM - 13/08/2018 - create
        /// </summary>
        /// <param name="id">id của errorMgs cần lấy thông tin</param>
        public ErrorMsgMaster LoadErrorMsg(string id)
        {
            try
            {

                ErrorMsgMaster errorMsgMaster = new ErrorMsgMaster();
                int idErrorMsg = 0;
                try
                {
                    idErrorMsg = Convert.ToInt32(id);
                }
                catch { }
                TblErrorMgs errorMsg = context.ErrorMsg.FirstOrDefault(x => x.Id == idErrorMsg && !x.DelFlag && x.Lang == Common.defaultLang);
                if (errorMsg != null)
                {
                    errorMsgMaster.Mode = (int)ModeMaster.Update;
                    errorMsgMaster.ErrorMsgInfo = new ErrorMsg();
                    errorMsgMaster.ErrorMsgInfo.Id = errorMsg.Id;
                    errorMsgMaster.ErrorMsgInfo.Title = errorMsg.Title;
                    errorMsgMaster.ErrorMsgInfo.Msg = errorMsg.Msg;
                    errorMsgMaster.ErrorMsgInfo.Type = errorMsg.Type;
                    errorMsgMaster.Language = context.Language.Where(x => !x.DelFlag)
                        .Select(x => new Lang
                        {
                            Id = x.Id,
                            Name = x.Name
                        }).OrderBy(x => x.Id).ToList();
                    if (errorMsgMaster.Language.Count > 0)
                    {
                        string lang = errorMsgMaster.Language[0].Id;
                        errorMsg = context.ErrorMsg.FirstOrDefault(x => x.Id == idErrorMsg && !x.DelFlag && x.Lang == lang);
                        errorMsgMaster.ErrorMsgTrans = new ErrorMsg();
                        errorMsgMaster.ErrorMsgTrans.Id = errorMsg.Id;
                        errorMsgMaster.ErrorMsgTrans.Title = errorMsg.Title;
                        errorMsgMaster.ErrorMsgTrans.Msg = errorMsg.Msg;
                    }
                }
                return errorMsgMaster;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Dùng để thêm hoăc sửa thông tin của ErrorMsg
        /// Author       :   HoangNM - 13/08/2018 - create
        /// </summary>
        /// <param name="errorMsg">thông tin của errorMsg gửi lên để thêm hoặc sửa</param>
        public ResponseInfo SaveErrorMsg(ErrorMsg errorMsg)
        {
            try
            {
                if (context.ErrorMsg.FirstOrDefault(x => x.Id == errorMsg.Id && !x.DelFlag) != null)
                {
                    return UpadateErrorMgs(errorMsg);
                }
                return AddErrorMgs(errorMsg);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// Dùng để thêm hoăc sửa thông tin của ErrorMsg
        /// Author       :   HoangNM - 13/08/2018 - create
        /// </summary>
        /// <param name="errorMsg">thông tin của errorMsg gửi lên để thêm hoặc sửa</param>
        public ResponseInfo UpadateErrorMgs(ErrorMsg errorMsg)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo response = new ResponseInfo();
                bool update = true;

                if (update)
                {

                    context.ErrorMsg.Where(x => x.Id == errorMsg.Id && !x.DelFlag)
                        .Update(x => new TTTH.DataBase.Schema.ErrorMsg
                        {
                            Type = errorMsg.Type
                        });
                    context.ErrorMsg.Where(x => x.Id == errorMsg.Id && x.Lang == Common.defaultLang && !x.DelFlag)
                        .Update(x => new TTTH.DataBase.Schema.ErrorMsg
                        {
                            Title = errorMsg.Title,
                            Msg = errorMsg.Msg,
                        });
                    context.SaveChanges();
                }
                transaction.Commit();
                return response;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }
        /// <summary>
        /// Dùng để thêm thông tin của ErrorMsg
        /// Author       :   HoangNM - 13/08/2018 - create
        /// </summary>
        /// <param name="errorMsg">thông tin của errorMsg gửi lên để thêm hoặc sửa</param>
        public ResponseInfo AddErrorMgs(ErrorMsg errorMgs)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo response = new ResponseInfo();

                List<Lang> listOfLang = context.Language.Where(x => !x.DelFlag).Select(x => new Lang
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList();
                errorMgs.Id = context.ErrorMsg.Count() == 0 ? 1 : context.ErrorMsg.Max(x => x.Id) + 1;
                foreach (Lang lang in listOfLang)
                {
                    context.ErrorMsg.Add(new TblErrorMgs
                    {
                        Id = errorMgs.Id,
                        Lang = lang.Id,
                        Title = errorMgs.Title,
                        Type = errorMgs.Type,
                        Msg = errorMgs.Msg
                    });
                }
                context.SaveChanges();
                response.ThongTinBoSung1 = errorMgs.Id + "";


                transaction.Commit();
                return response;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }
        /// <summary>
        /// Dùng để sửa thông tin của ErrorMsg
        /// Author       :   HoangNM - 13/08/2018 - create
        /// </summary>
        /// <param name="errorMsg">thông tin của errorMsg gửi lên để thêm hoặc sửa</param>
        public ResponseInfo UpdateErrorMsgTranslate(ErrorMsg_Trans errorMsg)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                ResponseInfo response = new ResponseInfo();
                context.ErrorMsg.Where(x => x.Id == errorMsg.Id && x.Lang == errorMsg.LanguageTrans && !x.DelFlag)
                    .Update(x => new TTTH.DataBase.Schema.ErrorMsg
                    {
                        Msg = errorMsg.Msg
                    });
                context.SaveChanges();
                response.ThongTinBoSung1 = errorMsg.Id + "";
                transaction.Commit();
                return response;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        /// <summary>
        /// Dùng để thay đổi ngôn ngữ
        /// Author       :   HoangNM - 13/08/2018 - create
        /// </summary>
        /// <param name="errorMsg">thông tin của errorMsg gửi lên để thêm hoặc sửa</param>
        public ErrorMsg_Trans ReferErrorMsgWithLang(int id, string lang)
        {
            try
            {
                return context.ErrorMsg.Where(x => x.Id == id && x.Lang == lang && !x.DelFlag)
                    .Select(x => new ErrorMsg_Trans
                    {
                        Msg = x.Msg
                    }).FirstOrDefault();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Dùng để kiểm tra tài khoản có thuộc develop hay admin không
        /// Author       :   HoangNM - 30/08/2018 - create
        /// </summary>
        /// <param name="id">id của errorMgs cần lấy thông tin</param>
        /// <return>
        /// trả về true nếu là develop hoặc admin và ngược lại trả về false cho các loại khác
        /// </return>
        /// 
        public bool ChekAccount()
        {
            try
            {
                Account account = XacThuc.GetAccount();
                if (account == null)
                {
                    return false;
                }
                else
                {
                    if (context.GroupOfAccount.FirstOrDefault(x => x.IdAccount == account.Id && x.IdGroup < 2) != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }

}