﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TTTH.Common;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.GroupManagement.Schema
{
    /// <summary>
    /// Class dùng để lấy danh sách group trả về cho trang danh sách group
    /// Author       :   HangNTD - 05/03/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ListOfGroup
    {
        public List<Group> GroupList { set; get; }
        public Paging Paging { set; get; }
        public GroupConditionSearch Condition { set; get; }
        public ListOfGroup()
        {
            this.GroupList = new List<Group>();
            this.Condition = new GroupConditionSearch();
            this.Paging = new Paging();
        }
    }
}