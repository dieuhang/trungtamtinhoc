﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TrungTamTinHoc.Areas.ControlPanel.Models.ThingsAchievedManagement.Schema;
using TTTH.Common;
using TTTH.DataBase;
using Z.EntityFramework.Plus;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.ThingsAchievedManagement
{
    /// <summary>
    /// Class dùng để xử lý các hoạt động liên quan đến Những điều đạt được.
    /// Author       :   HaLTH - 26/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   ControlPanel.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ListOfThingsAchievedModel
    {
        DataContext context;
        public ListOfThingsAchievedModel()
        {
            context = new DataContext();
        }
        /// <summary>
        /// Tìm kiếm những điều đạt được theo điều kiện cho trước.
        /// Author       :   HaLTH - 26/07/2018 - create
        /// </summary>
        /// <param name="condition">Đối tượng chứa điều kiện tìm kiếm</param>
        /// <returns>Danh sách những điều đạt được đã tìm kiếm được. Exception nếu có lỗi</returns>
        public ListOfThingsAchieved GetListOfThingsAchieved (ThingsAchievedConditionSearch condition)
        {
            try
            {
                // Nếu không tồn tại điều kiện tìm kiếm thì khởi tạo giá trị tìm kiếm ban đầu
                if(condition == null)
                {
                    condition = new ThingsAchievedConditionSearch();
                }
                string lang = Common.GetLang();

                ListOfThingsAchieved listOfThingsAchieved = new ListOfThingsAchieved();
                // Lấy các thông tin dùng để phân trang
                listOfThingsAchieved.Paging = new Paging(context.NhungDieuDatDuoc.Count(x =>
                    (condition.KeySearch == null || 
                    (condition.KeySearch != null && (x.Title.Contains(condition.KeySearch) || 
                                                   x.Content.Contains(condition.KeySearch))))
                    && (condition.TrangThai == null || (condition.TrangThai != null &&
                                                       x.IsShow == condition.TrangThai.Value)) 
                    && !x.DelFlag && x.Lang == lang), condition.CurrentPage, condition.PageSize);

                // Tìm kiếm và lấy dữ liệu theo trang
                listOfThingsAchieved.ThingsAchievedList = context.NhungDieuDatDuoc.Where(x =>
                    (condition.KeySearch == null ||
                    (condition.KeySearch != null && (x.Title.Contains(condition.KeySearch) ||
                                                   x.Content.Contains(condition.KeySearch))))
                    && (condition.TrangThai == null || (condition.TrangThai != null &&
                                                       x.IsShow == condition.TrangThai.Value)) 
                    && !x.DelFlag && x.Lang == lang).OrderBy(x => x.Id)
                    .Skip((listOfThingsAchieved.Paging.CurrentPage - 1) * listOfThingsAchieved.Paging.NumberOfRecord)
                    .Take(listOfThingsAchieved.Paging.NumberOfRecord).Select(x => new ThingsAchieved
                    {
                        Id = x.Id,
                        Title = x.Title,
                        Icon = x.Icon,
                        Content = x.Content,
                        IsShow = x.IsShow
                    }).ToList();
                listOfThingsAchieved.Condition = condition;
                return listOfThingsAchieved;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// Xóa Những điều đạt được trong DB.
        /// Author       :   HaLTH - 26/07/2018 - create
        /// </summary>
        /// <param name="idn">Danh sách id của những điều đạt được sẽ xóa</param>
        /// <returns>True nếu xóa thành công, False nếu không còn điều đạt được được hiển thị trên trang chủ, Excetion nếu có lỗi</returns>
        public bool DeleteThingsAchieved (List<int> ids)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                bool result = true;
                if (context.NhungDieuDatDuoc.Count(x => !ids.Contains(x.Id) && !x.DelFlag) > 1)
                {
                    context.NhungDieuDatDuoc.Where(x => ids.Contains(x.Id)).Delete();
                    context.SaveChanges();
                }
                else
                {
                    result = false;
                }
                transaction.Commit();
                return result;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }
        /// <summary>
        /// Cập nhật trạng thái hiển thị của những điều đạt được trên trang chủ.
        /// Author       :   HaLTH - 26/07/2018 - create
        /// </summary>
        /// <param name="id">id của những điều đạt được sẽ được cập nhật trạng thái</param>
        /// <returns>True nếu xóa thành công, False nếu không còn những điều đạt được được hiển thị trên trang chủ, Excetion nếu có lỗi</returns>
        public bool UpdateShow(int id)
        {
            DbContextTransaction transaction = context.Database.BeginTransaction();
            try
            {
                bool result = true;
                if(context.NhungDieuDatDuoc.Count(x => x.IsShow && x.Id != id && !x.DelFlag) > 1)
                {
                    context.NhungDieuDatDuoc.Where(x => x.Id == id && !x.DelFlag)
                        .Update(x => new TTTH.DataBase.Schema.NhungDieuDatDuoc
                        {
                            IsShow = !x.IsShow
                        });
                    context.SaveChanges();
                }
                else
                {
                    result = false;
                }
                transaction.Commit();
                return result;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }
    }
}