﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TTTH.Common;
using static TTTH.Common.Enums.ConstantsEnum;

namespace TrungTamTinHoc.Areas.ControlPanel.Models.ThingsAchievedManagement.Schema
{
    public class ThingsAchievedMaster
    {
        public ThingsAchieved ThingsAchievedInfo { set; get; }
        public ThingsAchieved ThingsAchievedTrans { set; get; }
        public List<Lang> Language { set; get; }
        public int Mode { set; get; }
        public ThingsAchievedMaster()
        {
            Mode = (int)ModeMaster.Insert;
            ThingsAchievedInfo = null;
            ThingsAchievedTrans = null;
            Language = new List<Lang>();
        }
    }
}