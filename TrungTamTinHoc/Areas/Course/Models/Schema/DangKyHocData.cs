﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrungTamTinHoc.Areas.Course.Models.Schema
{
    /// <summary>
    /// Class chứa tất cả dữ liệu cho việc hiển thị ở trang đăng ký lớp học.
    /// Author       :   AnTM - 07/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   Course.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class DangKyHocData
    {
        public int IdKhoaHocDuocChon { get; set; }
        public ThongTinUser ThongTinHocVien { get; set; }
        public ThongTinUser ThongTinNguoiGiamHo { get; set; }
    }

    /// <summary>
    /// Class chứa các thuộc tính của 1 user lấy từ DB cho việc fill dữ liệu ra các trường ở form đăng ký cho người dùng đã đăng nhập chọn ở trang đăng ký khóa học
    /// Author       :   AnTM - 07/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   Course.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class ThongTinUser
    {
        public string Ho { get; set; }
        public string Ten { get; set; }
        public bool GioiTinh { get; set; }
        public DateTime? NgaySinh { get; set; }
        public string IdXa { get; set; }
        public string IdHuyen { get; set; }
        public string IdTinh { get; set; }
        public int? IdNguoiGiamHo { get; set; }
        public string DiaChi { get; set; }
        public string Email { get; set; }
        public string SoDienThoai { get; set; }
        public string SoCMND { get; set; }
        public ThongTinUser()
        {
            IdXa = "00001";
            IdHuyen = "001";
            IdTinh = "01";
            GioiTinh = true;
        }
    }
    /// <summary>
    /// Class chứa các thuộc tính của 1 khóa học lấy từ DB cho việc hiển thị danh sách các khóa học để người dùng chọn ở trang đăng ký khóa học
    /// Author       :   AnTM - 07/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   Course.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class KhoaHocHienThi
    {
        public int IDKhoaHoc { get; set; }
        public string TenKhoaHoc { set; get; }
    }

    /// <summary>
    /// Class chứa các thuộc tính của 1 lớp học lấy từ DB cho việc hiển thị danh sách các lớp học để người dùng chọn ở trang đăng ký khóa học
    /// Author       :   AnTM - 07/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   Course.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class LopHocHienThi
    {
        public int IDLopHoc { get; set; }
        public string TenLopHoc { set; get; }
    }
    /// <summary>
    /// Class chứa các thuộc tính của 1 tỉnh lấy từ DB cho việc hiển thị danh sách các tỉnh để người dùng chọn ở trang đăng ký khóa học
    /// Author       :   AnTM - 07/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   Course.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class TinhHienThi
    {
        public string IDTinh { get; set; }
        public string TenTinh { set; get; }
    }
    /// <summary>
    /// Class chứa các thuộc tính của 1 huyện lấy từ DB cho việc hiển thị danh sách các huyện để người dùng chọn ở trang đăng ký khóa học
    /// Author       :   AnTM - 07/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   Course.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class HuyenHienThi
    {
        public string IDHuyen { get; set; }
        public string TenHuyen { set; get; }
    }
    /// <summary>
    /// Class chứa các thuộc tính của 1 xã lấy từ DB cho việc hiển thị danh sách các xã để người dùng chọn ở trang đăng ký khóa học
    /// Author       :   AnTM - 07/07/2018 - create
    /// </summary>
    /// <remarks>
    /// Package      :   Course.Models
    /// Copyright    :   Team Noname
    /// Version      :   1.0.0
    /// </remarks>
    public class XaHienThi
    {
        public string IDXa { get; set; }
        public string TenXa { set; get; }
    }

}